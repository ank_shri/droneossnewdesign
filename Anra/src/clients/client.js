import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";

@inject(EventAggregator, Notification, NewInstance.of(ValidationController))
export class Client {
  @bindable client;
  @bindable save;
  controller = null;
  rules = null;

  constructor(events, notification, controller) {
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
}

  attached(){
      this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
      this.heading = this.client.Name != null ? 'Edit Client' : 'Add Client';
  }

  setupValidationRules() {   

       this.rules = ValidationRules
            .ensure('Name').required()    
            .ensure('ContactName').required()
            .ensure('ContactPhone').required()
            .ensure('ContactEmail').matches(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).required()
            .ensure('Address').required()
            .ensure('City').required()
            .ensure('StateId').matches(/^[1-9][0-9]*$/).required()
            .ensure('Zipcode').matches(/^[1-9][0-9]*$/).required()
            .ensure('CountryId').matches(/^[1-9][0-9]*$/).required()
            .rules;
  }

  saveItem() {

      this.isWaiting = true;
      this.controller.validate({ object: this.client, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
						  if(this.client.ClientId!='')
							{
								this.notification.error('Updated Successfully.');
							}
							else
							{
								this.notification.error('Added Successfully.');  
							}
						  this.isWaiting = false;
                      })
              } 
              else {
                  // validation failed
                  this.notification.error('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
          });

      this.isWaiting = false;
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.events.publish('client-item-operation', {refresh: true});
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('client-item-operation', {refresh: false});
  }
}
