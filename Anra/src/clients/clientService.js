import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class ClientService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getRole() {
      return this.http.fetch(this.baseApi + 'users/getrole')
      .then(response => response.text());
  }

  getClients() {
      return this.http.fetch(this.baseApi + 'client/listing')
      .then(response => response.json());
  }

  getNewClient() {
      return this.http.fetch(this.baseApi + 'client/NewClient')
      .then(response => response.json());
  }

  getClient(id) {
      return this.http.fetch(this.baseApi + 'client/' + id)
      .then(response => response.json());
  }

  deleteClient(id) {
    return this.http.fetch(this.baseApi + 'client/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }
  saveClient(mission) {
    return this.http.fetch(this.baseApi + 'client/', {
      method: 'post',
      body: json(mission)
    }).then(response => response.json());
  }
}
