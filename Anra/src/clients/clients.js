import {inject} from 'aurelia-framework';
import {ClientService} from 'clients/clientService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {PermissionService} from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(ClientService, Router, EventAggregator, PermissionService)
export class Clients {

    //Set Filters for Searchbox
    filters = [
        { value: '', keys: ['Name', 'ContactName', 'Country', 'ContactPhone','ContactEmail','OrganizationName','IsActive']},
    ];

    constructor(clientService, router, events, permissionService) {
    this.clientService = clientService;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;
    this.heading = 'Manage Clients';
    this.clients = [];
    this.client = null;
    this.isEditing = false;
    this.isViewing = false;
    this.roleName = "";
    this.permission={};	  
    //Set Default Paging Size
    this.pageSize = clientService.defaultPagingSize;
    this.isWaiting = false;
  }

    activate() {
       this.getList();
  }
    // deactivate() {
    //   this.clientRoute.isActive = false;
    // }
  attached() {
    this.subscribeEvents();
  }
  detached() {
    this.subscriber.dispose();
  }
  getPermission(moduleId) {
      this.permissionService.getPermission(moduleId)
        .then(result => {
            this.permission = result;
            this.getRole();
        });
  }

  subscribeEvents() {
    this.subscriber = this.events.subscribe('client-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber = this.events.subscribe('client-item-operation-edit', payload => {

        if (payload) {
            if (payload.id > 0) {
                this.editItem(payload.id);
            }
            this.reset();
        }
    });

  }
 
  getPermission(moduleId) {
    this.permissionService.getPermission(moduleId)
      .then(result => {
          this.permission = result;
          this.getRole();
          console.log(this.getRole());
      });
}

getRole() {
    if(localStorage["Roles"].indexOf("Super Admin") > -1)
    {
        this.roleName = "super admin";
        return;
    }

    if(localStorage["Roles"].indexOf("Admin") > -1)
    {
        this.roleName = "admin";
        return;
    }

    this.roleName = "others";   
}

  getList() {
      this.isWaiting = true;
      this.clientService.getClients()
      .then(result => {
        this.clients = result;
        this.isEditing = false;
        this.getPermission(10);
        this.isWaiting = false;
      }).catch((e) => {
          this.notification.error(e.message);
          this.isWaiting = false;
      });
  }

  reset() {
    this.client = null;
    this.isEditing = false;
    this.isViewing = false;
  }

  add() {
      this.isWaiting = true;
      this.clientService.getNewClient()
      .then(result => {
        this.client = result;
        this.isEditing = true;
        this.isWaiting = false;
      });
  }

  editItem(id) {
      this.isWaiting = true;
      this.clientService.getClient(id)
      .then(result => {
        
        this.client = result;
        this.isEditing = true;
        this.isWaiting = false;
      });
  }

  viewItem(id) {
      this.isWaiting = true;
      this.clientService.getClient(id)
        .then(result => {            
            this.client = result;
          this.isViewing = true;
          this.isWaiting = false;
        });
  }


  deleteItem(id) {
    this.dialogService.open({viewModel: Prompt, model: 'Are you sure you want to delete?'}).then(response => {
         if (!response.wasCancelled) {
    this.clientService.deleteClient (id)
      .then(result => {
				  this.notification.error('Deleted Successfully.');
        this.getList();
			  });
         }
      });
  }

  saveItem(item) {
      return this.clientService.saveClient(this.client);
  }
}
