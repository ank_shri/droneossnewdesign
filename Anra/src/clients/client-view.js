import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';

@inject(EventAggregator, Notification)
export class ClientView {
  @bindable client;
  @bindable save;

  constructor(events, notification) {
    this.events = events;
    this.notification = notification;
  }

  bind() {
    this.heading = this.client.Name != null ? 'View Client' : 'Add Client';
  }
    
  cancel() {
    this.events.publish('client-item-operation', {refresh: false});
  }

  edit(clientId) {
      this.events.publish('client-item-operation-edit', {id: clientId});
  }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
