﻿import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class CommandService {

    constructor(http, config) {
        this.config = config;
        this.http = http;
        this.baseApi = this.config.get('gatewayBaseUrl');
        this.defaultPagingSize = this.config.get('defaultPagingSize');
    }
        
    saveCommand(message) {
        return this.http.fetch(this.baseApi + 'missioncommand/', {
            method: 'post',
            body: json(message)
        }).then(response => response.json());
    }

    saveCollisionWarning(warning) {
        return this.http.fetch(this.baseApi + 'missioncommand/SaveWarning', {
            method: 'post',
            body: json(warning)
        }).then(response => response.json());
    }

}