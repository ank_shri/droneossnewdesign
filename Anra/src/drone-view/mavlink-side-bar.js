import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {ChecklistService} from 'checklist/checklistService';
import {AnraNotification} from "../components/anra-notification";
import {AureliaConfiguration} from 'aurelia-configuration';
import moment from 'moment';

@inject(EventAggregator, ChecklistService, AnraNotification, AureliaConfiguration)
export class MavlinkSideBar {
  @bindable user;
  @bindable mission;

  constructor(events, checklistService, notification, config) {
    this.events = events;
    this.checklistService = checklistService;
    this.notification = notification;
    this.battery = 0;
    this.mode = 'OFFLINE';
    this.isArmed = false;
    this.armedMode = '';
    this.lat = '';
    this.lng = '';
    this.altitude = 0;
    this.linkStatus = '';
    this.linkStatusText = '';
    this.isAlive = false;
    this.selectedTab = 'menu1';
    this.waypoints = [];
    this.waypoints1 = [];
    this.baseStreamingUrl = config.get('baseStreamingUrl');
    this.weatherApiKey = config.get('weatherApiKey');
    this.liveMediaStreamSrc = '';
    this.progressBarType = 'progress-bar-info';
    this.weatherData = null;
    this.weatherLocation = '';
    this.weatherObservationDate = '';
    this.weatherCondition = '';
    this.weatherIconUrl = '';
    this.weatherTemprature = '';
    this.weatherHumidity = '';
    this.weatherWindSpeed = '';
    this.unitType = localStorage["UnitType"];
    this.checklist = [];
  }

  attached() {
    this.subscribeEvents();

    this.liveMediaStreamSrc = this.baseStreamingUrl + this.user.ApplicationKey + '|' + this.mission.DroneId;
    console.log("Streaming Url: " + this.liveMediaStreamSrc);
    //Call Live Streaming
    this.initLiveMediaStreaming();

    //Get Current Weather Data
    setTimeout(() => {
        this.getCurrentWeather();
    }, 100);

    this.getCheckList();

    console.log("Streaming Url: " + this.baseStreamingUrl);
  }

  subscribeEvents() {      
      this.events.subscribe('new-packet', p => {

          if (p.message.missionId != this.mission.MissionId) {
              return;
          }

        if (!isNaN(p.message.battery)) {
            this.battery = p.message.battery;
        }
        console.log('battery: ' + p.message.battery);

      this.mode = p.message.mode;
      this.armedMode = p.message.armedMode;
      this.isArmed = p.message.isArmed;
      if (!isNaN(p.message.lat) && !isNaN(p.message.lng) && p.message.lat != 181.0 && p.message.lng != 181.0
          && p.message.lat != 0.0 && p.message.lng != 0.0) {
          this.lat = p.message.lat;
          this.lng = p.message.lng;
      }
      this.altitude = p.message.alt.toFixed(2);
      this.isAlive = true;
      this.setBatteryProgressStyle();
    });

      this.events.subscribe('link-status', p => {
          if (p.missionId != this.mission.MissionId) {
              return;
          }
      this.linkStatus = p.status;
      this.linkStatusText = p.statusText;
    })
  }

  setBatteryProgressStyle() {
      if (this.battery >= 50) {
          this.progressBarType = 'progress-bar-info';
      }
      else if (this.battery >= 30 && this.battery < 50) {
          this.progressBarType = 'progress-bar-warning';
      }
      else {
          this.progressBarType = 'progress-bar-danger';
      }
  }

  onTabSelect(selectedTab) {
    this.selectedTab = selectedTab;
  }

getCheckList() {      
        this.checklistService.getChecklist(this.mission.MissionId)
          .then(result => {
              this.checklist = result.Checklist;              
              this.defaultData = JSON.stringify(this.checklist);
              this.initTreeview();
          }).catch((e) => {
              this.notification.ShowError(e.message);            
          });
    }
   

    initTreeview(){
        var treeChecklist = $('#treeChecklist').treeview({
            data: this.defaultData,
            showIcon: false,
            showCheckbox: true   
        });             
    }

  onWaypointLoad(event) {
    console.log('load wp');
    let reader = new FileReader();
    let file = event.target.files[0];
    reader.readAsText(file);
    let fileName = file.name;

    reader.onload = () => {
      var contents = reader.result;
      if (contents && contents.length > 0) {
        var lines = contents.split('\n');
        if (lines.length > 0) {
          for (var i = 1; i < lines.length; i++) {
            var arr = lines[i].split('\t');
            if (arr.length > 0 && arr[8] && arr[9]) {
              this.waypoints.push(L.latLng(arr[8], arr[9]));
            }
          }

          //Draw
          this.events.publish('drone-view:waypoints', {waypoints: this.waypoints});
          this.waypointFile = contents;
          this.waypoints1 = lines;
        }
      }
    };
  }

  onSaveWaypoints() {
    console.log('save wp');
    this.events.publish('drone-view:save-waypoints', {save:true});
  }

  onReadWaypoints() {
    console.log('read wp');
    this.events.publish('drone-view:read-waypoints', {read:true});
  }

  onWriteWaypoints() {
    console.log('write wp');
    this.events.publish('drone-view:write-waypoints', {waypoints: this.waypoints1});
  }

  startMission() {
    this.events.publish('drone-view:start-mission', {start: true});
  }

  closeMission() {
    this.events.publish('drone-view:close-mission', {close: true});
  }

  arm() {
    this.events.publish('drone-view:arm', {});
  }

  disarm() {
    this.events.publish('drone-view:disarm', {});
  }

  loiter() {
    this.events.publish('drone-view:loiter', {});
  }

  rtl() {
    this.events.publish('drone-view:rtl', {});
  }

  land() {
    this.events.publish('drone-view:land', {});
  }

 getCurrentWeather() {
      
      let latitude = '';
      let longitude = '';

      if (this.isAlive) {
          console.log('Weather Data Using Live Drone');
          latitude = this.lat;
          longitude = this.lng;
      }
      else {
          console.log('Weather Data Using Offline Drone');
          latitude = this.mission.BaseLat;
          longitude = this.mission.BaseLng;
      }

      var me = this; 

      var weatherLocation = latitude + ',' + longitude + '.json';

          $.ajax({
              url: 'https://api.wunderground.com/api/' + me.weatherApiKey + '/conditions/q/' + weatherLocation,              
              type: 'GET',              
              traditional: true,

              success: function (response) {

                  if (JSON.parse(JSON.stringify(response)).current_observation) {
                      me.weatherData = JSON.parse(JSON.stringify(response)).current_observation;
                      me.weatherLocation = me.weatherData.display_location.full;                      
                      me.weatherCondition = me.weatherData.weather;
                      me.weatherIconUrl = me.weatherData.icon_url;
                      me.weatherTemprature = me.unitType == 'Imperial' ? Math.round(me.weatherData.temp_f).toString() : Math.round(me.weatherData.temp_c).toString();                      
                      me.weatherHumidity = me.weatherData.relative_humidity;
                      me.weatherWindSpeed = me.unitType == 'Imperial' ? me.weatherData.wind_gust_mph : me.weatherData.wind_gust_kph;  

                      var dateTime = new Date(me.weatherData.observation_time_rfc822).toISOString();
                      me.weatherObservationDate = moment(dateTime).utcOffset(me.weatherData.local_tz_offset).format('LLLL');
                  }
                  else {
                      me.notification.ShowError('Weather details not found.Please try again.');
                  }
              },

              error: function (response) {                  
                  console.log('Weather Data Fetching Failed');
              }
          });      
  }

  initLiveMediaStreaming() {
    var player = projekktor('#player_a', {
        poster: '../image/logo-login.png',
        title: 'ANRA Livestream',
        playerFlashMP4: 'custom/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
        playerFlashMP3: 'custom/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
        width: 360,
        height: 250,
        platforms: ['browser', 'flash', 'vlc'],
        playlist: [
          {
            0: {
              src: this.liveMediaStreamSrc, streamType: 'rtmp', type: 'video/flv'
            }
          }
        ]
      }
    );
    /*var player = projekktor('#player_a', {
     poster: '../image/logo-login.png',
     title: 'ANRA Livestream',
     playerFlashMP4: 'custom/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
     playerFlashMP3: 'custom/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
     width: 360,
     height: 250,
     platforms: ['browser', 'flash', 'vlc'],
     playlist: [
     {
     0: {
     src: 'http://www.projekktor.com/wp-content/manual/intro.mp4', type:'video/mp4'
     }
     }
     ]
     }
     );*/
  }
}
