import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Router} from 'aurelia-router';
import {MissionService} from 'missions/missionService';
import {DroneService} from 'drones/droneService';
import {UserService} from 'users/userService';
import {Vehicle} from 'lib/vehicle.js';
import { AureliaConfiguration } from "aurelia-configuration";
import {AnraNotification} from "../components/anra-notification";

import * as L from 'leaflet';
import * as T from 'SpatialServer/Leaflet.MapboxVectorTile';
import * as C from 'leaflet-contextmenu';

@inject(EventAggregator, Router, MissionService, DroneService, Vehicle, AureliaConfiguration, UserService, AnraNotification)
export class Flight {

  constructor(events, router, missionService, droneService, vehicle, configure, userService, notification) {
    this.events = events;
    this.configuration = configure;
    this.router = router;
    this.isWaiting = false;
    this.isReady = false;
    this.missionService = missionService;
    this.droneService = droneService;
    this.userService = userService;
    this.vehicle = vehicle;
    this.showMavlinkSideBar = false;
    this.showDjiSideBar = false;    
    this.initState();
    this.isdronelive = false;
    this.gpssignalstatus = 0;
    this.notification = notification;
  }

  initState() {
    this.mission = null;
    this.drone = null;
    this.user = null;
    this.zoomLevel = 14;
    this.displaySidebar = false;
  }

  initMap() {
    let accessToken = this.configuration.get('mapboxAccessToken');

    var streetMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=' + accessToken, {
      attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
    });

    var satelliteMap = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
      attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
    });

    var sectionalMap = L.tileLayer('https://wms.chartbundle.com/tms/v1.0/sec/{z}/{x}/{y}.png?type=google', {
      attribution: '© <a href="https://www.chartbundle.com/">Chartbundle</a>'
    });


    this.airSpaceLayerGroup = new L.LayerGroup();

    this.baseMaps = {
      "Satellites": satelliteMap,
      "Streets": streetMap,
      "VFR Sectional": sectionalMap
    };

    var menuItems = this.showDjiSideBar ? [{ text: 'Land', callback: this.land.bind(this) },
        { text: 'Pause', callback: this.pause.bind(this) }]
        : [{ text: 'Fly To', callback: this.flyto.bind(this) },
            { text: 'Take Off', callback: this.takeoff.bind(this) }];

    let map = L.map('map', {
        contextmenu: true,
        contextmenuWidth: 140,
        contextmenuItems: menuItems,
        layers: [streetMap]
    })
        .setView([this.lat, this.lng], this.zoomLevel);

    this.layerControl = L.control.layers(this.baseMaps, this.overlayMaps).addTo(map);

    map.doubleClickZoom.disable();

    this.map = map;
    L.Icon.Default.imagePath = 'styles/images';

  }


  land(e) {     
      this.events.publish('command given', { command: 'Land' });
  }

  pause(e) {
      this.events.publish('command given', { command: 'Pause' });
  }

  flyto(e) {
      console.log('fly to callback not implemented yet');
  }

  takeoff(e) {
      console.log('take-off callback not implemented yet');
  }

  activate(params) {      
    this.resetView = true;
    this.subscribeEvents();
    let gufi = params.id;

    this.getMissionDetails(gufi);
  }

  handleNewPacket(p) {

      this.lastPacketTime = new Date();
      if (this.drone.DroneId == p.droneId && this.mission.MissionId == p.message.missionId ){
      this.lat = p.message.lat;
      this.lng = p.message.lng;
      this.isdronelive = true;
      this.gpssignalstatus = p.message.gpssignalstatus;
      //console.log('DroneLive', this.isdronelive);

      if (this.resetView) {
        this.map.setView([this.lat, this.lng], 18);
        this.resetView = false;
      }

      if (this.path === undefined || this.path == null) {
        this.path = new L.Polyline(new L.LatLng(this.lat, this.lng), {'color': 'red'});
        this.map.addLayer(this.path);
      }

      this.path.addLatLng(new L.LatLng(this.lat, this.lng));
    }
  }


  subscribeEvents() {
    console.log('Flight:: subscribing events')
    this.events.subscribe('new-packet', p => {
      this.handleNewPacket(p);
    });

    this.events.subscribe('leaflet:ready', p => {
      this.vehicle.init();
    });

    this.events.subscribe('drone-view:start-mission', () => {
      this.startMission()
    });
    this.events.subscribe('drone-view:close-mission', () => {
      this.closeMission()
    });

    this.events.subscribe('drone-view:arm', () => {
      this.vehicle.doCommand({Value: 'arm'});
    });
    this.events.subscribe('drone-view:disarm', () => {
      this.vehicle.doCommand({Value: 'disarm'});
    });
    this.events.subscribe('drone-view:loiter', () => {
      this.vehicle.doCommand({Value: 'loiter'});
    });
    this.events.subscribe('drone-view:rtl', () => {
      this.vehicle.doCommand({Value: 'rtl'});
    });
    this.events.subscribe('drone-view:land', () => {
      this.vehicle.doCommand({Value: 'land'});
    });
    this.events.subscribe('drone-view:write-waypoints', p => {
      this.vehicle.writeWaypoints(p.waypoints);
    });

    this.events.subscribe('drone-view:read-waypoints', p => {
      this.vehicle.readWaypoints();
    });

    this.events.subscribe('drone-view:save-waypoints', p => {
      this.vehicle.saveWaypoints();
    });

    this.events.subscribe('leaflet:add-vehicle', p => {
      this.map.addLayer(p.vehicle);
    });

    this.events.subscribe('connection-ready', p => {
      console.log('connection-ready');
      console.log(p.droneId);
      console.log(this.drone.droneId);
      if (p.droneId == this.drone.droneId) {
        this.isReady = true;
      }
    });
    this.events.subscribe('alert-message', p => {
      if (p.droneId === this.drone.DroneId) {
        this.notification.ShowError("There is a airspace near your operation volume");
      }
    });
  }

  getMissionDetails(gufi) {
    this.gufi = gufi;
    this.isWaiting = true;
    this.missionService.getMission(this.gufi)
      .then(result => {
          this.mission = result.Mission;
          this.lng = this.mission.BaseLng;
          this.lat = this.mission.BaseLat;
          this.user = result.Pilots.find(x => x.Id == this.mission.PilotId);          
          this.initDrone();
          this.isWaiting = false;          
      });
  }

  initDrone() {
    this.droneService.getDrone(this.mission.DroneId)
      .then(result => {
        this.drone = result;
        this.isReady = true;
        this.configureSideBar();
        this.initMap();
        this.vehicle.initDrone(this.mission);
      })
      .catch(err => {
        console.error(err);
      })

  }

  configureSideBar() {  
    this.showMavlinkSideBar = (this.drone && this.drone.BrandName.toLowerCase() == '3drobotics');
    this.showDjiSideBar = (this.drone && this.drone.BrandName.toLowerCase() == 'dji');
  }

  showsidebar() {
    this.displaySidebar = true;
  }

  startMission() {
    this.vehicle.startTracking(this.mission.MissionId);
  }

  closeMission() {
    this.vehicle.stopTracking(this.mission.MissionId);
    this.isWaiting = true;
    this.missionService.closeMission(this.mission)
      .then(result => {
        this.router.navigateToRoute('#/flight-data-listing')
        this.isWaiting = false;
      });
  }
}

