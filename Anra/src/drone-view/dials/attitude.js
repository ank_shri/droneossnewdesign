import {customElement, bindable, inject} from 'aurelia-framework';
import 'jquery';
import {FlightIndicator} from 'sebmatton/jQuery-Flight-Indicators';
import {EventAggregator} from 'aurelia-event-aggregator';

@customElement('jquery-attitude-indicator')
@inject(EventAggregator, Element)
export class Attitude {
  @bindable size;

  constructor(events, element) {
    this.element = element;
    this.events = events;
  }

  attached() {
    this.indicator = $.flightIndicator($(this.element), 'attitude', {size: this.size, pitch: 0, roll:0, showBox: false});
    this.subscribeEvents();
  }

  subscribeEvents(){
    this.events.subscribe('new-packet', p => {
      this.indicator.setPitch(p.message.pitch);
      this.indicator.setRoll(p.message.roll);
    });
  }

}
