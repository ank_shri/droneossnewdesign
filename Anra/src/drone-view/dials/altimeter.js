import {customElement, bindable, inject} from 'aurelia-framework';
import 'jquery';
import {FlightIndicator} from 'sebmatton/jQuery-Flight-Indicators';
import {EventAggregator} from 'aurelia-event-aggregator';

@customElement('jquery-altimeter-indicator')
@inject(EventAggregator, Element)
export class Altimeter {
  @bindable size;

  constructor(events, element) {
    this.element = element;
    this.events = events;
  }  

  attached() {
    this.indicator = $.flightIndicator($(this.element), 'altimeter', {size: this.size, altitude: 0, showBox: false});
    this.subscribeEvents();
  }

  subscribeEvents(){
    this.events.subscribe('new-packet', p => {
        this.indicator.setAltitude(p.message.alt.toFixed(2));
    });
  }


}
