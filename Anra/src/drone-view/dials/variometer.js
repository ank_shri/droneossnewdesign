import {customElement, bindable, inject} from 'aurelia-framework';
import 'jquery';
import {FlightIndicator} from 'sebmatton/jQuery-Flight-Indicators';
import {EventAggregator} from 'aurelia-event-aggregator';

@customElement('jquery-vario-indicator')
@inject(EventAggregator, Element)
export class Vario {
  @bindable size;

  constructor(events, element) {
    this.element = element;
    this.events = events;
  }

  attached() {
    this.indicator = $.flightIndicator($(this.element), 'variometer', {size: this.size, vario: 0, showBox: false});
    this.subscribeEvents();
  }

  subscribeEvents(){
    this.events.subscribe('new-packet', p => {
      this.indicator.setVario(p.message.vario);
    });
  }
}
