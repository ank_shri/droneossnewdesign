import {customElement, bindable, inject} from 'aurelia-framework';
import 'jquery';
import {FlightIndicator} from 'sebmatton/jQuery-Flight-Indicators';
import {EventAggregator} from 'aurelia-event-aggregator';

@customElement('jquery-heading-indicator')
@inject(EventAggregator, Element)
export class Heading {
  @bindable size;

  constructor(events, element) {
    this.element = element;
    this.events = events;
  }

  attached() {
    this.indicator = $.flightIndicator($(this.element), 'heading', {size: this.size, heading: 0, showBox: false});
    this.subscribeEvents();
  }

  subscribeEvents(){
    this.events.subscribe('new-packet', p => {
      this.indicator.setHeading(p.message.heading);
    });
  }

}
