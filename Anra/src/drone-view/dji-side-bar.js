import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AureliaConfiguration} from 'aurelia-configuration';
import { CommandService } from 'drone-view/commandService';
import {ChecklistService} from 'checklist/checklistService';
import {AnraNotification} from "../components/anra-notification";
import * as PahoMQTT from 'paho-mqtt';
import moment from 'moment';

@inject(EventAggregator, CommandService, ChecklistService, AureliaConfiguration, AnraNotification)
export class DjiSideBar {
  @bindable mission;
  @bindable user;
  @bindable isdronelive;
  @bindable gpssignalstatus;

  constructor(events, commandService, checklistService, config, notification) {
    this.events = events;
    this.commandService = commandService;
    this.checklistService = checklistService;
    this.notification = notification;
    this.battery = 0;
    this.mode = 'OFFLINE';
    this.isPaused = false;
    this.lat = '';
    this.lng = '';
    this.altitude = 0;    
    this.linkStatusCssVal = '';
    this.linkStatusTitle = '';
    this.isAlive = false;
    this.selectedTab = 'menu1';
    this.waypoints = [];
    this.waypoints1 = [];
    this.baseStreamingUrl = config.get('baseStreamingUrl');
    this.weatherApiKey = config.get('weatherApiKey');
    this.liveMediaStreamSrc = '';
    this.commandLand = false;
    this.commandLaunch = false;
    this.commandRth = false;
    this.progressBarType = 'progress-bar-info';

    this.mqttHost = config.get('mqttBrokerHost');
    this.mqttPort = config.get('mqttBrokerPort');
    this.mqttUser = config.get('mqttBrokerUserName');
    this.mqttPwd = config.get('mqttBrokerPwd');
    this.mqttUseSSL = config.get('mqttUseSSL') ? true : false;
    this.position = null;
    this.weatherData = null;
    this.weatherLocation = '';
    this.weatherObservationDate = '';
    this.weatherCondition = '';
    this.weatherIconUrl = '';
    this.weatherTemprature = '';
    this.weatherHumidity = '';
    this.weatherWindSpeed = '';
    this.unitType = localStorage["UnitType"];
    this.checklist = [];
    this.weatherApiKeyId='42f44d51b70793b9a955f8fec6155988';
    this.emulator_box = false;
  }

  attached() {     
      this.subscribeEvents();
    this.initMqttClient();

    this.linkStatusCssVal = this.getgpsSignalStatusText(this.gpssignalstatus);
    //Create Live Streaming Video source Url for player
    this.liveMediaStreamSrc = this.baseStreamingUrl + this.user.ApplicationKey + '|' + this.mission.DroneId;
    console.log("Streaming Url: " + this.liveMediaStreamSrc);
    //Get Current Weather Data
    setTimeout(() => {
        this.getCurrentWeather();
    }, 100);
    
    this.getCheckList();
  }

  initMqttClient() {
    try {
      this.mqtt = new Paho.MQTT.Client(this.mqttHost, Number(this.mqttPort), "web_" + parseInt(Math.random() * 100, 10));
      this.mqtt.connect({useSSL: this.mqttUseSSL,userName: this.mqttUser, password: this.mqttPwd, onSuccess: () => this.onMqttConnect()});

      this.mqtt.onMessageArrived = (m) => this.handleNewPacket(m);
      this.mqtt.onConnectionLost = (p) => this.onMqttConnectionLost(p);
    }
    catch (err) {
      console.error(err);
    }
  }

  subscribeEvents() {
      this.events.subscribe('new-packet', p => {
          if (p.message.missionId != this.mission.MissionId) {
              return;
          }

          if (!isNaN(p.message.battery) && p.message.battery !=0) {
              this.battery = p.message.battery;
          }
          console.log('battery: ' + p.message.battery);
          this.mode = p.message.mode;

          console.log('Mode: ' + p.message.mode);

          if (!isNaN(p.message.lat) && !isNaN(p.message.lng) && p.message.lat != 181.0 && p.message.lng != 181.0
              && p.message.lat != 0.0 && p.message.lng != 0.0) {
              this.lat = p.message.lat;
              this.lng = p.message.lng;
          }
          this.altitude = p.message.alt.toFixed(2);
          this.isAlive = true;
          this.getgpsSignalStatusText(p.message.gpssignalstatus);
          this.setBatteryProgressStyle();
      });  

      this.events.subscribe('command given', p => {
          this.commandfrommap(p);
      });
  }

  getCurrentWeather() {
      
      let latitude = '';
      let longitude = '';

      if (this.isAlive) {
          console.log('Weather Data Using Live Drone');
          latitude = this.lat;
          longitude = this.lng;
      }
      else {
          console.log('Weather Data Using Offline Drone');
          latitude = this.mission.BaseLat;
          longitude = this.mission.BaseLng;
      }

      var me = this; 

          $.ajax({
              url: 'https://api.openweathermap.org/data/2.5/weather?lat='+latitude+'&lon='+longitude+'&APPID='+this.weatherApiKeyId,              
              type: 'GET',              
              success: function (response) {

                  if (response) {
                      console.log('Response',response);
                      me.weatherData = JSON.parse(JSON.stringify(response));
                      me.weatherLocation = me.weatherData.name;                      
                      me.weatherCondition = me.weatherData.weather[0].main;
                      me.weatherIconUrl = "http://openweathermap.org/img/w/" + me.weatherData.weather[0].icon + ".png"; 
                    me.weatherTemprature = me.unitType == 'Imperial' ? (((me.weatherData.main.temp - 273.15) * 1.8) + 32).toFixed(1) : (me.weatherData.main.temp - 273.15).toFixed(1);                      
                      me.weatherHumidity = me.weatherData.main.humidity;
                    me.weatherWindSpeed = me.unitType == 'Imperial' ? (me.weatherData.wind.speed * 2.237) : me.weatherData.wind.speed;  
                  }
                  else {
                      me.notification.ShowError('Weather details not found.Please try again.');
                  }
              },

              error: function (response) {                  
                  console.log('Weather Data Fetching Failed');
              }
          });      
  }

  commandfrommap(p)
  {
      if (p.command == 'Land'){
          this.land();
      }

      if (p.command == 'Pause') {
          this.loiter();
      }
  }


  setBatteryProgressStyle() {
      if (this.battery >= 50) {
          this.progressBarType = 'progress-bar-info';
      }
      else if (this.battery >= 30 && this.battery < 50) {
          this.progressBarType = 'progress-bar-warning';
      }
      else {
          this.progressBarType = 'progress-bar-danger';
      }
  }

  onMqttConnectionLost(p) {
    this.isMqttConnected = false;
  }

  onMqttConnect() {
    this.isMqttConnected = true;
  }

  publishTopic(topic, msg) {
    let message = new Paho.MQTT.Message(JSON.stringify(msg));
    message.destinationName = topic;
    this.mqtt.send(message);
  }

  getgpsSignalStatusText(gpssignalval) {

    if (gpssignalval == 0) {
        this.linkStatusTitle = "Network Error";
        this.linkStatusCssVal = "NetworkError";      
    }
    else {
        this.linkStatusTitle = this.linkStatusCssVal = gpssignalval > 2 ? "Good" : "Lost";
        //this.linkStatusCssVal = gpssignalval > 2 ? "Good" : "Lost";
    }
  }

  onTabSelect(selectedTab) {
      this.selectedTab = selectedTab;

      if (projekktor('player_a') && projekktor('player_a')._isReady && this.selectedTab == 'menu2') {
          projekktor('player_a').destroy();          
          //Call Live Streaming
          this.initLiveMediaStreaming(true);
      }

      if (!projekktor('player_a') && this.selectedTab == 'menu2') {
          this.initLiveMediaStreaming(false);
      }    
  }

  getCheckList() {      
        this.checklistService.getChecklist(this.mission.Gufi)
          .then(result => {
              this.checklist = result.Checklist;              
              this.defaultData = JSON.stringify(this.checklist);
              this.initTreeview();
          }).catch((e) => {
              this.notification.ShowError(e.message);            
          });
    }
   

    initTreeview(){
        var treeChecklist = $('#treeChecklist').treeview({
            data: this.defaultData,
            showIcon: false,
            showCheckbox: true   
        });

      //disable treeview
      $("#treeChecklist").addClass("disabletreeview");
    }

  onWaypointLoad(event) {
    console.log('load wp');
    let reader = new FileReader();
    let file = event.target.files[0];
    reader.readAsText(file);
    let fileName = file.name;

    reader.onload = () => {
      var contents = reader.result;
      if (contents && contents.length > 0) {
        var lines = contents.split('\n');
        if (lines.length > 0) {
          for (var i = 1; i < lines.length; i++) {
            var arr = lines[i].split('\t');
            if (arr.length > 0 && arr[8] && arr[9]) {
              this.waypoints.push(L.latLng(arr[8], arr[9]));
            }
          }

          //Draw
          this.events.publish('drone-view:waypoints', {waypoints: this.waypoints});
          this.waypointFile = contents;
          this.waypoints1 = lines;
        }
      }
    };
  }

  onSaveWaypoints() {
    console.log('save wp');
    this.events.publish('drone-view:save-waypoints', {save: true});
  }

  onReadWaypoints() {
    console.log('read wp');
    this.events.publish('drone-view:read-waypoints', {read: true});
  }

  onWriteWaypoints() {
    console.log('write wp');
    this.events.publish('drone-view:write-waypoints', {waypoints: this.waypoints1});
  }

  startMission() {
    this.events.publish('drone-view:start-mission', {start: true});
  }

  closeMission() {
    this.events.publish('drone-view:close-mission', {close: true});
  }

  //arm() {
  //  this.events.publish('drone-view:arm', {});
  //}

  rth() {
    var element = document.getElementById("btnRth");
    this.commandRth = !this.commandRth;
    //Set Css class dynamically
    // element.classList.remove("btn-command");
    // element.classList.add("btn-command-clicked");

    var topic = 'drone/oss/' + this.mission.DroneId + '/command';
    var msg = {
      "commandId": 1,
      "droneId": this.mission.DroneId,
      "missionId": this.mission.MissionId,
      "userId": localStorage["UserId"],
      "command": "returnToHome"
    };

    this.publishTopic(topic, msg);
    this.saveCommand(JSON.stringify(msg));
  }

  loiter() {
    this.isPaused = true;
    var topic = 'drone/oss/' + this.mission.DroneId + '/command';
    var msg = {
      "commandId": 1,
      "droneId": this.mission.DroneId,
      "missionId": this.mission.MissionId,
      "userId": localStorage["UserId"],
      "command": "pause"
    };

    this.publishTopic(topic, msg);
    this.saveCommand(JSON.stringify(msg));
  }

  resume() {
    this.isPaused = false;
    var topic = 'drone/oss/' + this.mission.DroneId + '/command';
    var msg = {
      "commandId": 1,
      "droneId": this.mission.DroneId,
      "missionId": this.mission.MissionId,
      "userId": localStorage["UserId"],
      "command": "resume"
    };

    this.publishTopic(topic, msg);
    this.saveCommand(JSON.stringify(msg));
  }

  capture() {
    var topic = 'drone/oss/' + this.mission.DroneId + '/command';
    var msg = {
      "commandId": 1,
      "droneId": this.mission.DroneId,
      "missionId": this.mission.MissionId,
      "userId": localStorage["UserId"],
      "command": "capture"
    };

    this.publishTopic(topic, msg);
    this.saveCommand(JSON.stringify(msg));
  }

  land() {
    var element = document.getElementById("btnLand");
    this.commandLand = !this.commandLand;
    //Set Css class dynamically
    // element.classList.remove("btn-command");
    // element.classList.add("btn-command-clicked");

    var topic = 'drone/oss/' + this.mission.DroneId + '/command';
    var msg = {
      "commandId": 1,
      "droneId": this.mission.DroneId,
      "missionId": this.mission.MissionId,
      "userId": localStorage["UserId"],
      "command": "immediateLanding"
    };

    this.publishTopic(topic, msg);
    this.saveCommand(JSON.stringify(msg));
  }

  launch() {
      var element = document.getElementById("btnLaunch");
      this.commandLaunch = !this.commandLaunch;
      //Set Css class dynamically
      // element.classList.remove("btn-command");
      // element.classList.add("btn-command-clicked");

      var topic = 'drone/oss/' + this.mission.DroneId + '/command';
      var msg = {
          "commandId": 1,
          "droneId": this.mission.DroneId,
          "missionId": this.mission.MissionId,
          "userId": localStorage["UserId"],
          "command": "startWaypoint"
      };

      this.publishTopic(topic, msg);
      this.saveCommand(JSON.stringify(msg));
  }

  saveCommand(commandMessage) {

    this.commandService.saveCommand(commandMessage).then(result => {
      if (result.IsSuccess) {
        console.log('Command Saved.');
      }
      else {
        console.log('Command Saving Failed - ', result.ErrorMessage);
      }
    });
  }

  initLiveMediaStreaming(playAuto) {
      
    var player = projekktor('#player_a', {
        poster: '../image/logo-login.png',
        title: 'ANRA Livestream',
        controls: true,
        playerFlashMP4: 'custom/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
        playerFlashMP3: 'custom/projekktor/swf/StrobeMediaPlayback/StrobeMediaPlayback.swf',
        width: 335,
        height: 250,
        platforms: ['browser', 'flash', 'vlc'],
        autoplay: playAuto,
        playlist: [
          {
            0: {
              src: this.liveMediaStreamSrc, streamType: 'rtmp', type: 'video/flv'
            }
          }
        ]
      }
    );
  }
  //Emulator Command
  emulatorPad(){
    this.emulator_box = !this.emulator_box;
    if(this.emulatorPad)
    {
      this.commandLaunch = !this.commandLaunch;
      var topic = 'drone/oss/' + this.mission.DroneId + '/command';
      var msg = {
          "commandId": 1,
          "droneId": this.mission.DroneId,
          "missionId": this.mission.MissionId,
          "userId": localStorage["UserId"],
          "command": "remote_control"
      };
      this.publishTopic(topic, msg);
      this.saveCommand(JSON.stringify(msg));
    }
  }

  emulatorCommand(command)
  {
    var topic = 'drone/oss/' + this.mission.DroneId + '/command';
    var msg = {
      "commandId": 1,
      "droneId": this.mission.DroneId,
      "missionId": this.mission.MissionId,
      "userId": localStorage["UserId"],
      "command": command
    };
    this.publishTopic(topic, msg);
    this.saveCommand(JSON.stringify(msg));
   
  }
}
