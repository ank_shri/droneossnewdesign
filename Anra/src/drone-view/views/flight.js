import 'leaflet';
import 'SpatialServer/Leaflet.MapboxVectorTile';
import 'leaflet-easybutton';

import { UIApplication, UIDialog, UIDialogService, UIUtils } from 'aurelia-ui-framework';
import { autoinject, inject, observable, useView } from 'aurelia-framework';

import { AureliaConfiguration } from 'aurelia-configuration';
import { Contigencies } from 'contigency/contigencies';
import { Ureps } from 'ureps/ureps';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Vehicle } from 'lib/vehicle';
import { VehicleExDrone } from './lib/vehicleExDrone';
import { NotamService } from 'notams/notamService';
import { AnraNotification } from 'components/anra-notification';
import { MqttTopicHelper } from 'common/mqtt-topic-helper';
import { AnraMqttClient } from "components/mqttClient";
import { LookupHelper } from 'common/lookup-helper';
import { LunOperations } from 'lun-operations/lun-operations';
import { OperationService } from 'operations/operationService';
import { WeatherServices } from 'weather/weatherServices';
import { WeatherDetailsDialog } from 'weather/weatherDetails-dialog';
import { Detections } from 'detections/detections';
import { Collisions } from 'collisions/collisions';
import { CollisionService } from 'collisions/collisionService';
import { UtmService } from "utms/utmService";
import { ConstraintMessageService } from 'constraint-messages/constraintMessageService';
import moment from 'moment';
import { MapCommon } from 'common/map-common';


@inject(EventAggregator, UIDialogService, AureliaConfiguration, Detections, Vehicle, UtmService, NotamService, CollisionService, AnraNotification, AnraMqttClient, UIApplication, OperationService, WeatherServices, VehicleExDrone, ConstraintMessageService, MapCommon)
export class Flight {
  panEnabled = true;

  constructor(events, dlgService, config, detections, vehicle, utmService, notamService, collisionService, notification, anraMqttClient, application, operationService, weatherService, vehicleExDrone, constraintMessageService, maps) {
    this.maps = maps;
    this.events = events;
    this.dlgService = dlgService;
    this.detections = detections;
    this.operationService = operationService;
    this.weatherService = weatherService;
    this.collisionService = collisionService;
    this.constraintMessageService = constraintMessageService;
    this.config = config;
    this.drone = null;
    this.vehicle = vehicle;
    this.notamService = notamService;
    this.utmService = utmService;
    this.notification = notification;
    this.mqttClient = anraMqttClient;
    this.notamsDisplayBool = true;
    this.application = application;
    this.notamsCircles = [];
    this.detectedDrones = [];
    this.subscriptions = [];
    this.collisionCount = 0;
    this.isWaiting = false;
    this.vehicleExDrone = vehicleExDrone;
    this.externalVehicle = null;
    this.externalDrone = null;
    this.isStartPeriodicPosition = false;
    this.operationConflicts = [];
    this.drawConflictUsingMqtt = false;
    this.removedConflictLayer = [];

  }

  activate(model) {
    this.sensorType = LookupHelper.GetSensorTypes();
    this.drone = model.drone;
    this.operation = model.operation;
    this.userId = this.application.session("UserId");

    this.isAlive = false;
    this.battery = '-';
    this.mode = 'Offline';
    this.armedMode = '-';
    this.isArmed = false;
    this.lat = this.drone.BaseLat;
    this.lng = this.drone.BaseLng;
    this.airspeed = '-';
    this.altitude = '-';
    this.linkStatus = '-';
    this.dynamicRestriction = [];
  }

  attached() {
    if (!this.userId) {
      this.application.logout();
    }

    this.subscribeEvents();
    this.initMap();
    this.getCollisionWarnings();
    this.getConflicts();
    //this.getUssDetails();
  }

  detached() {
    this.map.remove();
    this.subscriptions.map(x => x.dispose());
    this.vehicle.stopSubscription();
  }

  subscribeEvents() {

    let watch1 = this.events.subscribe('leaflet:add-vehicle', p => {
      let v = this.map.addLayer(p.vehicle);
    });
    this.subscriptions.push(watch1);

    let watch2 = this.events.subscribe('leaflet:add-layer', p => {
      this.map.addLayer(p.newlayer);
    });
    this.subscriptions.push(watch2);

    let watch3 = this.events.subscribe('leaflet:set-view', p => {
      if (this.panEnabled) {
        this.updateMapPan(p.lat, p.lng, p.zoom);
      }
    });
    this.subscriptions.push(watch3);

    let watch4 = this.events.subscribe('leaflet:remove-layer', p => {
      this.map.removeLayer(p.layer);
    });
    this.subscriptions.push(watch4);

    let watch5 = this.events.subscribe('vehicle-update-droneinfo', p => {
      if (this.drone && this.drone.Uid == p.droneId) {
        this.isAlive = p.altitude > 0 ? true : false;
        this.battery = p.battery + ' %';
        this.mode = p.mode;
        this.armedMode = p.armedMode;
        this.isArmed = p.isArmed;
        this.lat = p.latitude;
        this.lng = p.longitude;
        this.airspeed = p.airspeed;
        this.altitude = p.altitude;
        this.linkStatus = p.linkStatus;
      }
    });
    this.subscriptions.push(watch5);


    let watch6 = this.events.subscribe('new-collision-warning', p => {
      if (this.operation.gufi == p.gufi) {
        this.vehicle.onCollisionWarning();
        this.collisionCount++;
        this.notification.ShowCollisionWarning('Collision Warning!');
      }
    });
    this.subscriptions.push(watch6);

    let watch7 = this.events.subscribe('link-status-update', p => {
      if (this.drone && this.drone.Uid == p.droneId) {
        this.linkStatus = p.status;
      }
    });
    this.subscriptions.push(watch7);

    let watch8 = this.events.subscribe('start-Periodic-Position', p => {
      if (!this.isStartPeriodicPosition) {
        this.externalDrone = this.drone;
        this.externalDrone.TypeName = 'Unknown';
        this.vehicleExDrone.initDrone(this.externalDrone, p.externalGufi);
        this.isStartPeriodicPosition = true;
      }
    });
    this.subscriptions.push(watch8);

    let watch9 = this.events.subscribe('stop-Periodic-Position', p => {
      if (this.isStartPeriodicPosition) {
        this.vehicleExDrone.resetOperation(p.externalGufi);
        this.isStartPeriodicPosition = false;
      }
    });
    this.subscriptions.push(watch9);

    this.mqttClient.subscribeTopic('operation/' + this.operation.gufi + '/uvr');
    let watch10 = this.events.subscribe('operation/' + this.operation.gufi + '/uvr', p => {
      if (p.packet.Gufi == this.operation.gufi) {
        this.showDynamicRestrictionNotification(p.packet);
      }
    });
    this.subscriptions.push(watch10);
    this.mqttClient.subscribeTopic(`notification/${this.userId}/generic`);
    let watch11 = this.events.subscribe(`notification/${this.userId}/generic`, p => {
      let message = "Message:" + p.packet.Message + " " + "Type:" + p.packet.Type;
      this.notification.ShowInfo(message);
    });
    this.subscriptions.push(watch11);

    let watch12 = this.events.subscribe('drawconflictedoperations', p => {
      this.drawConflictUsingMqtt = true;
      this.operationConflicts = [];
      this.operationConflicts = p.operations;
      console.log('Flight::Conflicted Operation Details(Mqtt):', this.operationConflicts);
      this.getConflicts();
    });

    this.subscriptions.push(watch12);
    //show hide conflicted layer based on user input.
    let watch13 = this.events.subscribe('showHideConflictLayer', p => {
      if (p.layer.isVisible) {
        this.removedConflictLayer.map(layer => {
          if (layer.feature && layer.feature.gufi == p.layer.operationconflict.conflicting_operation_gufi) {
            this.map.addLayer(layer);
          }

        })
      }
      else {
        this.map.eachLayer(layer => {
          if (layer.feature && layer.feature.gufi == p.layer.operationconflict.conflicting_operation_gufi) {
            this.map.removeLayer(layer);
            this.removedConflictLayer.push(layer);
          }
        })
      }
    });

    this.subscriptions.push(watch13);

  }

  initMap() {
    this.map = this.maps.getMap('drone-view-map', 'Street', 16, this.lat, this.lng);
    this.maps.displayNotams();
    this.onMapInit();
  }

  onMapInit() {
    setTimeout(() => {
      this.setupToggleButton();
      this.drawOperationVolume();
      this.fetchConstraints();
      this.vehicle.initDrone(this.drone, this.operation.gufi);
      this.detections.init(this.map, this.operation.gufi, this.userId);
    });
  }

  fetchConstraints() {
    this.constraintMessageService.getConstraints().then(result => {
      this.dynamicRestriction = result.filter(x => moment(x.effective_time_end).isAfter(moment.utc()));

      if (this.dynamicRestriction.length > 0) {
        this.drawConstraints();
      }
    })
      .catch(e => {
        console.error(e);
        this.isWaiting = false;
      });
  }

  notamsDisplay() {
    let scope = this;
    if (this.notamsDisplayBool) {
      this.multipolygon.addTo(this.map);
      this.notamsCircles.map(x => {
        x.addTo(scope.map);
      });
    } else {
      this.map.removeLayer(this.multipolygon);
      this.notamsCircles.map(x => {
        scope.map.removeLayer(x);
      });
    }
  }

  formatDateTime(dt) {
    return moment.utc(dt).utcOffset(moment().utcOffset()).format('L LT');
  }

  drawOperationVolume() {

    let features = [];
    this.operation.operation_volumes.map(p => {
      features.push(
        {
          type: 'Feature',
          geometry: p.operation_geography,
          properties: {
            Ordinal: p.ordinal,
            Begin_Time: this.formatDateTime(p.effective_time_begin),
            End_Time: this.formatDateTime(p.effective_time_end),
            Min_Altitude: p.min_altitude.altitude_value,
            Max_Altitude: p.max_altitude.altitude_value
          }
        });
    });


    let geoJsonData = {
      type: "FeatureCollection",
      crs: {
        type: "name",
        properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" }
      },
      features: features
    };

    let popupOptions = {
      'minWidth': '300',
      'className': 'leaflet-popup-style'
    };

    this.volumeLayer = L.geoJson(geoJsonData, {
      style: function (feature) {
        return {
          weight: 2,
          opacity: 1,
          color: 'rgb(0,0,255)',
          dashArray: '4',
          fillOpacity: 0.2
        };
      },
      onEachFeature: (feature, layer) => {
        let content = `<div class="popup-data-title">Operation Volume</div>`
        let properties = Object.entries(feature.properties);
        properties.forEach((item) => {
          content += `<div class="popup-data-item"><label>${item[0].replace('_', ' ')}</label>: ${item[1]}</div>`;
        })
        layer.bindPopup(content, popupOptions);
      }
    }).addTo(this.map);

    this.map.fitBounds(this.volumeLayer.getBounds(), { padding: [150, 150] });

  }

  drawFlights(flightData) {
    let planeIcon = L.icon({
      iconUrl: "./images/icons/" + this.config.get('drone-icon'),
      iconSize: [50, 47],
      iconAnchor: [25, 25],
      popupAnchor: [-3, -4]
    });

    if (flightData.SearchResult.aircraft.length > 0) {
      this.flightMarkerGroup.map(x => {
        this.map.removeLayer(x);
      });
      this.flightMarkerGroup = flightData.SearchResult.aircraft.map(x => {
        return new L.Marker([x.latitude, x.longitude], { icon: planeIcon, rotationAngle: x.heading });
      });
      this.flightMarkerGroup.map(x => {
        x.addTo(this.map);
      });
    } else {
      this.notification.ShowInfo('No Flight(s) available');
    }
  }

  setupToggleButton() {
    L.easyButton(
      '<strong>P</strong>',
      () => {
        this.panEnabled = !this.panEnabled;
      },
      'Enable/Disable Pan'
    ).addTo(this.map);
  }

  updateMapPan(lat, lng, zoom) {
    let offset = this.map
      ._getNewTopLeftPoint([lat, lng])
      .subtract(this.map._getTopLeftPoint());

    this.map.fire('movestart');
    this.map._rawPanBy(offset);
    this.map.fire('move');
    this.map.fire('moveend');

    this.map.setZoom(zoom);
  }

  showContingencies() {
    this.dlgService.show(Contigencies, {
      modal: this.operation,
      contigencyPlan: null,
      modelView: this,
      doSaveOperation: true
    });
  }

  showUrep() {
    this.dlgService.show(Ureps, {
      modal: this.operation,
      modelView: this
    });
  }

  showLunOperations() {
    this.dlgService.show(LunOperations, {
      modal: this.operation,
      modelView: this
    });
  }

  getUssDetails() {
    this.utmService.getUSSById(this.operation.uss_instance_id).then(result => {
      console.log('USS', result.coverage_area);
      L.polygon(result.coverage_area[0].coordinates, { color: 'red' }).addTo(this.map);
    });
  }

  showWeather() {
    let mapCenter = this.map.getCenter();
    let data = {
      type: 'Point',
      coordinates: [mapCenter.lng, mapCenter.lat]
    };
    this.isWaiting = true;

    this.weatherService.getWeather(data)
      .then((result) => {
        this.isWaiting = false;
        this.dlgService.show(WeatherDetailsDialog, {
          weatherData: result
        });
      });
  }

  showCollisions() {
    this.dlgService.show(Collisions, {
      modal: this.operation,
      modelView: this
    });
  }

  getCollisionWarnings() {
    this.collisionService.getListing(this.operation.gufi)
      .then(result => {
        this.collisionCount = result ? result.length : 0;
      })
  }

  showDynamicRestrictionNotification(packet) {
    let notification = {
      gufi: packet.Gufi,
      message: packet.Message,
      status: packet.Status,
      constraint_message: packet.ConstraintMessage,
      timestamp: packet.Timestamp
    };

    this.notification.ShowInfo(notification.message);

    let constraintMessage = notification.constraint_message;

    if (moment(constraintMessage.effective_time_end).isAfter(moment.utc())) {

      //First Filter out existing DRs
      this.dynamicRestriction = this.dynamicRestriction.filter(x => x.message_id != constraintMessage.message_id);
      this.dynamicRestriction.push(notification.constraint_message);
      this.drawConstraints();
    }
    else {
      this.notification.ShowInfo('Expired Constraint');
    }
  }

  drawConstraints() {

    let geoJsonData = {
      type: "FeatureCollection",
      crs: {
        type: "name",
        properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" }
      },
      features: []
    };

    this.dynamicRestriction.map(x => {
      let feature = {
        type: 'Feature',
        geometry: x.geography,
        properties: {
          message_id: x.message_id,
          name: 'UVR Details',
          uss_name: x.uss_name,
          type: x.type,
          effective_time_begin: x.effective_time_begin,
          effective_time_end: x.effective_time_end,
          min_altitude: x.min_altitude.altitude_value,
          max_altitude: x.max_altitude.altitude_value,
          permitted_gufis: x.permitted_gufis,
          permitted_uas: x.permitted_uas,
          reason: x.reason,
          cause: x.cause
        }
      };
      geoJsonData.features.push(feature);
    });


    // Create Feature Layer for each USS Coverage Area and Add to Collection
    this.restrictionLayer = L.geoJson(geoJsonData, {
      style: function (feature) {
        return {
          weight: 2,
          opacity: 1,
          color: 'rgb(255, 0, 0)',
          dashArray: '4',
          fillOpacity: 0.2
        };
      },
      onEachFeature: (feature, layer) => {
        var popContent = '<table>';
        popContent += '<tr><th colspan="2"><b>' + feature.properties.name + '</b></th></tr>';
        popContent += '<tr><th colspan="2"><hr></th></tr>';
        popContent += '<tr><td>Id :</td><td>' + feature.properties.message_id + '</td></tr>';
        popContent += '<tr><td>Uss Name :</td><td>' + feature.properties.uss_name + '</td></tr>';
        popContent += '<tr><td>Type :</td><td>' + feature.properties.type + '</td></tr>';
        popContent += '<tr><td>Cause :</td><td>' + feature.properties.cause + '</td></tr>';
        popContent += '<tr><td>Min Alt. :</td><td>' + feature.properties.min_altitude + ' Ft.</td></tr>';
        popContent += '<tr><td>Max Alt. :</td><td>' + feature.properties.max_altitude + ' Ft.</td></tr>';
        popContent += '<tr><td>Start Time :</td><td>' + moment.utc(feature.properties.effective_time_begin).utcOffset(moment().utcOffset()).format("L LT") + '</td></tr>';
        popContent += '<tr><td>End Time :</td><td>' + moment.utc(feature.properties.effective_time_end).utcOffset(moment().utcOffset()).format("L LT") + '</td></tr>';

        if (feature.properties.permitted_gufis && feature.properties.permitted_gufis.length > 0) {
          popContent += '<tr><td>Permitted Gufis :</td><td><font color="green">[' + feature.properties.permitted_gufis.join() + ']</td></tr>';
        }

        if (feature.properties.permitted_gufis && feature.properties.permitted_uas.length > 0) {
          popContent += '<tr><td>Permitted Uas :</td><td><font color="green">[' + feature.properties.permitted_uas.join() + ']</td></tr>';
        }


        popContent += '<tr rowspan="3"><td>Reason :</td><td>' + feature.properties.reason + '</td></tr>';
        popContent += '</table >';

        layer.bindPopup(popContent);
      }
    }).addTo(this.map);

    this.map.fitBounds(this.volumeLayer.getBounds());
  }

  getConflicts() {

    if (!this.drawConflictUsingMqtt) {
      this.operationService
        .getConflicts(this.operation.gufi)
        .then(result => {
          if (result && Array.isArray(result)) {
            this.operationConflicts = result;
            this.events.publish('conflictedoperations', { 'operations': this.operationConflicts });
            console.log('Flight::Conflicted Operation Details(DB):', this.operationConflicts);
            this.drawConflictedOperations();
          } else {
            this.notification.ShowError(result.message);
          }
        });
    }
    else {
      this.drawConflictedOperations();
    }
  }

  drawConflictedOperations() {
    this.operationConflicts.map(x => {
      this.drawConflictedOperation(x);
    });
  }

  drawConflictedOperation(conflictedOperation) {

    let features = [];
    let gufi = conflictedOperation.operationconflict.conflicting_operation_gufi;
    conflictedOperation.reason.map(p => {
      features.push(
        {
          type: 'Feature',
          gufi: gufi,
          geometry: p.operation_geography,
          properties: {
            Ordinal: p.ordinal,
            Begin_Time: this.formatDateTime(p.effective_time_begin),
            End_Time: this.formatDateTime(p.effective_time_end),
            Min_Altitude: p.min_altitude.altitude_value,
            Max_Altitude: p.max_altitude.altitude_value
          }
        });
    });


    let geoJsonData = {
      type: "FeatureCollection",
      crs: {
        type: "name",
        properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" }
      },
      features: features
    };

    let popupOptions = {
      'minWidth': '300',
      'className': 'leaflet-popup-style'
    };

    this.conflictOperationLayer = L.geoJson(geoJsonData, {
      style: function () {
        return {
          weight: 2,
          opacity: 1,
          color: 'rgb(255,0,0)',
          dashArray: '4',
          fillOpacity: 0.2
        };
      },
      onEachFeature: (feature, layer) => {
        let content = `<div class="popup-data-title">Conflicted Operation Details</div>`;
        content += `<div class="popup-data-item"><label>Gufi</label>: ${feature.gufi}</div>`;
        let properties = Object.entries(feature.properties);
        properties.forEach((item) => {
          content += `<div class="popup-data-item"><label>${item[0].replace('_', ' ')}</label>: ${item[1]}</div>`;
        });
        layer.bindPopup(content, popupOptions);
      }
    }).addTo(this.map);

    this.map.fitBounds(this.conflictOperationLayer.getBounds(), { padding: [150, 150] });

  }
}
