import "leaflet";
import 'SpatialServer/Leaflet.MapboxVectorTile';

import {UIDialog, UIDialogService, UIUtils} from "aurelia-ui-framework";
import {autoinject, inject, useView} from "aurelia-framework";
import {AureliaConfiguration} from "aurelia-configuration";
import {EventAggregator} from "aurelia-event-aggregator";
import {Vehicle} from '../../lib/vehicle';

@inject(EventAggregator, AureliaConfiguration, Vehicle)
export class Flight {
  panEnabled = true;
  zoomLevel = 14;

  constructor(events, config, vehicle) {
    this.events = events;
    this.config = config;
    this.drone = null;
    this.vehicle = vehicle;
  }

  activate(model) {
    this.drone = model.drone;
  }

  attached() {
    this.subscribeEvents();
  }

  subscribeEvents() {
    this.events.subscribe('link-status', p => {
      if (this.drone && this.drone.Uid == p.droneId) {
        this.linkStatus = p.status;
      }
    });

    this.events.subscribe('vehicle-update-droneinfo', p => {
      if (this.drone && this.drone.Uid == p.droneId) {
        this.isAlive = p.altitude > 0 ? true : false;
        this.battery = p.battery + ' % Remaining';
        this.mode = p.mode;
        this.armedMode = p.armedMode;
        this.isArmed = p.isArmed;
        this.lat = p.latitude;
        this.lng = p.longitude;
      }
    });
  }
}
