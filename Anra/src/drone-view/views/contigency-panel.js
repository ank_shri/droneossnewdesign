import 'au-table';
import {bindable, customElement, inject, observable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {LookupHelper} from 'common/lookup-helper';
import {UIApplication, UIDialogService} from 'aurelia-ui-framework';
import {Contigency} from "contigency/contigency";
import {OperationService} from 'operations/operationService';
import {AnraNotification} from 'components/anra-notification';

@inject(EventAggregator, UIApplication, OperationService, AnraNotification, UIDialogService)
export class ContigencyPanel {
  contingenyPlans = [];
  pageSize = 15;

  constructor(events, application, operationService, notification, dlgService) {
    this.events = events;
    this.application = application;
    this.dlgService = dlgService;
    this.operationService = operationService;
    this.notification = notification;
  }

  activate(model) {
    this.operation = model;
  }

  onAdd() {
    this.dlgService.show(Contigency, {
      modal: this.operation,
      contigencyPlan: null,
      modelView: this,
      doSaveOperation: true
    });
  }

  onEdit(item) {
    this.dlgService.show(Contigency, {
      modal: this.operation,
      contigencyPlan: item,
      modelView: this,
      doSaveOperation: true
    });
  }
}
