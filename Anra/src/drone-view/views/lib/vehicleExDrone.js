import 'leaflet';
import 'leaflet-rotatedmarker';

import {inject, transient} from 'aurelia-framework';

import {AnraNotification} from 'components/anra-notification';
import {AureliaConfiguration} from 'aurelia-configuration';
import {DroneService} from 'drones/droneService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {MqttTopicHelper} from 'common/mqtt-topic-helper';
import {VehicleServiceExDrone} from './vehicleServiceExDrone';

@inject(
  EventAggregator,
  AureliaConfiguration,
  DroneService,
  VehicleServiceExDrone,
  AnraNotification
)
@transient()
export class VehicleExDrone {
  constructor(events, config, droneService, vehicleServiceExDrone, notification) {
    this.isWaiting = false;
    this.events = events;
    this.config = config;
    this.droneService = droneService;
    this.vehicleServiceExDrone = vehicleServiceExDrone;
    this.notification = notification;
    this.drone = null;
    this.droneMarker = null;
    this.lastPoint = null;
    this.lastPacketTimeStamp = Date.now();
    this.collisionWarningTime = null;
    this.isDroneAssigned = false;
    this.isSubscribed = false;
  }

  initDrone(drone, gufi) {
    console.log('initdrone');

    this.drone = drone;
    this.gufi = gufi;

    if (!this.isSubscribed) {
      this.subscribeEvents();
      this.isSubscribed = true;
    }

    this.vehicleServiceExDrone.connect(this.gufi);
  }

  subscribeEvents() {


    this.events.subscribe('ex-vehicle-new-packet', p => {
      if (this.gufi == p.message.gufi) {
        this.lastPacketTimeStamp = Date.now();
        if (this.path === undefined || this.path == null) {
          this.trackPath(p.message.lat, p.message.lng);
        }
        console.log("new packet from external drone id", p.message.droneId);

        if (!this.isDroneAssigned) {
          this.assignIcon();
          this.isDroneAssigned = true;
        }

        this.updatePosition(
          p.message.lat,
          p.message.lng,
          p.message.heading,
          p.message.linkStatus
        );


      }
    });
  }

  onCollisionWarning() {
    this.collisionWarningTime = Date.now();
  }

  getDroneIconUrl(droneType) {
    let iconUrl;

    switch (droneType) {
      case 'Copter':
        iconUrl = this.config.get('appUrl') + '/images/icons/' + this.config.get('drone-icon')
        break;
      case 'FixedWing':
        iconUrl = this.config.get('appUrl') + '/images/icons/' + this.config.get('drone-icon')
        break;
      case 'Unknown':
        iconUrl = this.config.get('appUrl') + '/images/icons/' + this.config.get('drone-icon')
        break;
    }

    return iconUrl;
  }

  assignIcon() {

    let droneIcon = L.icon({
        iconUrl: this.getDroneIconUrl(this.drone.TypeName),
        iconSize: [50, 47]
      })
    ;

    this.droneMarker = new L.Marker(
      new L.LatLng(this.drone.BaseLat, this.drone.BaseLng), {
        icon: droneIcon,
        rotationAngle: 0,
        rotationOrigin: 'center center'
      }
    );

    this.events.publish('leaflet:add-vehicle', {vehicle: this.droneMarker});

  }

  trackPath(lat, lng) {
    this.path = new L.Polyline(new L.LatLng(lat, lng), {color: 'red'});
    this.events.publish('leaflet:add-layer', {newlayer: this.path});

  }

  updatePosition(lat, lng, heading, status) {
    let latlng = new L.LatLng(lat, lng);
    if (this.path != undefined || this.path != null) {
      this.path.addLatLng(latlng);
    }

    this.droneMarker.setLatLng(new L.LatLng(lat, lng));
    this.droneMarker.setRotationAngle(heading);
  }

  doCommand(cmd) {
    this.vehicleServiceExDrone.doCommand(cmd);
  }

  sendCommandToDJI(topic, msg) {
    this.vehicleServiceExDrone.sendCommandToDJI(topic, msg);
  }

  resetOperation(gufi) {

    if (this.path != undefined || this.path != null)
      this.events.publish('leaflet:remove-layer', {layer: this.path});

    if (this.droneMarker != undefined || this.droneMarker != null)
      this.events.publish('leaflet:remove-layer', {layer: this.droneMarker});

    this.isDroneAssigned = false;
    this.path = null;
    this.vehicleServiceExDrone.resetOperation(gufi);
  }
}
