import {inject, transient} from 'aurelia-framework';

import {AnraMqttClient} from 'components/mqttClient';
import {AureliaConfiguration} from 'aurelia-configuration';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(EventAggregator, AureliaConfiguration, AnraMqttClient)
@transient()
export class VehicleServiceExDrone {
  constructor(events, config, anraMqttClient) {
    this.events = events;
    this.config = config;
    this.mqttClient = anraMqttClient;

    this.isHubConnected = false;
    this.topicPrefix = this.config.get('topicPrefix');

    this.lastPacketTime = null;
    this.currentPacketTime = new Date();
    this.isSubscribed = false;
  }

  connect(gufi) {
    this.gufi = gufi;
    this.subscribeEvents();
    this.updateInterval = setInterval(() => {

    }, 2000);
  }

  subscribeEvents() {

    this.mqttClient.subscribeTopic('operation/' + this.gufi + '/telemetry');
    if (!this.isSubscribed) {
      this.events.subscribe('operation/' + this.gufi + '/telemetry', p => {
        this.lastPacketTime = new Date();
        this.processPacket(p.packet);
      });
      this.isSubscribed = true;
    }


  }


  sendCommandToDJI(topic, msg) {
    this.mqttClient.publishTopic(topic, msg);
  }

  doCommand(cmd) {
    let topic = 'command/drone/' + this.droneId;

    let msg = {
      Type: 'Command',
      Gufi: this.gufi,
      DroneId: this.droneId,
      Value: cmd.Value,
      Alt: cmd.Alt,
      Lat: cmd.Lat,
      Lng: cmd.Lng
    };

    this.mqttClient.publishTopic(topic, msg);
  }

  processPacket(packet) {
    let message = {
      droneId: packet.registration,
      lat: packet.location.coordinates[1].toFixed(6),
      lng: packet.location.coordinates[0].toFixed(6),
      pitch: packet.pitch,
      roll: packet.roll,
      airspeed: packet.air_speed_track_kn.toFixed(3),
      climb: packet.climbrate,
      alt: packet.altitude_gps.altitude_value.toFixed(3),
      heading: packet.heading,
      mode: packet.mode,
      isArmed: packet.armed,
      armedMode: packet.armed ? 'ARMED' : 'DISARMED',
      battery: packet.battery_remaining,
      tracktruenorthdeg: packet.track_true_north_deg,
      gufi: packet.gufi,
    };

    this.events.publish('ex-vehicle-new-packet', {
      droneId: packet.registration,
      message: message,
      telemetry: packet
    });
  }

  resetOperation(gufi) {
    this.mqttClient.unsubscribeTopic('operation/' + this.gufi + '/telemetry')
  }
}
