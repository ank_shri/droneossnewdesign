import {inject} from "aurelia-framework";
import {AureliaConfiguration} from "aurelia-configuration";
import {EventAggregator} from "aurelia-event-aggregator";
import {Router} from 'aurelia-router';
import {OperationService} from 'operations/operationService';
import {AnraNotification} from 'components/anra-notification';
import {AnraMqttClient} from 'components/mqttClient';
import {OperationNotification} from 'operations/operation-notification';
import {LunOperations} from 'lun-operations/lun-operations';
import {UIDialog, UIDialogService, UIUtils} from 'aurelia-ui-framework';
import {Conflicts} from "conflicts/conflicts";

@inject(Router, EventAggregator, AureliaConfiguration, OperationService, AnraNotification, AnraMqttClient, OperationNotification, UIDialogService)
export class OperationPanel {

  constructor(router, events, config, operationService, notification, anraMqttClient, operationNotification, dlgService) {
    this.events = events;
    this.config = config;
    this.router = router;
    this.operationService = operationService;
    this.notification = notification;
    this.mqttClient = anraMqttClient;
    this.isHubConnected = false;
    this.topicPrefix = this.config.get('topicPrefix');
    this.operationNotification = operationNotification;
    this.previousOperationState = undefined;
    this.dlgService = dlgService;
    this.audio = new Audio('audio/alert.mp3');
    this.operationConflicts = [];
  }

  activate(model) {

    this.drone = model.drone;
    this.operation = model.operation;

    if (model.operation) {

      this.previousOperationState = this.operation.state;
      this.subscribeEvents();
    }
  }

  activateOperation() {
    this.operationService
      .activateOperation(this.operation.gufi)
      .then(result => {

        if (result.http_status_code == 200) {
          this.notification.ShowInfo(result.message);
          this.getOperation();
        }
        else {
          this.notification.ShowError(result.message);
        }
        console.log('Activate Operation Response :', result);
      })
      .catch(e => {
        console.error(e);
      });
  }

  closeOperation() {
    this.operationService
      .closeOperation(this.operation.gufi)
      .then(result => {

        if (result.http_status_code == 200) {
          this.notification.ShowInfo(result.message);
          this.getOperation();
        }
        else {
          this.notification.ShowError(result.message);
        }
      })
      .catch(e => {
        console.error(e);
      });
  }

  getOperation() {
    this.operationService
      .getOperationDetails(this.operation.gufi)
      .then(result => {
        this.operation = result;
      });
  }

  canActivateOperation() {
    return this.operation && this.operation.state.toLowerCase() != 'accepted';
  }

  canCancelOperation() {
    return this.operation && (this.operation.state.toLowerCase() == 'closed');
  }

  subscribeEvents() {
    this.mqttClient.subscribeTopic(this.operation.user_id + '/operation');
    this.events.subscribe(this.operation.user_id + '/operation', p => {
      if (p.packet.Gufi == this.operation.gufi) {
        this.processPacket(p.packet);

        if (p.packet && Array.isArray(p.packet.Message)) {
          this.notification.ShowInfo("CONFLICT");
        }
        else {
          this.notification.ShowInfo(p.packet.Message);
        }        
      }
    });

    this.mqttClient.subscribeTopic('negotiation/' + this.operation.gufi);
    this.events.subscribe('negotiation/' + this.operation.gufi, p => {      
      this.notification.ShowInfo(p.packet.type);
    });

    this.events.subscribe('conflictedoperations', p => {      
      this.operationConflicts = p.operations;
      console.log('Flight::Conflicted Operation Details:', this.operationConflicts);
    });    
  }

  processPacket(packet) {
    this.audio.play();

    //Update Operation State
    this.operation.state = packet.Status;

    if (this.previousOperationState != this.operation.state) {
      this.previousOperationState = this.operation.state;
      this.operationNotification.NotifyOperationState(this.operation.state);
    }

    //If Conflict Then Only Draw Conflict Geography
    if (packet && Array.isArray(packet.Message)) {
        this.events.publish('drawconflictedoperations', { 'operations': packet.Message });
    }    
  }


  showLunOperations() {
    this.dlgService.show(LunOperations, {
      modal: this.operation,
      modelView: this,
    });
  }

  showConflicts(operation) {
    this.dlgService.show(Conflicts, {
      conflicts: this.operationConflicts,
      operation: operation
    });
  }
}
