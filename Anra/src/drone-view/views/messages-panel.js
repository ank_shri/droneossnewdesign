import 'au-table';

import { UIApplication, UIDialogService } from 'aurelia-ui-framework';
import { bindable, customElement, inject, observable } from 'aurelia-framework';

import { AnraNotification } from 'components/anra-notification';
import { EventAggregator } from 'aurelia-event-aggregator';
import { LookupHelper } from 'common/lookup-helper';
import { OperationService } from 'operations/operationService';
import { UssMessage } from 'uss-messages/uss-message';
import { UssMessageService } from 'uss-messages/ussMessageService';
import { AnraMqttClient } from 'components/mqttClient';
import { AureliaConfiguration } from "aurelia-configuration";
import moment from 'moment';

@inject(
  EventAggregator,
  UIApplication,
  OperationService,
  AnraNotification,
  UssMessageService,
  UIDialogService,
  AnraMqttClient,
  AureliaConfiguration
)
export class MessagesPanel {
  messages = [];
  pageSize = 15;

  constructor(events,
    application,
    operationService,
    notification,
    ussMessageService,
    dlgService,
    anraMqttClient,
    config) {
    this.events = events;
    this.config = config;
    this.subscriptions = [];
    this.application = application;
    this.dlgService = dlgService;
    this.operationService = operationService;
    this.notification = notification;
    this.ussMessageService = ussMessageService;
    this.mqttClient = anraMqttClient;
    this.topicPrefix = this.config.get('topicPrefix');
    this.audio = new Audio('audio/alert.mp3');
  }

  activate(model) {
    this.operation = model.operation;
    this.getListing();
  }

  attached() {
    this.subscribeEvents();
  }

  getListing() {
    this.messages = [];

    this.ussMessageService
      .getListing(this.operation.gufi)
      .then(result => {
        result.map(x => {
          let message = this.adaptToUtmModelFormat(x);
          this.messages.push(message);
        });
      });
  }

  adaptToUtmModelFormat(x) {
    let message = {};
    message.origin = x.Origin;
    message.originator_id = x.OriginatorId;
    message.originator_uss_instance_id = x.OriginatorUssInstanceId;
    message.severity = x.Severity;
    message.message_type = x.MessageType;
    message.free_text = x.FreeText;
    message.prev_message_id = x.PrevMessageId;
    message.gufi = x.Gufi;
    message.sent_time = moment.utc(x.SentTime);
    message.contingency = x.Contingency;
    message.last_known_position = x.LastKnownPosition;

    return message;
  }

  onAdd() {
    this.dlgService.show(UssMessage, {
      modal: this.operation,
      message: null,
      modelView: this,
      saveCallback: this.getListing
    });
  }

  subscribeEvents() {
    this.mqttClient.subscribeTopic('UtmMessages');
    let w1 = this.events.subscribe("UtmMessages", p => {
      this.processPacket(p.packet);
    });
    this.subscriptions.push(w1);

    let w2 = this.events.subscribe('refresh-utm-messages', p => {
      if (p.gufi == this.operation.gufi) {
        this.getListing();
      }
    });
    this.subscriptions.push(w2);
    //getting conflicted opertion from flight js to render on ui.
    let w3 = this.events.subscribe("conflictedoperations", p => {
      p.operations.map(operation => {
        operation.isVisible = true;
      })
      this.conflictedOperation = p.operations;
    });
    this.subscriptions.push(w3);
  }

  processPacket(packet) {
    console.log('Utm Message:', packet);
    this.audio.play();

    /*var message = {
      origin: packet.origin,
      originator_id: packet.originator_id,
      originator_uss_instance_id: packet.originator_uss_instance_id,
      severity: packet.severity,
      message_type: packet.message_type,
      free_text: packet.free_text,
      prev_message_id: packet.prev_message_id,
      gufi: packet.gufi,
      sent_time: moment.utc(packet.sent_time).utcOffset(moment().utcOffset()).format('L LT'),
      contingency: packet.contingency,
      last_known_position: packet.last_known_position
    };*/

    let message = this.adaptToUtmModelFormat(packet);
    this.messages.push(message);

    this.messages.sort((d1, d2) => new Date(d1.sent_time) - new Date(d2.sent_time));
  }

  detached() {
    this.subscriptions.map(x => x.dispose());
  }
  // function on click of show hide conflict button.
  onConflictsChange(data) {
    this.events.publish('showHideConflictLayer', { 'layer': data });
  }
}
