import {AureliaConfiguration} from 'aurelia-configuration';
import {DroneService} from 'drones/droneService';
import {EventAggregator} from 'aurelia-event-aggregator';
import {OperationService} from 'operations/operationService';
import 'Leaflet.vector-markers';
import {inject} from 'aurelia-framework';

@inject(EventAggregator, AureliaConfiguration, OperationService, DroneService)
export class DroneView {
  constructor(events, config, operationService, droneService) {
    this.isReady = false;
    this.events = events;
    this.config = config;

    this.operationService = operationService;
    this.droneService = droneService;

    this.operation = null;
    this.drone = null;
  }

  activate(params) {

    if (params.id) {
      this.gufi = params.id;
      this.getOperation();
    }
  }

  getOperation() {
    this.isWaiting = true;
    this.operationService.getOperationDetails(this.gufi).then(result => {
      this.operation = result;
      this.droneId = this.operation.uas_registrations[0].registration_id;
      this.initDrone();
    })
  }

  initDrone() {
    this.droneService
      .getDrone(this.droneId)
      .then(result => this.drone = result)
      .catch(e => console.error(e.message))
      .finally(() => {
        this.isReady = true;
        this.isWaiting = false;
      });
  }
}
