import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class WaypointService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
  }

  getWaypoints(missionId) {
    return this.http.fetch(this.baseApi + 'waypoints/'+missionId)
      .then(response => response.json());
  }

  saveWaypoints(waypointListing) {
    return this.http.fetch(this.baseApi + 'waypoints/', {
      method: 'post',
      body: json(waypointListing)
    }).then(response => response.json());
  }

}
