import { inject, BindingEngine } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { Router } from 'aurelia-router';
import { MissionService } from 'missions/missionService';
import { DroneService } from 'drones/droneService';
import { WaypointService } from 'waypoints/waypointService';
import { HttpClient } from 'aurelia-http-client';
import {AnraNotification} from "../components/anra-notification";
import { AureliaConfiguration } from "aurelia-configuration";
import { GeoSearchProvider } from 'components/geo-search-provider';

import * as L from 'leaflet';
import * as D from 'leaflet-draw';
import * as T from 'SpatialServer/Leaflet.MapboxVectorTile';
import 'smeijer/leaflet-geosearch';

const ONE_METER_OFFSET = 0.00000899322;
const ONE_METER_OFFSET_Lat = 0.00000899322;
const ONE_METER_OFFSET_LONG = 0.00000999322;
const PIE = 3.14159;

@inject(BindingEngine, HttpClient, EventAggregator, Router, MissionService, DroneService, WaypointService, AnraNotification, GeoSearchProvider, AureliaConfiguration)

export class Waypoints {
  constructor(bindingEngine, httpClient, events, router, missionService, droneService, waypointService, notification, geoSearchProvider, configure) {
    this.events = events;
    this.router = router;
    this.httpClient = httpClient;
    this.bindingEngine = bindingEngine;
    this.configuration = configure;
    this.geoSearchProvider = geoSearchProvider;
    this.missionService = missionService;
    this.droneService = droneService;
    this.waypointService = waypointService;
    this.notification = notification;
    this.mission = null;

    this.selectedTab = 'menu1';
    this.isReady = false;
    this.isWaiting = false;
    this.isLoading = true;
    this.wayPoints = [];
    this.zoomLevel = 18;
    this.lng = '';
    this.lat = '';
    this.controlledSpaceLayer = ['airports_recreational', 'class_b'];
    this.advisories = [];
    this.projectBoundaries = [];
    this.completedMissionWaypoints = [];
    this.inCompleteMissionWaypoints = [];
    this.advisoryCls = 'advisory-default';
    this.droneName = '';
    this.brandName = '';
    this.facadeAllPoints = [];
    this.lastFacadeMissionHeight = 0;
    this.facadeWayPoints = [];
  }

  activate(params) {
    if (params) {
      this.gufi = params.id;
    }
  }

  attached() {
    this.subscribeEvents();
    this.getDetails();
  }

  initMap() {
    this.isWaiting = true;
    let accessToken = this.configuration.get('mapboxAccessToken');

    var streetMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=' + accessToken, {
      attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
    });

    var satelliteMap = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
      attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
    });

    var sectionalMap = L.tileLayer('https://wms.chartbundle.com/tms/v1.0/sec/{z}/{x}/{y}.png?type=google', {
      attribution: '© <a href="https://www.chartbundle.com/">Chartbundle</a>'
    });

    let streetDarkMap = L.tileLayer(
      'https://api.mapbox.com/styles/v1/aganjoo/cjm2gw5p406qa2snwya18clid/tiles/256/{z}/{x}/{y}?access_token=' +
      accessToken, {
        attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
      }
    );


    this.airSpaceLayerGroup = new L.LayerGroup();

    this.baseMaps = {
      "Satellites": satelliteMap,
      "Streets": streetMap,
      "VFR Sectional": sectionalMap,
      "Street Dark":streetDarkMap
    };

    this.overlayMaps = {
      "airspace": this.airSpaceLayerGroup
    }

    let map = L.map('map', {
      zoomControl: false,
      drawControl: false,
      editable: false,
      maxNativeZoom: 22,
      layers: [streetDarkMap]
    })
      .setView(new L.LatLng(this.lat, this.lng), this.zoomLevel);

    L.control.zoom({
      maxZoom: 22,
      position: 'topleft'
    }).addTo(map);

    new L.Control.GeoSearch({
      provider: this.geoSearchProvider,
      showMarker: true,
      retainZoomLevel: false,
      draggable: false,
      searchLabel: "search for address.."
    }).addTo(map);

    map.doubleClickZoom.disable();

    L.Icon.Default.imagePath = 'styles/images';

    map.on('locationfound', e => {
      this.lat = e.latitude;
      this.lng = e.longitude;
      this.map.setView(new L.LatLng(this.lat, this.lng, 18));
    });

    this.map = map;
    this.isWaiting = false;
    this.attachDrawControls();
    this.drawMissionFlightPath();
    this.updateAirSpace();
    this.isLoading = false;
  }

  setMapView() {

    if (this.projectBoundaries.length > 0)
      return;

    if (this.mission != null && this.mission.PointOfInterestLat && this.mission.PointOfInterestLng && this.mission.MissionType !== 6) {
      this.map.setView(new L.LatLng(this.mission.PointOfInterestLat, this.mission.PointOfInterestLng), 18);
    }
    else {
      this.map.setView(new L.LatLng(this.lat, this.lng), 18);
    }
  }

  getProjectMissionWaypoints() {

    if (this.mission && this.mission.ProjectGeography) {
      this.projectBoundaries = JSON.parse(this.mission.ProjectGeography).map(x => new L.LatLng(x.Latitude, x.Longitude));
    }

    this.missionService.getMissionByProjId(this.mission.ProjectId)
      .then(result => {
        this.missions = result;
        var self = this;
        this.missions.forEach(function (mission) {
          if (!mission.FlightGeography || self.gufi == mission.Gufi)
            return;
          if (mission.StatusId == 3) {
            var cmission = JSON.parse(mission.FlightGeography).map(x => new L.LatLng(x.Latitude, x.Longitude));
            self.completedMissionWaypoints.push.apply(self.completedMissionWaypoints, cmission);
            return
          }

          var incompmission = JSON.parse(mission.FlightGeography).map(x => new L.LatLng(x.Latitude, x.Longitude));
          self.inCompleteMissionWaypoints.push.apply(self.inCompleteMissionWaypoints, incompmission);
        });

        this.initMap();

      })
      .catch((e) => {
        console.error(e);
      })

  }

getDetails() {    
    this.missionService.getMissionRefactor(this.gufi)
        .then(result => {
            this.mission = result.Mission;
            return this.droneService.getDrone(this.mission.DroneId);
        })
        .then(result => {
            this.drone = result;
            this.droneName = this.drone.Title;
            this.brandName = this.drone.BrandName;
            this.lng = this.drone.BaseLng;
            this.lat = this.drone.BaseLat;
            this.isReady = true; 
            this.initMap();
    }); 
  }

  subscribeEvents() {
    this.events.subscribe('air-space-update', p => {
      this.controlledSpaceLayer = p.controlledSpace;
      this.updateAirSpace();
    });

    this.events.subscribe('point-location-update', p => {
      if (this.mission.MissionType == 3 || this.mission.MissionType == 4 || this.mission.MissionType == 8) {
        console.log('tower', this.towerMarker)

        if (this.mission.MissionType == 4) {
          this.updateTornadoTowerPath();
        }
        else if (this.mission.MissionType == 3 || this.mission.MissionType == 8) {
          this.updateHexagonalTowerPath();
        }
      }
    });
  }

  save() {
    let waypointObject = {
      "MissionId": this.mission.MissionId,
      "Waypoints": this.wayPoints
    };

    return this.waypointService.saveWaypoints(waypointObject)
      .then(() => {
        this.notification.ShowShowInfo("Waypoints saved");
        this.router.navigate('missions/missions');
      });
  }

  cancel() {
    this.router.navigate('missions/missions');
  }

  updateAirSpace() {
    this.isWaiting = true;
    var airmap_key = this.configuration.get('airmapKey');
    var layerString = this.controlledSpaceLayer.toString().split(',');

    if (this.mvtSource) {
      this.airSpaceLayerGroup.clearLayers();
      this.map.removeLayer(this.mvtSource);
    }

    var self = this;
    this.mvtSource = new L.TileLayer.MVTSource({
      url: 'https://api.airmap.io/maps/v4/tiles' + '/' + layerString + "/{z}/{x}/{y}.pbf?apikey=" + airmap_key + '&theme=satellite-streets-v9',
      clickableLayers: this.controlledSpaceLayer,
      getIDForLayerFeature: function (feature) {
        return Math.random().toString(36).substr(2, 9);
      },
      style: (feature) => {
        var layer = feature.properties.type;
        return this.getStyle(layer);
      },
      onClick: function (evt) {
        if (evt.feature) {

          let props = Object.entries(evt.feature.properties);
          var names = [];
          props.forEach(x => {
            if (x[0] == 'type') {
              let val = x[1].toUpperCase().replace('_', ' ');
              let dispProp = {
                displayOrder: 1,
                prop: '<div><strong>' + x[0].toUpperCase() + '</strong>: ' + val + '</div>'
              };
              names.push(dispProp);
            }
            else if (x[0] == 'url') {
              let val = x[1]
              let dispProp = {
                displayOrder: 1,
                prop: '<div><strong>' + x[0].toUpperCase() + '</strong>: ' + '<a href=' + val + '>' + 'view more details' + '</a>' + '</div>'
              };
              names.push(dispProp);
            }
            else {
              let dispProp = {
                displayOrder: names.length + 2,
                prop: '<div><strong>' + x[0].toUpperCase() + '</strong>: ' + x[1] + '</div>'
              };
              names.push(dispProp);
            }
          })

          let content = names.sort(x => x.displayOrder).map(x => x.prop).join('');

          let popup = L.popup()
            .setLatLng(evt.latlng)
            .setContent(content)
            .openOn(self.map);
        }
      }
    });

    this.map.on("layerremove", function (removed) {
      if (removed.layer.removeChildLayers) {
        removed.layer.removeChildLayers(self.map);
      }
    });

    this.airSpaceLayerGroup.addLayer(this.mvtSource).addTo(this.map);
    this.isWaiting = false;
  }

  getStyle(layer) {
    var style = {};
    switch (layer) {
      case 'airports_recreational':
      case 'airports_recreational_private':
      case 'airports_commercial':
      case 'airports_commercial_private':
      case 'hospitals':
      case 'heliports':
      case 'power_plants':
      case 'schools':
        style.color = 'rgba(246, 165, 23, 0.2)';
        style.outline = {
          color: 'rgb(246, 165, 23)',
          size: 0
        }
        break;
      case 'national_parks':
      case 'noaa':
        style.color = 'rgba(224, 18, 18, 0.2)';
        style.outline = {
          color: 'rgb(224, 18, 18)',
          size: 0
        }
        break;
      case 'sua_restricted':
      case 'sua_prohibited':
        style.color = 'rgba(27, 90, 207, 0.2)';
        style.outline = {
          color: 'rgb(27, 90, 207)',
          size: 0
        }
        break;
      case 'class_b':
        style.color = 'rgba(26, 116, 179, 0.2)';
        style.outline = {
          color: 'rgb(26, 116, 179)',
          size: 0
        }
        break;
      case 'class_c':
        style.color = 'rgba(155, 108, 157, 0.3)';
        style.outline = {
          color: 'rgb(155, 108, 157)',
          size: 0
        }
        break;
      case 'class_d':
        style.color = 'rgba(26, 116, 179, 0.2)';
        style.outline = {
          color: 'rgb(26, 116, 179)',
          size: 0
        }
        break;
      case 'class_e0':
        style.color = 'rgba(155, 108, 157, 0.2)';
        style.outline = {
          color: 'rgb(155, 108, 157)',
          size: 0
        }
        break;
      case 'tfrs':
        style.color = 'rgba(224, 18, 18, 0.2)';
        style.outline = {
          color: 'rgb(224, 18, 18)',
          size: 0
        }
        break;
    }

    return style;
  }

  attachDrawControls() {
    this.drawnItems = new L.FeatureGroup();
    this.map.addLayer(this.drawnItems);

    var drawControl = new L.Control.Draw({
      position: 'topleft',
      draw: {
        polygon: this.mission.MissionType == 2,
        polyline: this.mission.MissionType == 1 || this.mission.MissionType == 5,
        rectangle: false,
        circle: false,
        edit: false,
        marker: this.mission.MissionType == 3 || this.mission.MissionType == 4 || this.mission.MissionType == 6 || this.mission.MissionType == 8 || this.mission.MissionType == 10,
        editable: false
      },
      edit: {
        featureGroup: this.drawnItems,
        edit: false,
        remove: this.mission.MissionType == 1 || this.mission.MissionType == 2 || this.mission.MissionType == 5
      }
    });

    this.map.on('draw:edited', (event) => {
      this.addOrUpdateLayer(event, true);
      this.getAirspaceAdvisories();
    });

    this.map.on('draw:created', (event) => {
      this.events.publish('open', { expand: true });
      this.addOrUpdateLayer(event);
      this.getAirspaceAdvisories();
    });

    this.map.on('draw:deletestop', (event) => {
      if (this.mission.FlightGeography) {
        this.mission.FlightGeography = null;
        this.saveItem();
      }
    });

    this.map.addControl(drawControl);
  }

  drawMissionFlightPath() {
    if (this.mission.FlightGeography) {
      let flightPath = JSON.parse(this.mission.FlightGeography);
      let pointList = flightPath.map(x => new L.LatLng(x.Latitude, x.Longitude, x.Altitude));
      if (this.mission.MissionType == 1 || this.mission.MissionType == 2 || this.mission.MissionType == 5 || this.mission.MissionType == 10) {
        this.polyline = new L.polyline(pointList, { color: '#3388ff', weight: 3, opacity: 0.5, smoothFactor: 1 });
        this.polyline.addTo(this.drawnItems);
      }
      else if (this.mission.MissionType == 4) {
        this.updateTornadoTowerPath();
      }
      else if (this.mission.MissionType == 3 || this.mission.MissionType == 8) {
        this.updateHexagonalTowerPath();
      }
      else if (this.mission.MissionType == 6) {
        let flightPath = JSON.parse(this.mission.FlightGeography);
        let guideWirePoints = [];
        flightPath.map(x => guideWirePoints.push(new L.LatLng(x.Latitude, x.Longitude)));
        this.updateGuideWirePath(guideWirePoints);
      }

      this.map.setView(new L.LatLng(pointList[0].lat, pointList[0].lng), 18);
    }
  }

  addOrUpdateLayer(event, isUpdate = false) {    
    if (!isUpdate && this.mission.MissionType !== 6 && this.mission.MissionType !== 10) {
      this.clearDrawnItems();
      if (event.layerType !== 'marker') {
        this.drawnItems.addLayer(event.layer);
      }
    }

    if (!isUpdate && this.mission.MissionType == 6) {
      if (!this.guideWirePoints || this.guideWirePoints.length == 2) {
        this.clearDrawnItems();
        this.guideWirePoints = [];
      }

      this.guideWirePoints.push(event.layer._latlng);
      this.drawnItems.addLayer(event.layer);

      if (this.guideWirePoints.length == 2) {
        this.clearDrawnItems();
        this.updateGuideWirePath(this.guideWirePoints);
      }
    }

    //Facade Mission New Implementation
    if (!isUpdate && this.mission.MissionType == 10) {
      if (!this.facadePoints) {
        this.clearDrawnItems();
        this.facadePoints = [];
        this.facadeAllPoints = [];
      }


      this.facadePoints.push(event.layer._latlng);
      this.facadeAllPoints.push(event.layer._latlng);
      this.drawnItems.addLayer(event.layer);

      //Check If Facade Wall is Valid Or not      
      if (this.facadeAllPoints.length >= 2 && this.facadeAllPoints.length <=5) {

        if (this.isValidFacadeWall()) {
          this.facadePoints = [];
          this.updateFacadeWallLine(this.facadeAllPoints);
          this.updatePathFromFacade(this.facadeAllPoints);
          
        }
        else {
          this.notification.ShowInfo("Invalid Facade Wall");
          this.drawnItems.removeLayer(event.layer);
          this.facadeAllPoints.pop(event.layer._latlng);
        }                
      }

      if (this.facadeAllPoints.length > 5) {
        this.notification.ShowInfo("Maximum 4 wall is allowed.");
        this.drawnItems.removeLayer(event.layer);
        this.facadeAllPoints.pop(event.layer._latlng);
      }
    }  

    if (this.mission.MissionType == 1) {
      var json = this.drawnItems.toGeoJSON();
      this.clearDrawnItems();
      this.updatePathFromLineString(json.features[json.features.length - 1].geometry.coordinates);
    }
    else if (this.mission.MissionType == 2) {
      var json = this.drawnItems.toGeoJSON();
      this.clearDrawnItems();
      this.updatePathFromPolygon(json.features[json.features.length - 1].geometry.coordinates[0]);
    }
    else if (this.mission.MissionType == 4) {
      this.mission.PointOfInterestLng = event.layer._latlng.lng;
      this.mission.PointOfInterestLat = event.layer._latlng.lat;
      this.updateTornadoTowerPath();
    }
    else if (this.mission.MissionType == 3 || this.mission.MissionType == 8) {
      this.mission.PointOfInterestLng = event.layer._latlng.lng;
      this.mission.PointOfInterestLat = event.layer._latlng.lat;
      this.updateHexagonalTowerPath();
    }
    else if (this.mission.MissionType == 2 || this.mission.MissionType == 5) {
      var json = this.drawnItems.toGeoJSON();
      this.clearDrawnItems();
      this.updatePathFromPolygon(json.features[json.features.length - 1].geometry.coordinates); //ToDo# - Need to merge it with free hand mission i.e 2
    }

  }

  updateGuideWirePath(coordinates) {
    this.clearDrawnItems();
    let waypoints = [];

    if (coordinates.length > 0) {

      coordinates.forEach(item => {

        let waypoint = {
          "Latitude": item.lat,
          "Longitude": item.lng,
          "Altitude": waypoints.length == 0 ? this.mission.Altitude : this.mission.TowerHeight,
          "WaypontHeadingType": 1,
          "WaypontActionType": 0,
          "WaypontActionParameter": 0,
          "StartTakePhoto": false
        };
        waypoints.push(waypoint);
      });

      this.mission.FlightGeography = JSON.stringify(waypoints);

      this.polyline = new L.polyline(coordinates, { color: 'red', weight: 3, opacity: 0.5, smoothFactor: 1 });
      this.polyline.addTo(this.drawnItems);
    }

  }

  clearDrawnItems() {
    this.drawnItems.getLayers().map(layer => this.drawnItems.removeLayer(layer));
  }

  onTabSelect(selectedTab) {
    this.selectedTab = selectedTab;
  }

  saveItem() {
    this.isLoading = true;
    return this.missionService.saveMission(this.mission)
      .then(result => {
        return this.handleSaveResult(result);
      })
      .catch((e) => {
        console.error(e);
        this.notification.error(e);
      });
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.isLoading = false;
      this.notification.ShowInfo("Mission Saved");
      this.events.publish('close');
      this.events.publish('item-operation', { refresh: true });
    }
    else {
      this.isLoading = false;
      this.notification.error(result.ErrorMessage);
    }
  }

  updatePathFromLineString(coordinates) {
    let waypoints = [];
    if (coordinates.length > 0) {
      coordinates.forEach(item => {

        let waypoint = {
          "Latitude": item[1],
          "Longitude": item[0],
          "Altitude": this.mission.Altitude,
          "WaypontHeadingType": 0,
          "WaypontActionType": 0,
          "WaypontActionParameter": 0,
          "StartTakePhoto": false
        };
        waypoints.push(waypoint);
      });

      this.mission.FlightGeography = JSON.stringify(waypoints);
      this.drawMissionFlightPath();
    }
  }

  updatePathFromPolygon(coordinates) {
    let waypoints = [];
    if (coordinates.length > 0) {
      coordinates.forEach(item => {

        let waypoint = {
          "Lat": item[1],
          "Lng": item[0],
          "Alt": 0
        };

        waypoints.push(waypoint);
      });

      var missionData = {
        "MissionId": this.mission.MissionId,
        "Speed": this.mission.Speed,
        "HatchAngle": this.mission.HatchAngle,
        "SideLapAngle": this.mission.SideLapAngle,
        "Points": waypoints,
        "Altitude": this.mission.Altitude
      }

      this.missionService.getMissionWaypoints(missionData)
        .then(result => {
          var flightPath = this.getWaypointlist(result);
          this.mission.FlightGeography = JSON.stringify(flightPath);
          this.getAirspaceAdvisories();
          this.drawMissionFlightPath();
        });
    }
  }

  updatePathFromFacade(waypoints) {

    //Pick Last 2 Point location to get the Facade Wall Points

    let point1 = {
      "Lat": waypoints[waypoints.length - 2].lat,
      "Lng": waypoints[waypoints.length - 2].lng,
      "Alt": 0
    };

    let point2 = {
      "Lat": waypoints[waypoints.length - 1].lat,
      "Lng": waypoints[waypoints.length - 1].lng,
      "Alt": 0
    };

    let facadeObject = {
      "Point1": point1,
      "Point2": point2,
      "ClearanceHeight": this.mission.ClearanceHeight,
      "SepBtwWallDrone": this.mission.DroneTowerDistance,
      "MissionSpeed": this.mission.Speed,
      "MissionHeight": this.mission.TowerHeight,
      "LastMissionHeight": this.lastFacadeMissionHeight
    };

    this.missionService.getFacadeMissionWaypoints(facadeObject)
      .then(result => {
        let flightPath = this.getFacadeWaypointlist(result);
        this.lastFacadeMissionHeight = flightPath[flightPath.length - 1].Altitude;
        this.mission.FlightGeography = JSON.stringify(this.facadeWayPoints);
        //this.getAirspaceAdvisories();
        this.drawMissionFlightPath();
      });
  }

  updateFacadeWallLine(coordinates) {
    if (coordinates.length > 0) {
      this.polyline = new L.polyline(coordinates, { color: 'red', weight: 3, opacity: 0.5, smoothFactor: 1 });
      this.polyline.addTo(this.drawnItems);
    }
  }

  // Below both methods can be optimized into one method.
  getFacadeWaypointlist(coordinates) {
    let waypoints = [];
    if (coordinates.length > 0) {
      coordinates.forEach(item => {
        let waypoint = {
          "Latitude": item.Lat,
          "Longitude": item.Lng,
          "Altitude": item.Alt
        };
        waypoints.push(waypoint);
        this.facadeWayPoints.push(waypoint);
      });
    }
    return waypoints;

  }

  getWaypointlist(coordinates) {
    let waypoints = [];
    if (coordinates.length > 0) {
      coordinates.forEach(function (item) {

        var lat = item.Lat
        var long = item.Lng

        let waypoint = {
          "Latitude": lat,
          "Longitude": long,
          "Altitude": 0,
          "WaypontHeadingType": 0,
          "WaypontActionType": 0,
          "WaypontActionParameter": 0,
          "StartTakePhoto": false
        };
        waypoints.push(waypoint);
      });
    }
    return waypoints;

  }

  pointsToString(points) {
    var str = "";
    for (var i = 0; i < points.length; i++) {
      str += "[" + points[i].Lat + "," + points[i].Lng + "]";
      if (i < (points.length - 1))
        str += ",";
    }

    return str;
  }

  devideLineSegment(startingPoint, endPoint, numberOfParts) {

    let startX = startingPoint.Longitude;
    let startY = startingPoint.Latitude;

    let endX = endPoint.Longitude;
    let endY = endPoint.Latitude;

    let distance = this.findDistanceBetween(startY, startX, endY, endX) / 100000;

    let angle = this.calcRotationAngleInDegrees(startingPoint, endPoint);

    let dividedPoints = [];
    let startinglineSegment = [];
    startinglineSegment.Longitude = startX;
    startinglineSegment.Latitude = startY;
    let endlineSegment = [];
    endlineSegment.Longitude = endX;
    endlineSegment.Latitude = endY;

    dividedPoints.push(startinglineSegment);
    for (let i = 0; i < numberOfParts - 1; i++) {
      let lineSegment = this.drawLineOnAngle(dividedPoints[i], 180 - angle, distance / numberOfParts);
      dividedPoints.push(lineSegment);
    }
    dividedPoints.push(endlineSegment);
    return dividedPoints;
  }

  drawLineOnAngle(startingPoint, angle, distance) {
    let segmentPoint = [];
    angle = angle * Math.PI / 180;
    let startX = startingPoint.Longitude;
    let startY = startingPoint.Latitude;
    let endX = startX + (distance + distance / 2) * Math.sin(angle);
    let endY = startY + (distance + distance / 2) * Math.cos(angle);

    segmentPoint.Longitude = endX;
    segmentPoint.Latitude = endY;
    return segmentPoint;
  }



  calcRotationAngleInDegrees(centerPt, targetPt) {
    let theta = Math.atan2(targetPt.Latitude - centerPt.Latitude, targetPt.Longitude - centerPt.Longitude);
    theta += Math.PI / 2;
    let angle = this.toDegrees(theta);
    if (angle < 0) {
      angle += 360;
    }
    return angle;
  }

  selectPathPoints(allPoints) {

    let minDistBetTwoHatchLines = this.mission.HatchLineSeparations;
    let selectedPointsStart = [];
    let selectedPointsEnd = [];
    let selectedInterPointDistanceStart = [];
    let selectedInterPointDistanceEnd = [];

    let pathToFollow = [];

    let totalSize = allPoints.length;
    let halfSize = allPoints.length / 2;

    for (let i = 0; i < halfSize; i++) {
      selectedPointsStart.push(allPoints[i]);
      selectedPointsEnd.push(allPoints[totalSize - i - 1]);
    }

    selectedPointsStart = this.isTooClose(selectedPointsStart, minDistBetTwoHatchLines, 0.5);
    selectedPointsEnd = this.isTooClose(selectedPointsEnd, minDistBetTwoHatchLines, 0.5);

    let minSize = 0;
    if (selectedPointsStart.length > selectedPointsEnd.length) {
      minSize = selectedPointsEnd.length;
      console.log("Size" + selectedPointsEnd.length + "  selectedPointsEnd");
    } else {
      minSize = selectedPointsStart.length;
      console.log("Size" + selectedPointsStart.length + "  selectedPointsStart");
    }

    for (let i = 0; i < minSize - 1; i++) {
      if (i < minSize) {
        selectedInterPointDistanceEnd.push(this.findDistanceBetween(selectedPointsStart[i].Latitude, selectedPointsStart[i].Longitude, selectedPointsStart[i + 1].Latitude, selectedPointsStart[i + 1].Longitude) * 1000);
        selectedInterPointDistanceStart.push(this.findDistanceBetween(selectedPointsEnd[i].Latitude, selectedPointsEnd[i].Longitude, selectedPointsEnd[i + 1].Latitude, selectedPointsEnd[i + 1].Longitude) * 1000);
      }

      pathToFollow.push(selectedPointsStart[i]);
      pathToFollow.push(selectedPointsEnd[i]);

      if ((i & 1) == 0) {
        pathToFollow.push(selectedPointsEnd[i]);
        pathToFollow.push(selectedPointsEnd[i + 1]);
      } else {
        pathToFollow.push(selectedPointsStart[i]);
        pathToFollow.push(selectedPointsStart[i + 1]);
      }
    }
    return pathToFollow;
  }

  isTooClose(allPoints, offSetDistance, errorDistance) {
    let pointsToRemove = [];
    let selectedInterPointDistance = [];

    for (let i = 0; i < allPoints.length - 2; i++) {
      selectedInterPointDistance.push(this.findDistanceBetween(allPoints[i].Latitude, allPoints[i].Longitude, allPoints[i + 1].Latitude, allPoints[i + 1].Longitude));
    }

    for (let i = 0; i < selectedInterPointDistance.length - 2; i++) {
      if (selectedInterPointDistance[i] < (offSetDistance + errorDistance) && selectedInterPointDistance[i + 1] < (offSetDistance + errorDistance) && selectedInterPointDistance[i + 2] < (offSetDistance + errorDistance)) {
        pointsToRemove.add(i + 1);
      }
    }

    pointsToRemove.map(x => allPoints.splice(x, 1));
    return allPoints;
  }

  findDistanceBetween(lat1, lon1, lat2, lon2) {
    let theta = lon1 - lon2;
    let dist = Math.sin(this.toRadians(lat1)) * Math.sin(this.toRadians(lat2)) + Math.cos(this.toRadians(lat1)) * Math.cos(this.toRadians(lat2)) * Math.cos(this.toRadians(theta));
    dist = Math.acos(dist);
    dist = this.toDegrees(dist);
    dist = dist * 60 * 1.1515;
    return (dist * 1000); // multiply by 1000 for distance in meters
  }

  toDegrees = function (rad) {
    return rad * (180 / Math.PI);
  }

  toRadians = function (deg) {
    return deg * (Math.PI / 180);
  }

  getPointOfInterestIcon() {
    let imgUrl = this.mission.MissionType == 4 ? 'image/ic_tower_top.png' : 'image/ic_tower_top.png';
    return L.icon({
      iconUrl: imgUrl,
      iconSize: [30, 30],
      iconAnchor: [15, 15],
      popupAnchor: [-3, -4]
    });
  }

  removeTowerIcon() {
    if (this.towerMarker) {
      this.drawnItems.getLayers().map(layer => this.drawnItems.removeLayer(layer));
    }
  }

  updateSpiralTowerPath() {
    this.removeTowerIcon();
    this.setMapView();

    let radius = (this.mission.DroneTowerDistance * 0.3048);
    let clearanceHeight = (this.mission.ClearanceHeight * 0.3048);
    let stepIncrease = ((this.mission.TowerHeight * 0.3048) / this.mission.InspectionDivisions);


    let hexagonPoints = [];
    let theta = 2 * Math.PI / 6;
    let center = new L.LatLng(this.mission.PointOfInterestLat, this.mission.PointOfInterestLng);

    this.towerMarker = new L.Marker(center, { 'icon': this.getPointOfInterestIcon() }).bindPopup('Tower').openPopup();
    this.towerMarker.addTo(this.drawnItems);

    for (let i = 0; i < 6; ++i) {
      let latLng = new L.LatLng(center.lat + ONE_METER_OFFSET_Lat * radius * Math.cos(theta * i), center.lng + ONE_METER_OFFSET_LONG * radius * Math.sin(theta * i));
      hexagonPoints.push(latLng);

      L.circleMarker(latLng, {
        color: 'white',
        fillColor: 'cyan',
        fillOpacity: 0.5
      })
        .addTo(this.drawnItems);

    }

    for (let i = 0; i < 6; ++i) {
      if (i >= 1) {
        L.polyline([hexagonPoints[i - 1], hexagonPoints[i]], { color: '#3388ff' }).addTo(this.drawnItems);
      } else {
        L.polyline([hexagonPoints[hexagonPoints.length - 1], hexagonPoints[0]], { color: '#3388ff' }).addTo(this.drawnItems);
      }
    }


    if (!this.mission.ClockwiseDirection) {
      hexagonPoints.reverse();
    }

    let missionWaypoints = [];

    for (let k = 0; k < hexagonPoints.length; k++) {
      for (let i = 0; i < this.mission.InspectionDivisions; i++) {
        missionWaypoints.push({
          "Latitude": hexagonPoints[k].lat,
          "Longitude": hexagonPoints[k].lng,
          "Altitude": (clearanceHeight + (stepIncrease * i)) * 3.2808399, //converting to feet
          "WaypontHeadingType": 4,
          "WaypontActionType": 0,
          "WaypontActionParameter": 0,
          "StartTakePhoto": false
        });
      }
    }

    this.mission.FlightGeography = JSON.stringify(missionWaypoints);
  }

  updateTornadoTowerPath() {

    this.removeTowerIcon();
    this.setMapView();

    let radius = (this.mission.DroneTowerDistance * 0.3048);
    let clearanceHeight = (this.mission.ClearanceHeight * 0.3048);
    let towerHeightWithIncreament = parseFloat(this.mission.TowerHeight) + 20;
    let stepIncrease = (((towerHeightWithIncreament * 0.3048) - clearanceHeight) / this.mission.InspectionDivisions);

    let missionWaypoints = [];
    let octagonPoints = [];
    let lastPointIndex = 0;

    let theta = 2 * Math.PI / 8;
    let center = new L.LatLng(this.mission.PointOfInterestLat, this.mission.PointOfInterestLng);

    this.towerMarker = new L.Marker(center, { 'icon': this.getPointOfInterestIcon() }).bindPopup('Tower').openPopup();
    this.towerMarker.addTo(this.drawnItems);

    for (let k = 0; k < this.mission.InspectionDivisions; k++) {

      for (let i = 0; i < 8; i++) {

        let increaseInRadius = radius * (k) / 4;
        let latitude = center.lat + ONE_METER_OFFSET_Lat * (radius + increaseInRadius) * Math.cos(theta * i);
        let longitude = center.lng + ONE_METER_OFFSET_LONG * (radius + increaseInRadius) * Math.sin(theta * i);
        let altitude = (clearanceHeight + (stepIncrease * k)) * 3.2808399;

        let latLng = new L.LatLng(latitude, longitude, altitude);
        octagonPoints.push(latLng);

        //Create Octagon Circle
        L.circleMarker(latLng, {
          color: 'white',
          fillColor: 'cyan',
          fillOpacity: 0.5
        }).addTo(this.drawnItems);

        //Create Polyline for Points created
        if (lastPointIndex > 0) {
          L.polyline([octagonPoints[lastPointIndex - 1], octagonPoints[lastPointIndex]], { color: '#3388ff' }).addTo(this.drawnItems);
        }

        lastPointIndex++;

        //Create Waypoint For Current Point 
        let wayPoint = {};
        let wayPointAction = {};

        wayPoint.Latitude = latitude;
        wayPoint.Longitude = longitude;
        wayPoint.Altitude = altitude;
        wayPoint.cornerRadiusInMeters = 1;
        wayPoint.action = [];

        wayPointAction.waypointactiontype = "GIMBAL_PITCH";
        wayPointAction.waypointactionvalue = -30;
        wayPoint.action.push(wayPointAction);

        if (this.mission.AutoCapture) {
          wayPointAction = {};
          wayPointAction.waypointactiontype = "START_TAKE_PHOTO";
          wayPointAction.waypointactionvalue = 0;
          wayPoint.action.push(wayPointAction);
        }

        //Finally Add current waypoint to Final Mission Waypoint List
        missionWaypoints.push(wayPoint);
      }
    }

    if (!this.mission.ClockwiseDirection) {
      missionWaypoints.reverse();
    }

    let wayPointAction1 = {};
    wayPointAction1.waypointactiontype = "GIMBAL_PITCH";
    wayPointAction1.waypointactionvalue = -45;

    let waypoint1 = {
      "Latitude": missionWaypoints[0].lat,
      "Longitude": missionWaypoints[0].lng,
      "Altitude": this.mission.TowerHeight + 1.5, //converting to feet         
      "action": wayPointAction1
    };
    missionWaypoints.splice(0, waypoint1);
    this.mission.FlightGeography = JSON.stringify(missionWaypoints);
  }

  updateHexagonalTowerPath() {
    this.removeTowerIcon();
    this.setMapView();

    let radius = (this.mission.DroneTowerDistance * 0.3048);
    let clearanceHeight = (this.mission.ClearanceHeight * 0.3048);
    let towerHeightWithIncreament = parseFloat(this.mission.TowerHeight) + 20;
    let stepIncrease = (((towerHeightWithIncreament * 0.3048) - clearanceHeight) / this.mission.InspectionDivisions);


    let hexagonPoints = [];
    let theta = 2 * Math.PI / 6;
    let center = new L.LatLng(this.mission.PointOfInterestLat, this.mission.PointOfInterestLng);

    this.towerMarker = new L.Marker(center, { 'icon': this.getPointOfInterestIcon() }).bindPopup('Tower').openPopup();
    this.towerMarker.addTo(this.drawnItems);

    for (let i = 0; i < 6; ++i) {
      let latLng = new L.LatLng(center.lat + ONE_METER_OFFSET_Lat * radius * Math.cos(theta * i), center.lng + ONE_METER_OFFSET_LONG * radius * Math.sin(theta * i));
      latLng.Altitude = clearanceHeight + (stepIncrease * i);
      hexagonPoints.push(latLng);

      L.circleMarker(latLng, {
        color: '#3388ff',
        fillColor: 'cyan',
        fillOpacity: 0.5
      })
        .addTo(this.drawnItems);

    }

    for (let i = 0; i < 6; ++i) {
      if (i >= 1) {
          L.polyline([hexagonPoints[i - 1], hexagonPoints[i]], { color: '#3388ff' }).addTo(this.drawnItems);
      } else {
          L.polyline([hexagonPoints[hexagonPoints.length - 1], hexagonPoints[0]], { color: '#3388ff' }).addTo(this.drawnItems);
      }
    }


    if (!this.mission.ClockwiseDirection) {
      hexagonPoints.reverse();
    }

    let missionWaypoints = [];
    let wayPointAction = {};
    let columnTransitionAtTop = this.mission.ColumnTransition;
    if (columnTransitionAtTop) {
        for (let k = 0; k < hexagonPoints.length; k++) {
            let listOfOneCoulumnPoints = [];
            for (let m = this.mission.InspectionDivisions - 1; m >= 0; m--) {


              var waypointHeight= (clearanceHeight + (stepIncrease * (m))) * 3.2808399;

              if(waypointHeight  > this.mission.altitude)
              {
                waypointHeight = this.mission.altitude;
              }


                wayPointAction = {};
                wayPointAction.waypointactiontype = "GIMBAL_PITCH";
                wayPointAction.waypointactionvalue = -45;

                let waypoint = {
                    "Latitude": hexagonPoints[k].lat,
                    "Longitude": hexagonPoints[k].lng,
                    "Altitude": waypointHeight + 20, //converting to feet
                    "WaypontHeadingType": 4,
                    "WaypontActionType": 0,
                    "WaypontActionParameter": 0,
                    "StartTakePhoto": false,
                    "action": wayPointAction
                };


                missionWaypoints.push(waypoint);
                listOfOneCoulumnPoints.push(waypoint);


            }




            if(listOfOneCoulumnPoints.length > 1)
            {

              var start = listOfOneCoulumnPoints.length - 2

              for (let pos = start; pos >= 0; pos--) {
                let waypointR = {
                      "Latitude": listOfOneCoulumnPoints[pos].Latitude,
                      "Longitude": listOfOneCoulumnPoints[pos].Longitude,
                      "Altitude": listOfOneCoulumnPoints[pos].Altitude, //converting to feet
                      "WaypontHeadingType": 4,
                      "WaypontActionType": 0,
                      "WaypontActionParameter": 0,
                      "StartTakePhoto": false,
                      "action": wayPointAction
                  };

                  missionWaypoints.push(waypointR);
              }
            }
            
        }
    }
    else
    {


        var waypointHeightF= (clearanceHeight + (stepIncrease * (this.mission.InspectionDivisions - 1))) * 3.2808399;

            


            let wayPointActionF = {};
            wayPointActionF.waypointactiontype = "GIMBAL_PITCH";
            wayPointActionF.waypointactionvalue = -45;

            let waypointF = {
                    "Latitude": hexagonPoints[0].lat,
                    "Longitude": hexagonPoints[0].lng,
                    "Altitude": waypointHeightF + 20, //converting to feet
                    "WaypontHeadingType": 4,
                    "WaypontActionType": 0,
                    "WaypontActionParameter": 0,
                    "StartTakePhoto": false,
                    "action": wayPointActionF
                };


            missionWaypoints.push(waypointF);

        for (let k = 0; k < hexagonPoints.length; k++) {
           let listOfOneCoulumnPoints = [];




            for (let m = 0 ; m <= this.mission.InspectionDivisions - 1; m++) {


              var waypointHeight= (clearanceHeight + (stepIncrease * (m))) * 3.2808399;

              if(waypointHeight  > this.mission.altitude)
              {
                waypointHeight = this.mission.altitude;
              }


                wayPointAction = {};
                wayPointAction.waypointactiontype = "GIMBAL_PITCH";
                wayPointAction.waypointactionvalue = -45;

                let waypoint = {
                    "Latitude": hexagonPoints[k].lat,
                    "Longitude": hexagonPoints[k].lng,
                    "Altitude": waypointHeight + 20, //converting to feet
                    "WaypontHeadingType": 4,
                    "WaypontActionType": 0,
                    "WaypontActionParameter": 0,
                    "StartTakePhoto": false,
                    "action": wayPointAction
                };


                missionWaypoints.push(waypoint);
                listOfOneCoulumnPoints.push(waypoint);


            }




            if(listOfOneCoulumnPoints.length > 1)
            {

              var start = listOfOneCoulumnPoints.length - 2

              for (let pos = start; pos >= 0; pos--) {
                let waypointR = {
                      "Latitude": listOfOneCoulumnPoints[pos].Latitude,
                      "Longitude": listOfOneCoulumnPoints[pos].Longitude,
                      "Altitude": listOfOneCoulumnPoints[pos].Altitude, //converting to feet
                      "WaypontHeadingType": 4,
                      "WaypontActionType": 0,
                      "WaypontActionParameter": 0,
                      "StartTakePhoto": false,
                      "action": wayPointAction
                  };

                  missionWaypoints.push(waypointR);
              }
            }
        }
    }

   // missionWaypoints[0].Altitude = towerHeightWithIncreament;
    

    this.mission.FlightGeography = JSON.stringify(missionWaypoints);


    console.log("FlightGeography" + JSON.stringify(missionWaypoints));
  }


  getDefaultLocation() {
    if (this.mission == null) {
      return new L.LatLng(this.lat, this.lng);
    }
    else if (this.mission.FlightGeography != null && this.mission.FlightGeography.length > 0) {
      var flightPath = JSON.parse(this.mission.FlightGeography);

      if (flightPath.length > 0) {
        return new L.LatLng(flightPath[0].Latitude, flightPath[0].Longitude);
      }
    }

    return null;
  }

  getAirspaceAdvisories() {

    let location = this.getDefaultLocation();

    if (location != null) {

      let url = 'https://api.airmap.com/status/v2/point/?latitude=' + location.lat + '&longitude=' + location.lng + '&weather=true&types=airport,controlled_airspace,special_use_airspace,school,tfr';

      this.httpClient.createRequest(url).withHeader('X-API-Key', this.configuration.get('airmapKey')).asGet().send()
        .then(result => {
          let response = JSON.parse(result.response);
          if (response.status == 'success') {
            response.data.advisories.map(data => {
              if (data.properties.laanc) {
                this.notification.error('Laanc Approval Required.');
              }
            })
            this.advisories = response.data.advisories;

            if (this.advisories.length > 0) {

              this.advisories.map(x => {
                x.type = x.type.replace('_', ' ').toUpperCase();
              });

              let notificationCls = 'humane-libnotify-success';

              if (response.data.advisory_color == 'yellow') {
                this.advisoryCls = 'advisory-yellow';
                notificationCls = 'humane-libnotify-ShowInfo';
              }
              else if (response.data.advisory_color == 'red') {
                this.advisoryCls = 'advisory-red';
                notificationCls = 'humane-libnotify-red';
              }

              this.notification.error('Please check the advisories.');
            }
            else {
              this.notification.error('There is no advisory for this location.');
            }

          }
        });

    }
  }

  liveDroneView() {
    this.router.navigate('drone-view/' + this.gufi);
  }

  back() {
    this.router.navigate('#/missions');
  }

  isValidFacadeWall() {
    let bearingAngleP1P2 = undefined;
    let bearingAngleP2P1 = undefined;
    let bearingAngleP2P3 = undefined;    

    if (this.facadeAllPoints.length <= 2) {
      return true;
    }

    if (this.facadeAllPoints.length >= 3) {
      let point1 = this.facadeAllPoints[this.facadeAllPoints.length - 3];
      let point2 = this.facadeAllPoints[this.facadeAllPoints.length - 2];
      let point3 = this.facadeAllPoints[this.facadeAllPoints.length - 1];

      bearingAngleP1P2 = this.getBearingAngle(point1, point2);
      bearingAngleP2P1 = this.getBearingAngle(point2, point1);
      bearingAngleP2P3 = this.getBearingAngle(point2, point3);

      if (bearingAngleP1P2 < 0) {
        bearingAngleP1P2 = 360 + bearingAngleP1P2;
      }

      if (bearingAngleP2P1 < 0) {
        bearingAngleP2P1 = 360 + bearingAngleP2P1;
      }

      if (bearingAngleP2P3 < 0) {
        bearingAngleP2P3 = 360 + bearingAngleP2P3;
      }

      //Set Band
      bearingAngleP2P1 = bearingAngleP2P1 + 50;
      bearingAngleP1P2 = bearingAngleP1P2 - 40;

      if (bearingAngleP1P2 < bearingAngleP2P1) {
        if (bearingAngleP2P3 > bearingAngleP2P1 || bearingAngleP2P3 < bearingAngleP1P2) {
          return true;
        }
      }
      else {
        if (bearingAngleP2P3 > bearingAngleP2P1 && bearingAngleP2P3 < bearingAngleP1P2) {
          return true;
        }
      }
    }

    return false;
  }

  getBearingAngle(point1,point2) {
    let lat1 = this.toRadians(point1.lat);
    let lon1 = this.toRadians(point1.lng);
    let lat2 = this.toRadians(point2.lat);
    let lon2 = this.toRadians(point2.lng);

    let dLon = lon2 - lon1;
    let y = Math.sin(dLon) * Math.cos(lat2);
    let x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);
    let radiansBearing = Math.atan2(y, x);

    return this.toDegrees(radiansBearing);
  }
}
