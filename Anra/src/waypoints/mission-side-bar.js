import { inject, bindable, BindingEngine } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(EventAggregator, BindingEngine)
export class MissionSideBar {
  @bindable mission;
  @bindable save;
  constructor(events, bindingEngine) {
    this.events = events;
    this.bindingEngine = bindingEngine;
    this.directions = [
        { value: true, text: 'Clockwise' },
        { value: false, text: 'Anti-clockwise' }
    ];
  }

  attached() {
    this.subscriptions = [];
    let latitudeObserver = this.bindingEngine.propertyObserver(this.mission, 'PointOfInterestLat').subscribe((newValue, oldValue) => {
      if (newValue !== oldValue) {
        this.events.publish('point-location-update');
      }
    });
    let lngitudeObserver = this.bindingEngine.propertyObserver(this.mission, 'PointOfInterestLng').subscribe((newValue, oldValue) => {

      if (newValue !== oldValue) {
        this.events.publish('point-location-update');
      }
    });

    this.subscriptions.push(latitudeObserver);
    this.subscriptions.push(lngitudeObserver);
  }

  detached() {
    this.subscriptions.map(x => x.dispose());
  }

  saveItem() {
    this.save();
  }

  /*onTabSelect(selectedTab) {
    this.selectedTab = selectedTab;
  }

  onWaypointLoad(event) {
    console.log('load wp');
    let reader = new FileReader();
    let file = event.target.files[0];
    reader.readAsText(file);
    let fileName = file.name;

    reader.onload = () => {
      var contents = reader.result;
      if (contents && contents.length > 0) {
        var lines = contents.split('\n');
        if (lines.length > 0) {
          for (var i = 1; i < lines.length; i++) {
            var arr = lines[i].split('\t');
            if (arr.length > 0 && arr[8] && arr[9]) {
              this.waypoints.push(L.latLng(arr[8], arr[9]));
            }
          }

          //Draw
          this.events.publish('drone-view:waypoints', {waypoints: this.waypoints});
          this.waypointFile = contents;
          this.waypoints1 = lines;
        }
      }
    };
  }*/
}
