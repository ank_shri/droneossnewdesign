import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {AnraNotification} from '../components/anra-notification';
import {Router} from 'aurelia-router';
import moment from 'moment';
import $ from 'bootstrap';
import 'Eonasdan/bootstrap-datetimepicker';


@inject(EventAggregator, Router, AnraNotification, NewInstance.of(ValidationController))
export class Effort {
  @bindable effort;
  @bindable save;
  controller = null;
  rules = null;

  constructor(events, router, notification, controller) {
        this.events = events;
        this.router = router;
        this.notification = notification;
        this.controller = controller;
        this.controller.addRenderer(new BootstrapFormValidationRenderer());
        this.controller.validateTrigger = validateTrigger.change;
        this.isWaiting = false;
        this.ShowMissionTime=true;
  }

  attached() {

      this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.heading = this.effort.TrackId > 0 ? 'Edit Mission Time' : 'Add Mission Time';    

    if(this.effort.TrackId < 1){
        this.effort.StartDateTime = null;
        this.effort.EndDateTime = null;

    }
  }

  setupValidationRules() {   
      //Custom Rule For Date Validation  
      ValidationRules.customRule(
        'date',
        (value, obj) => value === null || value === undefined || value instanceof Date || moment(value).isValid(),
        '\${$displayName} must be a Date.'
      );

      ValidationRules.customRule(
        'comparedate',
        (value, obj, otherPropertyName) => moment(value).isBefore(moment(obj[otherPropertyName])),
        'Time available begin must before than Time available end',
        otherPropertyName => ({ otherPropertyName })
      ); 

      this.rules = ValidationRules
           .ensure('UserId').required()    
           .ensure('TotalTravelHours').required()
           .ensure('StartDateTime').required().satisfiesRule('comparedate', 'EndDateTime')
           .ensure('EndDateTime').required()
           .rules;
  }

  saveItem() {
  
      this.isWaiting = true;

      this.controller.validate({ object: this.effort, rules: this.rules })      
          .then(valresult => {

              if (valresult.valid) {
                  // validation succeeded

                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
						  if(this.effort.TrackId!='')
						  {
							this.notification.ShowInfo('Updated Successfully.');  
						  }
						  else
						  {
							this.notification.ShowInfo('Added Successfully.');  
						  }
						  this.isWaiting = false;
                      })
              } 
              else {
                  console.log(valresult);      // validation failed
                  this.notification.ShowInfo('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.ShowInfo('Error.');
              this.isWaiting = false;
          });

      this.isWaiting = false;
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('item-operation', {refresh: true});
    }
    else {
      this.notification.ShowInfo(result.ShowInfoMessage);
    }

      //Reset DateTimePicker
      this.effort="";
  }

  cancel() {
    this.events.publish('item-operation', {refresh: false});
  }

  calcEffort(){      
      var startDate=moment(this.effort.StartDateTime).toDate();
      var endDate=moment(this.effort.EndDateTime).toDate();
      var msec = endDate.getTime() - startDate.getTime();
      var hrs = Math.abs(msec / 1000 / 60 / 60);
      this.effort.TotalEffortHours = isNaN(hrs) ? 0 : parseFloat(hrs.toFixed(2));
  }
}
