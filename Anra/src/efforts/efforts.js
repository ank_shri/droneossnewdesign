import {inject} from 'aurelia-framework';
import {EffortService} from './effortService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {AnraNotification} from "../components/anra-notification";
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(EffortService, Router, EventAggregator, AnraNotification)
export class Efforts {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['MissionName','UserName']},
    ];

  constructor(effortService, router, events, notification) {
    this.effortService = effortService;
    this.router = router;
    this.events = events;
    this.notification = notification;
    this.heading = 'Manage Mission Time';
    this.efforts = [];
    this.effort=null;
    this.gufi=null;    
    this.isEditing = false;
    //Set Default Paging Size
    this.pageSize = effortService.defaultPagingSize;
    this.isWaiting = false;
  }

  activate(params) {
      this.gufi = params.id;
    this.getList();
  }

  attached() {
    this.subscribeEvents();
  }

  subscribeEvents() {
    this.events.subscribe('item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });
  }


  getList() {
      this.isWaiting = true;
      this.effortService.getEfforts(this.gufi)
      .then(result => {
          this.efforts = result;          
          this.isEditing = false;
          this.isWaiting = false;
      }).catch((e) => {
          this.notification.error(e.message);
          this.isWaiting = false;
      });
  }

  reset() {
    this.isEditing = false;    
  }

  add() {
      this.isWaiting=true;
      this.effortService.getNewEffort(this.gufi)
      .then(result => {
          this.isEditing = true;
          this.effort = result;
          this.isWaiting=false;
      });
  }

  editItem(id) {
      this.isWaiting = true;
      this.effortService.getEffort(id)
        .then(result => {            
            this.effort = result;
            this.effort.StartDateTime = moment(result.StartDateTime).format('MM/DD/YYYY hh:mm A');
            this.effort.EndDateTime = moment(result.EndDateTime).format('MM/DD/YYYY hh:mm A');
            this.isEditing = true;
            this.isWaiting = false;
        });
  }

  deleteItem(id) {
      this.isWaiting=true;
      this.notification.Confirm(
        "Are you sure you want to delete?"
      ).then(result => {
        if (result) {
          this.effortService.deleteEffort (id)
          .then(result => {
              this.notification.ShowInfo('Deleted Successfully.');
            this.getList();
            });
        }
        else{
          this.isWaiting=false;
        }
      });
  }

  saveItem(item) {
      return this.effortService.saveEffort(this.effort);
  }

  showMissions() {            
      this.router.navigate('missions/missions');
  }
}
