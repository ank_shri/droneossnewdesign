import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class EffortService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getEfforts(gufi) {
    return this.http.fetch(this.baseApi + 'effort/listing/' + gufi)
      .then(response => response.json());
  }

  getNewEffort(gufi) {
      return this.http.fetch(this.baseApi + 'effort/NewEffort/' + gufi)
      .then(response => response.json());
  }

  getEffort(id) {
      return this.http.fetch(this.baseApi + 'effort/' + id)
      .then(response => response.json());
  }

  deleteEffort(id) {
      return this.http.fetch(this.baseApi + 'effort/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveEffort(Effort) {
      return this.http.fetch(this.baseApi + 'effort/', {
      method: 'post',
      body: json(Effort)
    }).then(response => response.json());
  }
}
