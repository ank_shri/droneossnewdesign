import {inject} from 'aurelia-framework';
import {DroneService} from 'drones/droneService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {PermissionService} from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
import {AnraNotification} from "../components/anra-notification";
import moment from 'moment';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(DroneService, Router, EventAggregator, PermissionService ,AnraNotification)
export class Drones {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['Title','LegalIdentification','OrganizationName']},
    ];

    constructor(droneService, router, events, permissionService,notification) {
        this.droneService = droneService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        this.notification = notification;
        this.heading = 'Manage Drones';
        this.drones = [];    
        this.dronestocsv=[];    
        this.drone = null;
        this.isEditing = false;
        this.isViewing = false;
        // this.droneRoute = router.routes[38].navModel;
        this.massUnit = localStorage["MassUnit"];
        this.lengthUnit = localStorage["LengthUnit"];
        this.permission={};		  
        // this.dialogService = dialogService;
        //Set Default Paging Size
        this.pageSize = droneService.defaultPagingSize;
        this.isWaiting = false;
    }

    

    activate() {
        this.getList();
        
        // this.droneRoute.isActive = true;
    }

    // deactivate() {
    //     this.droneRoute.isActive = false;
    // }

    attached() {
        this.subscribeEvents();
       
    }
    detached() {
      this.subscriber.dispose();
    }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
          });
    }

    subscribeEvents() {
      this.subscriber=this.events.subscribe('drone-item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                }
                this.reset();
            }
        });

        this.subscriber=this.events.subscribe('drone-item-operation-edit', payload => {

            if (payload) {
                if (payload.id > 0) {
                    this.editItem(payload.id);
                }
                this.reset();
            }
        });

    }

    getList() {

        this.isWaiting = true;
        this.droneService.getDrones()
          .then(result => {
              this.drones = result;              
              this.isEditing = false;
              this.getPermission(4);
              this.isWaiting = false;
              var newJson=[];
            this.drones.forEach(function(item) {
            newJson.push({
            "Drone Name":item.Title,
            "Purchase Date":moment(item.PurchaseDate).format('YYYY-MM-DD'),
            "Weight":item.Weight,
            "Legal Identification":item.LegalIdentification,
            "Organization":item.OrganizationName,
            "IsActive":item.IsActive,
            "Created Date":moment(item.DateCreated).format('YYYY-MM-DD')
                        });
            });
            this.dronestocsv=newJson;
          }).catch((e) => {
              this.notification.Showerror(e.message);
              this.isWaiting = false;
          });
        
    }
     //Convert For CSV Convert
       

    reset() {
        this.drone = null;
        this.isEditing = false;
        this.isViewing = false;
    }

  add() {
        this.isWaiting=true;
        this.droneService.getNewDrone()
          .then(result => {
              this.isWaiting = false;
              this.drone = result;
              this.isEditing = true;
          });
    }

    editItem(id) {
        this.isWaiting = true;
        this.droneService.getDrone(id)
          .then(result => {
              
              this.drone = result;
              this.drone.PurchaseDate = moment(result.PurchaseDate).format('YYYY-MM-DD');
              this.isEditing = true;
              this.isWaiting = false;
          });
    }

    viewItem(id) {
        this.isWaiting= true;
        this.droneService.getDrone(id)
          .then(result => {              
              this.drone = result;
              this.drone.PurchaseDate = moment(result.PurchaseDate).format('YYYY-MM-DD');
              this.isViewing = true;
              this.isWaiting= false;
          });
    }

    deleteItem(id) {
        this.isWaiting=true;
        this.notification.Confirm("Are you sure you want to delete?").then(result => {
            if (result) {
            this.droneService.deleteDrone (id)
            .then(result => {
              this.notification.ShowError('Deleted Successfully.');
              this.getList();
              this.isWaiting=false;
            });
           }
           else{
            this.isWaiting=false;
           }
        });
    }

    saveItem(item) {
        return this.droneService.saveDrone(this.drone);
    }

     //Download CSV
   convertArrayOfObjectsToCSV(args) {  
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        this.notification.Showerror("No data available to export.");          
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    var columnHeaders = ['Name','Purchase Date','Weight','Legal Identity','Organization','Status','Date Created'];
    
    result = '';
    result += columnHeaders.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
  }

    downloadCSV(args) {  
    var data, filename, link;

    //Setting Entity Attributes

    var csv = this.convertArrayOfObjectsToCSV({data:this.dronestocsv});
    if (csv == null) return;

    filename = args.filename || 'DronesDetails.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();

    }
}
