import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {AnraNotification} from '../components/anra-notification';
import {AureliaConfiguration} from "aurelia-configuration";
import {GeoSearchProvider} from '../components/geo-search-provider';

import * as L from 'leaflet';
import * as D from 'leaflet-draw';
import * as T from 'SpatialServer/Leaflet.MapboxVectorTile';
import 'smeijer/leaflet-geosearch';
import moment from 'moment';
import {UIApplication} from "aurelia-ui-framework";

const defaultZoom = 16;
const maxZoom = 22;
@inject(EventAggregator, AnraNotification, AureliaConfiguration, GeoSearchProvider, NewInstance.of(ValidationController),UIApplication)
export class Drone {
  @bindable drone;
  @bindable save;
  @bindable organizations;
  controller = null;
  rules = null;

  constructor(events, notification, config, geoSearchProvider, controller,application) {
    
    this.events = events;
    this.notification = notification;
    this.configuration = config;
    this.geoSearchProvider = geoSearchProvider;
    this.controller = controller;
    this.application = application;
    this.lat = 39.033931;
    this.lng = -77.376655;
    this.massUnit = localStorage["MassUnit"];
    this.lengthUnit = localStorage["LengthUnit"];
    this.ShowDroneInformation = true;
    
    
  }

  attached() {
   
    // $('#purchaseDate').attr("max",moment().format('YYYY-MM-DD'));
    this.setupValidationRules();
    this.initMap();
  }

  setupValidationRules() {   

      //Custom Rule For Date Validation  
      ValidationRules.customRule(
        'date',
        (value, obj) => value === null || value === undefined || value instanceof Date || moment(value,"YYYY-MM-DD",true).isValid(),
        '\${$displayName} must be a Date.'
      );

      this.rules = ValidationRules
           .ensure('Title').required()    
           .ensure('LegalIdentification').required().maxLength(20)
           .ensure('BrandId').matches(/^[1-9][0-9]*$/).required()
           .ensure('ModelName').required()
           .ensure('Weight').required()
           .ensure('FirmwareVersion').required().maxLength(20)
           .ensure('HardwareVersion').required().maxLength(20)
           //.ensure('SensorSrcUrl').required()
           .ensure('BaseLng').required()
           .ensure('BaseLat').required()
           .ensure('BaseAlt').matches(/^\d+$/).required()
           .ensure('PurchaseDate').required().satisfiesRule('date')

           .rules;
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.heading = this.drone.Name != null ? 'Edit Drone' : 'Add Drone';
  }

  initMap() {
    if (this.map) {
      this.marker = undefined;
      this.map.remove();
    }
    //if (!this.map) {
      let accessToken = this.configuration.get('mapboxAccessToken');

      var streetMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=' + accessToken, {
        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
      });

      var satelliteMap = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
      });

      let streetDarkMap = L.tileLayer(
        'https://api.mapbox.com/styles/v1/aganjoo/cjm2gw5p406qa2snwya18clid/tiles/256/{z}/{x}/{y}?access_token=' +
        accessToken, {
          attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
        }
      );

      this.baseMaps = {
        "Satellites": satelliteMap,
        "Streets": streetMap
      };

      let map = L.map('map', {
        zoomControl: false,
        drawControl: false,
        editable: false,
        maxNativeZoom: maxZoom,
        layers: [streetDarkMap]
      });

      //this.layerControl = L.control.layers(this.baseMaps, this.overlayMaps).addTo(map);

      L.control.zoom({
        maxZoom: maxZoom,
        position: 'topleft'
      }).addTo(map);

      new L.Control.GeoSearch({
        provider: this.geoSearchProvider,
        showMarker: true,
        retainZoomLevel: false,
      }).addTo(map);

      map.doubleClickZoom.disable();
      L.Icon.Default.imagePath = 'image';
      this.map = map;

      map.on('locationfound', e => {
        this.lat = e.latitude;
        this.lng = e.longitude;        
        this.setDefaultView();
      })

      map.on('click', e => {
        this.drone.BaseLat = e.latlng.lat;
        this.drone.BaseLng = e.latlng.lng;   
        this.enablebBaselatlong();
        this.setMarker();
      });
    //}

    this.map.locate({setView: true, maxZoom: maxZoom});
  }

  enablebBaselatlong()
  {      
      document.getElementsByName("baselat")[1].disabled = false;
      document.getElementsByName("baselong")[1].disabled = false;      
  }

  setMarker() {
    if (this.marker) {
      this.marker.setLatLng(new L.LatLng(this.drone.BaseLat, this.drone.BaseLng));
    }
    else {
      this.marker = L.marker(new L.LatLng(this.drone.BaseLat, this.drone.BaseLng)).addTo(this.map);
    }
  }


  setDefaultView() {
    if (this.drone && this.drone.BaseLat) {
      this.map.setView(new L.LatLng(this.drone.BaseLat, this.drone.BaseLng, defaultZoom));
    }
    else {
      this.drone.BaseLat = this.lat;
      this.drone.BaseLng = this.lng;
      this.map.setView(new L.LatLng(this.lat, this.lng), defaultZoom);
    }

    this.setMarker();
  }

  saveItem() {

      this.isWaiting=true;
      this.controller
      .validate({object:this.drone, rules: this.rules}) 
          .then(valresult => {
              if (valresult.valid) {
                // validation succeeded
                this.save()
                  .then(result => {
                      this.handleSaveResult(result);
                      if(this.drone.DroneId!='')
                      {
                      this.notification.ShowInfo('Updated Successfully.');
                      }
                      else
                      {
                      this.notification.ShowInfo('Added Successfully.');  
                      }
                      this.isWaiting = false;
                })
              } 
              else {
                let messages = this.controller.errors.map(x => x.message);
      
                this.application.toast(
                  Object.assign({ container: this.errorHolder }, {
                    title: 'Errors!',
                    message: messages.join(', '),
                    theme: 'danger',
                    timeout: 2000,
                    glyph: 'glyph-alert-info'
                  })
                );
      
                this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.ShowError('Error.');
              this.isWaiting = false;
          });

      // this.isWaiting = true;
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.events.publish('drone-item-operation', {refresh: true});
    }
    else {
      this.notification.ShowError(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('drone-item-operation', {refresh: false});
  }
}
