import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
// import {Notification} from 'aurelia-notification';

@inject(EventAggregator)
export class DroneView {
  @bindable drone;
  @bindable save;
  @bindable organizations;

  constructor(events) {
    this.events = events;
    // this.notification = notification;
    this.massUnit = localStorage["MassUnit"];
    this.lengthUnit = localStorage["LengthUnit"];
  }

  activate(params) {
    console.log(params);
  }

  bind() {
    this.heading = this.drone.Name != null ? 'View Drone' : 'Add Drone';
  } 

  saveItem() {
    return this.save()
          .then(result => {
            this.handleSaveResult(result)
          }).catch((e) => {
        this.notification.error(e);
      });
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('drone-item-operation', {refresh: true});
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('drone-item-operation', {refresh: false});
  }

  edit(droneId) {
      this.events.publish('drone-item-operation-edit', {id: droneId});
  }

}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
