import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class DroneService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getDrones() {
      return this.http.fetch(this.baseApi + 'Drone/listing')
      .then(response => response.json());
  }

  getNewDrone() {
      return this.http.fetch(this.baseApi + 'Drone/NewDrone')
      .then(response => response.json());
  }

  getDrone(id) {
    return this.http.fetch(this.baseApi + 'Drone/' + id )
      .then(response => response.json());
  }

  deleteDrone(id) {
    return this.http.fetch(this.baseApi + 'Drone/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }
  saveDrone(Drone) {
    return this.http.fetch(this.baseApi + 'Drone/', {
      method: 'post',
      body: json(Drone)
    }).then(response => response.json());
  }
}
