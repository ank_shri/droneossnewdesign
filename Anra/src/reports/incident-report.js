import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import {ReportService} from 'reports/reportService';
import moment from 'moment';

@inject(EventAggregator, AnraNotification, ReportService)
export class IncidentReport {
  @bindable reports;
  @bindable search;

    //Set Filters for Searchbox
  filters = [
            {value: '', keys: ['Name','IncidentCause','MissionName','ProjectName','DroneName','LocationName']},
  ];

  constructor(events, notification, reportService) {
    this.events = events;
    this.reportService = reportService;
    this.notification = notification;
    this.incidents = null;
    this.flagCustomFilter=false;
    this.showNoRecordFound=false;
    this.searchBy=null;
    this.filterOptions = null;
    this.reportPeriod = 'Report Period:';
    //Set Default Paging Size
    this.pageSize = reportService.defaultPagingSize;
    this.isWaiting = false;
  }

  bind() {
    this.heading = this.reports.ReportName != null ? (this.reports.ReportName + ' Report') : 'View Report';
    this.incidents = this.reports.Incidents;
    this.showNoRecordFound = this.reports.Incidents.length > 0 ? false : true;
    this.searchBy = this.reports.Criteria.SearchBy;
    this.filterOptions = this.reports.SearchLookup;
    this.setSearchPeriod();      
  }


  cancel() {
      this.resetSearchCriteria();
      this.events.publish('item-operation', {refresh: true});
  }

  showCustomFilter(event){
      this.searchBy = event.detail;
      document.getElementById('fromDt').value = null;
      document.getElementById('toDt').value = null;

      if(event.detail == 'CS'){
          this.flagCustomFilter=true;
      }
      else{
          this.flagCustomFilter=false;
      }
      
      // Set Checked attribute for selected item
      this.setSelectedOption(this.searchBy);    

      //Set Report Period
      this.setSearchPeriod();
  }

  setSearchPeriod(){

      var firstDay;
      var lastDay;      

      //Create Current Date & Its elements Day/Month/Year
      var currentDate = new Date();
      var day = currentDate.getDate();
      var month = currentDate.getMonth();
      var year = currentDate.getFullYear();

      if(this.searchBy == 'MN'){

          firstDay = new Date(year,month,1);
          lastDay = new Date(year,month+1,0);
          
          this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
      }

      if(this.searchBy == 'QR'){

          var qtr = Math.ceil((month - 1) / 3 + 1);

          firstDay = new Date(year,(3 * qtr - 3),1);
          lastDay = new Date(year,(3 * qtr),0);                    

          this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
      }

      if(this.searchBy == 'YR'){

          firstDay = new Date(year,0,1);
          lastDay = new Date(year,12,0);
          
          this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
      }

      if(this.searchBy == 'CS'){

          this.reportPeriod = 'Report Period:';

          if(moment(document.getElementById('fromDt').value, "YYYY-MM-DD", true).isValid() && moment(document.getElementById('toDt').value, "YYYY-MM-DD", true).isValid()){

              firstDay = new Date(document.getElementById('fromDt').value);
              lastDay = new Date(document.getElementById('toDt').value);
              this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
          }
      }

  }

  searchReport(){

      this.isWaiting = true;

      //Set search criteria before making call to service
      this.setSearchCriteria();

      return this.reportService.searchReport(this.reports)
        .then(result => {
            this.reports = null;
            this.reports = result;

            this.incidents = null;
            this.incidents = result.Incidents;
            this.showNoRecordFound = this.incidents.length > 0 ? false : true;
             
            if(this.reports.PrintReport){
                this.reports.PrintReport = false;
                this.downloadReport(this.reports.ReportContent);
            }

            this.isWaiting = false;
        }).catch((e) => {
            this.notification.error("Error.");          
            this.isWaiting = false;
        });
      this.isWaiting = false;
  }

  setSearchCriteria(){
      this.reports.Criteria.SearchBy = this.searchBy;
      this.reports.ReportContent = '';

      if(this.reports.Criteria.SearchBy == 'CS'){ //If Search By Custom then set From Date and To Date Entered on UI
          this.reports.Criteria.FromDate = document.getElementById('fromDt').value;
          this.reports.Criteria.ToDate = document.getElementById('toDt').value;
      }
  }

  resetSearchCriteria(){

      if(this.searchBy == 'CS'){//If Search By Custom then reset From Date and To Date to NUll
          
          document.getElementById('fromDt').value = null;
          document.getElementById('toDt').value = null;
          
          this.reports.Criteria.FromDate = document.getElementById('fromDt').value;
          this.reports.Criteria.ToDate = document.getElementById('toDt').value;
      }

      // Reset All option to unchecked
      this.resetSelectedOption();

      this.flagCustomFilter=false;
      this.searchBy = null;        
  }

  setSelectedOption(selectedVal){

      var rptFilter = document.getElementsByClassName('ui-option-input');
      
      for(var i = 0; i < rptFilter.length; i++){
          if(rptFilter[i].value == selectedVal){
              rptFilter[i].checked = true;
              rptFilter[i].defaultChecked = true;
          }
          else{
              rptFilter[i].checked = false;
              rptFilter[i].defaultChecked = false;
          }
      }
  }

  resetSelectedOption(){

    var rptFilter = document.getElementsByClassName('ui-option-input');
      
    for(var i = 0; i < rptFilter.length; i++){
        rptFilter[i].checked = false;
        rptFilter[i].defaultChecked = false;
    }

  }

  downloadPDF(){
      this.reports.PrintFormat = 'PDF';

      if (this.showNoRecordFound) {
          this.reports.PrintReport = false;
          this.notification.error("No Report Data is available to export.");                              
      }
      else{
          this.reports.PrintReport = true;
          this.searchReport();
      }      
  }

  getContentType(data)
  {
      if(data.charAt(0)=='/'){
          return "image/jpeg";
      }
      else if(data.charAt(0)=='R'){
          return "image/gif";
      }
      else if(data.charAt(0)=='i'){
          return "image/png";
      }
      else if(data.charAt(0)=='J'){
          return "application/pdf";
      }
  }

  base64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
  }

  downloadReport(data)
  {
      var contentType=this.getContentType(data);
      var blob=this.base64toBlob(data, contentType);
      var mimetype=blob.type;

      //Code to download PDF File Locally
      var fileName = "IncidentReport.pdf";
      var url = window.URL.createObjectURL(blob);
      var link = document.createElement('a');
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.click();
      window.URL.revokeObjectURL(url);
  }

  convertArrayOfObjectsToCSV(args) {  
      var result, ctr, keys, columnDelimiter, lineDelimiter, data;

      data = args.data || null;
      if (data == null || !data.length) {
          this.notification.error("No Report Data is available to export.");          
          return null;
      }

      columnDelimiter = args.columnDelimiter || ',';
      lineDelimiter = args.lineDelimiter || '\n';

      keys = Object.keys(data[0]);

      var columnHeaders = ['Name','Incident Date','Project','Drone','Location','Aircraft Damage','Other Damage','Notes','User','Organization','Mission','Contact','Pilot License','Legal Identification','Incident Cause'];


      result = '';
      result += columnHeaders.join(columnDelimiter);
      result += lineDelimiter;

      data.forEach(function(item) {
          ctr = 0;
          keys.forEach(function(key) {
              if (ctr > 0) result += columnDelimiter;
              if(key=='IncidentDate'){
                  result += moment(item[key]).format('YYYY-MM-DD');                  
              }
              else if(key=='MissionId' || key=='IncidentCauseId' || key=='OrganizationId'){
                  //skip the data writing
                  result = result.substring(0, result.length - 1);
              }
              else{
                  result += item[key];
              }
              ctr++;
          });
          result += lineDelimiter;
      });

      return result;
  }

  downloadCSV(args) {  
      var data, filename, link;

      //Setting Entity Attributes
      this.reports.PrintReport = true;
      this.reports.PrintFormat = 'CSV';

      var csv = this.convertArrayOfObjectsToCSV({data: this.incidents});
      if (csv == null) return;

      filename = args.filename || 'IncidentReport.csv';

      if (!csv.match(/^data:text\/csv/i)) {
          csv = 'data:text/csv;charset=utf-8,' + csv;
      }
      data = encodeURI(csv);

      link = document.createElement('a');
      link.setAttribute('href', data);
      link.setAttribute('download', filename);
      link.click();

      this.reports.PrintReport = false;
  }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}