import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import {ReportService} from 'reports/reportService';
import moment from 'moment';

@inject(EventAggregator, AnraNotification, ReportService)
export class SummaryReport {
  @bindable reports;
  @bindable search;

  constructor(events, notification, reportService) {
    this.events = events;
    this.reportService = reportService;
    this.notification = notification;
    this.summary = null;
    this.flagCustomFilter=false;
    this.showNoRecordFound=false;
    this.searchBy=null;
    this.filterOptions = null;
    this.reportPeriod = 'Report Period:';
    this.isWaiting = false;
    this.flight={};
  }

  bind() {
    this.heading = this.reports.ReportName != null ? (this.reports.ReportName + ' Report') : 'View Report';
    this.summary = this.reports.Summary;
    this.showNoRecordFound = this.reports.Summary.length > 0 ? false : true;
    this.searchBy = this.reports.Criteria.SearchBy;
    this.filterOptions = this.reports.SearchLookup;
    this.setSearchPeriod();      
  }
  attached()
  {
      this.drawPieChart();
  }


  cancel() {
      this.resetSearchCriteria();
      this.events.publish('item-operation', {refresh: true});
  }

  showCustomFilter(event){
      this.searchBy = event.detail;
      document.getElementById('fromDt').value = null;
      document.getElementById('toDt').value = null;

      if(event.detail == 'CS'){
          this.flagCustomFilter=true;
      }
      else{
          this.flagCustomFilter=false;
      }
      
      // Set Checked attribute for selected item
      this.setSelectedOption(this.searchBy);    

      //Set Report Period
      this.setSearchPeriod();
  }

  setSearchPeriod(){

      var firstDay;
      var lastDay;      

      //Create Current Date & Its elements Day/Month/Year
      var currentDate = new Date();
      var day = currentDate.getDate();
      var month = currentDate.getMonth();
      var year = currentDate.getFullYear();

      if(this.searchBy == 'MN'){

          firstDay = new Date(year,month,1);
          lastDay = new Date(year,month+1,0);
          
          this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
      }

      if(this.searchBy == 'QR'){

          var qtr = Math.ceil((month - 1) / 3 + 1);

          firstDay = new Date(year,(3 * qtr - 3),1);
          lastDay = new Date(year,(3 * qtr),0);                    

          this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
      }

      if(this.searchBy == 'YR'){

          firstDay = new Date(year,0,1);
          lastDay = new Date(year,12,0);
          
          this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
      }

      if(this.searchBy == 'CS'){

          this.reportPeriod = 'Report Period:';

          if(moment(document.getElementById('fromDt').value, "YYYY-MM-DD", true).isValid() && moment(document.getElementById('toDt').value, "YYYY-MM-DD", true).isValid()){

              firstDay = new Date(document.getElementById('fromDt').value);
              lastDay = new Date(document.getElementById('toDt').value);
              this.reportPeriod = 'Report Period: ' + ((firstDay.getMonth() + 1).toString() + '/' + firstDay.getDate().toString() + '/' + firstDay.getFullYear().toString()) + ' - ' + ((lastDay.getMonth() + 1).toString() + '/' + lastDay.getDate().toString() + '/' + lastDay.getFullYear().toString());
          }
      }

  }

  searchReport(){

      this.isWaiting = true;

      //Set search criteria before making call to service
      this.setSearchCriteria();

      return this.reportService.searchReport(this.reports)
        .then(result => {
            this.reports = null;
            this.reports = result;

            this.summary = null;
            this.summary = result.Summary;
            this.showNoRecordFound = this.summary.length > 0 ? false : true;

            if(this.reports.PrintReport){               
                this.reports.PrintReport = false;
                this.downloadReport(this.reports.ReportContent);
            }
            this.drawPieChart(); 
            this.isWaiting = false;
        }).catch((e) => {
            this.notification.error("Error.");          
            this.isWaiting = false;
        });
  }

  setSearchCriteria(){
      this.reports.Criteria.SearchBy = this.searchBy;
      this.reports.ReportContent = '';

      if(this.reports.Criteria.SearchBy == 'CS'){ //If Search By Custom then set From Date and To Date Entered on UI
          this.reports.Criteria.FromDate = document.getElementById('fromDt').value;
          this.reports.Criteria.ToDate = document.getElementById('toDt').value;
      }
  }

  resetSearchCriteria(){

      if(this.searchBy == 'CS'){//If Search By Custom then reset From Date and To Date to NUll
          
          document.getElementById('fromDt').value = null;
          document.getElementById('toDt').value = null;
          
          this.reports.Criteria.FromDate = document.getElementById('fromDt').value;
          this.reports.Criteria.ToDate = document.getElementById('toDt').value;
      }

      // Reset All option to unchecked
      this.resetSelectedOption();

      this.flagCustomFilter=false;
      this.searchBy = null;        
  }

  setSelectedOption(selectedVal){

      var rptFilter = document.getElementsByClassName('ui-option-input');
      
      for(var i = 0; i < rptFilter.length; i++){
          if(rptFilter[i].value == selectedVal){
              rptFilter[i].checked = true;
              rptFilter[i].defaultChecked = true;
          }
          else{
              rptFilter[i].checked = false;
              rptFilter[i].defaultChecked = false;
          }
      }
  }

  resetSelectedOption(){

    var rptFilter = document.getElementsByClassName('ui-option-input');
      
    for(var i = 0; i < rptFilter.length; i++){
        rptFilter[i].checked = false;
        rptFilter[i].defaultChecked = false;
    }

  }

  downloadPDF(){      

      if (this.showNoRecordFound) {
          this.reports.PrintReport = false;
          this.notification.error("No data available to export.");                              
      }
      else{
          this.reports.PrintReport = true;
          this.reports.PrintFormat = 'PDF';
          this.searchReport();
      }  
  }

  getPDF(){
 
    var HTML_Width = $(".canvas_div_pdf").width();
    var HTML_Height = $(".canvas_div_pdf").height();
    var top_left_margin = 5;
    var PDF_Width = HTML_Width+(top_left_margin*2);
    var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;
    
    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
    
    
    html2canvas($(".canvas_div_pdf")[0],{allowTaint:true}).then(function(canvas) {
    canvas.getContext('2d');
    
    
    var imgData = canvas.toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
    
    
    for (var i = 1; i <= totalPDFPages; i++) { 
    pdf.addPage(PDF_Width, PDF_Height);
    pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
    }
    
        pdf.save("Summary-Report.pdf");
           });
    }

  getContentType(data)
  {
      if(data.charAt(0)=='/'){
          return "image/jpeg";
      }
      else if(data.charAt(0)=='R'){
          return "image/gif";
      }
      else if(data.charAt(0)=='i'){
          return "image/png";
      }
      else if(data.charAt(0)=='J'){
          return "application/pdf";
      }
  }

  base64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
  }

  downloadReport(data)
  {
      var contentType=this.getContentType(data);
      var blob=this.base64toBlob(data, contentType);
      var mimetype=blob.type;

      //Code to download PDF File Locally
      var fileName = "SummaryReport.pdf";
      var url = window.URL.createObjectURL(blob);
      var link = document.createElement('a');
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.click();
      window.URL.revokeObjectURL(url);
  }

  convertArrayOfObjectsToCSV(args) {  
      var result, ctr, keys, columnDelimiter, lineDelimiter, data;

      data = args.data || null;
      if (data == null || !data.length) {
          this.notification.error("No data available to export.");          
          return null;
      }

      columnDelimiter = args.columnDelimiter || ',';
      lineDelimiter = args.lineDelimiter || '\n';

      keys = Object.keys(data[0]);

      var columnHeaders = ['Total Flights','Total Hours','Active Drones','Travel Time','Onsite Time','No. Of Incidents'];
      
      result = '';
      result += columnHeaders.join(columnDelimiter);
      result += lineDelimiter;

      data.forEach(function(item) {
          ctr = 0;
          keys.forEach(function(key) {
              if (ctr > 0) result += columnDelimiter;

              result += item[key];
              ctr++;
          });
          result += lineDelimiter;
      });

      return result;
  }

  downloadCSV(args) {  
      var data, filename, link;

      //Setting Entity Attributes
      this.reports.PrintReport = true;
      this.reports.PrintFormat = 'CSV';

      var csv = this.convertArrayOfObjectsToCSV({data: this.summary});
      if (csv == null) return;

      filename = args.filename || 'SummaryReport.csv';

      if (!csv.match(/^data:text\/csv/i)) {
          csv = 'data:text/csv;charset=utf-8,' + csv;
      }
      data = encodeURI(csv);

      link = document.createElement('a');
      link.setAttribute('href', data);
      link.setAttribute('download', filename);
      link.click();

      this.reports.PrintReport = false;
  }
  drawPieChart()
  {

    var data = new google.visualization.DataTable(3);
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Total Flights');
      data.addColumn('number', 'Total Hours');
      
    for(var i = 0; i < this.summary.length; i++) {
        var obj = this.summary[i];
        this.flight.TotalFlights=parseFloat(obj.TotalFlights);
        this.flight.TotalHours=parseFloat(obj.TotalHours);
        this.flight.TotalIncidents=parseFloat(obj.TotalIncidents);
        this.flight.TotalDroneUsed=parseFloat(obj.TotalDroneUsed);
        this.flight.TotalOnsiteTime=parseFloat(obj.TotalOnsiteTime);
        this.flight.TotalTravelTime=parseFloat(obj.TotalTravelTime);
    }
    var data = google.visualization.arrayToDataTable([
        ['Report_Details', 'Summary', { role: 'style' }],
        ['Total Flights',     this.flight.TotalFlights,'#36e4c6'],
        ['Total Hours',      this.flight.TotalHours,'#a1a1a1'],
        ['Total DroneUsed',  this.flight.TotalDroneUsed,'#76A7FA'],
        ['Total Incidents', this.flight.TotalIncidents,'#354052'],
        ['Total OnsiteTime',  this.flight.TotalOnsiteTime,'#C5A5CF'],
        ['Total TravelTime',this.flight.TotalTravelTime,'#1a74b3']
      ]);

    //   var options = {
    //     is3D: true,
    //     height:460,
    //     slices: {
    //         0: { color: '#36e4c6' },
    //         2: { color: '#a1a1a1' },
    //         3: { color: '#354052' },
    //         1: { color: '#1a74b3' },

    //       }
        
    //   };

    var options = {
        backgroundColor: '#151315',

        chartArea: {
            width:'80%',
            height:'70%'
        },
        plotOptions: {
            column: {
                minPointLength: 3
            }
        },
        legend: { position: 'top', maxLines: 1, textStyle:{color: '#FFF'},},
        bar: {groupWidth: '50%'},          
        hAxis: {
            title: '',              
            titleTextStyle:{color: '#fff', bold: true, fontSize: 18},
            textStyle:{color: '#FFF'},
            viewWindow: {
                min: ['Jan'],
                max: ['Dec']
            }
        },
        vAxis: {
            title: 'Summary Report',
            titleTextStyle:{color: '#fff', bold: true, fontSize: 18},
            textStyle:{color: '#FFF'},
            viewWindow: {
                min: [0],
                max: [this.maxRangeVal]
            }
        },
    };

      var chart = new google.visualization.ColumnChart(document.getElementById('piechart'));

      chart.draw(data, options);
  }
}