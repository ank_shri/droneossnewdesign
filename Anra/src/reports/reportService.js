import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class ReportService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getReports() {
      return this.http.fetch(this.baseApi + 'report/listing')
      .then(response => response.json());
  }

  getReport(id) {
      return this.http.fetch(this.baseApi + 'report/' + id)
      .then(response => response.json());
  }

  searchReport(Report) {
      return this.http.fetch(this.baseApi + 'report/', {
          method: 'post',
          body: json(Report)
      }).then(response => response.json());
  }

  getProjects() {
    return this.http.fetch(this.baseApi + 'project/listing')
    .then(response => response.json());
    }

  getProjectSummary(id)
  {
    return this.http.fetch(this.baseApi + 'report/ProjectSummaryReport/' + id)
    .then(response => response.json());
  }

}
