import { AureliaConfiguration } from 'aurelia-configuration';
import { inject } from "aurelia-dependency-injection";
import { EventAggregator } from "aurelia-event-aggregator";
import { FlightDataServices } from 'flightdata/flightDataServices';
import { OrganizationService } from 'organizations/organizationService';
import { ProjectService } from 'projects/projectService';
import { Router } from "aurelia-router";
@inject(
  AureliaConfiguration,
  EventAggregator, 
  FlightDataServices,
  Router,
  OrganizationService,
  ProjectService
)
export class selectMissionDialog {

  constructor(config, events, flightDataServices, router, organizationService, projectService) {
    this.config = config;
    this.events = events;  
    this.flightDataServices = flightDataServices;
    this.router = router;
    this.organizationService = organizationService;
    this.isSelectMission = false;
    this.projectService = projectService;
    this.showProjectId = -1;
    this.showMissionId = -1;
  }
  //attached() {
  //  this.setupValidationRules();
  //}
  activate() {
    
    this.role = localStorage["Roles"]
    this.getOrganizations();
    this.getProjects();
    this.getMissions();
    this.events.subscribe('show-missionDialog', p => {
      //this.isSelectMission = true;
      var updateshowdialog = setInterval(() => {
        //this.map.updateSize()
        if (this.totalProjects && this.totalOrganizations) {
          clearInterval(updateshowdialog);
          this.isSelectMission = true;
          this.events.publish('stopLoader', { loader: 'true' });
        }
       

      }, 1)
    
    })
  
  }
  attached() {
    //setTimeout(() => {
      // Filter the mission inside particular project and push to missionInsideProject array of project
    var updateProj = setInterval(() => {
      //this.map.updateSize()
      if (this.totalProjects && this.totalProjects.length > 0) {
        clearInterval(updateProj);
        this.totalProjects.map((project, index) => {
          project.missionInsideProject = []
          var updateMission = setInterval(() => {
            if (this.totalMissions && this.totalMissions.length > 0) {
              clearInterval(updateMission);
              this.totalMissions.map((mission) => {
                if (mission.ProjectName == project.Name) {
                  project.missionInsideProject.push(mission)
                }
              })
            }

          }, 1)
         
        })
     
     
      }
    }, 1)
     
       // Filter the projects inside organization project and push to projectInsideOrganization array of organization
    var updateOrganization = setInterval(() => {
      //this.map.updateSize()
      if (this.totalOrganizations && this.totalOrganizations.length > 0) {
        clearInterval(updateOrganization);
        this.totalOrganizations.map((organization, index) => {
          organization.projectInsideOrganization = []
          this.totalProjects.map((project) => {
            if (project.OrganizationName == organization.Name) {
              organization.projectInsideOrganization.push(project)
            } 
          })
        })
        
      }
      
     

    }, 1)
    
    //}, 3000)
  }
  cancelMissionDialog() {
    this.isSelectMission = false;
  }
    //Get Total Missions
  getMissions() {
    //this.isWaiting = true;
    this.flightDataServices.getFlights()
      .then(result => {
        this.totalMissions = result.Flights;
        //this.getPermission(12);
        //this.isWaiting = false;
      }).catch((e) => {
        //this.notification.error(e.message);
        //this.isWaiting = false;
      });
  }
  generateReport(gufi) {
    $('#myModal').modal('hide');
    this.isSelectMission = false;
    this.router.navigate('/workspace/inspection-report/' + gufi)
  }
   //Get Total Organizations
  getOrganizations() {
    this.organizationService.getOrganizations()
      .then(result => {
        if (localStorage["Roles"] == "Super Admin") {
          result.push({ "Name": 'ANRA Technologies' })
        }
        this.totalOrganizations = result;
      }).catch((e) => {
        //this.notification.error(e.message);
        //this.isWaiting = false;
      });
  }
  //Get Total Projects
  getProjects() {
    this.projectService.getProjects()
      .then(result => {
        //result.MissionCount=[]
        this.totalProjects = result;
      }).catch((e) => {
        //this.notification.error(e.message);
        //this.isWaiting = false;
      });
  }
  //show and hide projects inside organization
  toggleOrganization(id) {
    this.showProjectId = (this.showProjectId == id) ? -1 : id;
  }
   //show and hide missions inside projects
  toggleProject(id) {
    this.showMissionId = (this.showMissionId == id) ? -1 : id;
  }
}
