import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import {ReportService} from 'reports/reportService';
import moment from 'moment';
@inject(EventAggregator, AnraNotification, ReportService)
export class ProjectSummary{
  constructor(events, notification, reportService) {
    this.events = events;
    this.reportService = reportService;
    this.notification = notification;
    this.isWaiting = false;
    this.ProjectId='';
    this.summaryDetails=[];
  }

  attached()
  {
    this.getList();
  }
  getList()
  {
    this.isWaiting=true;
    this.reportService.getProjects()
    .then(result =>
        {
          this.projects=result;
          this.isWaiting=false;  
        });
  }
  getSummaryReport(args)
  { 
    
    if(this.ProjectId=='0')
    {
        this.notification.error('Please Select A Project');
    }
    else
    {  
       this.isWaiting=true;
       this.reportService.getProjectSummary(this.ProjectId)
       .then(result =>
          {
            this.summaryDetails=result;
            if(this.summaryDetails==[])
            {
                this.notification.error('No Data Found');
                this.isWaiting=false;
            }
            else
            {
                var data, filename, link;
                var csv = this.convertArrayOfObjectsToCSV({data: this.summaryDetails});
                if (csv == null) return;
    
                filename = args.filename || 'ProjectReport.csv';
    
                if (!csv.match(/^data:text\/csv/i)) {
                    csv = 'data:text/csv;charset=utf-8,' + csv;
                }
                data = encodeURI(csv);
    
                link = document.createElement('a');
                link.setAttribute('href', data);
                link.setAttribute('download', filename);
                link.click();
                this.isWaiting=false;
            }
       });
        
     }
    
  }
  convertArrayOfObjectsToCSV(args) {  
      var result, ctr, keys, columnDelimiter, lineDelimiter, data;

      data = args.data || null;
      if (data == null || !data.length) {
          this.notification.error("No data available to export.");          
          return null;
      }

      columnDelimiter = args.columnDelimiter || ',';
      lineDelimiter = args.lineDelimiter || '\n';

      keys = Object.keys(data[0]);

      var columnHeaders = ['Total CompletedTask','Total Flight Hours','Total Drone Used','Total Battery Used','Total Resources','Total Onsite Time','Total TravelTime'];
      
      result = '';
      result += columnHeaders.join(columnDelimiter);
      result += lineDelimiter;

      data.forEach(function(item) {
          ctr = 0;
          keys.forEach(function(key) {
              if (ctr > 0) result += columnDelimiter;

              result += item[key];
              ctr++;
          });
          result += lineDelimiter;
      });

      return result;
  }
  cancel()
  {
    this.events.publish('item-operation', {refresh: true});
    this.showDetails=false;
    var myDiv= document.getElementById("piechart");
    myDiv.innerHTML = "";
    this.ProjectId=0;
  }

  drawPieChart()
  {

    if(this.ProjectId=='0')
    {
        this.notification.error('Please Select A Project');
    }
    else
    {  
       this.isWaiting=true;
       this.reportService.getProjectSummary(this.ProjectId)
       .then(result =>
          {
            this.summaryDetails=result;
            if(this.summaryDetails==[])
            {
                this.notification.error('No Data Found');
                this.isWaiting=false;
            }
            else
            {
                for(var i = 0; i < this.summaryDetails.length; i++) {
                    var obj = this.summaryDetails[i];
                    this.TotalBatteryUsed=parseFloat(obj.TotalBatteryUsed);
                    this.TotalCompletedMission=parseFloat(obj.TotalCompletedMission);
                    this.TotalDroneUsed=parseFloat(obj.TotalDroneUsed);
                    this.TotalFlightHours=parseFloat(obj.TotalFlightHours);
                    this.TotalOnsiteTime=parseFloat(obj.TotalOnsiteTime);
                    this.TotalResources=parseFloat(obj.TotalResources);
                    this.TotalTravelTime=parseFloat(obj.TotalTravelTime);
            
                    
                }
                var data = google.visualization.arrayToDataTable([
                    ['Report_Details', 'Summary',{ role: 'style' }],
                    ['Total BatteryUsed',     this.TotalBatteryUsed,'#a1a1a1'],
                    ['Total CompletedMission',      this.TotalCompletedMission,'#36e4c6'],
                    ['Total DroneUsed',  this.TotalDroneUsed,'#354052'],
                    //['Total FlightHours', this.TotalFlightHours,'#1a74b3'],
                    ['Total OnsiteTime',    this.TotalOnsiteTime,'#C5A5CF'],
                    ['Total TravelTime',this.TotalTravelTime,'#76A7FA'],
                    ['Total Resources',this.TotalResources,'#3386FF']
                  ]);
            
            
                  // var options = {
                  //   is3D: true,
                  //   height:460,
                  //   slices: {
                  //       0: { color: '#36e4c6' },
                  //       2: { color: '#a1a1a1' },
                  //       3: { color: '#354052' },
                  //       1: { color: '#1a74b3' },
            
                  //     }
                    
                  // };

                  var options = {
                    backgroundColor: '#151315',
                    chartArea: {
                        width:'80%',
                        height:'70%'
                    },
                    plotOptions: {
                        column: {
                            minPointLength: 3
                        }
                    },
                    legend: { position: 'top', maxLines: 1,textStyle:{color: '#FFF'}, },
                    bar: {groupWidth: '50%'},          
                    hAxis: {
                        title: '',              
                        titleTextStyle:{color: 'white', bold: true, fontSize: 18},
                        textStyle:{color: '#FFF'},
                        viewWindow: {
                            min: ['Jan'],
                            max: ['Dec']
                        }
                    },
                    vAxis: {
                        title: 'Project Summary Reports',
                        titleTextStyle:{color: 'white', bold: true, fontSize: 18},
                        textStyle:{color: '#FFF'},
                        viewWindow: {
                            min: [0],
                            max: [this.maxRangeVal]
                        }
                    },
                };
            
                  var chart = new google.visualization.ColumnChart(document.getElementById('piechart'));
            
                  chart.draw(data, options);
                  this.showDetails=true;
                this.isWaiting=false;
            }
       });
        
     }
   
  }

 

}