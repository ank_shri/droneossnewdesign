﻿import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import {ReportService} from 'reports/reportService';
import {PermissionService} from 'security/permissionService';
import {Router} from 'aurelia-router';


@inject(ReportService, Router, EventAggregator,AnraNotification, PermissionService)
export class Reports{

    constructor(reportService, router, events,notification, permissionService) {
        this.reportService = reportService;
        this.router = router;
        this.events = events;
        this.notification=notification;
        this.permissionService = permissionService;
        this.heading = 'Manage Reports';
        this.reports = [];
        this.reportAggregate = null;    
        this.roleName = "";
        this.permission={};
        this.showReportList=false;
        this.showIncidentReport=false;
        this.showProjectReport=false;
        this.isWaiting = false;
    }

    activate(params) {        
        this.getList();
    }  

    attached() {
        this.subscribeEvents();
        this.getList();
    }

    subscribeEvents() {
        this.events.subscribe('item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                }
                this.reset();
            }
        });
      this.events.subscribe('stopLoader', payload => {

        if (payload) {
          if (payload.loader) {
            this.isWaiting = false;
          }
        }
      });
    }

    getList() {
        this.isWaiting = true;
        this.reportService.getReports()
          .then(result => {
              this.reports = result;
              this.showReportList=false;
              this.showProjectReport=false;
              this.isWaiting = false;

          }).catch((e) => {
              this.notification.error(e.message);
              this.isWaiting = false;
          });
    }

    reset() {
        this.reportAggregate = null;
        this.showReportList = false;
    }

  viewItem(id) {    
       this.isWaiting = true;
        this.reportService.getReport(id)
          .then(result => {
            this.isWaiting = false;
              this.reportAggregate = result;
              this.showReportList=true;
          });
    }

    searchReport(item) {
        return this.reportService.searchReport(this.reportAggregate);
    }
  onSelectMission() {
    this.isWaiting = true;
    this.events.publish('show-missionDialog', { gufi: this.gufi });
  }
  onSelectProject()
  {
    this.showProjectReport=true;

  }
}
