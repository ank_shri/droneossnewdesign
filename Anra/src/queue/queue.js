import { inject } from 'aurelia-framework';
import { QueueService } from 'queue/queueService';
import { Router } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';
import { PermissionService } from 'security/permissionService';
// import { Notification } from 'aurelia-notification';
import { HttpClient, json } from 'aurelia-fetch-client';
import { AureliaConfiguration } from 'aurelia-configuration';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';
// import 'jquery';
// import 'bootstrap-fileinput';


@inject(QueueService, Router, EventAggregator, PermissionService, AureliaConfiguration)
export class Queue {

    //Set Filters for Searchbox
    filters = [
        { value: '', keys: ['MissionId', 'MissionName', 'OrganizationName'] },
    ];

    constructor(queueService, router, events, permissionService, config) {
        this.queueService = queueService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        // this.notification = notification;
        // this.dialogService = dialogService; 
        this.baseApi = config.get('gatewayBaseUrl');
        this.heading = 'Manage Queue';
        this.threedtasks = [];
        this.task = null;
        this.selectedModelFiles = [];
        this.statusOptions = null;
        this.roleName = "";
        this.permission = {};
        this.selectedModelFiles = [];
        this.isUploading3dModel = false;
        this.isUploading2DTiles=false;
        this.isWaiting = false;
        //Set Default Paging Size
        this.pageSize = queueService.defaultPagingSize;
        this.uploadUrl = '';
        this.is3DModelProcessed = false;
        this.is3DModelSettingProcessed = false;
        this.uploadTypes = null;
        this.filteredUploadTypes = null;
        this.isUploadingOutput = false;
        this.showOutputTypes = false;
        this.uploadDialogTitle = '';
        this.fileExtensions = [];
    }

    activate() {
        this.getList();
    }

    attached() {
        this.subscribeEvents();
    }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
            .then(result => {
                this.permission = result;
                this.getRole();
            });
    }

    subscribeEvents() {
        this.events.subscribe('item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                }
            }
        });

    }

    getRole() {
        if (localStorage["Roles"].indexOf("Super Admin") > -1) {
            this.roleName = "super admin";
            return;
        }

        if (localStorage["Roles"].indexOf("Admin") > -1) {
            this.roleName = "admin";
            return;
        }

        this.roleName = "others";
    }

    getList() {
        this.isWaiting = true;
        this.queueService.getList()
            .then(result => {
                this.threedtasks = result.ThreadTasks;
                this.statusOptions = result.ProcessStatuses;               
                this.getPermission(10);
                this.isWaiting = false;
            }).catch((e) => {
                this.notification.error(e.message);
                this.isWaiting = false;
            });
    }

    showUploadTypes(item){
        this.task = item;
        this.filterUploadTypes();
        this.isUploadingOutput = false;
        this.showOutputTypes = true;
        this.uploadDialogTitle = 'Upload Types';
    }


    filterUploadTypes(){
        this.queueService.getUploadTypes(this.task.Gufi)
            .then(result => {
                this.uploadTypes = result;              
                this.filteredUploadTypes = this.uploadTypes;
                //List Out filtered extension
                this.filteredUploadTypes.map(x => this.fileExtensions.push(x.FileFormat));
                //List Out only unique extensions
                this.fileExtensions = this.fileExtensions.filter((x, i, a) => a.indexOf(x) == i);
            }).catch((e) => {
                this.notification.error(e.message);               
            });        
    }

    deleteUploadedItem(item){
      console.log('Upload Item: ',item);
        this.queueService.deleteUploadedItem(this.task.MissionId, item)
            .then(result => {
                if(result.IsSuccess){
                  this.notification.info(result.SuccessMessage);                
                }
                else{
                  this.notification.error(result.ErrorMessage);
                }
            });
    }

    uploadOutput(item){
      return;
        this.closeOutputTypeDialog();        
        this.uploadUrl = this.baseApi + 'upload/mission/' + this.task.MissionId + '/uploadtype/' + item.TypeId + '/upload-output';
        this.missionId = this.task.MissionId;
        this.gufi = this.task.Gufi;
        this.isUploadingOutput = true;
        this.events.publish('item-operation', { refresh: true }); 
        console.log('Upload Url For Output :',this.uploadUrl);
    }

    /*closeUploadDialog() {
        $('#file').fileinput('clear');
        this.isUploadingOutput = false;
        this.events.publish('item-operation', {refresh: true});  
    }*/

    close3DUploadDialog() {
        this.isUploadingOutput = false;
        this.events.publish('item-operation', {refresh: true});        
    }

    close2DUploadDialog(){
        this.isUploading2DTiles = false;
        this.events.publish('item-operation', {refresh: true});    
    }

    closeOutputTypeDialog() {       
        this.showOutputTypes = false;
        $('#myModal').modal('hide');
    }

    closeOutputUploadDialog() {
        $('#file').fileinput('clear');
        this.isUploadingOutput = false;
        this.events.publish('item-operation', { refresh: true }); 
    }

    ChangeProcessingStatus(event, item) {

        if(item.TaskStatus == 2){
            this.updateQueueStatus(item);
        }
        
        //If Status changed to Completed then check 2 things before processing
        //1 : Whether Model has been processed or not
        //2 : If Processed then only change the 3D settings of the model        

        if(item.TaskStatus == 3){            
                this.updateQueueStatus(item);
                if (item.ModelId != null) {
                    this.checkSFModelProcessingStatus(item);
                }
        }
    }

    updateQueueStatus(item){
        //Update DB
        this.queueService.save(item)
        .then(result => {
            this.notification.info('Email notification has been sent.');
            this.getList();
        });
    }

    uploadManually() {
        
        var selectedOutputs = this.filteredUploadTypes.filter(x => x.IsManualUploaded == true);
        //Update DB
        this.queueService.uploadOutputManually(this.task.Gufi, selectedOutputs)
            .then(result => {
                this.notification.info('Data output uploaded successfully into database');                
            });
    }

    display3dModelDialog(item) {
        this.selectedItem = item;

        if(item.ModelId != null){
            this.dialogService.open({viewModel: Prompt, model: '3D Model already exists.Do you want to delete it?'}).then(response => {
                if (!response.wasCancelled) {                    
                    this.delete3DModel(item);
                }
            });
        }
        else{
            this.isUploading3dModel = true;
        }
    }

    // detached() {
    //     $('#file').fileinput('destroy');
    // }

    configure3dUpload(item) {
        $('#modelFile').fileinput({
            uploadUrl: 'https://api.sketchfab.com/v3/models',
            ajaxSettings: { headers: { 'Authorization': 'Token 82a0a50f435046daa22e2a97a5535d1e' } },
            uploadExtraData: { 'isPublished': 'true', 'private': 'true', 'name': item.MissionName },
            allowedFileTypes: ['.zip'],
            uploadAsync: true,
            minFileCount: 1,
            maxFileCount: 1,
            overwriteInitial: true,
            showUploadedThumbs: false
        }).on('fileuploaded', (event, data, id, index) => {
            item.ModelId = data.response.uid;
            this.update3dTask(item,'uploaded');
        })
    }

    checkSFModelProcessingStatus(item){
        var me = this;
        var modelId = item.ModelId;
        if (modelId.length > 0){       
            $.ajax({
                url: 'https://api.sketchfab.com/v3/models/' + modelId,
                ajaxSettings: { headers: { 'Authorization': 'Token 82a0a50f435046daa22e2a97a5535d1e' } },
                type: 'GET',
                contentType: 'application/json',
                traditional: true,

                success: function (response) {
                    console.log('Model Status:', response.status.processing);
                    me.is3DModelProcessed = response.status.processing == 'SUCCEEDED' ? true : false;
                    if (me.is3DModelProcessed) {
                        if (!me.is3DModelSettingProcessed) {
                            //Update 3D Model settings
                            me.update3DModelSettings(item);                
                        }
                        me.notification.info('3D Model has been processed.');
                    }            
                    else{
                        me.notification.info('SketchFab 3D Model is not processed yet.Please try after sometime.');                        
                    }
                    console.log( 'SketchFab Model Processing Status : ' + response.status.processing );
                },

                error: function( response ) {
                    me.is3DModelProcessed = false;                
                    me.notification.info('SketchFab 3D Model processing has error.Please try after sometime.');                   
                    console.log( 'SketchFab Model Processing Error: ' + response.responseText );
                }
            });
        }        
    }

    update3DModelSettings(item){
        var me = this;
        var options = {
            'background': '{"color": "#333333"}',
            'shading':'shadeless'
        };

        var jsondata = JSON.stringify(options);

        $.ajax({
            url: 'https://api.sketchfab.com/v3/models/' + item.ModelId + '/options',
            data: jsondata,
            type: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token 82a0a50f435046daa22e2a97a5535d1e'
            },
            traditional: true,

            success: function( response ) {
                console.log('PATCH successful: ' + item.ModelId);
                me.is3DModelSettingProcessed = true;
            },

            error: function( response ) {
                console.log( 'Description PATCH failed: ' + response );
            }
        });

    }

    update3dTask(item,action) {
        
        this.queueService.updateMission3dModel(item.Gufi, item.ModelId)
            .then(response => {
                this.notification.info('3D model ' + action + ' and being process.');
            })
    }

    delete3DModel(item) {

        var objModel = item;
        var me = this;

        $.ajax({
            url: 'https://api.sketchfab.com/v3/models/' + objModel.ModelId,
            type: 'DELETE',
            headers: { 'Authorization': 'Token 82a0a50f435046daa22e2a97a5535d1e' },
                success: function(data) {
                    console.log('3DModel Deleted Successfully.');
                    objModel.ModelId = null;
                    me.update3dTask(objModel,'deleted');
                },
                error: function() {
                    console.log('delete3DModel - Some Error Happened.');
                }
        });
    }

    /*deleteOrthophotos(item){
        this.queueService.deleteOrthopictures(item.MissionId)
            .then(response => {
                this.notification.info('Orthophotos Deleted Successfully.');
            })
    }*/

    updateDefaultSetting(item) {
        this.queueService.updateDefaultSetting(this.task.MissionId, item.TypeId)
            .then(response => {
                if (response.IsSuccess) {
                    this.notification.info(response.SuccessMessage);
                }
                else {
                    this.notification.error(response.ErrorMessage);
                }                
            })
    }

    upload2DTiles(item){
      this.isUploading2DTiles = true;
      this.gufi = item.Gufi;
      this.entityName = '2d';
      this.title = "Upload 2d Tiles";
      this.fileType = '2dTiles';
      this.allowedExtensions = '.zip';
      this.uploadPath = 'mission/' + this.gufi + '/2d/Tiles';
    }    
}
