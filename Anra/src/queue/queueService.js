﻿import { inject } from 'aurelia-framework';
import { AureliaConfiguration } from 'aurelia-configuration';
import { json } from 'aurelia-fetch-client';
import { CustomHttpClient } from 'lib/customHttpClient';
// import "babel-polyfill";

@inject(CustomHttpClient, AureliaConfiguration)
export class QueueService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getRole() {
    return this.http.fetch(this.baseApi + 'users/getrole')
      .then(response => response.text());
  }

  getList() {
    return this.http.fetch(this.baseApi + 'threedtask/listing')
      .then(response => response.json());
  }

  getUploadTypes(gufi) {
      return this.http.fetch(this.baseApi + 'threedtask/uploadtypelisting/' + gufi)
          .then(response => response.json());
  }


  getDownloads(gufi)
  {
      return this.http.fetch(this.baseApi + 'threedtask/downloads/' + gufi)
          .then(response => response.json());
  }

  getUploads(gufi) {
      return this.http.fetch(this.baseApi + 'threedtask/getmissionuploads/' + gufi)
          .then(response => response.json());
  }

  save(item) {
    return this.http.fetch(this.baseApi + 'threedtask/', {
      method: 'post',
      body: json(item)
    }).then(response => response.json());
  }

  save3DSettings(item) {
      return this.http.fetch(this.baseApi + 'threedtask/save3dsettings', {
          method: 'post',
          body: json(item)
      }).then(response => response.json());
  }

  getNewTask() {
    return this.http.fetch(this.baseApi + 'threedtask/new')
      .then(response => response.json());
  }

  getMissionThreedTask(gufi) {
    return this.http.fetch(this.baseApi + 'threedtask/mission/' + gufi)
      .then(response => response.json());
  }

  getVolumeData(points, message, gufi) {
    return this.http.fetch(this.baseApi + 'threedtask/volumedata/' + gufi, {
      method: 'post',
      body: json(points)
    }).then(response => response.json());
  }

  updateMission3dModel(gufi, uid) {
    return this.http.fetch(this.baseApi + 'threedtask/mission/' + gufi, {
      method: 'put',
      body: json(uid)
    }).then(response => response.json());
  }

  uploadOrthoPictureFile(missionId, data) {
    return this.http.fetch('threedtask/mission/' + missionId + '/upload-ortho-pictures', {
      method: 'post',
      body: data
    }).then(response => {
      return response.json(); 
    })
  }

  uploadOutputManually(gufi, items) {
      return this.http.fetch('threedtask/mission/' + gufi + '/uploadOutputManually', {
          method: 'post',
          body: json(items)
      }).then(response => {
          return response.json();
      })
  }

  deleteUploadedItem(missionId, item) {
      return this.http.fetch('threedtask/mission/' + missionId + '/deleteUploadedItem', {
          method: 'post',
          body: json(item)
      }).then(response => {
          return response.json();
      })
  }

  deleteOrthopictures(missionId) {
      return this.http.fetch(this.baseApi + 'threedtask/mission/' + missionId, {
          method: 'delete'
      })
        .then(response => {
            response.json();
        }
        );
  }

  updateDefaultSetting(missionid,typeid) {
      return this.http.fetch(this.baseApi + 'threedtask/mission/' + missionid + '/updateDefaultSetting/' + typeid, {
          method: 'post'
      }).then(response => response.json());
  }
}
