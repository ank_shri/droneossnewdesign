import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
// import {Notification} from 'aurelia-notification';
import {AnraNotification} from "../components/anra-notification";
import {FlightDataServices} from 'flightdata/flightDataServices';
import moment from 'moment';
import $ from 'bootstrap';
import 'Eonasdan/bootstrap-datetimepicker';

@inject(FlightDataServices, EventAggregator, AnraNotification, NewInstance.of(ValidationController))
export class FlightData {
  @bindable flightAggregate;
  @bindable save;
  @bindable back;
  controller = null;
  rules = null;


  constructor(flightDataServices, events, notification, controller) {
    this.flightDataServices = flightDataServices;
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    // this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.temperatureUnit = localStorage["TemperatureUnit"];
    this.lengthUnit = localStorage["LengthUnit"];
    this.unitType = localStorage["UnitType"];
    this.isWaiting = false;
    this.flightStartDateTime = null;
    this.flightEndDateTime = null;
    this.StartTime = null;
    this.EndTime = null;
    this.selectedMission = 0;
    this.ShowFlightDetailForm = true;
    this.EquipmentInformationForm = false;
    this.WeatherInformationForm = false;
  }

  attached() {
    console.log(this.flightAggregate);
    if (this.flightAggregate.Missions.length > 0) {
      var scope = this;

      $('#flightDate').attr("max", moment().format('YYYY-MM-DD'));

      $(this.startdatetime).datetimepicker({
        format: 'LT'
      });

      $(this.enddatetime).datetimepicker({
        format: 'LT'
      });

      $(this.startdatetime).on('change dp.change', function (e) {
          console.log($('#startdt').val)
        $('#startdt').val($(this).data('date'));
        scope.flightStartDateTime = moment(scope.flightAggregate.FlightDate).format('MM/DD/YYYY') + ' ' + $('#startdt').val();
        console.log("flightStartDateTime",scope.flightStartDateTime);
      });

      $(this.enddatetime).on('change dp.change', function (e) {
        $('#enddt').val($(this).data('date'));
        scope.flightEndDateTime = moment(scope.flightAggregate.FlightDate).format('MM/DD/YYYY') + ' ' + $('#enddt').val();
        console.log("flightEndDateTime",scope.flightEndDateTime);
      });

      this.setupValidationRules();

      this.selectedMission = this.flightAggregate.Missions[0].MissionId;
    }
    else {
      this.notification.ShowError('No Approved Mission Found.');
      this.back();
    }
  }

  detached() {

      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.flight = this.flightAggregate.Flight;

    //Convert Weather Data as per unit system
    this.flight.Temperature = this.unitType == 'Imperial' ? ((this.flight.Temperature * 1.8) + 32).toFixed(1) : this.flight.Temperature.toFixed(1);    
    this.flight.WindSpeed = this.unitType == 'Imperial' ? (this.flight.WindSpeed * 2.237).toFixed(1) : this.flight.WindSpeed.toFixed(1);

    this.missions = this.flightAggregate.Missions;
    this.drones = this.flightAggregate.Drones;
    this.locations = this.flightAggregate.Locations;  

    this.heading = this.flight.Name != null ? 'Edit Flight Details' : 'Add Flight Details';

    if (this.flight.FlightId > 0) {

          this.StartTime = moment(this.flight.StartTime).format('h:m A');
          this.EndTime = moment(this.flight.EndTime).format('h:m A');
          this.flightStartDateTime = moment(this.flightAggregate.FlightDate).format('MM/DD/YYYY') + ' ' + this.StartTime;
          this.flightEndDateTime = moment(this.flightAggregate.FlightDate).format('MM/DD/YYYY') + ' ' + this.EndTime;
      }

  }

  rebindMission(id)
  { 
      if(id <= 0)
      {
          return;
      }
      else {
          this.selectedMission = id;
      }


      this.flightDataServices.bindMission(id)
        .then(result => {
            console.log(result);
            this.flightAggregate.PilotName = result.PilotName;
            this.flightAggregate.DroneName = result.DroneName;
            this.flightAggregate.Project = result.Project;
            this.flightAggregate.Location = result.Location;
            this.flightAggregate.Type = result.Type;
            this.flightAggregate.Battery = result.Battery;
            this.flightAggregate.Equipment = result.Equipment;
            this.flightAggregate.FlightDate = moment(result.FlightDate).format('YYYY-MM-DD');
            this.flightAggregate.Duration = result.Duration;
            this.flightAggregate.MissionComment = result.MissionComment;
            this.flightAggregate.OtherPayload = result.OtherPayload;
            this.flightAggregate.Flight.OrganizationId = result.Flight.OrganizationId;
            this.flightAggregate.Flight.MissionId = result.Flight.MissionId;
            this.flightAggregate.BatteryList=result.BatteryList;
            this.flightAggregate.SensorList=result.SensorList;
            this.flightAggregate.EquipmentList=result.EquipmentList;
            this.isEditing = false;
        });
  }

  setupValidationRules() {   

      //Custom Rule For Date Validation  
      ValidationRules.customRule(
          'date',
          (value, obj) => value === null || value === undefined || value instanceof Date || moment(value, "MM/DD/YYYY h:m A", true).isValid(),
          '\${$displayName} must be a Valid Time.'
      );

      ValidationRules.customRule(
          'comparedate',
          (value, obj, otherPropertyName) => moment(value, "MM/DD/YYYY h:m A", true).isBefore(moment(obj[otherPropertyName], "MM/DD/YYYY h:m A", true)),
          '${$displayName} must before than ${$getDisplayName($config.otherPropertyName)}',
          otherPropertyName => ({ otherPropertyName })
      );

      this.rules = ValidationRules
           .ensure('Gufi').matches(/^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/).required()   
           .ensure('Landings').matches(/^[1-9][0-9]*$/).required()
           .ensure('Distance').matches(/^[1-9][0-9]*$/).required()
           .ensure('MaxAltitude').matches(/^[1-9][0-9]*$/).required()          
           .ensure('VoltageBegin').matches(/^[1-9][0-9]*$/).required().withMessage('VoltageBegin is required.')
           .ensure('VoltageEnd').matches(/^[1-9][0-9]*$/).required() 
           .ensure('CloudCover').required()
           .ensure('Temperature').required()
           .ensure('WindSpeed').required()
           .ensure('Humidity').required()
           .ensure('StartTime').required().satisfiesRule('date').satisfiesRule('comparedate', 'EndTime')
           .ensure('EndTime').required().satisfiesRule('date')
           .on(this.flightAggregate)
           .rules;
  }

  handleCustomValidation(valresult) {
      
      var failedValidations = [];
      //Filter Only Failed Validation Related to StartTime & EndTime
      valresult.filter(x => x.valid == false && (x.propertyName == 'StartTime' || x.propertyName == 'EndTime')).map(x => { failedValidations.push(x) });

      if (failedValidations.length > 0) {          
          for (let i = 0; i < failedValidations.length; i++) {
              if (failedValidations[i].propertyName == 'StartTime') {
                  $('#startdt').parent().addClass('has-error');
              }
              if (failedValidations[i].propertyName == 'EndTime') {
                  $('#enddt').parent().addClass('has-error');
              }
          }
      }
  }

  resetCustomValidation(valresult) {

      var failedValidations = [];
      //Filter Only Failed Validation Related to StartTime & EndTime
      valresult.filter(x => x.valid == false && (x.propertyName == 'StartTime' || x.propertyName == 'EndTime')).map(x => { failedValidations.push(x) });

      if (failedValidations.length > 0) {
          for (let i = 0; i < failedValidations.length; i++) {
              if (failedValidations[i].propertyName == 'StartTime') {
                  $('#startdt').parent().removeClass('has-error');
              }
              if (failedValidations[i].propertyName == 'EndTime') {
                  $('#enddt').parent().removeClass('has-error');
              }
          }
      }
  }


  saveItem() {
      this.isWaiting = true;
      var datetimeStart = moment(this.flightStartDateTime, "MM/DD/YYYY h:m A", true);
      var datetimeEnd = moment(this.flightEndDateTime, "MM/DD/YYYY h:m A", true);

      this.flight.StartTime = datetimeStart.isValid() ? datetimeStart.format("MM/DD/YYYY h:m A") : null;
    this.flight.EndTime = datetimeEnd.isValid() ? datetimeEnd.format("MM/DD/YYYY h:m A") : null;


    //Convert Weather Data as per default unit system i.e. metric system
    this.flight.Temperature = this.unitType == 'Imperial' ? ((this.flight.Temperature - 32) * 1.8).toFixed(1) : this.flight.Temperature.toFixed(1);
    this.flight.WindSpeed = this.unitType == 'Imperial' ? (this.flight.WindSpeed / 2.237).toFixed(1) : this.flight.WindSpeed.toFixed(1);


      this.controller.validate({ object: this.flight, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.save()
                      .then(result => {

                          if (result.IsSuccess) {
                              this.notification.ShowInfo(result.SuccessMessage);
                              this.events.publish('flight-item-operation', { refresh: true });
                          }
                          else {
                              this.notification.ShowError(result.ErrorMessage);
                          }                          
						    this.isWaiting = false;
                      })
              } 
              else {
                let messages = this.controller.errors.map(x => x.message);
                console.log(messages);
                this.application.toast(
                  Object.assign({ container: this.errorHolder }, {
                    title: 'Errors!',
                    message: messages.join(', '),
                    theme: 'danger',
                    timeout: 2000,
                    glyph: 'glyph-alert-info'
                  })
                );
                //   this.handleCustomValidation(valresult.results);
                //   // validation failed
                //   this.notification.ShowError('Please enter a valid input.');
                //   this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.ShowError('Error.');
              this.isWaiting = false;
          });

      this.isWaiting = false;
  }

  cancel() {
      //Reset Custom Validation
      this.resetCustomValidation(this.controller.results);

      if(this.back){
          this.back();
      }
  }

  calcDuration() {
      var startDate = moment(this.flightStartDateTime, "MM/DD/YYYY hh:mm A").toDate();
      var endDate = moment(this.flightEndDateTime, "MM/DD/YYYY hh:mm A").toDate();
      var msec = endDate.getTime() - startDate.getTime();
      var minute = Math.abs(msec / 1000 / 60);
      console.log(msec);
      console.log(minute);

      this.flightAggregate.Duration = isNaN(minute) ? 0 : parseFloat(minute.toFixed(2));
      console.log(this.flightAggregate.Duration)
  }
}
