import {inject} from 'aurelia-framework';
import {FlightDataServices} from 'flightdata/flightDataServices';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import { PermissionService } from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
import {AnraNotification} from "../components/anra-notification";
import { AureliaConfiguration } from 'aurelia-configuration';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';


@inject(FlightDataServices, Router, EventAggregator, PermissionService,AnraNotification, AureliaConfiguration)
export class FlightDataListing {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['MissionName','DroneName','Duration','StatusText','OrganizationName']},
    ];

    constructor(flightDataServices, router, events, permissionService, notification, config) {
    this.flightDataServices = flightDataServices;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;   
    this.notification = notification;
    this.config = config;
    this.heading = 'Manage Flight Log';
    this.flights = [];
    this.flightstocsv=[];
    this.downloads = [];
    this.flightAggregate = null;
    this.isEditing = false;
    this.isViewing = false;
    // this.flightdataRoute = router.routes[30].navModel;
    this.baseUrl = this.config.get('authConfig').baseUrl;
    this.permission = {};
    // this.dialogService = dialogService;
    //Set Default Paging Size
    this.pageSize = flightDataServices.defaultPagingSize;
    this.isWaiting = false;
    this.showDownloadDialog = false;
    this.availableOutputs = null;
    this.outputCategories = null;
    this.filteredAvailableOutputs = null;
    this.noOutputAvailable = true;
    this.selectedCategory = null;
    this.selectedType = null;
    this.selectedOutput = null;
    this.isWaiting = false;
  }

  activate(params) {
    if(params)
    {
      this.id = params.id;
    if (isNaN(parseInt(this.id, 10))) {
        this.getList();   
             
      // this.flightdataRoute.isActive = true;
    }
    else {
        this.editItem(this.id);
    }
    } 
    this.getList();   
  }

  // deactivate() {
  //   this.flightdataRoute.isActive = false;
  // }

  attached() {
    this.subscribeEvents();
    
  }
  detached() {
    this.subscriber.dispose();
  }
  getPermission(moduleId) {
    this.permissionService.getPermission(moduleId)
      .then(result => {
        this.permission = result;
      });
  }

  subscribeEvents() {
    this.subscriber=this.events.subscribe('flight-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    //this.subscriber= this.events.subscribe('item-operation-edit', payload => {

    //    if (payload) {
    //        if (payload.id > 0) {
    //            this.editItem(payload.id);
    //        }
    //        this.reset();
    //    }
    //});

  }

  //Convert For CSV Convert
  ConvertNewJson()
  {
    var newJson=[];
    this.flights.forEach(function(item) {
    newJson.push({
      "Mission Name": item.MissionName,
      "Date":moment(item.StartTime).format('YYYY-MM-DD'),
      "Organization":item.OrganizationName,
      "Drone":item.DroneName,
      "Duration":item.Duration,
      "Status":item.StatusText,
      "Created Date":moment(item.DateCreated).format('YYYY-MM-DD')
                });
    });
    this.flightstocsv=newJson;
  }

  getList() {
      this.isWaiting = true;
    this.flightDataServices.getFlights()
      .then(result => {
        this.flights = result.Flights;
        this.isEditing = false;
        this.isViewing = false;
        this.getPermission(12);
        this.isWaiting = false;
        this.ConvertNewJson();
      }).catch((e) => {
      this.notification.ShowError(e.message);
      this.isWaiting = false;
    });
  }

  getMissionOutputList(id) {
      this.isWaiting = true;
      this.flightDataServices.getMissionOutputs(id)
        .then(result => {
            this.outputCategories = result.UploadCategories;
            this.availableOutputs = result.UploadMaster;
            this.isEditing = false;
            this.isViewing = false;
            this.getPermission(12);
            this.isWaiting = false;
        }).catch((e) => {
            this.notification.ShowError(e.message);
            this.isWaiting = false;
        });
  }

  filterAvailableOutputs(category){

      this.filteredAvailableOutputs = this.availableOutputs.filter(x => x.CategoryName == category);

      if(this.filteredAvailableOutputs.length == 0){
          this.noOutputAvailable = true;          
      }
      else{
          this.noOutputAvailable = false;
      }
  }

  setCategory(event){
      if(event.target.selectedOptions.length > 0 && event.target.selectedOptions[0].index > 0){
          this.selectedCategory = event.target.selectedOptions[0].text;
          this.filterAvailableOutputs(this.selectedCategory);
      }              
  }

  setType(event){
      if(event.target.selectedOptions.length > 0 && event.target.selectedOptions[0].index > 0){
          this.selectedType = event.target.selectedOptions[0].text; 
          this.selectedOutput =  this.availableOutputs.filter(x => x.CategoryName == this.selectedCategory && x.TypeName == this.selectedType);
      }              
  }

  downloadOutput(item)
  {
      //Code to download File Locally
      var downloadItem = item;
      var fileName = downloadItem.FileName;
      var filePath = downloadItem.FileLocation;
      var url = filePath + fileName;
      var link = document.createElement('a');
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.click();
  }

  reset() {
    this.flightAggregate = null;
    this.isEditing = false;
    this.isViewing = false;

  }

  add() {
    this.isWaiting = true;
    this.flightDataServices.getNewFlight()
      .then(result => {
        this.flightAggregate = result;
        this.isEditing = true;
        this.isWaiting = false;
      });

  }

  editItem(id) {
      this.isWaiting = true;
    this.flightDataServices.getFlight(id)
      .then(result => {        
        this.flightAggregate = result;
        this.flightAggregate.FlightDate = moment(result.FlightDate).format('YYYY-MM-DD');
        this.isEditing = true;
        this.isWaiting = false;
      });
  }

  
  deleteItem(id) {
    this.dialogService.open({viewModel: Prompt, model: 'Are you sure you want to delete?'}).then(response => {
         if (!response.wasCancelled) {
    this.flightDataServices.deleteFlight(id)
      .then(result => {
				  this.notification.ShowError('Deleted Successfully.');
        this.getList();
			  });
         }
      });
  }

  viewItem(id) {
      this.router.navigate('flight-data-listing/flightdata/' + id);
  }

  saveItem(item) {
    return this.flightDataServices.saveFlight(this.flightAggregate.Flight);
  }

  closeViewDetails(){
    this.flightAggregate = null;
    this.isEditing = false;
    this.getList();
  }

  displayDownloadDialog(item){
      this.getMissionOutputList(item.MissionId);
      this.showDownloadDialog = true;
  }

  closeDialog() {
      this.selectedCategory = null;
      this.noOutputAvailable= null;
      this.showDownloadDialog = false;
  }

  closeOutputTypeDialog() {
      this.showDownloads = false;
      $('#myModal').modal('hide');
  } 
  workspaceRoute(gufi) {
    //href = "#/workspace/inspection/${item.Gufi}"
    this.router.navigate('workspace/inspection/' + gufi);
  }
  //Download CSV
  convertArrayOfObjectsToCSV(args) {  
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        this.notification.ShowError("No data available to export.");          
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    var columnHeaders = ['Mission','Date','Organization','Drone','Duration','Status','Date Created'];
    
    result = '';
    result += columnHeaders.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
  }

    downloadCSV(args) {  
    var data, filename, link;

    //Setting Entity Attributes

    var csv = this.convertArrayOfObjectsToCSV({data:this.flightstocsv});
    if (csv == null) return;

    filename = args.filename || 'FlightSummaryReport.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();



    }
}
