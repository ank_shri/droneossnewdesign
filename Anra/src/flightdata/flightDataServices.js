import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class FlightDataServices {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getFlights() {
      return this.http.fetch(this.baseApi + 'flight/listing')
      .then(response => response.json());
  }

  getNewFlight() {
      return this.http.fetch(this.baseApi + 'flight/NewFlight')
      .then(response => response.json());
  }

  getFlight(id) {
      return this.http.fetch(this.baseApi + 'flight/' + id)
      .then(response => response.json());
  }

  getMissionOutputs(id) {
      return this.http.fetch(this.baseApi + 'flight/mission/' + id)
      .then(response => response.json());
  }

  bindMission(id) {
      return this.http.fetch(this.baseApi + 'flight/bindmission/' + id)
        .then(response => response.json());
  }

  deleteFlight(id) {
    return this.http.fetch(this.baseApi + 'flight/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }
  saveFlight(flight) {
    return this.http.fetch(this.baseApi + 'flight/', {
      method: 'post',
      body: json(flight)
    }).then(response => response.json());
  }
}
