import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
// import {Notification} from 'aurelia-notification';
import {Router} from 'aurelia-router';
import {AureliaConfiguration} from "aurelia-configuration";
import {FlightDataServices} from 'flightdata/flightDataServices';
import moment from 'moment';

import * as L from 'leaflet';
import * as D from 'leaflet-draw';
import * as T from 'SpatialServer/Leaflet.MapboxVectorTile';

@inject(EventAggregator, Router, AureliaConfiguration, FlightDataServices)
export class FlightDataView {
  @bindable flightAggregate;
  @bindable save;
  @bindable back;

    constructor(events, router, configure, flightDataServices) {
        this.events = events;
        this.router = router;
        this.temperatureUnit = localStorage["TemperatureUnit"];
        this.lengthUnit = localStorage["LengthUnit"];
        this.configuration = configure;
        this.flightDataServices = flightDataServices;
        this.flightAggregate = null;
        this.flightdataid = null;
        this.flightid = null;
        this.showMap = false;
        this.isWaiting = false;
        this.weatherApiKey = this.configuration.get('weatherApiKey');
        this.weatherData = null;
        this.weatherLocation = '';
        this.weatherObservationDate = '';
        this.weatherCondition = '';
        this.weatherIconUrl = '';
        this.weatherTemprature = '';
        this.weatherHumidity = '';
        this.weatherWindSpeed = '';
        this.unitType = localStorage["UnitType"];
        this.weatherApiKeyId='42f44d51b70793b9a955f8fec6155988';
    }

    activate(params) {
        this.isWaiting = true;
        if(params.flightdataid)
        {
            this.flightdataid=params.flightdataid;

            //Call to get Flight Data Service
            this.flightDataServices.getFlight(this.flightdataid)
                .then(result => {
                  
                    this.flightAggregate = result;
                    this.flightAggregate.FlightDate = moment(result.FlightDate).format('YYYY-MM-DD');
                    this.flight = this.flightAggregate.Flight;
                    this.mission = this.flightAggregate.Missions.length > 0 ? this.flightAggregate.Missions[0] : null;
                    this.drones = this.flightAggregate.Drones;
                    this.locations = this.flightAggregate.Locations;
                  
                    if (this.flight.Packets && this.flight.Packets.length > 0) {
                        this.showMap = true;
                        //this.initMap(this.flight.Packets[0]);
                        setTimeout(() => {
                            this.initMap(this.flight.Packets[0]);  
                        }, 1000);
                    }

                  //Set Weather
                  this.weatherLocation = this.flight.WeatherLocation;
                  this.weatherCondition = this.flight.WeatherCondition;
                  this.weatherIconUrl = this.flight.WeatherIconUrl;
                  this.weatherTemprature = this.unitType == 'Imperial' ? ((this.flight.Temperature * 1.8) + 32).toFixed(1) : this.flight.Temperature.toFixed(1);
                  this.weatherHumidity = this.flight.Humidity;
                  this.weatherWindSpeed = this.unitType == 'Imperial' ? (this.flight.WindSpeed * 2.237).toFixed(1) : this.flight.WindSpeed.toFixed(1);

                    this.heading = this.flight.Name != null ? 'View Flight Details' : 'Add Flight Details';
                    this.isWaiting = false;
                });

        }

        if(params.flightid)
        {
            this.flightid= params.flightid;

            //Call to get Flight Data Service
            this.flightDataServices.getFlight(this.flightid)
                .then(result => {                    
                    this.flightAggregate = result;
                    this.flightAggregate.FlightDate = moment(result.FlightDate).format('YYYY-MM-DD');
                    this.flight = this.flightAggregate.Flight;
                    this.mission = this.flightAggregate.Missions.length > 0 ? this.flightAggregate.Missions[0] : null;
                    this.drones = this.flightAggregate.Drones;
                    this.locations = this.flightAggregate.Locations;
                  
                    if (this.flight.Packets && this.flight.Packets.length > 0) {
                        this.showMap = true;                        
                        setTimeout(() => {
                            this.initMap(this.flight.Packets[0]);  
                        }, 1000);
                    }

                  //Set Weather
                  this.weatherLocation = this.flight.WeatherLocation;
                  this.weatherCondition = this.flight.WeatherCondition;
                  this.weatherIconUrl = this.flight.WeatherIconUrl;
                  this.weatherTemprature = this.unitType == 'Imperial' ? ((this.flight.Temperature * 1.8) + 32).toFixed(1) : this.flight.Temperature.toFixed(1);
                  this.weatherHumidity = this.flight.Humidity;
                  this.weatherWindSpeed = this.unitType == 'Imperial' ? (this.flight.WindSpeed * 2.237).toFixed(1) : this.flight.WindSpeed.toFixed(1);

                    this.heading = this.flight.Name != null ? 'View Flight Details' : 'Add Flight Details';
                    this.isWaiting = false;
                });
        }

        if(params.Profileflightdataid)
        {
            this.Profileflightdataid= params.Profileflightdataid;

            //Call to get Flight Data Service
            this.flightDataServices.getFlight(this.Profileflightdataid)
                .then(result => {                    
                    this.flightAggregate = result;
                    console.log(result);
                    this.flightAggregate.FlightDate = moment(result.FlightDate).format('YYYY-MM-DD');
                    this.flight = this.flightAggregate.Flight;
                    this.mission = this.flightAggregate.Missions.length > 0 ? this.flightAggregate.Missions[0] : null;
                    this.drones = this.flightAggregate.Drones;
                    this.locations = this.flightAggregate.Locations;
                  
                    if (this.flight.Packets && this.flight.Packets.length > 0) {
                        this.showMap = true;
                        setTimeout(() => {
                            this.initMap(this.flight.Packets[0]);  
                        }, 1000);
                    }

                  //Set Weather
                  this.weatherLocation = this.flight.WeatherLocation;
                  this.weatherCondition = this.flight.WeatherCondition;
                  this.weatherIconUrl = this.flight.WeatherIconUrl;
                  this.weatherTemprature = this.unitType == 'Imperial' ? ((this.flight.Temperature * 1.8) + 32).toFixed(1) : this.flight.Temperature.toFixed(1);
                  this.weatherHumidity = this.flight.Humidity;
                  this.weatherWindSpeed = this.unitType == 'Imperial' ? (this.flight.WindSpeed * 2.237).toFixed(1) : this.flight.WindSpeed.toFixed(1);

                    this.heading = this.flight.Name != null ? 'View Flight Details' : 'Add Flight Details';
                    this.isWaiting = false;
                });
        }
    }

    getWeather() {      
        var me = this; 
        var weatherLocation = this.mission.BaseLat + ',' + this.mission.BaseLng + '.json';

        $.ajax({
            url: 'https://api.openweathermap.org/data/2.5/weather?lat='+this.mission.BaseLat+'&lon='+this.mission.BaseLng+'&APPID='+this.weatherApiKeyId,              
            type: 'GET',              
            success: function (response) {

                if (response) {
                    console.log('Response',response);
                    me.weatherData = JSON.parse(JSON.stringify(response));
                    me.weatherLocation = me.weatherData.name;                      
                    me.weatherCondition = me.weatherData.weather[0].main;
                    me.weatherIconUrl = "http://openweathermap.org/img/w/" + me.weatherData.weather[0].icon + ".png"; 
                    me.weatherTemprature = me.unitType == 'Imperial' ? (((me.weatherData.main.temp - 273.15) * 1.8) + 32).toFixed(1) : (me.weatherData.main.temp - 273.15).toFixed(1);                      
                    me.weatherHumidity = me.weatherData.main.humidity;
                    me.weatherWindSpeed = me.unitType == 'Imperial' ? (me.weatherData.wind.speed * 2.237) : me.weatherData.wind.speed;
                }
                else {
                    me.notification.error('Weather details not found.Please try again.');
                }
            },

            error: function (response) {                  
                console.log('Weather Data Fetching Failed');
            }
        });      
    }
  
    cancel() {
        this.flightdataid != null ? this.router.navigate('#/dashboard') : this.Profileflightdataid != null ? this.router.navigate('#/profile') : this.router.navigate('#/flight-data-listing');        
    }

    edit(flightId) {
        this.router.navigate('flight-data-listing/' + flightId);      
    }


    initMap(packet) {
        if (!this.map) {

            let accessToken = this.configuration.get('mapboxAccessToken');

            var streetMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=' + accessToken, {
                attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
            });

            var satelliteMap = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
                attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
            });

            let streetDarkMap = L.tileLayer(
                'https://api.mapbox.com/styles/v1/aganjoo/cjm2gw5p406qa2snwya18clid/tiles/256/{z}/{x}/{y}?access_token=' +
                accessToken, {
                  attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
                }
              );
        

            this.baseMaps = {
                "Satellites": satelliteMap,
                "Streets": streetMap
            };

            let map = L.map('map', {
                zoomControl: false,
                drawControl: false,
                editable: false,
                maxNativeZoom: 22,
                layers: [streetDarkMap]
            });

            //this.layerControl = L.control.layers(this.baseMaps, this.overlayMaps).addTo(map);

            L.control.zoom({
                maxZoom: 22,
                position: 'topleft'
            }).addTo(map);

            map.doubleClickZoom.disable();

            L.Icon.Default.imagePath = 'styles/images';
            this.map = map;
        }
        else{
            this.map.removeLayer(this.path);
        }
        
        this.map.setView(new L.LatLng(packet.Latitude, packet.Longitude), 22);

        let pointList = this.flight.Packets.map(x => new L.LatLng(x.Latitude, x.Longitude, x.Altitude));
        this.path = L.polyline(pointList, {color: '#37e4c6', weight: 2, opacity: 0.8, smoothFactor: 1}).addTo(this.map);

    }
}

export class FilterOnPropertyValueConverter {
    toView(array, property, exp, name) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }

        var arr = array.filter((item) => item[property] == (exp));

        return arr.length > 0 ? arr[0][name] : '';
    }
}
