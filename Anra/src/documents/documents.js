import {inject} from 'aurelia-framework';
import {DocumentService} from 'documents/documentService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {PermissionService} from 'security/permissionService';
import {AnraNotification} from "../components/anra-notification";
// import {DialogService} from 'aurelia-dialog';
// import { Prompt } from 'components/my-modal';
import { Utility } from 'common/utility';

@inject(DocumentService, Router, EventAggregator, PermissionService,AnraNotification)
export class Documents {

    constructor(documentService, router, events, permissionService,notification) {
        this.documentService = documentService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        this.baseApi = this.documentService.baseApi;
        this.heading = 'Manage Documents';
        this.documents = [];
        this.documentTableId = 0;
        this.displayTab=false;
        this.notification= notification;
        this.document = null;
        this.isEditing = false;
        this.documentRoute = router.routes[7].navModel;
        this.permission={};
        this.isWaiting = false;
    }

    activate(params) {
        this.getPermission(13);
        this.getList();
        this.documentRoute.isActive = true;
    }

    deactivate() {
        this.documentRoute.isActive = false;
    }

    attached() {
        this.subscribeEvents();
    }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
          });
    }

    subscribeEvents() {
        this.events.subscribe('item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                }
                this.reset();
            }
        });
    }


    getList() {

        this.isWaiting = true;
        this.documentService.getDocumentList()
          .then(result => {
            this.documents = result;

            if (this.documents.length <= 0) {
              this.notification.ShowError('No Documents Found.');        
            }
                        
            this.isEditing = false;
            this.isWaiting = false;
          }).catch((e) => {
              this.notification.ShowError(e.message);
              this.isWaiting = false;
          });
    }

    reset() {
        this.document = null;
        this.isEditing = false;
    }

    add(id) {
        this.router.navigate('documents/document/' + id);
    }

    editItem(id) {
        this.router.navigate('documents/document/' + id);
    }

    deleteItem(id) {
        this.notification.Confirm("Are you sure you want to delete?").then( result => {
           if(result){
            this.documentService.deleteDocument(id)
            .then(result => {
                this.getList();
            });
           }
        })
    }

    downloadDocument(url)
    {        
        Utility.downloadFile(url);
    }

    showDocumentTable(document){        
        document.OpenTable=!document.OpenTable;
    }

    cancel(){
        this.router.navigate("#/missions");
    }

}
