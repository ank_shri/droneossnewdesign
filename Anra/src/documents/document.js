﻿import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject, NewInstance} from 'aurelia-dependency-injection';
import {ValidationController, ValidationRules, validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {AnraNotification} from "../components/anra-notification";
import {Router} from 'aurelia-router';
import {DocumentService} from 'documents/documentService';
import { Utility } from 'common/utility';
import {UIDialogService} from "aurelia-ui-framework";
import {Dialog} from '../components/dialog';

@inject(EventAggregator, Router, AnraNotification, DocumentService, NewInstance.of(ValidationController),UIDialogService)
export class Document {
  controller = null;
  rules = null;

  constructor(events, router, notification, documentService, controller,dlgService) {
    this.router = router;
    this.events = events;
    this.notification = notification;
    this.documentService = documentService;
    this.asset = null;
    this.gufi = null;
    this.docId = null;
    this.fileType = 'Document';    
    this.documents=[];
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
    this.uploadedFileName = '';
    this.userId = localStorage["UserId"];
    this.baseApi = this.documentService.baseApi;
    this.documentTableId = 0;
    this.displayTab=false;
    this.isView=false;
    this.document = null;
    this.isEditing = false;
    this.documentRoute = router.routes[7].navModel;
    this.permission={};
    this.dlgService = dlgService;
  }

  activate(params) {
    this.isWaiting=true;
    if (params.missionid) {
      this.gufi = params.missionid;
      //Add Document Case When Redirect From Mission Listing Page
      this.documentService.getNewDocument()
        .then(result => {

          this.asset = result;
          this.asset.ReferenceId = this.gufi;
          this.heading = this.asset.AssetId != 0 ? 'Edit Document' : 'Add Document';
          this.documentService.getDocumentList()
            .then(result => {
              this.documents = result;
              var selectedMission2 = this.documents.filter(x => x.ReferenceId === this.gufi);
              this.document=selectedMission2[0];
              if(this.document=='' || this.document==null)
              {
                this.isView=false;
                this.isWaiting=false;
              }
              else
              {
                this.isView=true;
                this.isWaiting=false;
              }
              if (this.documents.length <= 0) {
                this.notification.ShowError('No Documents Found.');        
              }
              
            }).catch((e) => {
                this.notification.ShowError(e.message);          
            });
          this.isWaiting = false;
        });
    }

    if (params.docid) {
      this.docId = params.docid;

      if (parseInt(this.docId) > 0) {
        //Edit Document Case
        this.documentService.getDocument(parseInt(this.docId))
          .then(result => {
            this.asset = result;
            this.uploadedFileName = this.asset.AssetLocation.substring(this.asset.AssetLocation.lastIndexOf('/') + 1);
            this.heading = this.asset.AssetId != 0 ? 'Edit Document' : 'Add Document';
            this.documentService.getDocumentList()
            .then(result => {
              this.documents = result;
              var selectedMission2 = this.documents.filter(x => x.ReferenceId === this.asset.ReferenceId);
              this.document=selectedMission2[0];
              if(this.document=='' || this.document==null)
              {
                this.isView=false;
                this.isWaiting=false;
              }
              else
              {
                this.isView=true;
                this.isWaiting=false;
              }
        
              
            }).catch((e) => {
                this.notification.ShowError(e.message);          
            });
            this.isWaiting = false;
          });
      }
      else {
    
        //Add Document Case
        this.documentService.getNewDocument()
          .then(result => {
            this.asset = result;
            this.asset.userId = localStorage["UserId"]
            this.heading = this.asset.AssetId != 0 ? 'Edit Document' : 'Add Document';
            this.isWaiting = false;
          });
      }
    }
  }

  setupValidationRules() {

    this.rules = ValidationRules
      .ensure('MissionId').matches(/^[1-9][0-9]*$/).required()
      .rules;
  }

  detached() {
    //Finally Reset Validation
    this.controller.reset();
  }

  cancel() {
    this.router.navigate("#/documents");
    // this.gufi != null ? this.router.navigate('#/missions') : this.router.navigate('#/documents');
  }

  setMissionItems() {
    var selectedMission = this.asset.MissionLookup.find(x => x.Gufi === this.asset.ReferenceId);
    this.asset.MissionName = selectedMission.Name;
    var Gufi=selectedMission.Gufi;
    this.isWaiting=true;
    this.documentService.getDocumentList()
    .then(result => {
      this.documents = result;
      var selectedMission2 = this.documents.filter(x => x.ReferenceId === selectedMission.Gufi);
      this.document=selectedMission2[0];
      if(this.document=='' || this.document==null)
      {
        this.isView=false;
        this.isWaiting=false;
      }
      else
      {
        this.isView=true;
        this.isWaiting=false;
      }
      if (this.documents.length <= 0) {
        this.notification.ShowError('No Documents Found.');        
      }
      
    }).catch((e) => {
        this.notification.ShowError(e.message);          
    });
    

  }

  showUpload() {
    this.controller.validate({object: this.asset, rules: this.rules})
      .then(valresult => {
        if (valresult.valid) {
          // validation succeeded
          this.events.publish('show-generic-upload', {               
            title: 'Upload Document', 
            referenceId: this.asset.ReferenceId,
            entityId: this.asset.AssetId, 
            fileType: this.fileType, 
            uploadPath: 'assets',
            userId: this.userId,
          });
        }
        else {
          // validation failed
          this.notification.ShowError('Please enter a valid input.');
        }
      }).catch((e) => {
      this.notification.ShowError('Error.');
    });
  }

  close() {
    this.cancel();
  }

  //add table
  downloadDocument(url)
  {        
      Utility.downloadFile(url);
  }


}
