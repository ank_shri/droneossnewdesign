﻿import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class DocumentService {

    constructor(http, config) {
        this.config = config;
        this.http = http;
        this.baseApi = this.config.get('gatewayBaseUrl');
    }

    getDocumentList() {
        return this.http.fetch(this.baseApi + 'Asset/listing')
          .then(response => response.json());
    }

    getNewDocument() {
        return this.http.fetch(this.baseApi + 'Asset/NewAsset')
          .then(response => response.json());
    }

    getDocument(id) {
        return this.http.fetch(this.baseApi + 'Asset/' + id)
          .then(response => response.json());
    }

    deleteDocument(id) {
        return this.http.fetch(this.baseApi + 'Asset/' + id, {
            method: 'delete'
        })
          .then(response => {
              response.json();
          }
          );
    }    
}
