import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class OrganizationService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getRole() {
      return this.http.fetch(this.baseApi + 'users/getrole')
      .then(response => response.text());
  }

  getOrganizations() {
      return this.http.fetch(this.baseApi + 'Organization/listing')
      .then(response => response.json());
  }

  getNewOrganization() {
    return this.http.fetch(this.baseApi + 'Organization/NewOrganization')
      .then(response => response.json());
  }

  getNewNotificationPreference() {
    return this.http.fetch(this.baseApi + 'Organization/NewNotificationPreference')
      .then(response => response.json());
  }

  getOrganization(id) {
    return this.http.fetch(this.baseApi + 'Organization/' + id)
      .then(response => response.json());
  }

  deleteOrganization(id) {
    return this.http.fetch(this.baseApi + 'Organization/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }
  saveOrganization(Organization) {
    return this.http.fetch(this.baseApi + 'Organization/', {
      method: 'post',
      body: json(Organization)
    }).then(response => response.json());
  }

  savePreference(preference) {
    return this.http.fetch(this.baseApi + 'Organization/SavePreference/', {
      method: 'post',
      body: json(preference)
    }).then(response => response.json());
  }

  getPreferences() {
    return this.http.fetch(this.baseApi + 'Organization/PreferenceListing')
      .then(response => response.json());
  }

  getPreference(id) {
    return this.http.fetch(this.baseApi + 'Organization/Preference/' + id)
      .then(response => response.json());
  }

  deletePreference(id) {
    return this.http.fetch(this.baseApi + 'Organization/Preference/' + id, {
      method: 'delete'
    })
      .then(response => {
        response.json();
      }
      );
  }
}
