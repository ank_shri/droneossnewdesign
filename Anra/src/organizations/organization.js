import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Notification} from 'aurelia-notification';


@inject(EventAggregator, Notification, NewInstance.of(ValidationController))
export class Organization {
  @bindable organization;
  @bindable save;
  controller = null;
  rules = null;

  constructor(events, notification, controller) {
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.selectedFile = null;
    this.allowedFiles = [".gif",".jpg", ".jpeg", ".png"];
    this.ConfirmPassword='';
    this.IsPasswordsMatched=true;
    this.isWaiting = false;
    this.organizationLogoBox= false;
  }

  attached() {

    if (this.organizationLogoBox && this.organization.LogoFile != undefined) {
      //Set Logo file in Edit mode
      let imgCtrl = document.getElementById('OrgUpload');
      imgCtrl.src = this.organization.LogoFile;
    }

    this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.heading = this.organization.Name != null ? 'Edit Organization' : 'Add Organization';

    this.organizationLogoBox = this.organization.Name != null ? true : false;
  }

  setupValidationRules() {   

      this.rules = ValidationRules
           .ensure('Name').required()    
           .ensure('ContactEmail').email().required()
           .ensure('ContactPhone').required()
           .ensure('Address').required()           
           .ensure('CurrencyId').matches(/^[1-9][0-9]*$/).required()
           .ensure('UnitId').matches(/^[1-9][0-9]*$/).required()
           .ensure('Password').matches(/^(?=.*\d)(?=.*[a-z]).{6,}$/).required()
           .ensure('Website').required()
           .ensure('Email').email().required()
           .ensure('FirstName').required()
           .ensure('LastName').required()
           .ensure('GovtLicenseNumber').maxLength(20)
            .rules;
  }

  removeRule(propName){
      let list = this.rules[0];
      for( var i = list.length; i--;){
          if ( list[i].property.name === propName) list.splice(i, 1);
      }
  }

  saveItem() {
      this.isWaiting = true;

      //In case of Edit, skip Password Validation
      if(this.organization.OrganizationId > 0){
          this.removeRule('Password');
      }

      this.controller.validate({ object: this.organization, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded

                  //Check For File Extension Validation
                  if(this.validateFileExtension(this.selectedFile))
                  {
                      //Compare Passwords
                      if(this.comparePassword()){
                          return this.save()
                              .then(result => {
                                  this.handleSaveResult(result);
                                  this.isWaiting = false;
                              });
                      }
                  }
                  else
                  {
                      this.notification.error("Please upload files having extensions: " + this.allowedFiles.join(', ') + " only.");
                      this.organization.logoFile=null;
                  }
              } 
              else {
                  // validation failed
                  this.notification.error('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
          });

      this.isWaiting = false;
      $('#ctrllogoFile').val('');
      this.organizationLogoBox = false;
  }

  fileSelected(event) {
    let reader = new FileReader();
    let file = event.target.files[0];
    reader.readAsDataURL(file);
    var out=document.getElementById('OrgUpload');
    let fileName = file.name;
    reader.onload = () => {
      if (this.organization.LogoFile != null)
        {
            let res = reader.result;
            out.src = res;
            this.organization.LogoFile=out.src;
            this.organizationLogoBox = true;
        }
        else{
            out.src=reader.result;
            this.organization.LogoFile=out.src;
            this.organizationLogoBox = true;
        }
    };

    this.selectedFile = fileName;
  }


  handleSaveResult(result){
      if (result.IsSuccess) {
          this.notification.info(result.SuccessMessage);
        this.events.publish('organization-item-operation', {refresh: true});
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
    this.ConfirmPassword='';
  }

  cancel() {
      this.ConfirmPassword='';
      this.IsPasswordsMatched=true;
      this.organizationLogoBox = false;
      $('#ctrllogoFile').val('');
    this.events.publish('organization-item-operation', {refresh: false});
  }

  comparePassword() {
      if(this.organization.OrganizationId == 0)
      {
          if (this.organization.Password!=null && this.ConfirmPassword.length > 0 && (this.organization.Password == this.ConfirmPassword)) {
              this.IsPasswordsMatched=true;
          }
          else{
              this.IsPasswordsMatched=false;
          }
      }

      return this.IsPasswordsMatched;
  }

  hideErrorMsg(){
      this.IsPasswordsMatched=true;
  }

  validateFileExtension(filename) {
      var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + this.allowedFiles.join('|') + ")$");
      var flag=true;

        if ( filename != null && !regex.test(filename.toLowerCase())) {
            flag=false;
        }
      return flag;
  }
}
