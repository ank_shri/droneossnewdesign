import { inject } from 'aurelia-framework';
import { OrganizationService } from 'organizations/organizationService';
import { Router } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';
import { PermissionService } from 'security/permissionService';
// import { Notification } from 'aurelia-notification';
// import { DialogService } from 'aurelia-dialog';
// import { Prompt } from 'components/my-modal';

@inject(Router, EventAggregator, PermissionService, OrganizationService)

export class NotificationPreferences {
  //Set Filters for Searchbox
  filters = [
    { value: '', keys: ['EventName', 'Frequency'] },
  ];

  constructor(router, events, permissionService, organizationService) {
    this.organizationService = organizationService;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;
    // this.notification = notification;
    this.heading = 'Manage Notifications';
    this.preferences = [];
    this.preference = null;

    this.isEditing = false;
    this.isViewing = false;
    this.roleName = "";

    this.permission = {};
    // this.dialogService = dialogService;
    //Set Default Paging Size
    this.pageSize = organizationService.defaultPagingSize;
    this.isWaiting = false;
  }

  activate() {
    //this.getRole();
    this.getList();
  }

  attached() {
    this.subscribeEvents();
  }

  detached() {

  }

  subscribeEvents() {
    this.subscriber = this.events.subscribe('preference-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });
  }

  reset() {
    this.preference = null;
    this.isEditing = false;
    this.isViewing = false;
  }

  getPermission(moduleId) {
    this.permissionService.getPermission(moduleId)
      .then(result => {
        this.permission = result;
        this.getRole();
        console.log(this.getRole());
      });
  }

  getRole() {
    if (localStorage["Roles"].indexOf("Super Admin") > -1) {
      this.roleName = "super admin";
      return;
    }

    if (localStorage["Roles"].indexOf("Admin") > -1) {
      this.roleName = "admin";
      return;
    }

    this.roleName = "others";
  }

  getList() {

    this.isWaiting = true;
    this.organizationService.getPreferences()
      .then(result => {
        this.preferences = result;
        this.isEditing = false;
        this.getPermission(17);
        this.isWaiting = false;
        console.log(result);
      }).catch((e) => {
        this.notification.error(e.message);
        this.isWaiting = false;
      });
  }

  add() {
    this.isWaiting = true;
    this.organizationService.getNewNotificationPreference()
      .then(result => {
        this.preference = result;
        this.isEditing = true;
        this.isWaiting = false;
      });
  }

  editItem(id) {
    this.isWaiting = true;
    this.organizationService.getPreference(id)
      .then(result => {
        this.preference = result;
        this.isEditing = true;
        this.isWaiting = false;
      });
  }

  saveItem() {
    return this.organizationService.savePreference(this.preference);
  }

  deleteItem(id) {
    this.dialogService.open({ viewModel: Prompt, model: 'Are you sure you want to delete?' }).then(response => {
      if (!response.wasCancelled) {
        this.organizationService.deletePreference(id)
          .then(result => {
            this.notification.error('Deleted Successfully.');
            this.getList();
          });
      }
    });
  }
}
