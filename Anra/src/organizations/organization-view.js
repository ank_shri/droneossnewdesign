import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';

@inject(EventAggregator, Notification)
export class OrganizationView {
  @bindable organization;
  @bindable save;

    constructor(events, notification) {
        this.events = events;
        this.notification = notification;
        this.selectedFiles = null;
    }    

    bind() {
        this.heading = this.organization.Name != null ? 'View Organization' : 'Add Organization';
    }    

    fileSelected(event) {
        let reader = new FileReader();
        let file = event.target.files[0];
        reader.readAsDataURL(file);
        let fileName = file.name;
        reader.onload = () => {
            this.organization.logoFile = reader.result;
        };
    }    

    cancel() {
        this.events.publish('organization-item-operation', {refresh: false});
    }

  edit(organizationId) {
      this.events.publish('organization-item-operation-edit', {id: organizationId});
  }
}
