import {inject} from 'aurelia-framework';
import {OrganizationService} from 'organizations/organizationService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {PermissionService} from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(OrganizationService, Router, EventAggregator, PermissionService)
export class Organizations {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['Name','ContactEmail','ContactPhone','Website','IsInsuranceAvailableText']},
    ];

    constructor(organizationService, router, events, permissionService) {
    this.organizationService = organizationService;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;
    // this.notification = notification;
    //state
    this.heading = 'Manage Organizations';
    this.organizations = [];

    this.organization = null;
    this.isEditing = false;
    this.isViewing = false;
    this.roleName = "";
    // this.organizationRoute = router.routes[27].navModel;

    this.permission={};
    // this.dialogService = dialogService;
    //Set Default Paging Size
    this.pageSize = organizationService.defaultPagingSize;
    this.isWaiting = false;
  }

    activate() {
    this.getList();
    // this.organizationRoute.isActive = true;
  }

  // deactivate() {
  //     this.organizationRoute.isActive = false;
  // }

  attached() {
    this.subscribeEvents();
  }
  detached() {
    this.subscriber.dispose();
  }

  getPermission(moduleId) {
      this.permissionService.getPermission(moduleId)
        .then(result => {
            this.permission = result;
            this.getRole();
        });
  }

  getRole() {
      if(localStorage["Roles"].indexOf("Super Admin") > -1)
      {
          this.roleName = "super admin";
          return;
      }

      if(localStorage["Roles"].indexOf("Admin") > -1)
      {
          this.roleName = "admin";
          return;
      }

      this.roleName = "others";   
  }

  subscribeEvents() {
    this.subscriber = this.events.subscribe('organization-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber =  this.events.subscribe('organization-item-operation-edit', payload => {

        if (payload) {
            if (payload.id > 0) {
                this.editItem(payload.id);
            }
            this.reset();
        }
    });
  }

  reset() {
      this.organization = null;
      this.isEditing = false;
      this.isViewing = false;
  }

  add() {
    this.isWaiting = true;
    this.organizationService.getNewOrganization()
      .then(result => {
        this.organization = result;
        this.isEditing = true;
        this.isWaiting = false;
      //$('#ctrllogoFile').val('');

      });
  }

  editItem(id) {
      this.isWaiting = true;
    this.organizationService.getOrganization(id)
      .then(result => {
        this.organization = result;
        this.isEditing = true;
        this.isWaiting = false;
      });
  }

  viewItem(id) {
      this.isWaiting = true;
      this.organizationService.getOrganization(id)
        .then(result => {
            this.organization = result;
            this.isViewing = true;
            this.isWaiting = false;
        });
  }

  deleteItem(id) {
    this.dialogService.open({viewModel: Prompt, model: 'Are you sure you want to delete?'}).then(response => {
         if (!response.wasCancelled) {
    this.organizationService.deleteOrganization (id)
      .then(result => {
				  this.notification.error('Deleted Successfully.');
        this.getList();
			  });
         }
      });
  }

  saveItem(item) {
    return this.organizationService.saveOrganization(this.organization);
  }

  getList() {
      this.isWaiting = true;
      this.organizationService.getOrganizations()
      .then(result => {
        this.organizations = result;
        this.isEditing = false;
        this.getPermission(3);
        this.isWaiting = false;
      }).catch((e) => {
          this.notification.error(e.message);
          this.isWaiting = false;
      });
  }
}
