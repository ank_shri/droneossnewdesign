import { bindable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { inject, NewInstance } from 'aurelia-dependency-injection';
import { ValidationController, ValidationRules, validateTrigger } from 'aurelia-validation';
import { BootstrapFormValidationRenderer } from "lib/bootstrap-form-validation-renderer.js";
import { Notification } from 'aurelia-notification';


@inject(EventAggregator, Notification, NewInstance.of(ValidationController))
export class NotificationPreferenceEditor {
  @bindable preference;
  @bindable save;
  @bindable organizations;
  controller = null;
  rules = null;

  constructor(events, notification, controller) {
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;    
  }

  attached() {
    this.setupValidationRules();
  }

  detached() {
    //Finally Reset Validation
    this.controller.reset();
  }

  bind() {
    this.heading = this.preference.NotificationPreferenceId > 0 ? 'Edit Preference' : 'Add Preference';
  }

  setupValidationRules() {
    this.rules = ValidationRules
      .ensure('EventId').matches(/^[1-9][0-9]*$/).required()
      .ensure('FrequencyInterval').matches(/^[1-9][0-9]*$/).required()      
      .rules;
  }

  saveItem() {
    this.isWaiting = true;

    this.controller.validate({ object: this.preference, rules: this.rules })
      .then(valresult => {
        if (valresult.valid) {
          // validation succeeded
          this.save()
            .then(result => {
              this.handleSaveResult(result);              
              this.isWaiting = false;
            });
        }
        else {
          // validation failed
          this.notification.error('Please enter a valid input.');
          this.isWaiting = false;
        }
      }).catch((e) => {
        this.notification.error('Error.');
        this.isWaiting = false;
      });
  }


  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.notification.error(result.SuccessMessage);
      this.events.publish('preference-item-operation', { refresh: true });
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('preference-item-operation', { refresh: false });
  }
}
