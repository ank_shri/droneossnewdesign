import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class ProjectService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getRole() {
      return this.http.fetch(this.baseApi + 'users/getrole')
      .then(response => response.text());
  }

  getProjects() {
      return this.http.fetch(this.baseApi + 'project/listing')
      .then(response => response.json());
  }

  getNewProject() {
      return this.http.fetch(this.baseApi + 'project/new')
      .then(response => response.json());
  }

  getProject(id) {
      return this.http.fetch(this.baseApi + 'project/' + id)
      .then(response => response.json());
  }

  deleteProject(id) {
    return this.http.fetch(this.baseApi + 'project/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveProject(Project) {
    return this.http.fetch(this.baseApi + 'project/', {
      method: 'post',
      body: json(Project)
    }).then(response => response.json());
  }
}
