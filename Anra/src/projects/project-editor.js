import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Notification} from 'aurelia-notification';


@inject(EventAggregator, Notification, NewInstance.of(ValidationController))
export class ProjectEditor {
  @bindable project;
  @bindable save;
  @bindable organizations;
  controller = null;
  rules = null;

  constructor(events, notification, controller) {
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
    this.allowedFiles = [".kml"];
    this.selectedFiles = [];
  }

  attached() {
      this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.heading = this.project.Name != null ? 'Edit Project' : 'Add Project';
  }

  setupValidationRules() {   

      this.rules = ValidationRules
           .ensure('Name').required()    
           .ensure('ClientId').matches(/^[1-9][0-9]*$/).required()
           .ensure('RevenueAmount').required()
           .ensure('OrganizationId').matches(/^[1-9][0-9]*$/).required()
           .rules;
  }

  removeRule(propName){
      let list = this.rules[0];
      for( var i = list.length-1; i--;){
          if ( list[i].property.name === propName) list.splice(i, 1);
      }
  }

  saveItem() {
      this.isWaiting = true;

      //In case User is not Admin, skip OrganizationId Validation
      if(!this.project.IsAdmin){
          this.removeRule('OrganizationId');
      }

      this.controller.validate({ object: this.project, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid && this.validateFileExtension(this.selectedFiles)) {
                  // validation succeeded
                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
						  if(this.project.ProjectId!='')
							{
								this.notification.error('Updated Successfully.');
							}
							else
							{
								this.notification.error('Added Successfully.');  
							}
						  this.isWaiting = false;
                      })
              } 
              else {
                  // validation failed
                  this.notification.error('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
          });

      this.isWaiting = false;
  }

  fileSelected(event) { 
      let file = event.target.files[0];      
      var selectedFileExtnsn = file.name.split('.').pop();
      if (selectedFileExtnsn != 'kml')
      {
          this.notification.error('Please select a valid kml file.');
          document.getElementById("ctrlFile").value = "";
          return;
      }
      this.selectedFiles.push(file);

      let reader = new FileReader();
      reader.readAsText(file);
      reader.onload = (r) => {
          var boundaries = this.readXml(r.target.result);
          var myJsonString = JSON.stringify(boundaries);
          this.project.ProjectGeography = myJsonString;
      };
  }

  readXml(xmldata){

      var boundaries = [];
      var xmlDoc;
      if (window.DOMParser  ) {
          // code for modern browsers
          var parser = new DOMParser();
          xmlDoc = parser.parseFromString(xmldata,"text/xml");
      } else {
          // code for old IE browsers
          xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
          xmlDoc.async = false;
          xmlDoc.loadXML(xmldata); 
      } 

      var tagObj= xmlDoc.getElementsByTagName("coordinates");
      var data = tagObj[0].childNodes[0].data;
      
      var coords = data.split(' ');
      
      coords.forEach(function(coord) {
          var latlngalt = coord.split(',');
          boundaries.push({                
              "Longitude": latlngalt[0],
              "Latitude": latlngalt[1],
              "Altitude": latlngalt[2]
          });
      });
      
      return boundaries;
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('project-item-operation', {refresh: true});
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('project-item-operation', {refresh: false});
  }

  validateFileExtension(files) {
      var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + this.allowedFiles.join('|') + ")$");
      var flag = true;
      for (let i = 0; i < files.length; i++) {
          let file = files[i];
          if (!regex.test(file.name.toLowerCase())) {
              flag = false;
              break;
          }
      }
      return flag;
  }
}
