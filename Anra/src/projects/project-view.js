import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';
import {AureliaConfiguration} from "aurelia-configuration";
import {MissionService} from 'missions/missionService';

import * as L from 'leaflet';
import * as D from 'leaflet-draw';
import * as T from 'SpatialServer/Leaflet.MapboxVectorTile';

@inject(EventAggregator, MissionService, Notification, AureliaConfiguration)
export class ProjectView {
  @bindable project;
  @bindable organizations;

  constructor(events, missionService, notification, configure) {
      this.events = events;
      this.missionService = missionService;
      this.notification = notification;
      this.configuration = configure;
      this.showMap = false;   
      this.projectBoundaries = [];
      this.completedMissionWaypoints = [];
      this.inCompleteMissionWaypoints = [];
      this.missions = [];
  }

  bind() {    
      this.heading = this.project.Name != null ? 'View Project' : 'Add Project';
      if(this.project.ProjectGeography)
      {
          this.showMap = true;         
          this.projectBoundaries = JSON.parse(this.project.ProjectGeography).map(x => new L.LatLng(x.Latitude, x.Longitude));
          var self = this;
          this.projectMissionWaypoints(self);
          setTimeout(() => {             
              this.initMap();                          
          }, 1000);          
      }
  }

  projectMissionWaypoints(self) {      
      this.missionService.getMissionByProjId(this.project.ProjectId)
      .then(result => {       
          this.missions = result;
         
              this.missions.forEach(function(mission) {
                  if(!mission.FlightGeography)
                      return;
                  if(mission.StatusId == 3)
                  {
                      var cmission = JSON.parse(mission.FlightGeography).map(x => new L.LatLng(x.Latitude, x.Longitude));                     
                      self.completedMissionWaypoints.push.apply(self.completedMissionWaypoints, cmission);
                      return
                  }

                  var incompmission = JSON.parse(mission.FlightGeography).map(x => new L.LatLng(x.Latitude, x.Longitude));
                  self.inCompleteMissionWaypoints.push.apply(self.inCompleteMissionWaypoints, incompmission); 
              });             

      })
      .catch((e) => {
          console.error(e);
          this.isWaiting = false;
      })
      
      this.isWaiting = false;
  }

  initMap() {
      if (!this.map) {

          let accessToken = this.configuration.get('mapboxAccessToken');

          var streetMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=' + accessToken, {
              attribution: '� <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
          });

          var satelliteMap = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
              attribution: '� <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
          });

          this.baseMaps = {
              "Satellites": satelliteMap,
              "Streets": streetMap
          };         
         

          var projectPolyline = L.polyline(this.projectBoundaries, { className: 'my_polyline' });
          var completedMissionPolyline = L.polyline(this.completedMissionWaypoints, {color: 'black', weight: 1, opacity: 0.8, smoothFactor: 1});
          var inCompleteMissionPolyline = L.polyline(this.inCompleteMissionWaypoints, {color: 'blue', weight: 1, opacity: 0.8, smoothFactor: 1});
                

          let map = L.map('map', {
              zoomControl: false,
              drawControl: false,
              editable: false,
              maxNativeZoom: 16,             
              layers: [streetMap, projectPolyline, completedMissionPolyline, inCompleteMissionPolyline]
          });         
          

          var overlaymaps = {"Project Boundaries": projectPolyline, "Completed Missions": completedMissionPolyline, "In-Complete Missions": inCompleteMissionPolyline }     

          this.layerControl = L.control.layers(this.baseMaps, overlaymaps).addTo(map);

          L.control.zoom({
              maxZoom: 16,
              position: 'topleft'
          }).addTo(map);

          map.doubleClickZoom.disable();

          L.Icon.Default.imagePath = 'styles/images';
          this.map = map;
      }
      else{
          this.map.removeLayer(this.path);
      }
        
      this.map.setView(new L.LatLng(this.projectBoundaries[0].lat, this.projectBoundaries[0].lng), 16);      
      
      //var polyline = L.polyline(this.projectBoundaries, { className: 'my_polyline' }).addTo(this.map);
      //polyline.setStyle({
      //    color: 'black'
      //});

  }


  cancel() {
    this.events.publish('project-item-operation', {refresh: false});
  }

  edit(projectId) {
      this.events.publish('project-item-operation-edit', {id: projectId});
  }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
