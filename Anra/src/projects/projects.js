import {inject} from 'aurelia-framework';
import {ProjectService} from 'projects/projectService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {PermissionService} from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(ProjectService, Router, EventAggregator, PermissionService)
export class Projects {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['Name','RevenueAmount','OrganizationName']},
    ];

    constructor(projectService, router, events, permissionService) {
        this.projectService = projectService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        // this.notification = notification;
        this.heading = 'Manage Projects';
        this.projects = [];
        this.project = null;
        this.isEditing = false;
        this.isViewing = false;
        this.roleName = "";
        this.permission={};
        // this.dialogService = dialogService; 
        //Set Default Paging Size
        this.pageSize = projectService.defaultPagingSize;
        this.isWaiting = false;
    }

    activate() {
        this.getList();
    }

    attached() {
        this.subscribeEvents();
    }
  detached() {
    this.subscriber.dispose();
  }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
              this.getRole();
          });
    }

    getRole() {
        if(localStorage["Roles"].indexOf("Super Admin") > -1)
        {
            this.roleName = "super admin";
            return;
        }

        if(localStorage["Roles"].indexOf("Admin") > -1)
        {
            this.roleName = "admin";
            return;
        }

        this.roleName = "others";   
    }

    subscribeEvents() {
      this.subscriber= this.events.subscribe('project-item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                }
                this.reset();
            }
        });

      this.subscriber=this.events.subscribe('project-item-operation-edit', payload => {

            if (payload) {
                if (payload.id > 0) {
                    this.editItem(payload.id);
                }
                this.reset();
            }
        });
    }


    getList() {
        this.isWaiting = true;
        this.projectService.getProjects()
          .then(result => {
              this.projects = result;
              this.isEditing = false;
              this.getPermission(2);
              this.isWaiting = false;
          }).catch((e) => {
              this.notification.error(e.message);
              this.isWaiting = false;
          });
    }

    reset() {
        this.project = null;
        this.isEditing = false;
        this.isViewing = false;
    }

  add() {
        this.isWaiting = true;
        this.projectService.getNewProject()
          .then(result => {
              this.project = result;
              this.isEditing = true;
              this.isWaiting = false;
          });
    }

    editItem(id) {
        this.isWaiting = true;
        this.projectService.getProject(id)
          .then(result => {
              console.log(result);  
              this.project = result;
              this.isEditing = true;
              this.isWaiting = false;
          });
    }

  viewItem(id) {
        this.isWaiting = true;
        this.projectService.getProject(id)
          .then(result => {              
              this.project = result;
            this.isViewing = true;
            this.isWaiting = false;
          });
    }

    
    deleteItem(id) {
    this.dialogService.open({viewModel: Prompt, model: 'Are you sure you want to delete this project and related all media?'}).then(response => {
         if (!response.wasCancelled) {
        this.projectService.deleteProject (id)
          .then(result => {
				  this.notification.error('Deleted Successfully.');
              this.getList();
			  });
         }
          });
    }

    saveItem(item) {
        return this.projectService.saveProject(this.project);
    }
}
