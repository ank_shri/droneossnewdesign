import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class SignUpService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
  }

  getNewUser() {
      return this.http.fetch(this.baseApi + 'users/NewUser/null')
        .then(response => response.json());
  }

  signUp(user) {
    return this.http.fetch(this.baseApi + 'users/', {
      method: 'post',
      body: json(user)
    }).then(response => response.json());
  }
}
