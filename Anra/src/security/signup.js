import {AuthService} from 'aurelia-auth';
import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {SignUpService} from 'security/signupService';

@inject(Router, SignUpService)
export class SignUp {
    loginError = '';  
    packName='';

    activate(params){
        this.packName = params.pk;
    }

    constructor(router, signupservice) {    
        this.router = router;
        this.signupservice = signupservice;
        this.user = {};
        this.add();       
    }

    add() {
        this.signupservice.getNewUser()
          .then(result => {
              this.user = result;
              var pck = (this.user.PackageLookup.filter((item) => item['PackageName'] == (this.packName)));
              if(pck.length > 0)
              {
                  this.user.PackageId = pck[0].PackageId;
              }
              else
              {
                  this.user.PackageId = 0;
              }
          });
    }

    signUp(item) {
        this.signupservice.signUp(this.user).then(result => {

            if(result.IsSuccess)
            {
                this.loginError = 'You have been registered successfully.';
                this.router.navigate("login");
            }
            else
            {
                this.loginError = result.ErrorMessage;
            }
        });
        
    }

}
