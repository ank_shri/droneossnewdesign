import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class PermissionService {

    constructor(http, config) {
        this.config = config;
        this.http = http;
        this.baseApi = this.config.get('gatewayBaseUrl');
    }

    getPermission(moduleId) {
        return this.http.fetch(this.baseApi + 'users/accesspermission/'+ moduleId)
          .then(response => response.json());
    }
}
