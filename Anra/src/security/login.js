import {inject, NewInstance} from "aurelia-dependency-injection";
import {AuthService} from "aurelia-authentication";
import {Router} from "aurelia-router";
import {UIApplication, UIEvent,UIDialogService } from "aurelia-ui-framework";
import {validateTrigger, ValidationController, ValidationRules} from "aurelia-validation";
import {AnraNotification} from "../components/anra-notification";
import { MyDialog } from "./dialog";
import { EUconsentForCookies } from "lib/cookies.js";
import { BootstrapFormValidationRenderer } from "lib/bootstrap-form-validation-renderer.js";

@inject(AuthService,Router,UIApplication,UIDialogService, NewInstance.of(ValidationController),AnraNotification,EUconsentForCookies)
export class Login{
    constructor(auth,router,app,dlgService,controller,notification,cookies){
        this.auth = auth;
        this.router=router;
        this.app = app;
        this.dlgService = dlgService;
        this.controller = controller;
        this.controller.validateTrigger = validateTrigger.change;
        this.loginError = '';
        this.notification = notification;
        this.cookies = cookies;
        this.operation={};
       
    }

    activate(){
      this.isConsentAccepted = this.cookies.getEUConsentCookie('ANRAEUconsent');   
    }

    attached() {
        console.log("attached");
        this.setupValidationRules();
        if(!this.isConsentAccepted)
        {
          this.dlgService.show(MyDialog, {
            modal: this.operation,
            modelView: this
          });
        }
    }
    
    detached() {
        this.controller.reset();
    }
    
    setupValidationRules() {

        this.rules = ValidationRules.ensure("email")
            .required().withMessage('User Name is required.')
            .email().withMessage('Please enter a valid email.')
            .ensure("password")
            .required().withMessage('Password is required.')
            .rules;
    }

    authentication(){
        this.controller
      .validate({object: this, rules: this.rules})
      .then(valresult => {
        if (valresult.valid) {
          this.auth
            .login(this.email, this.password)
            .then(response => {
                console.log("response",response);
                if(response.IsAuthenticated)
                {
                  	
                  localStorage["UserId"] = response.UserId;
                  localStorage["email"] = response.UserEmail;
                  localStorage["UserName"] = response.UserName;
                  localStorage["Roles"] = response.Roles;
                  localStorage["UnitType"] = response.UnitType;
                    localStorage["MassUnit"] = response.MassUnit;
                    localStorage["LengthUnit"] = response.LengthUnit;
                    localStorage["TemperatureUnit"] = response.TemperatureUnit;						
                    localStorage["ProfilePhoto"] = response.ProfilePhoto == null ? null : response.ProfilePhoto;
                    localStorage['OrgLogo']=response.OrgLogo ==null ? null : response.OrgLogo;
                    this.router.navigate("dashboard");
                    this.isWaiting = false;
                }
                else if(this.email!='')
                {
                    localStorage["email"] = null;
                    this.loginError = "Invalid Email-Id/Password.";
                    setTimeout(() => {
                        $('#login_error').hide();
                    }, 3000);
                    
                    this.isWaiting = false;
                }
            })
            .catch(error => {
              console.error(error);
              this.loginError = "There is some issue. Please try later.";
              this.notification.ShowError("Not able to establish connection with server.");
              setTimeout(() => {
                $('#login_error').hide();
              }, 3000);
              
            });
        }
        else {
          valresult.results.map(x => {
            if (x.message) {
              $('#login_error').show();
              this.loginError += x.message;
              setTimeout(() => {
                $('#login_error').hide();
              }, 3000);
            }
          });

        }
      })
      .catch(e => {
        console.error(e.ErrorMessage);
      });
    }

    acceptEUConsent(){
      this.cookies.setEUConsentCookie(true);
      this.isConsentAccepted = true;
    }
    
}