import {inject, NewInstance} from "aurelia-dependency-injection";
import { UIDialog } from "aurelia-ui-framework";
import { EUconsentForCookies } from "lib/cookies.js";
@inject(UIDialog,EUconsentForCookies)
export class MyDialog extends UIDialog {
    constructor(dialog,cookies)
    {
        super();
        this.dialog=dialog;
        this.cookies=cookies;

    }
    canActivate(model)
    {
        this.title = "User Agreement";
        this.width = "500px";
        this.height = "300px";
        this.parentView= model.modelView;
        this.theme = 'dark';
        return true;
    }

    setConsent(event){
        if(this.chkTermsCondition && this.chkPrivacyPolicy){
            document.getElementById('btnAgree').disabled = false;    
        }
        else
        {
          document.getElementById('btnAgree').disabled = true;
        }
    }

    acceptEUConsent(){
        this.cookies.setEUConsentCookie(true);
       this.isConsentAccepted = true;
       this.close();
      }
}