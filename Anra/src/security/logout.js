import {AuthService} from 'aurelia-authentication';
import {inject} from 'aurelia-framework';


@inject(AuthService )

export class Logout{
	constructor(authService){
		this.authService = authService;
	};
	
	 activate(){
		this.authService.logout("#/login")
		.then(response=>{
		    localStorage["email"] = null;
		    localStorage["UserName"] = null;
		    localStorage["Roles"] = null;
			console.log("ok logged out on  logout.js");
		})
		.catch(err=>{
		    localStorage["email"] = null;
		    localStorage["UserName"] = null;
		    localStorage["Roles"] = null;
			console.log("error logged out  logout.js");

		});
	}
}
