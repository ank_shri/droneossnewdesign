import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import { validateTrigger, ValidationController, ValidationRules } from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {AnraNotification} from "../components/anra-notification";
import moment from 'moment';

@inject(EventAggregator, AnraNotification,NewInstance.of(ValidationController))
export class IncidentEditor {
  @bindable incident;
  @bindable drones;
  @bindable projects;
  @bindable locations;
  @bindable causes;
  @bindable save;
  @bindable organizations;
  @bindable missions;
  controllerF = null;
  rules = null;


  constructor(events, notification, controller) {
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    // this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change; 
    this.isWaiting = false;
    this.ShowIncidentForm= true;
  }

  attached() {
      $('#incidentDate').attr("max",moment().format('YYYY-MM-DD'));
      this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
      this.heading = this.incident.IncidentName != null ? 'Edit Incident' : 'Add Incident';      
  }

  setupValidationRules() {   
    this.rules = ValidationRules
      .ensure('Name')
      .required().withMessage('Organization Name is required.')
      .rules;
      // this.rules = ValidationRules
      //   .ensure('Name').required()    
      //   .ensure('MissionId').matches(/^[1-9][0-9]*$/).required()
      //   .ensure('IncidentCauseId').matches(/^[1-9][0-9]*$/).required()
      //   .ensure('AircraftDamage').required()
      //   .rules;
  }

  saveItem() {
      this.isWaiting = true;
      this.controller.validate({ object: this.incident, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
						  if(this.incident.IncidentId!='')
						  {
							this.notification.ShowInfo('Updated Successfully.');
						  }
						  else
						  {
							this.notification.ShowInfo('Added Successfully.');  
						  }
						  this.isWaiting = false;
                      })
              } 
              else {
                  // validation failed
                  this.notification.ShowError('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.ShowError('Error.');
              this.isWaiting = false;
          });

    //   this.isWaiting = false;
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('incident-item-operation', {refresh: true});
    }
    else {
      this.notification.ShowError(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('incident-item-operation', {refresh: false});
  }

  fillIncident(){
      
      if(this.incident.MissionId!=null)
      {
          let objMission=this.missions.find(x => x.MissionId === this.incident.MissionId);
          let objProject= this.projects.find(x => x.ProjectId === objMission.ProjectId);
          let objDrone=this.drones.find(x => x.DroneId === objMission.DroneId);
          let objLocation=this.locations.find(x => x.LocationId === objMission.LocationId);

          this.incident.MissionId=objMission.MissionId;
          this.incident.MissionName=objMission.Name;
          this.incident.ProjectName=objProject.Name;
          this.incident.DroneName=objDrone.Title;
          this.incident.LocationName=objLocation.Name;
      }

  }
}
