import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
// import {Notification} from 'aurelia-notification';
import { Router } from 'aurelia-router';
@inject(EventAggregator, Router)
export class IncidentView {
  @bindable incident;
  @bindable drones;
  @bindable projects;
  @bindable locations;
  @bindable causes;
  @bindable save;
  @bindable organizations;

  constructor(events ,router) {
    this.events = events;
    this.router = router;
  }

  bind() {
    this.heading = this.incident.IncidentName != null ? 'View Incident' : 'Add Incident';
  }

  cancel() {
    this.events.publish('incident-item-operation', {refresh: false});
  }

  edit(incidentId) {
    this.events.publish('incident-item-operation-edit', { id: incidentId });
    this.router.navigate("incidents");
  
  }

}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
