import {inject} from 'aurelia-framework';
import {IncidentService} from 'incidents/incidentService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {PermissionService} from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
import {AnraNotification} from "../components/anra-notification";
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(IncidentService, Router, EventAggregator, PermissionService,AnraNotification)
export class Incidents {

  //Set Filters for Searchbox
  filters = [
      {value: '', keys: ['Name','MissionName','OrganizationName']},
  ];

  constructor(incidentService, router, events, permissionService,notification) {
      this.incidentService = incidentService;
      this.router = router;
      this.events = events;
      this.permissionService = permissionService;
      this.notification = notification;
      this.heading = 'Manage Incidents';
      this.incidents = [];
      this.incidentAggregate = null;
  
      this.isEditing = false;
      this.isViewing = false;

      this.permission={};
      //Set Default Paging Size
      this.pageSize = incidentService.defaultPagingSize;
      this.isWaiting = false;
  }

  activate() {
    this.getList();
  }

  attached() {
      this.subscribeEvents();
  }
  detached() {
    this.subscriber.dispose();
  }
  getPermission(moduleId) {
      this.permissionService.getPermission(moduleId)
        .then(result => {
            this.permission = result;
        });
  }

  subscribeEvents() {
    this.subscriber =  this.events.subscribe('incident-item-operation', payload => {

          if (payload) {
              if (payload.refresh) {
                  this.getList();
              }
              this.reset();
          }
      });

    this.subscriber =  this.events.subscribe('incident-item-operation-edit', payload => {

          if (payload) {
              if (payload.id > 0) {
                  this.editItem(payload.id);
              }
              this.reset();
          }
      });
  }


    getList() {
        this.isWaiting = true;
        this.incidentService.getIncidents()
        .then(result => {
            this.incidents = result;
            this.isEditing = false;
            this.getPermission(9);
            this.isWaiting = false;
        }).catch((e) => {
            this.notification.ShowError(e.message);
            this.isWaiting = false;
        });
    }

  reset() {
    this.incidentAggregate = null;
    this.isEditing = false;
    this.isViewing = false;
  }

  add() {
    this.isWaiting = true;
        this.incidentService.getNewIncident()
        .then(result => {
            this.incidentAggregate = result;
            this.incidentAggregate.Incident.IncidentDate = moment(result.Incident.IncidentDate).format('YYYY-MM-DD');
          this.isEditing = true;
          this.isWaiting = false;
          }).catch((e) => {
            this.notification.ShowError('Error.');
            this.isWaiting = false;
          });
    }

    editItem(id) {
        this.isWaiting = true;
    this.incidentService.getIncident(id)
      .then(result => {
        
        this.incidentAggregate = result;
        this.incidentAggregate.Incident.IncidentDate = moment(result.Incident.IncidentDate).format('YYYY-MM-DD');
        this.isEditing = true;
        this.isWaiting = false;
      }).catch((e) => {
        this.notification.ShowError('Error.');
        this.isWaiting = false;
      });
  }

  viewItem(id) {
    this.isWaiting = true;
      this.incidentService.getIncident(id)
        .then(result => {            
            this.incidentAggregate = result;
            this.incidentAggregate.Incident.IncidentDate = moment(result.Incident.IncidentDate).format('YYYY-MM-DD');
          this.isViewing = true;
          this.isWaiting = false;
        }).catch((e) => {
          this.notification.ShowError('Error.');
          this.isWaiting = false;
        });
  }

  
  deleteItem(id) {
    this.isWaiting = true;
    this.notification.Confirm("Are you sure you want to delete?").then(result => {
         if(result) {
          this.incidentService.deleteIncident (id)
            .then(result => {
                this.notification.ShowError('Deleted Successfully.');
                this.getList();
                this.isWaiting = false;
              });
          }
          else{
            this.isWaiting = false;
          }
        }).catch((e) => {
          this.notification.ShowError('Error.');
          this.isWaiting = false;
        });
  }

    saveItem(item) {
        return this.incidentService.saveIncident(this.incidentAggregate.Incident);
    }
}
