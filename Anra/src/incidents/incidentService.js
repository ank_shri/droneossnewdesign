import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class IncidentService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getIncidents() {
      return this.http.fetch(this.baseApi + 'incident/listing')
      .then(response => response.json());
  }

  getNewIncident() {
      return this.http.fetch(this.baseApi + 'incident/new')
      .then(response => response.json());
  }

  getIncident(id) {
    return this.http.fetch(this.baseApi + 'incident/' + id)
      .then(response => response.json());
  }

  deleteIncident(id) {
    return this.http.fetch(this.baseApi + 'incident/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveIncident(Incident) {
    return this.http.fetch(this.baseApi + 'incident/', {
      method: 'post',
      body: json(Incident)
    }).then(response => response.json());
  }
}
