import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from "../components/anra-notification";
import {AureliaConfiguration} from "aurelia-configuration";
import {GeoSearchProvider} from 'components/geo-search-provider';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";


import * as L from 'leaflet';
import * as D from 'leaflet-draw';
import * as T from 'SpatialServer/Leaflet.MapboxVectorTile';
import 'smeijer/leaflet-geosearch';

const defaultZoom = 16;
const maxZoom = 22;
@inject(EventAggregator, AnraNotification, AureliaConfiguration, GeoSearchProvider, NewInstance.of(ValidationController))

export class Location {
  @bindable location;
  @bindable save;
  controller = null;
  rules = null;

  constructor(events, notification, configuration, geoSearchProvider, controller) {
    this.events = events;
    this.notification = notification;
    this.lengthUnit = localStorage["LengthUnit"];
    this.configuration = configuration;
    this.geoSearchProvider = geoSearchProvider;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
  }

  attached() {
      this.setupValidationRules();
      this.initMap();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.heading = this.location.Name != null ? 'Edit Location' : 'Add Location';
  }

  setupValidationRules() {   
      this.rules = ValidationRules
        .ensure('Name').required()    
        .ensure('Latitude').required()
        .ensure('Longitude').required()
        .ensure('Altitude').required()
        .ensure('City').required()
        .ensure('StateId').matches(/^[1-9][0-9]*$/).required()
        .ensure('Zipcode').required()
        .ensure('CountryId').matches(/^[1-9][0-9]*$/).required()
        .ensure('ProjectId').matches(/^[1-9][0-9]*$/).required()
        .rules;
  }

  initMap() {
    if (!this.map) {

      let accessToken = this.configuration.get('mapboxAccessToken');

      var streetMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=' + accessToken, {
        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
      });

      var satelliteMap = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
        attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
      });

      this.baseMaps = {
        "Satellites": satelliteMap,
        "Streets": streetMap
      };

      let map = L.map('map', {
        zoomControl: false,
        drawControl: false,
        editable: false,
        maxNativeZoom: maxZoom,
        layers: [streetMap]
      });

      //this.layerControl = L.control.layers(this.baseMaps, this.overlayMaps).addTo(map);

      L.control.zoom({
        maxZoom: maxZoom,
        position: 'topleft'
      }).addTo(map);

      new L.Control.GeoSearch({
        provider: this.geoSearchProvider,
        showMarker: true,
        retainZoomLevel: false,
        draggable: false,
        searchLabel:"search for address.."
      }).addTo(map);

      map.doubleClickZoom.disable();

      L.Icon.Default.imagePath = 'styles/images';
      this.map = map;

      map.on('locationfound', e => {
        this.lat = e.latitude;
        this.lng = e.longitude;
        this.setDefaultView();
      })

      map.on('click', e => {
        this.location.Latitude = e.latlng.lat;
        this.location.Longitude = e.latlng.lng;
        this.setMarker();
      });
    }

    this.map.locate({setView: true, maxZoom: maxZoom});
  }

  setDefaultView() {
    if (this.location && this.location.Latitude) {
      console.log(this.location.Latitude + " , " + this.location.Longitude);
      this.map.setView(new L.LatLng(this.location.Latitude, this.location.Longitude, defaultZoom));
    }
    else {
      this.location.Latitude = this.lat;
      this.location.Longitude = this.lng;
      this.map.setView(new L.LatLng(this.lat, this.lng), defaultZoom);
    }

    this.setMarker();
  }

  setMarker(){
    if(this.marker){
      this.marker.setLatLng(new L.LatLng(this.location.Latitude, this.location.Longitude));
    }
    else {
      this.marker = L.marker(new L.LatLng(this.location.Latitude, this.location.Longitude)).addTo(this.map);
    }
  }

  saveItem() {
      this.isWaiting = true;
      this.controller.validate({ object: this.location, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
						  if(this.location.LocationId!='')
						  {
							this.notification.ShowError('Updated Successfully.');
						  }
						  else
						  {
							this.notification.ShowError('Added Successfully.');  
						  }
						  this.isWaiting = false;
                      })
              } 
              else {
                  // validation failed
                  this.notification.ShowError('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.ShowError('Error.');
              this.isWaiting = false;
          });

      // this.isWaiting = false;
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.events.publish('location-item-operation', {refresh: true});
    }
    else {
      this.notification.ShowError(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('location-item-operation', {refresh: false});
  }
}
