import {inject} from 'aurelia-framework';
import {LocationService} from 'locations/locationService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {PermissionService} from 'security/permissionService';
import {AnraNotification} from "../components/anra-notification";
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(LocationService, Router, EventAggregator, PermissionService,AnraNotification)
export class Locations {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['Name','City','Zipcode','OrganizationName']},
    ];

    constructor(locationService, router, events, permissionService,notification) {
        this.locationService = locationService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        this.notification = notification;
        this.heading = 'Manage Locations';
        this.locations = [];

        this.location = null;
        this.isEditing = false;
        this.isViewing = false;

        this.permission={};
        // this.dialogService = dialogService;
        //Set Default Paging Size
        this.pageSize = locationService.defaultPagingSize;
        this.isWaiting = false;
    }

  activate() {
    this.getList();
  }

    attached() {
        this.subscribeEvents();
    }
    detached() {
      this.subscriber.dispose();
    }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
          });
    }

  subscribeEvents() {
    this.subscriber =this.events.subscribe('location-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber = this.events.subscribe('location-item-operation-edit', payload => {

        if (payload) {
            if (payload.id > 0) {
                this.editItem(payload.id);
            }
            this.reset();
        }
    });
  }


  getList() {
      this.isWaiting = true;
        this.locationService.getLocations()
        .then(result => {
            this.locations = result;
            this.isEditing = false;
            this.getPermission(8);
            this.isWaiting = false;
        }).catch((e) => {
            this.notification.ShowError(e.message);
            this.isWaiting = false;
        });
    }

  reset() {
    this.location = null;
    this.isEditing = false;
    this.isViewing = false;
  }

    add() {
      this.isWaiting = true;
        this.locationService.getNewLocation()
        .then(result => {
            this.location = result;
            this.isEditing = true;
            this.isWaiting = false;
        });
    }
    
    editItem(id) {
        this.isWaiting = true;
        this.locationService.getLocation(id)
        .then(result => {
            
            this.location = result;
            this.isEditing = true;
            this.isWaiting = false;
        });
    }

    viewItem(id) {
        this.locationService.getLocation(id)
          .then(result => {              
              this.location = result;
              this.isViewing = true;
          });
    }

  deleteItem(id) {
	  
	  this.dialogService.open({viewModel: Prompt, model: 'Are you sure you want to delete?'}).then(response => {
         if (!response.wasCancelled) {
    this.locationService.deleteLocation (id)
      .then(result => {
				  this.notification.ShowError('Deleted Successfully.');
        this.getList();
      });
  }
      });
	}


    saveItem(item) {
        return this.locationService.saveLocation(this.location);
    }
}
