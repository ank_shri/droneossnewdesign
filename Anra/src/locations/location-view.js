import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from "../components/anra-notification";

@inject(EventAggregator, AnraNotification)
export class LocationView {
  @bindable location;
  @bindable save;

  constructor(events, notification) {
    this.events = events;
    this.notification = notification;
    this.lengthUnit = localStorage["LengthUnit"];
  }

  bind() {
    this.heading = this.location.Name != null ? 'View Location' : 'Add Location';
  }

  cancel() {
    this.events.publish('location-item-operation', {refresh: false});
  }

  edit(locationId) {
      this.events.publish('location-item-operation-edit', {id: locationId});
  }

}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
