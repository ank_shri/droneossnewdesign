import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class LocationService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getLocations() {
      return this.http.fetch(this.baseApi + 'location/listing')
      .then(response => response.json());
  }

  getNewLocation() {
      return this.http.fetch(this.baseApi + 'location/NewLocation')
      .then(response => response.json());
  }

  getLocation(id) {
      return this.http.fetch(this.baseApi + 'location/' + id)
      .then(response => response.json());
  }

  deleteLocation(id) {
    return this.http.fetch(this.baseApi + 'location/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }
  saveLocation(mission) {
    return this.http.fetch(this.baseApi + 'location/', {
      method: 'post',
      body: json(mission)
    }).then(response => response.json());
  }
}
