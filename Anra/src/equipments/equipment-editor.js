import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {AnraNotification} from '../components/anra-notification';
import moment from 'moment';
import {UIApplication} from "aurelia-ui-framework";

@inject(EventAggregator, NewInstance.of(ValidationController),AnraNotification,UIApplication)
export class EquipmentEditor {
  @bindable equipment;
  @bindable drones;
  @bindable types;
  @bindable save;
  @bindable organizations;
  controller = null;
  rules = null;

  constructor(events, controller,notification,application) {
    this.events = events;
    this.massUnit = localStorage["MassUnit"];
    this.controller = controller;
    this.notification=notification;
    this.application=application;
    this.isWaiting = false;
    this.ShowEquipmentInfo=true;
  }

  attached() {
      // $('#purchaseDate').attr("max",moment().format('YYYY-MM-DD'));
      this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.heading = this.equipment.Name != null ? 'Edit Equipment' : 'Add Equipment';
  }

  setupValidationRules() {   

      //Custom Rule For Date Validation  
      ValidationRules.customRule(
        'date',
        (value, obj) => value === null || value === undefined || value instanceof Date || moment(value,"YYYY-MM-DD",true).isValid(),
        '\${$displayName} must be a Date.'
      );

      this.rules = ValidationRules
           .ensure('Name').required()    
           .ensure('SerialNumber').required()
           .ensure('DroneId').matches(/^[1-9][0-9]*$/).required()
           .ensure('TypeId').matches(/^[1-9][0-9]*$/).required()
           .ensure('Weight').required()
           .ensure('FirmwareVersion').required()
           .ensure('HardwareVersion').required()
           .ensure('PurchaseDate').required()
           .rules;
  }

  saveItem() {
      this.isWaiting = true;
      this.controller.validate({ object: this.equipment, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
						  if(this.equipment.EquipmentId!='')
							{
								this.notification.ShowInfo('Updated Successfully.');
							}
							else
							{
								this.notification.ShowInfo('Added Successfully.');  
							}
						  this.isWaiting = false;
                      })
              } 
              else {
                  // validation failed
                  this.notification.ShowInfo('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
          });

      // this.isWaiting = false;
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('equipment-item-operation', {refresh: true});
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('equipment-item-operation', {refresh: false});
  }
}
