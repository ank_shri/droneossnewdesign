import {inject} from 'aurelia-framework';
import {EquipmentService} from 'equipments/equipmentService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {PermissionService} from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';
import {AnraNotification} from '../components/anra-notification';

@inject(EquipmentService, Router, EventAggregator, PermissionService,AnraNotification)
export class Listing {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['Name','SerialNumber','OrganizationName']},
    ];

    constructor(equipmentService, router, events,permissionService,notification) {
        this.equipmentService = equipmentService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        this.notification = notification;
        this.heading = 'Manage Equipments';
        this.equipments = [];
        this.equipmentstocsv=[];
        this.equipmentAggregate = null;
        this.massUnit = localStorage["MassUnit"];
        this.isEditing = false;
        this.isViewing = false;

        this.permission={};
        // this.dialogService = dialogService;
        //Set Default Paging Size
        this.pageSize = equipmentService.defaultPagingSize;
        this.isWaiting = false;
    }

  activate() {
    this.getList();
  }

    attached() {
        this.subscribeEvents();
    }
    detached() {
      this.subscriber.dispose();
    }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
          });
    }

  subscribeEvents() {
    this.subscriber = this.events.subscribe('equipment-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber = this.events.subscribe('equipment-item-operation-edit', payload => {

        if (payload) {
            if (payload.id > 0) {
                this.editItem(payload.id);
            }
            this.reset();
        }
    });
  }


  getList() {
      this.isWaiting = true;
        this.equipmentService.getEquipments()
        .then(result => {
            this.equipments = result;
            this.isEditing = false;
            this.getPermission(7);
            this.isWaiting = false;
            var newJson=[];
            this.equipments.forEach(function(item) {
              newJson.push({
              "Name":item.Name,
              "Purchase Date":moment(item.PurchaseDate).format('YYYY-MM-DD'),
              "Serial No":item.SerialNumber,
              "Weight":item.Weight,
              "Organization":item.OrganizationName,
              "Created Date":moment(item.DateCreated).format('YYYY-MM-DD')
                          });
              });
              this.equipmentstocsv=newJson;
        }).catch((e) => {
            this.notification.error(e.message);
            this.isWaiting = false;;
        });
    }

  reset() {
    this.equipmentAggregate = null;
    this.isEditing = false;
    this.isViewing = false;
  }

  add() {
        this.isWaiting = true;
        this.equipmentService.getNewEquipment()
          .then(result => {
              this.isWaiting = false;
              this.equipmentAggregate = result;
              this.isEditing = true;
          });
    }

    editItem(id) {
        this.isWaiting = true;
        this.equipmentService.getEquipment(id)
          .then(result => {
              
              this.equipmentAggregate = result;
	      this.equipmentAggregate.Equipment.PurchaseDate = moment(result.Equipment.PurchaseDate).format('YYYY-MM-DD');
	      this.isEditing = true;
	      this.isWaiting = false;
          });
    }

  viewItem(id) {
    this.isWaiting=true;
      this.equipmentService.getEquipment(id)
        .then(result => {            
            this.equipmentAggregate = result;
            this.equipmentAggregate.Equipment.PurchaseDate = moment(result.Equipment.PurchaseDate).format('YYYY-MM-DD');
            this.isViewing = true;
            this.isWaiting=false;
        });
  }

  deleteItem(id) {
    this.isWaiting=true;
    this.notification.Confirm(
      "Are you sure you want to delete?"
    ).then(result => {
      if (result) {
        this.equipmentService.deleteEquipment(id)
      .then(result => {
        this.isWaiting=false;
				  this.notification.ShowInfo('Deleted Successfully.');
        this.getList();
			  });
      }
      else{
        this.isWaiting=false;
      }
    });
  }

  saveItem(item) {
    return this.equipmentService.saveEquipment(this.equipmentAggregate.Equipment);
  }
  convertArrayOfObjectsToCSV(args) {  
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        this.notification.error("No data available to export.");          
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    var columnHeaders = ['Name','Purchase Date','Serial No','Weight','Organization','Date Created'];
    
    result = '';
    result += columnHeaders.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
  }

    downloadCSV(args) {  
    var data, filename, link;

    //Setting Entity Attributes

    var csv = this.convertArrayOfObjectsToCSV({data:this.equipmentstocsv});
    if (csv == null) return;

    filename = args.filename || 'OtherEquipmentsDetails.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();

    }
}
