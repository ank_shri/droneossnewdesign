import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class EquipmentService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getEquipments() {
      return this.http.fetch(this.baseApi + 'equipment/listing')
      .then(response => response.json());
  }

  getNewEquipment() {
      return this.http.fetch(this.baseApi + 'equipment/new')
      .then(response => response.json());
  }

  getEquipment(id) {
    return this.http.fetch(this.baseApi + 'equipment/' + id)
      .then(response => response.json());
  }

  deleteEquipment(id) {
    return this.http.fetch(this.baseApi + 'equipment/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveEquipment(Equipment) {
    return this.http.fetch(this.baseApi + 'equipment/', {
      method: 'post',
      body: json(Equipment)
    }).then(response => response.json());
  }
}
