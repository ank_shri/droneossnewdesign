import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
// import {Notification} from 'aurelia-notification';

@inject(EventAggregator)
export class EquipmentView {
  @bindable equipment;
  @bindable drones;
  @bindable types;
  @bindable save;
  @bindable organizations;

  constructor(events) {
    this.events = events;
    // this.notification = notification;
    this.massUnit = localStorage["MassUnit"];
  }

  bind() {
    this.heading = this.equipment.Name != null ? 'View Equipment' : 'Add Equipment';
  }

  cancel() {
    this.events.publish('equipment-item-operation', {refresh: false});
  }

  edit(equipmentId) {
    this.events.publish('equipment-item-operation-edit', {id: equipmentId});
  }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
