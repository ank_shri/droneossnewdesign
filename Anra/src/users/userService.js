import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class UserService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getRole() {
      return this.http.fetch(this.baseApi + 'users/getrole')
      .then(response => response.text());
  }

  getUsers() {
      return this.http.fetch(this.baseApi + 'users/listing')
      .then(response => response.json());
  }

  getNewUser() {
      return this.http.fetch(this.baseApi + 'users/NewUser')
      .then(response => response.json());
  }

  getUserByLoginId() {
      return this.http.fetch(this.baseApi + 'users/LoginUser')
      .then(response => response.json());
  }

  getUser(id) {
    return this.http.fetch(this.baseApi + 'users/' + id)
      .then(response => response.json());
  }

  deleteUser(id) {
    return this.http.fetch(this.baseApi + 'users/' + id, {
        method: 'delete'
      })
      .then(response => {          
          response.json();
        }
      );
  }

  saveUser(user) {
    return this.http.fetch(this.baseApi + 'users/SaveUser', {
      method: 'post',
      body: json(user)
    }).then(response => response.json());
  }

  changePassword(user) {
      return this.http.fetch(this.baseApi + 'users/ChangePassword', {
          method: 'post',
          body: json(user)
      }).then(response => response.json());
  }

  resetPassword(user) {
      return this.http.fetch(this.baseApi + 'users/ResetPassword', {
          method: 'post',
          body: json(user)
      }).then(response => response.json());
  }

  associateDrones(associations) {
          return this.http.fetch(this.baseApi + 'users/associate-drones', {
                  method: 'post',
                  body: json(associations)
                })
        .then(response => response.json());
    }

  getAssociatedDrones(userId) {
        return this.http.fetch(this.baseApi + 'users/associated-drones/' + userId)
          .then(response => response.json());    
      }

  //For Profile Page
  getDashboard() {
    return this.http.fetch(this.baseApi + 'Dashboard')
      .then(response => response.json());
  }  
  getUserDetail(id){
      return this.http.fetch(this.baseApi +'users/UserDetail/'+id)
      .then(response=>response.json());
  }  
  getCertificates() {
    return this.http.fetch(this.baseApi + 'training/listing/')
    .then(response => response.json());
}

}
