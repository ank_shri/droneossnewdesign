import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';
import {UserService} from 'users/userService';
import moment from 'moment';
@inject(EventAggregator, Notification,UserService)
export class UserView {
  @bindable user;
  @bindable save;
  @bindable dashboardItem;
  
  


  constructor(events, notification,userService) {
    this.events = events;
    this.notification = notification;
    this.userService=userService;
    this.certificateUser=[];
    this.certificateDetails=[];
    this.mimetypeToext = {
      'image/jpeg': 'jpg',
      'image/png': 'png',
      'image/gif': 'gif',
      'application/pdf': 'pdf'
  };
  }

  bind() {    
    this.heading = this.user.UserName != null ? 'View User' : 'Add User';
  }

  saveItem() {
    return this.save()
          .then(result => {
              this.handleSaveResult(result)
          })
        .catch((e) => {
        this.notification.error(e);
      });
  }

 getUserCertificates()
 {
  this.userService.getCertificates()
  .then(result2 => {

    if (this.RoleType == 'Super Admin' || this.RoleType == 'Adminstrator') {
      this.certificateDetails = result2;
    } else {
      this.certificateUser = result2;
      for (var i = 0; i < this.certificateUser.length; i++) {
        var obj = this.certificateUser[i];
        if (obj.UserId == this.user.Id) {
          this.certificateDetails.push(obj);
          console.log(this.certificateDetails);
        }
      }
    }
  }).catch((e) => {
    this.notification.error(e.message);
    this.isWaiting = false;
  });
 }
  getPDF(){
 
    var HTML_Width = $(".canvas_div_pdf").width();
    var HTML_Height = $(".canvas_div_pdf").height();
    var top_left_margin = 15;
    var PDF_Width = HTML_Width+(top_left_margin*2);
    var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;
    
    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
    
    
    html2canvas($(".canvas_div_pdf")[0],{allowTaint:true}).then(function(canvas) {
    canvas.getContext('2d');
    
    console.log(canvas.height+"  "+canvas.width);
    
    
    var imgData = canvas.toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
    
    
    for (var i = 1; i <= totalPDFPages; i++) { 
    pdf.addPage(PDF_Width, PDF_Height);
    pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
    }
    
        pdf.save("Profile.Html.pdf");
           });
    }
    
  attached()
  {
    this.userService.getUserDetail(this.user.Id)
    .then(result2=> {
        this.userDetail = result2;
        console.log(result2);
    });
    this.getUserCertificates();
    var date= new Date();
    var current_date=moment(date).format('YYYY-MM-DD');
    if(this.user.PilotLicenseExpiryDate=='0001-01-01')
    {
      this.user.PilotLicenseExpiryDate=current_date;
    }
    if(this.user.MedicalLicenseExpiryDate == '0001-01-01')
    {
      this.user.MedicalLicenseExpiryDate=current_date;
    }   
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('item-operation', {refresh: true});
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('users-item-operation', {refresh: false});
  }

  edit(userId) {
      this.events.publish('users-item-operation-edit', {id: userId});
  }
  getContentType(data) {
        if (data.charAt(0) == '/') {
          return "image/jpeg";
        }
        else if (data.charAt(0) == 'R') {
          return "image/gif";
        }
        else if (data.charAt(0) == 'i') {
          return "image/png";
        }
        else if (data.charAt(0) == 'J') {
          return "application/pdf";
        }
      }
    
      base64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
    
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
    
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);
    
          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }
    
          var byteArray = new Uint8Array(byteNumbers);
    
          byteArrays.push(byteArray);
        }
    
        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
      }
    
      makeFileName(filename,mimetype) {
          var newFileName = filename;
    
          if (this.mimetypeToext.hasOwnProperty(mimetype)) {
              newFileName = filename + '.' + this.mimetypeToext[mimetype];
          }
    
          return newFileName;
      }
    
      downloadDocument(data,filename) {
        if (/Edge/.test(navigator.userAgent)) {
          var contentType = this.getContentType(data);
          var blob = this.base64toBlob(data, contentType);
          var fileName = this.makeFileName(filename, contentType);
          navigator.msSaveBlob(blob, fileName);
        }
        else {
          var contentType = this.getContentType(data);
          var blob = this.base64toBlob(data, contentType);
    
          //Code to download File Locally
          var fileName = this.makeFileName(filename,contentType);
          var url = window.URL.createObjectURL(blob);
          var link = document.createElement('a');
          link.setAttribute('href', url);
          link.setAttribute('download', fileName);
          link.click();
          window.URL.revokeObjectURL(url);
        }
        }

}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
