﻿import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';
import {UserService} from 'users/userService';
import {Router} from 'aurelia-router';



@inject(UserService, EventAggregator, Notification,Router)
export class ProfileView {
  @bindable user;
  @bindable save;

  constructor(userService, events, notification,router) {
      this.userService = userService;
        this.events = events;
        this.notification = notification;
        this.user = null;
        this.router=router;
        this.loginId = localStorage["email"];
        this.isEditing = false;
        this.isViewing = false;
        this.isWaiting=false;
        this.dashboardItem=[];
        this.userDetail=null;
        this.certificateDetails=[];
        this.certificateUser=[];
        this.UserId=null;
        this.ProfilePhoto=null;
        this.mimetypeToext = {
            'image/jpeg': 'jpg',
            'image/png': 'png',
            'image/gif': 'gif',
            'application/pdf': 'pdf'
        },
        this.pageSize = UserService.defaultPagingSize;
    }

  activate() {
      this.loginId=localStorage["email"];
      this.UserId=localStorage["UserId"];
      this.RoleType=localStorage["Roles"];
      this.ProfilePhoto=localStorage["ProfilePhoto"];
      this.viewItem(this.loginId);
      this.userService.getUserDetail(this.UserId)
      .then(result2=> {
          this.userDetail = result2;
          console.log(this.userDetail);
      });
      console.log(this.UserId);
      
    }

  attached() {
      this.subscribeEvents();
      this.userService.getDashboard()
      .then(result => {
          this.dashboardItem = result.Flights;
          this.getUserCertificates();
      });
     
      
      
  }
  detached() {
    this.subscriber.dispose();
  }

  subscribeEvents() {
    this.subscriber= this.events.subscribe('users-item-operation', payload => {

          if (payload) {
              if (payload.refresh) {
                  this.viewItem(this.loginId);
              }
              this.reset();
          }
      });
  }
  
  getUserCertificates()
  {
    // this.isWaiting=true;    
   this.userService.getCertificates()
   .then(result2 => {
       this.certificateUser = result2;
       for (var i = 0; i < this.certificateUser.length; i++) {
         var obj = this.certificateUser[i];
         if (obj.UserId == this.UserId) {
           this.certificateDetails.push(obj);
         }
       }
       this.isWaiting=false;
   }).catch((e) => {
     this.notification.error(e.message);
     this.isWaiting = false;
   });
  }

  getPDF(){
 
    var HTML_Width = $(".canvas_div_pdf").width();
    var HTML_Height = $(".canvas_div_pdf").height();
    var top_left_margin = 15;
    var PDF_Width = HTML_Width+(top_left_margin*2);
    var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;
    
    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
    
    
    html2canvas($(".canvas_div_pdf")[0],{allowTaint:true}).then(function(canvas) {
    canvas.getContext('2d');
    
    console.log(canvas.height+"  "+canvas.width);
    
    
    var imgData = canvas.toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
    
    
    for (var i = 1; i <= totalPDFPages; i++) { 
    pdf.addPage(PDF_Width, PDF_Height);
    pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
    }
    
        pdf.save("Profile.Html.pdf");
           });
    }
  reset() {
      this.user.CurrentPassword=null;
      this.user.NewPassword=null;
      this.isEditing = false;
      this.isViewing = false;
  }

  changePassword() {
      this.isEditing = true;
    }
    viewFlight(id){      
        this.router.navigate('flight-data-listing/profile/' + id);      
    }
  
  viewItem(loginid) {
    this.isWaiting=true; 
    this.userService.getUserByLoginId(loginid)
        .then(result => {
            this.user = result;
            this.user.Fullname=localStorage['UserName'];
            this.isEditing = false;
            this.isWaiting=false; 
        });
    }

  savePassword() {
      return this.userService.changePassword(this.user);
    }
    //Save File Download
    fileSelected(event) {
        let reader = new FileReader();
        let file = event.target.files[0];
        
        if(event.target.id=='licensedoc')
        {
            reader.readAsDataURL(file);
            let fileName = file.name;
            reader.onload = () => {
                this.user.LicenseFile = reader.result.split(',')[1];
            };
        }
        if(event.target.id=='medicaldoc')
        {
            reader.readAsDataURL(file);
            let fileName = file.name;
            reader.onload = () => {
                this.user.MedicalFile = reader.result.split(',')[1];
            };
        }
        if(event.target.id=='otherdoc')
        {
            reader.readAsDataURL(file);
            let fileName = file.name;
            reader.onload = () => {
                this.user.OtherFile = reader.result.split(',')[1];
            };
        }
  
    }
    getContentType(data) {
        if (data.charAt(0) == '/') {
          return "image/jpeg";
        }
        else if (data.charAt(0) == 'R') {
          return "image/gif";
        }
        else if (data.charAt(0) == 'i') {
          return "image/png";
        }
        else if (data.charAt(0) == 'J') {
          return "application/pdf";
        }
      }
    
      base64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
    
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
    
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);
    
          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }
    
          var byteArray = new Uint8Array(byteNumbers);
    
          byteArrays.push(byteArray);
        }
    
        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
      }
    
      makeFileName(filename,mimetype) {
          var newFileName = filename;
    
          if (this.mimetypeToext.hasOwnProperty(mimetype)) {
              newFileName = filename + '.' + this.mimetypeToext[mimetype];
          }
    
          return newFileName;
      }
    
      downloadDocument(data,filename) {
        
          var contentType = this.getContentType(data);
          var blob = this.base64toBlob(data, contentType);
    
          //Code to download File Locally
          var fileName = this.makeFileName(filename,contentType);
          var url = window.URL.createObjectURL(blob);
          var link = document.createElement('a');
          link.setAttribute('href', url);
          link.setAttribute('download', fileName);
          link.click();
          window.URL.revokeObjectURL(url);
      }

}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
