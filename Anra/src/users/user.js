import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Notification} from 'aurelia-notification';
import { Router } from 'aurelia-router';
import Croppie from 'croppie';
import moment from 'moment';

@inject(EventAggregator, Router, Notification, NewInstance.of(ValidationController))
export class User {
  @bindable user;
  @bindable save;
  confirmPasswordError = '';
  controller = null;
  rules = null;

  constructor(events, router, notification, controller) {
    this.events = events;
    this.router = router;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.ConfirmPassword='';
    this.IsPasswordsMatched=true;
    this.allowedFiles = [".doc", ".docx", ".pdf",".jpg", ".jpeg", ".png"];
    this.isWaiting = false;
    this.isProfilePhotoSelected = false;
    this.uploadCrop = null;
    this.isCropped = false;
    this.filesData='';
    this.mimetypeToext = {
        'image/jpeg': 'jpg',
        'image/png': 'png',
        'image/gif': 'gif',
        'application/pdf': 'pdf'
    }
  }

  userComparer = (userA, userB) => userA.id === userB.id;
      
  attached(){      
      if(this.user.UserName != null)
      {
          this.setSelectedRole();
      } 
      var date= new Date();
      var current_date=moment(date).format('YYYY-MM-DD');
      if(this.user.PilotLicenseExpiryDate=='0001-01-01')
      {
        this.user.PilotLicenseExpiryDate=current_date;
      }
      if(this.user.MedicalLicenseExpiryDate == '0001-01-01')
      {
        this.user.MedicalLicenseExpiryDate=current_date;
      }  
      this.setupValidationRules();
  }

  setSelectedRole(){

      var matched= this.user.SelectedRole;
      $("#ddlRole option").each(function(i){
          for(let i=0 ; i < matched.length ; i++)
          {
              if(matched[i].RoleId === parseInt($(this).val()))
              {
                  $(this).prop("selected", "selected");
              }
          }
      });
  }

  bind() {
    this.heading = this.user.UserName != null ? 'Edit User' : 'Add User';
  }

  setupValidationRules() {   

      this.rules = ValidationRules
            .ensure('FirstName').maxLength(30).required() 
            .ensure('LastName').maxLength(30).required()              
            .ensure('Email').matches(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).required()
            .ensure('PhoneNumber').required()
            .ensure('IMEINumber').maxLength(15).required()   
            .ensure('Password').matches(/^(?=.*\d)(?=.*[a-z]).{6,}$/).required()
            .rules;
  }

  removeRule(propName){
      let list = this.rules[0];
      for( var i = list.length; i--;){
          if ( list[i].property.name === propName) list.splice(i, 1);
      }
  }


  saveItem() {

      this.isWaiting = true;

      //In case of Edit, skip Password Validation
      if(this.user.UserName != null){
        this.removeRule('Password');
      }

      this.controller.validate({ object: this.user, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.isCropped=false;  
                  //Check For File Extension Validation
                  if(this.validateFileExtension())
                  {
                      if(this.isRoleSelected()){
                          //Compare Passwords
                          if(this.comparePassword()){
                              return this.save()
                                  .then(result => {
                                      this.handleSaveResult(result);
                                      this.isWaiting = false;
                                  });
                          }
                      }
                      else{
                          this.notification.error("Please select role.");
                      }

                  }
                  else
                  {
                      this.notification.error("Please upload files having extensions: " + this.allowedFiles.join(', ') + " only.");
                      this.organization.logoFile=null;
                  }
              }

              else {
                  // validation failed
                  this.notification.error('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false; 
          });
            
          this.confirmPassword='';
          this.isWaiting = true;
          $('#licensedoc').val(null);
          $('#medicaldoc').val(null);
          $('#otherdoc').val(null);
          
  }

  handleSaveResult(result) {
      if (result.IsSuccess) {
          this.notification.info(result.SuccessMessage);
          this.events.publish('users-item-operation', {refresh: true});
      }
      else {
        this.notification.error(result.ErrorMessage);
      }
      this.ConfirmPassword='';
  }

  fileSelected(event) {
      let reader = new FileReader();
      let file = event.target.files[0];
      
      

      if(event.target.id=='licensedoc')
      { 
          reader.readAsDataURL(file);
          let fileName = file.name;
          console.log(fileName);
          reader.onload = () => {
              this.user.LicenseFile = reader.result.split(',')[1];
          };
      }
      
      if(event.target.id=='medicaldoc')
      {
          reader.readAsDataURL(file);
          let fileName = file.name;
          
          reader.onload = () => {
              this.user.MedicalFile = reader.result.split(',')[1];
          };
      }
      if(event.target.id=='otherdoc')
      {
          reader.readAsDataURL(file);
          let fileName = file.name;
          reader.onload = () => {
              this.user.OtherFile = reader.result.split(',')[1];
          };
      }

  }

  cancel() {
      this.ConfirmPassword='';
      this.IsPasswordsMatched=true;
      this.isCropped=false;
    this.events.publish('users-item-operation', {refresh: false});
  }

  getContentType(data) {
    if (data.charAt(0) == '/') {
      return "image/jpeg";
    }
    else if (data.charAt(0) == 'R') {
      return "image/gif";
    }
    else if (data.charAt(0) == 'i') {
      return "image/png";
    }
    else if (data.charAt(0) == 'J') {
      return "application/pdf";
    }
  }

  base64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  makeFileName(filename,mimetype) {
      var newFileName = filename;

      if (this.mimetypeToext.hasOwnProperty(mimetype)) {
          newFileName = filename + '.' + this.mimetypeToext[mimetype];
      }

      return newFileName;
  }

  downloadDocument(data,filename) {
    if (/Edge/.test(navigator.userAgent)) {
      var contentType = this.getContentType(data);
      var blob = this.base64toBlob(data, contentType);
      var fileName = this.makeFileName(filename, contentType);
      navigator.msSaveBlob(blob, fileName);
    }
    else {
      var contentType = this.getContentType(data);
      var blob = this.base64toBlob(data, contentType);

      //Code to download File Locally
      var fileName = this.makeFileName(filename, contentType);
      var url = window.URL.createObjectURL(blob);
      var link = document.createElement('a');
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.click();
      window.URL.revokeObjectURL(url);
    }
  }

  setselectedOption(event){
      this.user.SelectedRole=[];
      var cnt=event.target.options;
      for(let i = 0; i < event.target.options.length; i++) {
          let option = event.target.options[i];                    
            var item = {
                'RoleId': parseInt(option.value),
                'RoleName': option.text,
                'IsSelected': option.selected
            }
            console.log(option);
            //Only Push selected option to SelectedRole
            if(item.IsSelected==true)
            {
                this.user.SelectedRole.push(item);          
            }            
      }
  }

  comparePassword() {

      if(this.user.UserName == null)
      {
          if (this.user.Password!=null && this.ConfirmPassword.length > 0 && (this.user.Password == this.ConfirmPassword)) {
              this.IsPasswordsMatched=true;
          }
          else{
              this.IsPasswordsMatched=false;
          }
      }

      return this.IsPasswordsMatched;
  }

  isRoleSelected(){
      return this.user.SelectedRole != null && this.user.SelectedRole.length > 0;
  }

  hideErrorMsg(){
      this.IsPasswordsMatched=true;
  }

  validateFileExtension() {
  
      var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + this.allowedFiles.join('|') + ")$");
      var licensefile=document.getElementById('licensedoc');
      var medicalfile=document.getElementById('medicaldoc');
      var otherfile=document.getElementById('otherdoc');

      var flagLicense=true;
      var flagMedical=true;
      var flagOther=true;

      if ( licensefile.files.length > 0 && licensefile.files[0].name!=null && !regex.test(licensefile.files[0].name.toLowerCase())) 
      {
              flagLicense=false;
      }

      if ( medicalfile.files.length > 0 && medicalfile.files[0].name!=null && !regex.test(medicalfile.files[0].name.toLowerCase())) 
      {
          flagMedical=false;
      }

      if ( otherfile.files.length > 0 && otherfile.files[0].name!=null && !regex.test(otherfile.files[0].name.toLowerCase())) 
      {
          flagOther=false;
      }


      return flagLicense && flagMedical && flagOther;
  }

  uploadPhoto(){
      this.isProfilePhotoSelected = true;      
  }

  readProfilePhoto(event){

      if (this.uploadCrop) {
          this.uploadCrop.destroy();
      }

      var el = document.getElementById('profilephotouploaded');
      this.uploadCrop = new Croppie(el,{
          viewport: {
              width: 300,
              height: 300,
              type: 'circle'
          },
          boundary: {
              width: 300,
              height: 300
          }
      });

      var me = this;      
      var reader = new FileReader();
      let file = event.target.files[0];
      
      reader.onload = function (e) {
          $('.upload').addClass('ready');
          me.uploadCrop.bind({
              url: e.target.result
          }).then(function(){
              console.log('jQuery bind complete');
          });
      }

      reader.readAsDataURL(file);
  }

  cropPhoto(){
      var me = this;
      this.uploadCrop.result({type:'base64',format:'jpeg'}).then(function(base64) {
          // do something with cropped image
          me.user.ProfilePhoto = base64;       
          me.isCropped = true;
          me.notification.info("Profile Photo Cropped Successfully.");
          me.closeDialog();
      });  
  }

  detached() {
      if ($('.modal-backdrop')) {
          $('.modal-backdrop').remove();
      }

      //Finally Reset Validation
      this.controller.reset();
  }

  closeDialog() {      
      $('.upload').removeClass('ready');   
      
      this.isProfilePhotoSelected = false; 

      if (this.uploadCrop) {
          $('#upload')[0].value = null;
      }

      $('.modal-backdrop').remove();      
  }

  resetPassword() {
      this.router.navigate('users/reset-password/' + this.user.UserId);
  }
}
