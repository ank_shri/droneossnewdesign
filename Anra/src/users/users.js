import {inject,TaskQueue} from 'aurelia-framework';
import {UserService} from 'users/userService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import {PermissionService} from 'security/permissionService';
// import {Notification} from 'aurelia-notification';
// import {DialogService} from 'aurelia-dialog';
// import { Prompt } from 'components/my-modal';
import moment from 'moment';

@inject(UserService, Router, EventAggregator, TaskQueue, PermissionService)
export class Users {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['FullName','Email','RoleName','StatusText']},
    ];

    constructor(userService, router, events, taskQueue, permissionService) {
        this.userService = userService;
        this.router = router;
        this.events = events;
        this.taskQueue = taskQueue;
        this.permissionService = permissionService;
        // this.notification = notification;
        //state
        this.heading = 'Users';
        this.users = [];
        this.userstocsv=[];
        this.roleName = "";
        this.user = null;
        this.isEditing = false;
        this.isViewing = false;
        this.dashboardItem=null;
        this.UserID=localStorage["UserId"];
        this.permission={};
        this.certificateDetails=[];
        // this.dialogService = dialogService;
        //Set Default Paging Size
        this.pageSize = userService.defaultPagingSize;
        this.isWaiting = false;
    }

    activate() {
        this.getList();
    }

    ConvertNewJson()
    {
      var newJson=[];
      this.users.forEach(function(item) {
      newJson.push({
      "Name": item.FullName,
      "Email":item.Email,
      "Role Name":item.RoleName,
      "Staus":item.StatusText,
      "Organization":item.OrganizationName ,
      "Date Created": moment(item.DateCreated).format('YYYY-MM-DD')
      });
      });
      this.userstocsv=newJson;
    }

    attached() {
        this.getuserDashboard();
        this.subscribeEvents();
        this.getuserDetail();
    }
    getuserDashboard(){
        this.userService.getDashboard()
      .then(result => {
          this.dashboardItem = result.Flights;
      });
    }
    getuserDetail(id){
        this.userService.getUserDetail(id)
        .then(result2=> {
            this.userDetail = result2;
        }); 
      }
    detached() {
      this.subscriber.dispose();
    }
    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
              this.getRole();
          });
    }
    
    getRole() {
        if(localStorage["Roles"].indexOf("Super Admin") > -1)
        {
            this.roleName = "super admin";
            return;
        }

        if(localStorage["Roles"].indexOf("Admin") > -1)
        {
            this.roleName = "admin";
            return;
        }

        this.roleName = "others";   
    }

    subscribeEvents() {
      this.subscriber = this.events.subscribe('users-item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                }
                this.reset();
            }
        });

      this.subscriber = this.events.subscribe('users-item-operation-edit', payload => {

            if (payload) {
                if (payload.id !=null && payload.id.length > 0) {
                    this.editItem(payload.id);
                }
                this.reset();
            }
        });
    }

    reset() {
        this.user = null;
        this.isEditing = false;
        this.isViewing = false;
    }

  add() {
       this.isWaiting = true;
        this.userService.getNewUser()
          .then(result => {
              this.user = result;
              this.isEditing = true;
              this.isWaiting = false;
          });
    }

    

    editItem(id) {
        this.isWaiting = true;
        this.userService.getUser(id)
          .then(result => {
            this.user = result;
            console.log(result);
            this.user.PilotLicenseExpiryDate = moment(result.PilotLicenseExpiryDate).format('YYYY-MM-DD');
            this.user.MedicalLicenseExpiryDate = moment(result.MedicalLicenseExpiryDate).format('YYYY-MM-DD');
              this.isEditing = true;
              this.isWaiting = false;
            console.log(result);  
          });
    }

  viewItem(id) {
        this.isWaiting = true;
        this.userService.getUser(id)
          .then(result => {
            this.user = result;
            this.user.PilotLicenseExpiryDate = moment(result.PilotLicenseExpiryDate).format('YYYY-MM-DD');
            this.user.MedicalLicenseExpiryDate = moment(result.MedicalLicenseExpiryDate).format('YYYY-MM-DD');
            this.isViewing = true;
            this.isWaiting = false;
            this.certificateDetails=[];  
            console.log(result);
          });
    }

    deleteItem(id) {
    this.dialogService.open({viewModel: Prompt, model: 'Are you sure you want to delete?'}).then(response => {
         if (!response.wasCancelled) {
        this.userService.deleteUser (id)
          .then(result => {
				  this.notification.error('Deleted Successfully.');
              this.getList();
			  });
         }
          });
    }

    saveItem(item) {
        return this.userService.saveUser(this.user);
    }

    getList() {
        this.isWaiting = true;
        this.userService.getUsers()
        .then(result => {
            this.users = result.Users;  
          this.users.map(x=>{
             var roleName = x.RoleName.toString();
             var rolesArray= roleName.split(',');      
             var pilotRole = rolesArray.filter(i => i == 'Pilot');
            if(pilotRole.length>0)
            {
            x.pilotRole = true; 
            }
            else
            {
            x.pilotRole = false; 
            }
          });
          console.log(this.users);
            this.isEditing = false;
            this.getPermission(1);
            this.isWaiting = false;
            this.ConvertNewJson();
            console.log('USERS',this.users);
        }).catch((e) => {
            this.notification.error(e.message);
            this.isWaiting = false;
        });
    }

    associateDrones(item){        
        this.router.navigate('user-drone-association/'+item.Id);
    }

    resetPassword(id){
        this.router.navigate('users/reset-password/' + id);
    }

    //Download CSV
  convertArrayOfObjectsToCSV(args) {  
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        this.notification.error("No data available to export.");          
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    var columnHeaders = ['Name','Email','Role Name','Status','Organization','Date Created'];
    
    result = '';
    result += columnHeaders.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
    }

    downloadCSV(args) {  
    var data, filename, link;

    //Setting Entity Attributes

    var csv = this.convertArrayOfObjectsToCSV({data:this.userstocsv});
    if (csv == null) return;

    filename = args.filename || 'UsersReport.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();



    }
}
