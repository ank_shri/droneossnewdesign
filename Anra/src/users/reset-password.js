﻿import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Notification} from 'aurelia-notification';
import {Router} from 'aurelia-router';
import {UserService} from 'users/userService';
import {PermissionService} from 'security/permissionService';


@inject(EventAggregator, Notification, UserService, Router, PermissionService, NewInstance.of(ValidationController))
export class ResetPassword {
    loginError = '';
    controller = null;
    rules = null;

    constructor(events, notification, userService, router, permissionService, controller) {
        this.events = events;
        this.userService = userService;
        this.permissionService = permissionService;
        this.notification = notification;
        this.router = router;
        this.roleName = "";
        this.confirmPassword='';
        this.user = null;
        this.userId=null;
        this.permission={};
        this.controller = controller;
        this.controller.addRenderer(new BootstrapFormValidationRenderer());
        this.controller.validateTrigger = validateTrigger.change;
    }

    activate(params) {
        this.userId=params.id;
        this.userService.getUser(this.userId)
          .then(result => {
              this.user = result;              
              this.getPermission(1);
              this.heading = 'Reset Password for ' + this.user.FullName;
            });        
    }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
              this.getRole();
          });
    }

    getRole() {
        this.userService.getRole()
        .then(result => {
            this.roleName = result;
        });
    }


    attached() {
        this.setupValidationRules();
    }

    detached() {
        //Finally Reset Validation
        this.controller.reset();
    }

    setupValidationRules() {   

        this.rules = ValidationRules
             .ensure('NewPassword').matches(/^(?=.*\d)(?=.*[a-z]).{6,}$/).required()
             .rules;
    }

   
    resetPassword() {
        this.loginError='';
        
        $.loader.open();

        this.controller.validate({ object: this.user, rules: this.rules })      
            .then(valresult => {
                if (valresult.valid) {
                    // validation succeeded
                    if(this.comparePassword())
                    {
                        this.userService.resetPassword(this.user)
                            .then(result => {
                                this.handleSaveResult(result);
								this.notification.info('Password updated successfully.');
                                $.loader.close(true);
                            }).catch((e) => {
                                this.notification.error(e);
                                this.loginError = error.response;
                                $.loader.close(true);
                            });
                    }
                } 
                else {
                    // validation failed
                    this.notification.error('Please enter a valid input.');
                    $.loader.close(true);
                }
            }).catch((e) => {
                this.notification.error('Error.');
                $.loader.close(true);
            });

        $.loader.close(true);
    }

    handleSaveResult(result){
        if (result.IsSuccess) {
            this.notification.success(result.SuccessMessage);
            //this.events.publish('item-operation', {refresh: true});
            this.router.navigate('/Users');        
        }
        else {
            this.notification.success(result.ErrorMessage);
        }
    }

    cancel() {
        this.loginError='';
        this.router.navigate('/Users');        
    }

    comparePassword() {

        if (this.user.UserName != null && this.user.NewPassword !=null && (this.user.NewPassword != this.confirmPassword)) {
            this.loginError='Passwords do not match.';
            return false;
        }
        else{
            return true;
        }
    }
}
