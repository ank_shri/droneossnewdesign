﻿import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Notification} from 'aurelia-notification';


@inject(EventAggregator, Notification, NewInstance.of(ValidationController))
export class ChangePassword {
  @bindable user;
  @bindable save;
  controller = null;
  rules = null;
  loginError = '';

  constructor(events, notification, controller) {
      this.events = events;
      this.notification = notification;
      this.controller = controller;
      this.controller.addRenderer(new BootstrapFormValidationRenderer());
      this.controller.validateTrigger = validateTrigger.change;
  }    

    attached() {
        this.setupValidationRules();
    }

    detached() {
        //Finally Reset Validation
        this.controller.reset();
    }

    bind() {
        this.heading = 'Change Password';
    }

    setupValidationRules() {   

        this.rules = ValidationRules
             .ensure('CurrentPassword').matches(/^(?=.*\d)(?=.*[a-z]).{6,}$/).required()
             .ensure('NewPassword').matches(/^(?=.*\d)(?=.*[a-z]).{6,}$/).required()
             .rules;
    }
    
    savePassword() {
       
        this.loginError='';
        
        $.loader.open();

        this.controller.validate({ object: this.user, rules: this.rules })
            .then(valresult => {
                if (valresult.valid) {
                    // validation succeeded
                    this.save()
                        .then(result => {
                            this.handleSaveResult(result);
								//this.notification.info('Password updated successfully.');
                                $.loader.close(true);
                            }).catch((e) => {
                                this.notification.error(e);
                                this.loginError = error.response;
                                $.loader.close(true);
                            });
                } 
                else {
                    // validation failed
                    this.notification.error('Please enter a valid input.');
                    $.loader.close(true);
                }
            }).catch((e) => {
                this.notification.error('Error.');
                $.loader.close(true);
            });

        $.loader.close(true);
    }

    handleSaveResult(result) {
        if (result.IsSuccess) {
            this.notification.info('Password updated successfully.');
            this.events.publish('users-item-operation', {refresh: true});
          
        }
        else {
            this.loginError = "Incorrect current password";
        }
        
    }

    cancel() {
        this.loginError='';
        this.events.publish('users-item-operation', {refresh: false});
    }

}
