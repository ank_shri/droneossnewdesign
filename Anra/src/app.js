import {inject} from "aurelia-framework";
import {Router} from 'aurelia-router';
import {AuthService} from "aurelia-authentication";
import AppRouterConfig from "./common/routes.config";

@inject(Router, AuthService, AppRouterConfig)
export class App {

  constructor(router, authService, appRouterConfig) {
    this.router = router;
    this.authService = authService;
    this.appRouterConfig = appRouterConfig;
    // appRouterConfig.configure();
    //this.username = '';
    //this.isAuthenticated = false;
    //this.authService = authService;
  }


  activate() {
    this.appRouterConfig.configure();
  }

  attached() {
    /*UIEvent.subscribe('security:loggedin', data => {
     if (this.authService.isAuthenticated()) {
     let payload = this.authService.getTokenPayload();
     this.username = payload.sub;
     console.log(payload);
     //this.app.login('home', {'X-Authorization': `${payload["EmailId"]}:${this.authService.getAccessToken()}`});
     }
     });*/
  }

  onLogout() {
    //this.authService.logout();
    //this.app.logout();
  }
}
