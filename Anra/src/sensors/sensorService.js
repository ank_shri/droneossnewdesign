import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class SensorService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getSensors() {
      return this.http.fetch(this.baseApi + 'Sensor')
      .then(response => response.json());
  }

  getNewSensor() {
      return this.http.fetch(this.baseApi + 'Sensor/new')
      .then(response => response.json());
  }

  getSensor(id) {
    return this.http.fetch(this.baseApi + 'Sensor/' + id)
      .then(response => response.json());
  }

  deleteSensor(id) {
    return this.http.fetch(this.baseApi + 'Sensor/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveSensor(Sensor) {
    return this.http.fetch(this.baseApi + 'Sensor/', {
      method: 'post',
      body: json(Sensor)
    }).then(response => response.json());
  }
}
