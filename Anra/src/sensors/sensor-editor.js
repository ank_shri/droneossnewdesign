import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
// import {Notification} from 'aurelia-notification';
import {AnraNotification} from '../components/anra-notification';
import moment from 'moment';
import {UIApplication} from "aurelia-ui-framework";


@inject(EventAggregator, NewInstance.of(ValidationController),AnraNotification,UIApplication)
export class SensorEditor {
  @bindable sensor;
  @bindable drones;
  @bindable save;
  @bindable organizations;
  controller = null;
  rules = null;

  constructor(events, controller,notification,application) {
    this.events = events;
   
    this.massUnit = localStorage["MassUnit"];
    this.controller = controller;
    this.notification = notification;
    this.application= application;
    // this.controller.addRenderer(new BootstrapFormValidationRenderer());
    // this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
    this.ShowSensorInfo= true;
  }

  attached() {
  
      $('#purchaseDate').attr("max",moment().format('YYYY-MM-DD'));
      this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
    this.heading = this.sensor.Name != null ? 'Edit Sensor' : 'Add Sensor';
  }

  setupValidationRules() {   

      //Custom Rule For Date Validation  
      ValidationRules.customRule(
        'date',
        (value, obj) => value === null || value === undefined || value instanceof Date || moment(value,"YYYY-MM-DD",true).isValid(),
        '\${$displayName} must be a Date.'
      );

      this.rules = ValidationRules
           .ensure('Name').required()    
           .ensure('SerialNumber').required()
           .ensure('DroneId').matches(/^[1-9][0-9]*$/).required()
           .ensure('Weight').required()
           .ensure('FirmwareVersion').required()
           .ensure('HardwareVersion').required()
           .ensure('PurchaseDate').required().satisfiesRule('date')
           .rules;
  }

  saveItem() {
      this.isWaiting = true;
      this.controller
      .validate({object: this.sensor, rules: this.rules})    
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
						  if(this.sensor.SensorId!='')
							{
								this.notification.ShowInfo('Updated Successfully.');
							}
							else
							{
								this.notification.ShowInfo('Added Successfully.');  
							}
						  this.isWaiting = false;
                      })
              } 
              else {
                let messages = this.controller.errors.map(x => x.message);
      
                this.application.toast(
                  Object.assign({ container: this.errorHolder }, {
                    title: 'Errors!',
                    message: messages.join(', '),
                    theme: 'danger',
                    timeout: 2000,
                    glyph: 'glyph-alert-info'
                  })
                );
      
                this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
          });

  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('sensor-item-operation', {refresh: true});
    }
    else {
      this.notification.ShowInfo(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('sensor-item-operation', {refresh: false});
  }
}
