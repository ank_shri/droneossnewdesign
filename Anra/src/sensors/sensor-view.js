import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
// import {Notification} from 'aurelia-notification';

@inject(EventAggregator)
export class SensorView {
  @bindable sensor;
  @bindable drones;
  @bindable types;
  @bindable save;
  @bindable organizations;

  constructor(events) {
    this.events = events;
    // this.notification = notification;
    this.massUnit = localStorage["MassUnit"];
  }

  bind() {
    this.heading = this.sensor.Name != null ? 'View Sensor' : 'Add Sensor';
  }

  cancel() {
    this.events.publish('sensor-item-operation', {refresh: false});
  }

  edit(equipmentId) {
    this.events.publish('sensor-item-operation-edit', {id: equipmentId});
  }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
