import {inject} from 'aurelia-framework';
import { SensorService } from 'sensors/sensorService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {PermissionService} from 'security/permissionService';
import {AnraNotification} from '../components/anra-notification';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(SensorService, Router, EventAggregator, PermissionService,AnraNotification)
export class Listing {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['Name','SerialNumber','OrganizationName']},
    ];

  constructor(sensorService, router, events,permissionService,notification) {
        this.sensorService = sensorService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        this.heading = 'Manage Sensors';
        this.sensors = [];
        this.sensorAggregate = null;
        this.massUnit = localStorage["MassUnit"];
        this.isEditing = false;
        this.isViewing = false;
        this.sensorstocsv=[];
        this.permission={};
        this.notification = notification;
        // this.dialogService = dialogService;
        //Set Default Paging Size
        this.pageSize = sensorService.defaultPagingSize;
        this.isWaiting = false;
    }

  activate() {
    this.getList();
    
  }

  attached() {
        this.subscribeEvents();
      
    }
    detached() {
      //this.subscriber.dispose();
    }

    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
            this.permission = result;
          });
    }

  subscribeEvents() {
    this.subscriber = this.events.subscribe('sensor-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber = this.events.subscribe('sensor-item-operation-edit', payload => {

        if (payload) {
            if (payload.id > 0) {
                this.editItem(payload.id);
            }
            this.reset();
        }
    });
  }

  getList() {
      this.isWaiting = true;
        this.sensorService.getSensors()
          .then(result => {
            this.sensors = result;
            this.isEditing = false;
            this.getPermission(7);
            this.isWaiting = false;
            var newJson=[];
        this.sensors.forEach(function(item) {
        newJson.push({
        "Name":item.Name,
        "Purchase Date":moment(item.PurchaseDate).format('YYYY-MM-DD'),
        "Serial No":item.SerialNumber,
        "Weight":item.Weight,
        "Organization":item.OrganizationName,
        "Created Date":moment(item.DateCreated).format('YYYY-MM-DD')
                    });
        });
        this.sensorstocsv=newJson;
        }).catch((e) => {
            this.notification.error(e.message);
            this.isWaiting = false;;
        });
    }

    //Convert For CSV Convert
   
  reset() {
    this.sensorAggregate = null;
    this.isEditing = false;
    this.isViewing = false;
  }

  add() {
       this.isWaiting = true;
        this.sensorService.getNewSensor()
          .then(result => {
            this.sensorAggregate = result;
            this.isEditing = true;
            this.isWaiting = false;
          });
    }

  editItem(id) {
        this.isWaiting = true;
        this.sensorService.getSensor(id)
          .then(result => {
            this.sensorAggregate = result;
            this.sensorAggregate.Sensor.PurchaseDate = moment(result.Sensor.PurchaseDate).format('YYYY-MM-DD');
	         this.isEditing = true;
	         this.isWaiting = false;
          });
    }

  viewItem(id) {
    this.isWaiting = true;
      this.sensorService.getSensor(id)
        .then(result => {            
            this.sensorAggregate = result;
            this.sensorAggregate.Sensor.PurchaseDate = moment(result.Sensor.PurchaseDate).format('YYYY-MM-DD');
            this.isViewing = true;
            this.isWaiting = false;
        });
  }

  deleteItem(id) {

    this.isWaiting=true;
      this.notification.Confirm(
        "Are you sure you want to delete?"
      ).then(result => {
        if (result) {
          this.sensorService.deleteSensor(id)
             .then(result => {
				  this.notification.ShowInfo('Deleted Successfully.');
        this.getList();
        this.isWaiting= false;
			  });
        }
        else{
          this.isWaiting=false;
        }
      });
  }

  saveItem(item) {
    return this.sensorService.saveSensor(this.sensorAggregate.Sensor);
  }

  //Download CSV

  convertArrayOfObjectsToCSV(args) {  
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        this.notification.error("No data available to export.");          
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    var columnHeaders = ['Name','Purchase Date','Serial No','Weight','Organization','Date Created'];
    
    result = '';
    result += columnHeaders.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
  }

    downloadCSV(args) {  
    var data, filename, link;

    //Setting Entity Attributes

    var csv = this.convertArrayOfObjectsToCSV({data:this.sensorstocsv});
    if (csv == null) return;

    filename = args.filename || 'SensorsDetails.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();

    }
}
