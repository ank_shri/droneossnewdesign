import {inject} from 'aurelia-framework';
import {UserService} from 'users/userService';
import {DroneService} from 'drones/droneService';
import {Router} from 'aurelia-router';

@inject(Router, UserService, DroneService)
export class UserDroneAssociation {

  constructor(router, userService, droneService) {
    this.router = router;
    this.userService = userService;
    this.droneService = droneService;
    this.isReady = false;
  }

  activate(params) {

    let tasks = [];
    tasks.push(this.userService.getAssociatedDrones(params.id));
    tasks.push(this.droneService.getDrones());
    tasks.push(this.userService.getUser(params.id));

    Promise.all(tasks)
      .then(result => {
        this.associatedDrones = result[0];
        this.drones = result[1];

        if(this.associatedDrones.length > 0){
          this.drones.forEach(d => {
            d.isSelected = (this.associatedDrones.findIndex(x => x.DroneId == d.DroneId) != -1);
          })
        }

        this.user = result[2];        
        this.isReady = true;
      })
      .catch(err => {
        console.error(err);
      })

  }

  save() {
    let selectedDrones = this.drones.filter(x => x.isSelected);
    if(selectedDrones.length > 0){
      let associations = [];
      selectedDrones.forEach(item => {
        associations.push({"DroneId": item.DroneId, "UserId": this.user.Id});
      });

      this.userService.associateDrones(associations);
      this.router.navigate('users/users');
    }
  }

  cancel() {
    this.router.navigate('users/users');
  }
}
