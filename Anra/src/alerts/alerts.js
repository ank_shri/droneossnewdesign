import {inject} from 'aurelia-framework';
import {AlertService} from 'alerts/alertService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {PermissionService} from 'security/permissionService';
import {AnraNotification} from "../components/anra-notification";


@inject(AlertService, Router, EventAggregator, PermissionService, AnraNotification)
export class Alerts {   
    
    //Set Filter For Searchbox
    filters = [
              {value: '', keys: ['Name']},
    ];

    constructor(alertService, router, events, permissionService, notification) {
        this.alertService = alertService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        this.notification = notification;
        this.heading = 'Manage Alerts';
        this.maintenances = [];
        this.permission={};
        //Set Default Paging Size
        this.pageSize = alertService.defaultPagingSize;
        this.isWaiting = false;
    }

    activate() {
        this.getAlerts();
        this.Drone_partially_completed = '/image/Maintanance_img/drone_partially_completed.svg';
        this.Drone_new_task = '/image/Maintanance_img/drone.svg';
        this.Drone_postpone = '/image/Maintanance_img/drone_postpone.svg';

        this.Equipments_partially_completed = '/image/Maintanance_img/equipments_partially_completed.svg';
        this.Equipments_new_task = '/image/Maintanance_img/equipments_newtask.svg';
        this.Equipments_postpone = '/image/Maintanance_img/equipments_postpone.svg';

        this.Sensor_partially_completed = '/image/Maintanance_img/sensor_partially_completed.svg';
        this.Sensor_new_task = '/image/Maintanance_img/sensor_newtask.svg';
        this.Sensor_postpone = '/image/Maintanance_img/sensor_postpone.svg';

    }  


    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
          });
    }

    getAlerts() {
        this.isWaiting = true;
        this.alertService.getAlerts()
          .then(result => {
              this.maintenances = result.Maintenance;
              for(var i=0; i< this.maintenances.length; i++){
                  let obj = this.maintenances[i];
                  if(obj.NextActionAfterDate == null){
                    let MaintananceDate = obj.MaintenanceDate;
                    let nextDate = moment(MaintananceDate).add(obj.NextActionAfterHours, 'hours').format('MM/DD/YYYY');
                    obj.NextActionAfterHours = nextDate;
                  }
                console.log(obj);
              }
              this.isWaiting = false;
            //   console.log(result);
          }).catch((e) => {
              this.notification.ShowError(e.message);
              this.isWaiting = false;
          });
    }

    

    viewItem(id){        
        this.router.navigate('alerts/maintenance/' + id);
    }
}
