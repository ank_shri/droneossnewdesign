import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class MissionService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getMissions() {
      return this.http.fetch(this.baseApi + 'missions/listing' )
      .then(response => response.json());
  }

  getNewMission() {
      return this.http.fetch(this.baseApi + 'missions/NewMission')
      .then(response => response.json());
  }

  getMissionListingRefactor() {
    return this.http.fetch(this.baseApi + 'missions/MissionListingRefactor')
      .then(response => response.json());
  }

  getNewMissionRefactor() {
  return this.http.fetch(this.baseApi + 'missions/NewMissionRefactor')
      .then(response => response.json());
  }

  getMissionRefactor(gufi) {
    return this.http.fetch(this.baseApi + 'missions/MissionRefactor/' + gufi)
      .then(response => response.json());
  }

  getMission(gufi) {
    return this.http.fetch(this.baseApi + 'missions/' + gufi )
      .then(response => response.json());
  }

  getMissionByProjId(id) {
      return this.http.fetch(this.baseApi + 'missions/missionbyprojid/' + id )
        .then(response => response.json());
  }

  getMissionWaypoints(mission) {
      return this.http.fetch(this.baseApi + 'missions/missionwaypoints', {
          method: 'post',
          body: json(mission)
      }).then(response => response.json());
  }

  getFacadeWaypoints(mission) {
      return this.http.fetch(this.baseApi + 'missions/facadewaypoints', {
          method: 'post',
          body: json(mission)
      }).then(response => response.json());
  }

  deleteMission(gufi) {
    return this.http.fetch(this.baseApi + 'missions/' + gufi, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  approveMission(gufi) {
    return this.http.fetch(this.baseApi + 'missions/Approve/' + gufi, {
      method: 'post'
    }).then(response => response.json());
  }

  deleteMissionGeometry(id) {
      return this.http.fetch(this.baseApi + 'missions/deletemissiongeometry/' + id, {
          method: 'delete'
      })
          .then(response => {
              response.json();
          }
          );
  }

  saveMission(mission) {
    return this.http.fetch(this.baseApi + 'missions/', {
      method: 'post',
      body: json(mission)
    }).then(response => response.json());
  }

  cloneMission(gufi) {
      return this.http.fetch(this.baseApi + 'missions/Clone/' + gufi, {
          method: 'post'         
      }).then(response => response.json());
  }

  saveMissionGeometry(missionGeometry) {
      return this.http.fetch(this.baseApi + 'missions/savemissiongeometry', {
          method: 'post',
          body: json(missionGeometry)
      }).then(response => response.json());
  }

  closeMission(mission) {
    return this.http.fetch(this.baseApi + 'missions/close', {
        method: 'post',
        body:json(mission)
      })
      .then(response => {
          response.json();
        }
      );
  }

  getDrones() {
    return this.http.fetch(this.baseApi + 'Drone/listing')
    .then(response => response.json());
  }

  getFacadeMissionWaypoints(facade) {
    return this.http.fetch(this.baseApi + 'missions/getFacadeMission', {
      method: 'post',
      body: json(facade)
    }).then(response => response.json());
  }

}
