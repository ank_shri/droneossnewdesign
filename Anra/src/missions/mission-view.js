import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from "../components/anra-notification";
import { Router } from 'aurelia-router';
import { LookupHelper } from 'common/lookup-helper';

@inject(EventAggregator, Router, AnraNotification)
export class MissionView {
  @bindable missionAggregate;
  @bindable save;
  @bindable organizations;

  constructor(events, router, notification) {
    this.router = router;
    this.events = events;
    this.notification = notification;
    this.lengthUnit = localStorage["LengthUnit"]; 
    this.flightTypeLookup = undefined;
  }

  bind() {
    this.flightTypeLookup = LookupHelper.FlightTypeLookup();
    this.mission = this.missionAggregate.Mission;
    this.CrewMembers = this.missionAggregate.CrewMembers;

    this.missionUsers = [];
    this.missionAggregate.CrewMembers.map(x => {
      if (this.mission.SelectedUsers.findIndex(u => u == x.Id) > -1) {
        this.missionUsers.push(x);
      }
    });

    this.missionBattery = [];
    this.missionAggregate.Batteries.map(x => {
      if (this.mission.BatteryIdsList.findIndex(u => u == x.Id) > -1) {
        this.missionBattery.push(x);
      }
    });

    this.missionEquipment = [];
    this.missionAggregate.Equipments.map(x => {
      if (this.mission.EquipmentIdsList.findIndex(u => u == x.Id) > -1) {
        this.missionEquipment.push(x);
      }
    });

    this.missionSensor = [];
    this.missionAggregate.Sensors.map(x => {
      if (this.mission.SensorIdsList.findIndex(u => u == x.Id) > -1) {
        this.missionSensor.push(x);
      }
    });

    this.heading = this.mission.Name != null ? 'View Mission' : 'Add Mission';
  }
  
  cancel() {
    this.events.publish('mission-item-operation', {refresh: false});
  }

  getContentType(data)
  {
      if(data.charAt(0)=='/'){
          return "image/jpeg";
      }
      else if(data.charAt(0)=='R'){
          return "image/gif";
      }
      else if(data.charAt(0)=='i'){
          return "image/png";
      }
      else if(data.charAt(0)=='J'){
          return "application/pdf";
      }
  }

  base64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
  }
    
  viewDocument(item)
  {
      var data = item.DocumentFile;

      var contentType = this.getContentType(data);
      var blob = this.base64toBlob(data, contentType);

      //Code to download File Locally
      var fileName = item.FileName;
      var url = window.URL.createObjectURL(blob);
      var link = document.createElement('a');
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.click();
      window.URL.revokeObjectURL(url);
  }

  edit(gufi) {
      this.events.publish('mission-item-operation-edit', {id: gufi});
  }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
