import { EventAggregator } from 'aurelia-event-aggregator';
import { BindingEngine } from 'aurelia-framework';
import { inject, NewInstance } from 'aurelia-dependency-injection';
import { ValidationController, ValidationRules, validateTrigger } from 'aurelia-validation';
import {AnraNotification} from "../components/anra-notification";
import { Router } from 'aurelia-router';
import { MissionService } from 'missions/missionService';
import moment from 'moment';
import { LookupHelper } from 'common/lookup-helper';
import {UIApplication} from "aurelia-ui-framework";

const CAMERA_WIDTH_IN_PIXELS = 4000.0;
const LENS_FOCAL_LENGTH_IN_MM = 3.57;
const WIDTH_IN_MM = 6.17;

@inject(EventAggregator, Router, AnraNotification, MissionService, NewInstance.of(ValidationController), BindingEngine,UIApplication)
export class Mission {
  controller = null;
  rules = null;

  constructor(events, router, notification, missionService, controller, bindingEngine,application) {
    this.router = router;
    this.events = events;
    this.notification = notification;
    this.bindingEngine = bindingEngine;
    this.missionService = missionService;
    this.controller = controller;
    // this.controller.addRenderer(new BootstrapFormValidationRenderer());
    // this.controller.validateTrigger = validateTrigger.change;
    this.mission = null;
    this.lengthUnit = localStorage["LengthUnit"];
    this.isWaiting = false;
    this.batteries = [];
    this.pilots = [];
    this.crewMembers = [];
    this.locations = [];
    this.equipments = [];
    this.projects = [];
    this.showAllBatteries = false;
    this.showAllSensors = false;
    this.showAllOtherEquipments = false;
    this.drones = [];
    this.sensors = [];
    this.other_equipment = [];
    this.ShowAddmissionBox =  true;
    this.addEquipmentBox = false;
    this.application = application;
    this.directions = [
      { value: true, text: 'Clockwise' },
      { value: false, text: 'Anti-clockwise' }
    ];
    this.flightTypeLookup = undefined;
    //Facade Mission Constants
    this.groundSampleDistance = 0;
    this.isPilot = false;
  }

  activate(params) {
    this.flightTypeLookup = LookupHelper.FlightTypeLookup();
    if (params.id) {
      this.missionService.getMissionRefactor(params.id)
        .then(result => {
          this.missionAggregate = result;
          this.mission = result.Mission;
          this.missionAggregate.Mission.FlightDate = moment(result.Mission.FlightDate).format('YYYY-MM-DD');
          this.step = 'step2';
          this.setupValidationRules();
          this.heading = 'Edit Mission';
          this.Init(true);
          this.addEquipmentBox = true;
          if (this.mission.MissionType == 10) {
            this.groundSampleDistance = (this.mission.DroneTowerDistance * WIDTH_IN_MM * 1000 / (3.28084 * LENS_FOCAL_LENGTH_IN_MM * CAMERA_WIDTH_IN_PIXELS)).toFixed(1);
          } 
        });
    }
    else {
      this.step = 'step1';
      this.missionService.getNewMissionRefactor()
        .then(result => {
          console.log("Result",result);
          this.missionAggregate = result;
          this.mission = result.Mission;
          this.isEditing = false;
          this.setupValidationRules();
          this.heading = 'Add Mission';
          this.isWaiting = false;
          this.mission.Speed = 10;
          this.mission.Altitude = 50;
          this.mission.SideLapAngle = 70;
          this.mission.HatchAngle = 0;
          this.mission.AutoCapture = true;
          this.mission.AutoCaptureTime = 1;
          this.mission.AutoCaptureDistance = 1;
          this.setupValidationRules();
          this.heading = 'Add Mission';
          this.Init(false);
          this.isWaiting = false;
          if (localStorage["Roles"].indexOf("Pilot") > -1) {
            this.isPilot = true;
            this.mission.PilotId = localStorage["UserId"];
          }
        });
    }
  }

  Reset() {
    this.batteries = [];
    this.pilots = [];
    this.crewMembers = [];
    this.locations = [];
    this.equipments = [];
    this.projects = [];
    this.drones = [];
    this.sensors = [];
    this.other_equipment = [];
  }

  Init(isEdit) {
    if (!isEdit && !this.mission.IsAdmin) {
      this.pilots = this.missionAggregate.Pilots;
      this.crewMembers = this.missionAggregate.CrewMembers;
      this.projects = this.missionAggregate.Projects;
      this.drones = this.missionAggregate.Drones;
      this.locations = this.missionAggregate.Locations;
    }
    else {
      this.loadDroneEquipments();
      this.loadAll();
    }
  }

  attached() {
    let watch = this.bindingEngine.propertyObserver(this, 'groundSampleDistance').subscribe((newval, oldVal) => {
      if (this.mission) {
        this.mission.DroneTowerDistance = this.getDistanceBtwWallDroneInFeet(this.groundSampleDistance);
      }
    });
  }

  detached() {
    //Finally Reset Validation
    this.controller.reset();
  }

  selectMissionType(id) {
    this.mission.MissionType = id;
    this.step = 'step2';
  }

  setupValidationRules() {

    //Custom Rule For Date Validation  
    ValidationRules.customRule(
      'date',
      (value, obj) => value === null || value === undefined || value instanceof Date || moment(value, "YYYY-MM-DD", true).isValid(),
      '\${$displayName} must be a Date.'
    );
    ValidationRules.customRule(
      'speedRange',
      (value, obj, min, max) => {
        var num = Number.parseInt(value);
        return num === null || num === undefined || (Number.isInteger(num) && num >= min && num <= max);
      },
      "${$displayName} must be an integer between 1 and 30.",
      (min, max) => ({ min, max })
    );
    ValidationRules.customRule(
      'altitudeRange',
      (value, obj, min, max) => {
        var num = Number.parseInt(value);
        return num === null || num === undefined || (Number.isInteger(num) && num >= min && num <= max);
      },
      "${$displayName} must be an integer between 5 and 1500.",
      (min, max) => ({ min, max })
    );
    this.rules = ValidationRules
      .ensure('Name').required()
      .ensure('PilotId').required()
      .ensure('DroneId').matches(/^[1-9][0-9]*$/).required()
      .ensure('ProjectId').matches(/^[1-9][0-9]*$/).required()
      .ensure('LocationId').matches(/^[1-9][0-9]*$/).required()
      .ensure('Duration').required()
      .ensure('FlightTypeId').matches(/^[1-9][0-9]*$/).required()
      .ensure('FlightDate').required().satisfiesRule('date')
      .ensure('OrganizationId').matches(/^[1-9][0-9]*$/).required()
      .ensure('Speed').required().satisfiesRule('speedRange', 1, 30)
      .ensure('Altitude').required().satisfiesRule('altitudeRange', 5, 1500)
      .ensure('TowerHeight').required()
      .ensure('HatchAngle').required()
      .ensure('ClearanceHeight').required()
      .ensure('DroneTowerDistance').required()
      .ensure('InspectionDivisions').required()
      .ensure('AutoCaptureTime').required().rules;
  }

  removeRule(propName) {
    let list = this.rules[0];
    for (var i = list.length; i--;) {
      if (list[i].property.name === propName) list.splice(i, 1);
    }
  }



  reviewValidation() {

    if (!this.mission.IsAdmin) {
      this.removeRule('OrganizationId');
    }

    if (this.mission.MissionType == 1 || this.mission.MissionType == 2) {
      this.removeRule('ClearanceHeight');
      this.removeRule('DroneTowerDistance');
      this.removeRule('Altitude');
      this.removeRule('TowerHeight');
      this.removeRule('InspectionDivisions');
    }

    if (this.mission.MissionType == 3 || this.mission.MissionType == 4) {
      this.removeRule('Altitude');
      this.removeRule('HatchLineSeparations');
    }

    if (this.mission.MissionType == 1) {
      this.removeRule('AutoCaptureTime');
      this.removeRule('AutoCaptureDistance');
      this.removeRule('Altitude');
      this.removeRule('TowerHeight');
    }

    if (this.mission.MissionType == 6) {
      this.removeRule('Altitude');
      this.removeRule('TowerHeight');
      this.removeRule('ClearanceHeight');
      this.removeRule('DroneTowerDistance');
      this.removeRule('Altitude');
      this.removeRule('HatchLineSeparations');
      this.removeRule('DroneTowerDistance');
    }

    if (this.mission.MissionType != 2 || this.mission.MissionType != 5) {
      this.removeRule('HatchAngle');
    }

  }

  saveandwaypoints() {
    this.saveItem();
  }

  saveItem() {
    if (this.mission.SelectedUsers == undefined) {
      this.notification.ShowError('crew members are required.');
      return;
    }
    this.missionAggregate.Mission.EquipmentId = 1;
    this.missionAggregate.Mission.BatteryId = 1;
    this.isWaiting = true;
    if (this.mission.MissionType == 4) {
      this.mission.Altitude = this.mission.TowerHeight;
    }

    if (this.mission.MissionType == 3 || this.mission.MissionType == 4 || this.mission.MissionType == 8) {
      if (parseFloat(this.mission.TowerHeight) <= parseFloat(this.mission.ClearanceHeight)) {
        this.notification.ShowError('Tower height must be greater than clearance height.');
        this.isWaiting = false;
        return;
      }

      if (this.mission.MissionType != 10 && this.mission.MissionType != 4 && parseInt(this.mission.InspectionDivisions) > 8) {
        this.notification.ShowError('No of inspection division cannot be more than 8 ');
        this.isWaiting = false;
        return;
      }
      if (this.mission.MissionType == 4 && parseInt(this.mission.InspectionDivisions) > 12) {
        this.notification.ShowError('No of inspection division cannot be more than 12 ');
        this.isWaiting = false;
        return;
      }
    }

    //Review Validation Rules Before Validation
    this.reviewValidation();
    this.isWaiting=true;
    this.controller.validate({ object: this.mission, rules: this.rules })
      .then(valresult => {
        if (valresult.valid) {
          // validation succeeded
          this.missionAggregate.Mission.UserId = this.loginId;
          console.log(this.missionAggregate.Mission);
          console.log(this.missionAggregate.Mission,this.missionAggregate.Mission.UserId);
          this.missionService.saveMission(this.missionAggregate.Mission).then(result => {
            this.handleSaveResult(result);
            this.isWaiting = false;
          }).catch((e) => {
            this.notification.ShowError('Error.');
            this.isWaiting = false;
          });
        }
        else {
          // validation failed
          let messages = this.controller.errors.map(x => x.message);
            this.application.toast(
              Object.assign({ container: this.errorHolder }, {
                title: 'Errors!',
                message: messages.join(', '),
                theme: 'danger',
                timeout: 2000,
                glyph: 'glyph-alert-info'
              })
            );
      
            this.isWaiting = false;
        }

      }).catch((e) => {
        this.notification.ShowError('Error.');
        this.isWaiting = false;
      });
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {  
      this.notification.ShowError(result.SuccessMessage);
      this.router.navigate('waypoints/mission/' + result.ResultData.Gufi);
    }
    else {
      this.notification.ShowError(result.ErrorMessage);
    }
  }

  cancel() {
    this.router.navigate('#/missions');
  }

  getAllBatteries() {
    this.batteries = [];

    if (this.showAllBatteries) {
      this.batteries = this.missionAggregate.Batteries;
    }
    else {
      this.batteries = this.missionAggregate.Batteries.filter(x => x.ReferenceId == this.mission.DroneId);
    }
  }

  getAllSensors() {
    this.sensors = [];
    if (this.showAllSensors) {
      this.sensors = this.missionAggregate.Sensors;
    }
    else {
      this.sensors = this.missionAggregate.Sensors.filter(x => x.ReferenceId == this.mission.DroneId);
    }
  }

  getAllOtherEquipments() {
    this.other_equipment = [];

    if (this.showAllOtherEquipments) {
      this.other_equipment = this.missionAggregate.Equipments;
    }
    else {
      this.other_equipment = this.missionAggregate.Equipments.filter(x => x.ReferenceId == this.mission.DroneId);
    }
  }

  loadDroneEquipments() {

    this.batteries = this.missionAggregate.Batteries.filter(x => x.ReferenceId == this.mission.DroneId);
    this.sensors = this.missionAggregate.Sensors.filter(x => x.ReferenceId == this.mission.DroneId);
    this.other_equipment = this.missionAggregate.Equipments.filter(x => x.ReferenceId == this.mission.DroneId);

  }
  loadAll() {
    //this.Reset();
    this.pilots = this.missionAggregate.Pilots.filter(x => x.OrganizationId == this.mission.OrganizationId);
    this.crewMembers = this.missionAggregate.CrewMembers.filter(x => x.OrganizationId == this.mission.OrganizationId);
    this.projects = this.missionAggregate.Projects.filter(x => x.OrganizationId == this.mission.OrganizationId);
    this.drones = this.missionAggregate.Drones.filter(x => x.OrganizationId == this.mission.OrganizationId);
    this.locations = this.missionAggregate.Locations.filter(x => x.OrganizationId == this.mission.OrganizationId);
  }

  setselectedOption(event) {
    this.mission.SelectedUsers = [];
    for (let i = 0; i < event.target.options.length; i++) {
      let option = event.target.options[i];
      //Only Push selected option to SelectedRole
      if (option.selected == true) {
        this.mission.SelectedUsers.push(option.value);
      }
    }
  }

  isSelected(userid) {
    return this.mission.SelectedUsers == null ? false : this.mission.SelectedUsers.findIndex(x => x == userid) > -1;
  }
  setselectedBatteryOption(event) {
    this.mission.BatteryIdsList = [];
    for (let i = 0; i < event.target.options.length; i++) {
      let option = event.target.options[i];
      //Only Push selected option to SelectedRole
      if (option.selected == true) {
        this.mission.BatteryIdsList.push(option.value);
      }
    }
  }

  isBatterySelected(batteryid) {
    return this.mission.BatteryIdsList == null ? false : this.mission.BatteryIdsList.findIndex(x => x == batteryid) > -1;
  }
  // selected sensor
  setselectedSensorOption(event) {
    this.mission.SensorIdsList = [];
    for (let i = 0; i < event.target.options.length; i++) {
      let option = event.target.options[i];
      //Only Push selected option to SelectedRole
      if (option.selected == true) {
        this.mission.SensorIdsList.push(option.value);
      }
    }
  }

  isSensorSelected(sensorid) {
    return this.mission.SensorIdsList == null ? false : this.mission.SensorIdsList.findIndex(x => x == sensorid) > -1;
  }

  // selcete other equipment
  setselectedEquipmentOption(event) {
    this.mission.EquipmentIdsList = [];
    for (let i = 0; i < event.target.options.length; i++) {
      let option = event.target.options[i];
      //Only Push selected option to SelectedRole
      if (option.selected == true) {
        this.mission.EquipmentIdsList.push(option.value);
      }
    }
  }

  isEquipmentSelected(equipmentid) {
    return this.mission.EquipmentIdsList === null ? false : this.mission.EquipmentIdsList.findIndex(x => x == equipmentid) > -1;
  }

  getDistanceBtwWallDroneInFeet(gsd) {    
    let distanceInMeter = ((gsd / 1000) * LENS_FOCAL_LENGTH_IN_MM * CAMERA_WIDTH_IN_PIXELS) / WIDTH_IN_MM;
    return Math.floor(distanceInMeter * 3.28084);
  }
}
