import {inject} from 'aurelia-framework';
import {MissionService} from 'missions/missionService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {PermissionService} from 'security/permissionService';
import {AnraNotification} from "../components/anra-notification";
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';


@inject(MissionService, Router, EventAggregator, PermissionService,AnraNotification)
export class Missions {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['Name','StatusText','TotalEffortHours']},
    ];

  constructor(missionService, router, events, permissionService,notification) {
    this.missionService = missionService;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;
    this.notification = notification;
    this.heading = 'Manage Missions';
    this.missions = [];
    this.missiontocsv=[];
    this.missionAggregate = null;
    this.isEditing = false;
    this.isViewing = false;
    this.missionRoute = router.routes[2].navModel;
    this.permission = {};
    //Set Default Paging Size
    this.pageSize = missionService.defaultPagingSize;
    this.isWaiting = false;
    this.ActionItems = false;
  }

  activate() {
    this.userId = localStorage["UserId"];
    this.userRole = localStorage["Roles"]
    this.getList();
    this.missionRoute.isActive = true;
  }

  deactivate() {
    this.missionRoute.isActive = false;
  }

  attached() {
    this.subscribeEvents();
  }
  detached() {
    this.subscriber.dispose();
  }

  getPermission(moduleId) {
    this.permissionService.getPermission(moduleId)
      .then(result => {
        this.permission = result;
      });
  }
 
  subscribeEvents() {
    this.subscriber=this.events.subscribe('mission-item-operation', payload => {
      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber= this.events.subscribe('mission-item-operation-edit', payload => {

      if (payload) {
        this.editItem(payload.id);
        this.reset();
      }
    });
  }


  getList() {
      this.isWaiting = true;
    this.missionService.getMissionListingRefactor()
      .then(result => {
        //this.missions = result;
         if(this.userRole == "Pilot") {
          result.map(res => {
            if (res.UserId == this.userId || res.PilotId == this.userId) {
              this.missions.push(res);
              this.ConvertNewJson();
            }
          })
        }
        else {
          this.missions=result;
          //For CSV Convert
         this.ConvertNewJson();
          
          
        }
        this.isEditing = false;
        this.getPermission(5);
        this.isWaiting = false;
     
      })
      .catch((e) => {
        this.notification.ShowError(e.message);
        this.isWaiting = false;
      })
  }

  //for creating new json
  ConvertNewJson()
  {
    var newJson=[];
    this.missions.forEach(function(item) {
    newJson.push({
    "Name": item.Name,
    "Organization Name":item.OrganizationName,
    "Type":item.MissionTypeName,
    "Staus":item.StatusText,
    "Onsite/Travel Time":item.TotalEffortHours +'/'+ item.TotalTravelHours,
    "Date Requested": moment(item.DateCreated).format('YYYY-MM-DD'),
    "Flight Date":moment(item.FlightDate).format('YYYY-MM-DD')
    });
    });
    this.missiontocsv=newJson;
  }

  reset() {
    this.missionAggregate = null;
    this.isEditing = false;
    this.isViewing = false;
  }

  editItem(gufi) {
    this.router.navigate('mission-details/' + gufi);
  }


  viewItem(gufi) {
    this.isWaiting = true;
    this.missionService.getMissionRefactor(gufi)
      .then(result => {
        this.missionAggregate = result;
        console.log(result);
        this.missionAggregate.Mission.FlightDate = moment(result.Mission.FlightDate).format('YYYY-MM-DD');
        this.isViewing = true;
        this.isWaiting = false;
      }).catch((e) => {
        this.notification.ShowError(e.message);
        this.isWaiting = false;
      })
  }

  deleteItem(gufi) {
    this.notification.Confirm(
      "Are you sure you want to clone it?"
    ).then(result => {
      if (result) {
        this.isWaiting=true;
        this.missionService.deleteMission(gufi)
        .then(result => {
        
            this.notification.ShowError('Deleted Successfully.'); 
          this.getList();
          this.isWaiting=false;
        });
      }
      else{
        this.isWaiting=false;
      }
    });
	}

  

  flyMission(row) {
    console.log("row item",row);
    let gufi = row.Gufi;
    this.router.navigate('drone-view/' + gufi);

  }

  saveItem() {
    return this.missionService.saveMission(this.missionAggregate.Mission);
  }

  cloneMission(gufi) {

    this.isWaiting=true;
    this.notification.Confirm(
      "Are you sure you want to clone it?"
    ).then(result => {
      if (result) {
      this.missionService.cloneMission(gufi).then(result => {
          this.notification.ShowError('Mission cloned successfully.');
          this.isWaiting = false;
          this.getList(); });
      }
      else{
        this.isWaiting=false;
      }
    });
  }

  uploadDoc(gufi) {
    this.router.navigate('documents/mission/' + gufi);
  }

  approveMission(gufi) {
    this.dialogService.open({ viewModel: Prompt, model: 'Are you sure you want to approve it?' }).then(response => {
      if (!response.wasCancelled) {
        this.isWaiting = true;
        this.missionService.approveMission(gufi).then(result => {
          this.notification.ShowError('Mission approved successfully.');
          this.isWaiting = false;
          this.getList();
        });
      }
    });
  }

  flightPlan(row) {
     
      if (row.StatusText=='Accepted' || row.StatusText=='Proposed') {
          let gufi = row.Gufi;
          this.router.navigate('waypoints/mission/' + gufi);
      }
      else
      { 
          this.notification.ShowError('Waypoint can be created for new mission only. (Mission current state is - '+ row.StatusText);
       }
  }  

  trackTime(gufi) {    
    this.router.navigate('efforts/' + gufi);
  }

  add() {
    this.isWaiting = true;
    this.router.navigate('mission-details');
  }

  showChecklist(gufi){
      this.router.navigate('checklist/' + gufi);      
  }

  //Download CSV
  convertArrayOfObjectsToCSV(args) {  
        var result, ctr, keys, columnDelimiter, lineDelimiter, data;

        data = args.data || null;
        if (data == null || !data.length) {
            this.notification.ShowError("No data available to export.");          
            return null;
        }

        columnDelimiter = args.columnDelimiter || ',';
        lineDelimiter = args.lineDelimiter || '\n';

        keys = Object.keys(data[0]);

        var columnHeaders = ['Name','Organization','Type','Status','Travel/Onsite Time','Date Requested','Flight Date'];
        
        result = '';
        result += columnHeaders.join(columnDelimiter);
        result += lineDelimiter;

        data.forEach(function(item) {
            ctr = 0;
            keys.forEach(function(key) {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];
                ctr++;
            });
            result += lineDelimiter;
        });

        return result;
    }

downloadCSV(args) {  
    var data, filename, link;
    
    //Setting Entity Attributes
    
    var csv = this.convertArrayOfObjectsToCSV({data:this.missiontocsv});
    if (csv == null) return;

    filename = args.filename || 'MissionSummaryReport.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
  

   
  }

  showHideAction(index){
    this.selectedIndex = index;
    if(this.selectedIndex == index){
      this.ActionItems = !this.ActionItems
    }
  }
}
