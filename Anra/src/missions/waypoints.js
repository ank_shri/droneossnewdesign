import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Router} from 'aurelia-router';
import {MissionService} from 'missions/missionService';
import {DroneService} from 'drones/droneService';
import * as L from 'leaflet';
import * as D from 'leaflet-draw';

@inject(EventAggregator, Router, MissionService, DroneService)
export class Waypoints {
  constructor(events, router, missionService, droneService) {
    this.events = events;
    this.router = router;
    this.missionService = missionService;
    this.droneService = droneService;
    this.isReady = false;
    this.isWaiting = false;
    this.wayPoints = [];
    this.defaultAltitude = 100;
    this.radius = 45;
  }

  activate(params) {
    if (params) {
      this.missionId = params.id;
      this.getDetails();
      this.setupCommandsList();
    }
  }

  getDetails() {
    this.isWaiting = true;
    this.missionService.getMission(this.missionId)
      .then(result => {
        this.mission = result.Mission;
        return this.droneService.getDrone(this.mission.DroneId);
      }).then(result => {
      this.drone = result;
      this.lng = this.drone.BaseLng;
      this.lat = this.drone.BaseLat;
      this.isReady = true;
      this.initMap();
    });
  }

  initMap() {
    let map = L.map('mapid', {zoomControl: false}).setView([this.lat, this.lng], 16);
    L.Icon.Default.imagePath = 'styles/images';
    L.control.zoom({
      position: 'bottomleft'
    }).addTo(map);

    this.map = map;

    map.addLayer(L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {minZoom: 4}));

    this.attachDrawControls(map);

    this.polyline = new L.Polyline([]).addTo(map);
  }

  attachDrawControls(map) {

    var drawnItems = new L.FeatureGroup();

    map.addLayer(drawnItems);

    var drawControl = new L.Control.Draw({
      position: 'bottomleft',
      draw: {
        polygon: true,
        polyline: false,
        rectangle: false,
        circle: false,
        marker: true,
      }
    });

    /*map.on('draw:created', function (event) {
     var layer = event.layer;
     drawnItems.addLayer(layer);
     var json = drawnItems.toGeoJSON();
     console.log(json.features);
     this.addWaypoint(json.features);
     }, this);
     */
    //map.addControl(drawControl);

    map.on('click', (e) => this.onMapClick(e));

  }


  onMapClick(event) {
    let marker = new L.Marker(event.latlng).addTo(this.map);
    this.polyline.addLatLng(event.latlng);    
    this.createWP(event.latlng.lat, event.latlng.lng);
  }

  createWP(lat, lng) {
    let wp = {
      index: 1,
      command: 16,
      lat: lat,
      lng: lng,
      alt: this.alt,
      displayOrder: 1,
      grad: 0,
      angle: 0,
      dist: 0,
      az: 0
    };

    this.wayPoints.push(wp);
  }

  setupCommandsList() {

    this.commands = [
      {name: 'WAYPOINT', id: 16},
      {name: 'SPLINE_WAYPOINT', id: 82},
      {name: 'LOITER_TURNS', id: 18},
      {name: 'LOITER_TIME', id: 19},
      {name: 'LOITER_UNLIM', id: 17},
      {name: 'RETURN_TO_LAUNCH', id: 20},
      {name: 'LAND', id: 190},
      {name: 'TAKEOFF', id: 84},
      {name: 'GUIDED_ENABLE', id: 92},
      {name: 'DO_GUIDED_LIMITS', id: 222},
      {name: 'DO_SET_ROI', id: 201},
      {name: 'CONDITION_DELAY', id: 112},
      {name: 'CONDITION_CHANGE_ALT', id: 113},
      {name: 'CONDITION_DISTANCE', id: 114},
      {name: 'CONDITION_YAW', id: 115},
      {name: 'DO_JUMP', id: 177},
      {name: 'DO_CHANGE_SPEED', id: 178},
      {name: 'DO_PARACHUTE', id: 208},
      {name: 'DO_SET_CAM_TRIGG_DIST', id: 206},
      {name: 'DO_SET_RELAY', id: 181},
      {name: 'DO_REPEAT_RELAY', id: 182},
      {name: 'DO_SET_SERVO', id: 183},
      {name: 'DO_REPEAT_SERVO', id: 184},
      {name: 'DO_DIGICAM_CONFIGURE', id: 202},
      {name: 'DO_DIGICAM_CONTROL', id: 203},
      {name: 'DO_MOUNT_CONTROL', id: 205}
    ];


  }

}
