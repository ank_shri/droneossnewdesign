import { Router } from 'aurelia-router';
import {inject} from 'aurelia-framework';

@inject(Router)
export class Layout{
    constructor(router){
        this.router = router;
    }
    activate(){
        console.log("activate")
    }

    DocumentRoute(){
        console.log("router");
        this.router.navigate("#/documents");
    }
}