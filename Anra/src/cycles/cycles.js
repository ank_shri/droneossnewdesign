import {inject} from 'aurelia-dependency-injection';
import {CycleService} from './cycleService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {AnraNotification} from'../components/anra-notification';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(CycleService, Router, EventAggregator, AnraNotification)
export class Cycles {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['BatteryName','Duration','Capacity']},
    ];

    constructor(cycleService, router, events, notification) {
    this.cycleService = cycleService;
    this.router = router;
    this.events = events;
    this.notification = notification;
    this.heading = 'Manage Cycles';
    this.cycles = [];
    this.batteryId=null;

    this.isEditing = false;
    this.isViewing = false;
    // this.dialogService = dialogService;

    //Set Default Paging Size
    this.pageSize = cycleService.defaultPagingSize;
    this.isWaiting = false;
  }

    activate(params) {
        this.batteryId = !isNaN(parseInt(params.id, 10)) ? parseInt(params.id) : 0;
        this.getList();
  }

  attached() {
    this.subscribeEvents();
  }

  subscribeEvents() {
    this.events.subscribe('item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });
  }


  getList() {
      this.isWaiting = true;
        this.cycleService.getCycles(this.batteryId)
          .then(result => {
            this.cycles = result;
            this.isEditing = false;
            this.isWaiting = false;
          }).catch((e) => {
              this.notification.error(e.message);
              this.isWaiting = false;
          });
  }

  reset() {
    this.isEditing = false;
    this.isViewing=false;
  }

  add() {
    this.isWaiting=true;
      this.cycleService.getNewCycle(this.batteryId)
      .then(result => {
          this.cycle = result;
          this.cycle.BatteryId = !isNaN(parseInt(this.batteryId, 10)) ? parseInt(this.batteryId):this.cycle.BatteryId;
          this.isEditing = true;
          this.cycle.CycleDate = moment(this.CycleDate).format('YYYY-MM-DD');
          this.isWaiting=false;
      });
  }

  editItem(id) {
      this.isWaiting = true;
      this.cycleService.getCycle(id)
        .then(result => {            
            this.cycle = result;
            this.cycle.CycleDate = moment(result.CycleDate).format('YYYY-MM-DD');
            this.isEditing = true;
            this.isWaiting = false;
        });
  }

  
  deleteItem(id) {
    this.isWaiting=true;
    this.notification.Confirm(
      "Are you sure you want to delete?"
    ).then(result => {
      if (result) {
        this.cycleService.deleteCycle (id)
      .then(result => {
				  this.notification.ShowInfo('Deleted Successfully.');
        this.getList();
        this.isWaiting=false;
			  });
      }
    });
  }

  saveItem(item) {
      return this.cycleService.saveCycle(this.cycle);
  }

  showBatteries() {            
      this.router.navigate('batteries/batteries');
  }

}
