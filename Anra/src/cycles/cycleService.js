import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class CycleService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getCycles(batteryId) {
    return this.http.fetch(this.baseApi + 'cycle/listing/' + batteryId)
      .then(response => response.json());
  }

  getNewCycle(batteryId) {
      return this.http.fetch(this.baseApi + 'cycle/NewCycle/' + batteryId)
      .then(response => response.json());
  }

  getCycle(id) {
    return this.http.fetch(this.baseApi + 'cycle/' + id)
      .then(response => response.json());
  }

  deleteCycle(id) {
    return this.http.fetch(this.baseApi + 'cycle/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveCycle(Cycle) {
    return this.http.fetch(this.baseApi + 'cycle/', {
      method: 'post',
      body: json(Cycle)
    }).then(response => response.json());
  }
}
