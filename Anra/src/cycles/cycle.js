import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {AnraNotification} from '../components/anra-notification';
import moment from 'moment';

@inject(EventAggregator,AnraNotification, NewInstance.of(ValidationController))
export class Cycle {
  @bindable cycle;
  @bindable save;
  controller = null;
  rules = null;


  constructor(events,notification, controller) {
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    this.isWaiting = false;
    this.ShowBatteryInfo=true;
  }

  attached(){      
      this.setupValidationRules();
  }

  bind() {
    this.heading = this.cycle.CycleId > 0 ? 'Edit Cycle' : 'Add Cycle';
  }

  setupValidationRules() {   
      //Custom Rule For Date Validation  
     
      // ValidationRules.customRule(
      //   'date',
      //   (value, obj) => value === null || value === undefined || value instanceof Date || moment(value,"YYYY-MM-DD",true).isValid(),
      //   '\${$displayName} must be a Date.'
      // );

      this.rules = ValidationRules
           .ensure('CycleDate').required()
           .rules;
      
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  saveItem() {

      this.isWaiting = true;
      this.controller
      .validate({object: this.cycle, rules: this.rules})   
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                  this.save()
                      .then(result => {
                          this.handleSaveResult(result);
                          if(this.cycle.CycleId!='')
							{
								this.notification.showInfo('Updated Successfully.');
							}
							else
							{
								this.notification.showInfo('Added Successfully.');  
							}
                          this.isWaiting = false;
                      })
              } 
              else {
                let messages = this.controller.errors.map(x => x.message);
      
                this.application.toast(
                  Object.assign({ container: this.errorHolder }, {
                    title: 'Errors!',
                    message: messages.join(', '),
                    theme: 'danger',
                    timeout: 2000,
                    glyph: 'glyph-alert-info'
                  })
                );
      
                this.isWaiting = false;
              }
          }
          
          ).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
          });

      this.isWaiting = false;
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('item-operation', {refresh: true});
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('item-operation', {refresh: true});
  }
}
