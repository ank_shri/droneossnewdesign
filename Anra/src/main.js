import "bootstrap";
import 'leaflet';
import {UIConfig, UIConstants} from "aurelia-ui-framework";
import {AureliaConfiguration} from "aurelia-configuration";

export function configure(aurelia) {

  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin('aurelia-animator-css')
    .plugin('aurelia-validation')
    .plugin('aurelia-table')
    .plugin("aurelia-validation")
    .plugin('aurelia-plugins-dropdown')
    .plugin("aurelia-configuration", config => {
      config.setDirectory(".");
      config.setConfig("anra.json");
      config.setEnvironments({
        development: ["localhost"],
        production: ["flyanra.net"]
      });
    });


  let configInstance = aurelia.container.get(AureliaConfiguration);

  aurelia.use
    .plugin('aurelia-ui-virtualization')
    .plugin("aurelia-ui-framework", (config) => {

      UIConstants['themes'] = 'light,muted,dark,primary,secondary,info,danger,success,warning';
      UIConstants['colors'] = 'red,pink,violet,purple,indigo,blue,cyan,teal,green,lime,yellow,amber,orange,brown,lightGray,darkGray';

      config
        .title("USS")
        .version("1.0.0")
        .appKey("Anra-USS")
        .apiUrl(configInstance.get("authConfig").baseUrl);
    });

  aurelia.use.plugin("aurelia-authentication", c => c.configure(configInstance.get("authConfig")));

  aurelia.start().then(() => aurelia.setRoot());
}
