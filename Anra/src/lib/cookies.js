﻿export class EUconsentForCookies {

    constructor(){
        this.COOKIE_NAME = "ANRAEUconsent";
        this.ACCEPTANCE_DURATION_IN_DAYS = 365;
        this.COOKIE_PATH = "/";
    }

    setEUConsentCookie (value) {
        
        var days = "expires=" + new Date(+new Date() + 24 * 60 * 60 * this.ACCEPTANCE_DURATION_IN_DAYS * 1000).toUTCString() + "; ";
        var path = "path=" + this.COOKIE_PATH + "; ";

        document.cookie = this.COOKIE_NAME + "=" + JSON.stringify(value) + "; " + days + path;
    }

    getEUConsentCookie (name) {
        var c = String(document.cookie);
        var a = c == "" ? null : c.split(';');
        var isConsentAccepted = false;

        for (var i in a) {
            var p = String(a[i]).trim().match(/^(.*?)=(.*)/, '$1, $2');
            if (!p) continue;

            var key = p[1];
            if (name == key) {
                var value = p[2];
                try { value = JSON.parse(value); } catch (e) { }

                isConsentAccepted = value;
            }
        }
        return isConsentAccepted;
    }
}