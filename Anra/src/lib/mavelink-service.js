import {inject, transient} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';
import {AureliaConfiguration} from 'aurelia-configuration';
import {EventAggregator} from 'aurelia-event-aggregator';
import $ from 'jquery';
//import 'ms-signalr-client';
import * as PahoMQTT from 'paho-mqtt';

@inject(HttpClient, EventAggregator, AureliaConfiguration)
@transient()
export class MavelinkService {

  constructor(httpClient, events, config) {
    this.http = httpClient;

    this.events = events;
    this.config = config;
    this.linkStatus = 1;
    this.isHubConnected = false;
    this.isApmConnected = false;
    this.currentWayPoints = null;
    this.socket = null;
    this.lastPacketTime = null;
    this.mqttHost = config.get('mqttBrokerHost');
    this.mqttPort = config.get('mqttBrokerPort');
    this.mqttUser = config.get('mqttBrokerUserName');
    this.mqttPwd = config.get('mqttBrokerPwd');
    this.mqttUseSSL = config.get('mqttUseSSL')? true: false;
  }

  connect(droneId, missionId) {
    this.droneId = droneId;
    this.missionId = missionId;
    this.initHub();
  }

  initHub() {
    this.ConnectMqttClient();
    this.updateInterval = setInterval(() => this.updateLinkStatus(), 2000);
  }

  updateLinkStatus() {

      if (this.lastPacketTime == null) {
          this.events.publish('link-status', { droneId: this.droneId, missionId: this.missionId, status: 'NoNetwork', statusText: 'No Network' });
      }
      else {
          let packetGap = (new Date() - this.lastPacketTime) / 1000;
          let statusText = '';

          if (packetGap <= 10) {
              this.isHubConnected = true;
              statusText = "Good";
              
          }
          else if (packetGap > 10 && packetGap <= 15) {
              this.isHubConnected = true;
              statusText = "Weak";
          }
         else {
              this.isHubConnected = false;
              statusText = "Lost";
          }

          this.events.publish('link-status', { droneId: this.droneId, missionId: this.missionId, status: statusText, statusText: statusText });
      }    
  }

  ConnectMqttClient() {
    try {
      this.mqtt = new Paho.MQTT.Client(this.mqttHost, Number(this.mqttPort), "web_" + parseInt(Math.random() * 100, 10));
      this.mqtt.connect({useSSL: this.mqttUseSSL,userName: this.mqttUser, password: this.mqttPwd, onSuccess: () => this.onMqttConnect()});

      this.mqtt.onMessageArrived = (m) =>  this.handleNewPacket(m);
      this.mqtt.onConnectionLost = (p) => this.onMqttConnectionLost(p);
    }
    catch (err) {
      console.error(err);
    }
  }

  onMqttConnectionLost(p) {
    console.log('Mqtt Broker Disconnected...', p);
    this.isHubConnected = false;
    this.events.publish('link-status', { droneId: this.droneId, missionId: this.missionId, status: 'Lost', statusText: 'Lost'});
  }

  onMqttConnect() {
    console.log('Mqtt Broker Connected...', this.droneId);
    this.mqtt.subscribe('drone/oss/' + this.droneId + '/telemetry');
    this.mqtt.subscribe('drone/oss/' + this.droneId + '/alerts');
    this.isHubConnected = true;
    this.events.publish('link-status', { droneId: this.droneId, missionId: this.missionId, status: 'Good', statusText: 'Good'});
  }

  subscribeTopic(topic) {
    if (!this.isHubConnected) {
      this.notification.error('Connection Error! Please refesh the page.');
    }
    this.mqtt.subscribe(topic);
  }

  doCommand(cmd) {
    var msg = {
      Type: "Command",
      MissionId: this.missionId,
      DroneId: this.droneId,
      Value: cmd.Value,
      Alt: cmd.Alt,
      Lat: cmd.Lat,
      Lng: cmd.Lng
    }

    this.publishMqttTopic('drone_cmd_' + this.droneId, msg);
  }

  publishMqttTopic(topic, msg){
    let message = new Paho.MQTT.Message(JSON.stringify(msg));
    message.destinationName = topic;
    this.mqtt.send(message);
  }

  handleNewPacket(message) {
    this.lastPacketTime = new Date();
    if (this.isHubConnected) {
      if (message.destinationName === 'drone/oss/' + this.droneId + '/alerts') {
        this.events.publish('alert-message', { droneId: this.droneId, message: message});
      }

      if (message.destinationName === 'drone/oss/' + this.droneId + '/telemetry') {
        this.processPacket(JSON.parse(message.payloadString));
      }
    }
  }

  processPacket(packet) {
      let message = {
      missionId: packet.MissionId,
      droneId: this.droneId,
      lat: packet.Lat.toFixed(6),
      lng: packet.Lng.toFixed(6),
      pitch: packet.Pitch,
      roll: packet.Roll,
      airspeed: packet.Airspeed,
      climb: packet.Climbrate,
      alt: packet.Alt,
      heading: packet.Heading,
      mode: packet.Mode,
      isArmed: packet.Armed,
      armedMode: packet.Armed ? 'ARMED' : 'DISARMED',
      battery: packet.BatteryRemaining,
      gpssignalstatus: packet.GpsSignalStatus
    }

    this.events.publish('new-packet', {droneId: this.droneId, message: message});
  }

  writeWaypoints(waypoints, droneId) {
    var msg = {
      Type: "WriteWayPoints",
      MissionId: this.missionId,
      Value: JSON.stringify(waypoints)
    }

    this.publishMqttTopic('drone_cmd_' + this.droneId, msg);
  }

  readWaypoints(droneId) {
    var msg = {
      Type: "ReadWaypoints",
      MissionId: this.missionId
    }

    this.publishMqttTopic('drone_cmd_' + this.droneId, msg);
  }

  handleReadWaypoint(message) {
    this.events.publish('drone-view:waypoints', {waypoints: message});
  }

}
