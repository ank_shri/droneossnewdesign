import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class FeedbackService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
  }

  getFeedbacks() {
      return this.http.fetch(this.baseApi + 'feedback/listing')
      .then(response => response.json());
  }

  getNewFeedback() {
      return this.http.fetch(this.baseApi + 'feedback/NewFeedback')
      .then(response => response.json());
  }

  deleteFeedback(id) {
    return this.http.fetch(this.baseApi + 'feedback/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveFeedback(Feedback) {
    return this.http.fetch(this.baseApi + 'feedback/', {
      method: 'post',
      body: json(Feedback)
    }).then(response => response.json());
  }
}
