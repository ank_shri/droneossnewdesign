import { AureliaConfiguration } from "aurelia-configuration";
import { AuthService } from "aurelia-authentication";
import { HttpClient } from "aurelia-fetch-client";
import { inject } from "aurelia-framework";

@inject(AuthService, AureliaConfiguration)
export class CustomHttpClient extends HttpClient {
  constructor(auth, configure) {
    super();
    this.configure(config => {
      config
        .withBaseUrl(configure.get("gatewayBaseUrl"))
        .withDefaults({
          credentials: "same-origin",
          headers: {
            Accept: "application/json",
            "X-Requested-With": "Fetch"
          }
        })
        .withInterceptor({
            request(request) {
              request.headers.append('Authorization', "bearer " + auth.authentication.accessToken);
              return request;
          },
          response(response) {
            return response;
          }
        });
    });
  }
}
