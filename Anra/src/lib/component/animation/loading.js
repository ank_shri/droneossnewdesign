import {customElement, bindable} from 'aurelia-framework';

@customElement("loading")
export class Loading {
  @bindable displayed;
  @bindable message;

  constructor() {
    this.message = "Loading";
  }
}
