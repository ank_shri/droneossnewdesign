import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-http-client';
import {AureliaConfiguration} from 'aurelia-configuration';
import {EventAggregator} from 'aurelia-event-aggregator';
import $ from 'jquery';
import 'ms-signalr-client';
import {Notification} from 'aurelia-notification';

@inject(HttpClient, EventAggregator, AureliaConfiguration, Notification)
export class MavelinkServiceEx {

  /**
   * Vehicle types
   * @enum {number}
   */

  MavType = {
    FIXED_WING: 1,
    QUADROTOR: 2
  };


  /**
   * ArduPlane flight modes.
   * @type {Object.<number, string>}
   */
  ArduPlaneFlightModes = {
    0: 'MANUAL',
    1: 'CIRCLE',
    2: 'STABILIZE',
    5: 'FBWA',
    6: 'FBWB',
    7: 'FBWC',
    10: 'AUTO',
    11: 'RTL',
    12: 'LOITER',
    13: 'TAKEOFF',
    14: 'LAND',
    15: 'GUIDED',
    16: 'INITIALIZING'
  };


  /**
   * ArduCopter flight modes
   * @type {Object.<number, string>}
   */
  ArduCopterFlightModes = {
    0: 'STABILIZE',
    1: 'ACRO',
    2: 'ALT_HOLD',
    3: 'AUTO',
    4: 'GUIDED',
    5: 'LOITER',
    6: 'RTL',
    7: 'CIRCLE',
    8: 'POSITION',
    9: 'LAND',
    10: 'OF_LOITER',
    11: 'APPROACH'
  };

  MavModeFlag = {
    CUSTOM_MODE_ENABLED: 1,
    TEST_ENABLED: 2,
    AUTO_ENABLED: 4,
    GUIDED_ENABLED: 8,
    STABILIZE_ENABLED: 16,
    HIL_ENABLED: 32,
    MANUAL_INPUT_ENABLED: 64,
    SAFETY_ARMED: 128
  };

  CommStatusModelState = {
    UNINITIALIZED: 0,
    OK: 1,
    TIMED_OUT_ONCE: 2,
    TIMED_OUT_MANY: 3,
    ERROR: 4
  };

  constructor(httpClient, events, config, notification) {
    this.http = httpClient;
    this.events = events;
    this.config = config;
    this.notification = notification;
    this.linkStatus = 1;
    this.isHubConnected = false;
    this.isApmConnected = false;
    this.currentWayPoints = null;
  }

  connect(droneId, address, port) {
    this.droneId = droneId;
    this.isLogging = false;
    this.protocol = 'http';
    this.ipAddress = address;
    this.portnumber = port;
    //this.listener();
    this.initHub();
  }


  initHub() {
    let apmUrl = this.config.get('apmServiceUrl');
    this.connection = $.hubConnection(apmUrl);
    this.proxy = this.connection.createHubProxy('MavlinkMessageHub');

    this.proxy.on('broadcastMessage', this.onBroadCast);
    this.proxy.on('handleConnectionResponse', message => {
      this.handleConnectionResponse(message)
    });

    this.proxy.on('handleNewPacket', message => {
      this.handleNewPacket(message)
    });

    this.proxy.on('handleReadWaypoint', message => {
      this.handleReadWaypoint(message)
    });


    this.proxy.on('handleSaveWaypoint', message => {
      this.handleSaveWaypoint(message)
    });



    this.startConnection();
  }

  startConnection() {
    let self = this;
    this.connection.start({jsonp: true})
      .done(function () {
        self.notification.info('Connection established.');
        console.log('Now connected, connection ID=' + self.connection.id);
        self.isHubConnected = true;
        self.startMavlinkConnection();
        self.startHeartBeat();
      })
      .fail(function () {
        self.notification.error('Could not connect.');
        self.isHubConnected = false;
      });


    /*   if (!this.isHubConnected) {
     this.delay(500)
     .then(() => {
     this.startConnection();
     });
     }*/
  }

  startHeartBeat() {
    this.delay(2000)
      .then(() => {
        this.proxy.invoke('handleClientPing', this.droneId, this.ipAddress, this.portnumber);
      }, this);
  }

  startMavlinkConnection() {
    /*console.log('startMavlinkConnection '+this.isApmConnected);
     if (!this.isApmConnected) {
     this.delay(5000)
     .then(() => {
     if (this.isHubConnected) {
     this.proxy.invoke('handleClientConnect', this.droneId, this.ipAddress, this.portnumber);
     }
     this.startMavlinkConnection();
     }, this);
     }*/
    if (this.isHubConnected) {
      this.proxy.invoke('handleClientConnect', this.droneId, this.ipAddress, this.portnumber);
    }
  }

  handleConnectionResponse(message) {
    console.log('handleConnectionResponse ' + message);
    
    if (message.IsConnected) {
      this.isApmConnected = true;
      this.events.publish('connection-ready', {droneId: this.droneId});
      this.processPacket(message);
    }
    else {
      if (!this.isApmConnected) {
        this.notification.error('Retrying connection..');
        console.log('retrying apm connection');
        this.delay(500)
          .then(() => {
            this.startMavlinkConnection();
          }, this);
      }
    }
  }

  onBroadCast(message) {
    this.notification.info(message);
  }

  startListener() {
    this.delay(500)
      .then(() => {
        if (this.isApmConnected) {
          this.proxy.invoke('handleClientStart', this.droneId, this.ipAddress, this.portnumber, this.isLogging);
        }
        this.startListener();
      }, this);
  }

  startTracking(missionId) {
    this.isLogging = true;
    if (this.isHubConnected) {
      this.proxy.invoke('handleClientStartTracking', this.droneId, missionId, this.ipAddress, this.portnumber);
    }
  }

  stopTracking(missionId) {
    if (this.isHubConnected) {
      this.proxy.invoke('handleClientStopTracking', this.droneId, missionId, this.ipAddress, this.portnumber);
    }
  }


  handleNewPacket(message) {
    if (message.IsConnected) {
      this.processPacket(message);
    }
  }

  processPacket(packet) {
    let message = {
      droneId: this.droneId,
      lat: packet.Lat.toFixed(6),
      lng: packet.Lng.toFixed(6),
      pitch: packet.Pitch,
      roll: packet.Roll,
      airspeed: packet.Airspeed,
      climb: packet.Climbrate,
      alt: packet.Alt,
      heading: packet.Heading,
      mode: packet.Mode,
      isArmed: packet.Armed,
      armedMode: packet.Armed ? 'ARMED' : 'DISARMED',
      battery: packet.BatteryRemaining
    }

    this.events.publish('new-packet', {droneId: this.droneId, message: message});
  }


  listener() {
    this.delay(500)
      .then(() => {
        this.getPackets();
        this.listener();
      }, this);
  }

  delay(interval) {
    return new Promise(function (resolve) {
      setTimeout(resolve, interval);
    });
  }

  getPackets() {
    let url = this.protocol + '://' + this.ipAddress + ':' + this.portnumber;
    url += '/mavlink/ATTITUDE+VFR_HUD+NAV_CONTROLLER_OUTPUT+META_WAYPOINT+GPS_RAW_INT+HEARTBEAT+META_LINKQUALITY+STATUSTEXT+GPS_STATUS+SYS_STATUS';
    return this.http.get(url).then(r=> {
      let packet = JSON.parse(r.response);

      this.updateLinkStatus(false);

      let message = {
        droneId: this.droneId,
        lat: packet.GPS_RAW_INT.msg.lat / 1.0e7,
        lng: packet.GPS_RAW_INT.msg.lon / 1.0e7,
        pitch: packet.ATTITUDE.msg.pitch * 100,
        roll: packet.ATTITUDE.msg.roll * 100,
        airspeed: packet.VFR_HUD.msg.airspeed,
        climb: packet.VFR_HUD.msg.climb,
        alt: packet.VFR_HUD.msg.alt,
        heading: packet.VFR_HUD.msg.heading,
        mode: this.getModestring(packet.HEARTBEAT.msg),
        isArmed: this.getIsArmed(packet.HEARTBEAT.msg),
        armedMode: this.getIsArmed(packet.HEARTBEAT.msg) ? 'ARMED' : 'DISARMED',
        battery: packet.SYS_STATUS.msg.battery_remaining
      }

      this.events.publish('new-packet', {droneId: this.droneId, message: message});

    }).catch(err => {
      this.updateLinkStatus(true);
    });
  }

  updateLinkStatus(isError) {
    if (isError) {
      let mavstat = this.linkStatus;
      if (mavstat === this.CommStatusModelState.OK) {
        this.linkStatus = this.CommStatusModelState.TIMED_OUT_ONCE;
      } else if (mavstat === this.CommStatusModelState.TIMED_OUT_ONCE) {
        this.linkStatus = this.CommStatusModelState.TIMED_OUT_MANY;
      }
    }
    else {
      this.linkStatus = this.CommStatusModelState.OK;
    }

    let statusText = '';
    switch (this.linkStatus) {
      case 0:
        statusText = 'None';
        break;
      case 1:
        statusText = 'Good';
        break;
      case 2:
        statusText = 'Lost';
        break;
      case 3:
        statusText = 'Lost';
        break;
      case 4:
        statusText = 'Error';
        break;
    }

    this.events.publish('link-status', {status: this.linkStatus, statusText: statusText});
  }

  doCommand(cmd) {
    if (this.isHubConnected) {
      this.proxy.invoke('handleMavlinkCommand', cmd, this.droneId, this.ipAddress, this.portnumber);
    }
  }

  writeWaypoints(waypoints, droneId) {
    if (this.isHubConnected) {
      this.proxy.invoke('handleWriteWaypointCommand', waypoints, this.droneId, this.ipAddress, this.portnumber);
    }
  }

  readWaypoints(droneId) {
    if (this.isHubConnected) {
      this.proxy.invoke('handleReadWaypointCommand', this.droneId, this.ipAddress, this.portnumber);
    }
  }

  handleReadWaypoint(message){
    this.events.publish('drone-view:waypoints', {waypoints: message});
  }

  saveWaypoints(droneId) {
    if (this.isHubConnected) {
      this.proxy.invoke('handleSaveWaypointCommand', this.droneId, this.ipAddress, this.portnumber);
    }
  }

  handleSaveWaypoint(message){

    let fileName = 'waypoints.txt';
    let textFileAsBlob = new Blob([message], { type: 'text/plain' });
    if ('msSaveOrOpenBlob' in navigator) {
      navigator.msSaveOrOpenBlob(textFileAsBlob, fileName);
    } else {
      var downloadLink = document.createElement('a');
      downloadLink.download = fileName;
      downloadLink.innerHTML = 'Download File';
      if ('webkitURL' in window) {
        // Chrome allows the link to be clicked without actually adding it to the DOM.
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
      } else {
        // Firefox requires the link to be added to the DOM before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = 'none';
        document.body.appendChild(downloadLink);
      }

      downloadLink.click();
    }

  }
}
