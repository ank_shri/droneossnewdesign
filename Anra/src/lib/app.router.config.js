import {AuthorizeStep} from "aurelia-auth";
import {inject} from "aurelia-framework";
import {Router} from "aurelia-router";

@inject(Router)
export default class {

  constructor(router) {
    this.router = router;
  }

  configure() {
    var appRouterConfig = function (config) {
      config.title = 'ANRA - Drone Operations';
      config.addPipelineStep('authorize', AuthorizeStep);

      config.map([
        {
          route: ['', 'dashboard'],
          moduleId: 'dashboard/welcome',
          nav: true,
          title: 'DASHBOARD',
          auth: true,
          settings: {icon: 'anra-ico_cockpit_dark'}
        },
        {
          route: ['flightplan'],
          moduleId: 'flightplan/flightplan',
          nav: true,
          title: 'FLIGHT PLAN',
          auth: true,
          href: '#/missions',
          settings: {
            icon: 'anra-ico_flightplan_dark',
            subMenu: [
              {href: '#/missions', title: 'Missions', settings: {icon: 'anra-ico_plane_dark'}},
              {href: '#/locations', title: 'Locations', settings: {icon: 'anra-ico_location_dark'}},
              {href: '#/documents', title: 'Documents', settings: {icon: 'anra-ico_certificate'}}
            ]
          }
        },
        {
          route: ['mission-details', 'mission-details/:id'],
          moduleId: 'missions/mission',
          nav: false,
          title: 'Mission Details',
          auth: true,
          settings: {icon: 'anra-ico_plane_dark'}
        },
        {
          title: '',
          route: 'workspace/share/:id',
          moduleId: 'workspace/shared-project',
          nav: false,
          auth: false,
          activationStrategy: 'replace'
        },
        {
          route: 'workspace/:id',
          moduleId: 'workspace/workspace-main',
          nav: false,
          auth: true,
          activationStrategy: 'replace'
        },
        { route: 'workspace/2d/:id', moduleId: 'workspace/workspace-2d', nav: false, auth: true },
        { route: 'workspace/2d-compare/:id', moduleId: 'workspace/workspace-2d-compare', nav: false, auth: true },
        {route: 'workspace/three/:id', moduleId: 'workspace/workspace-three', nav: false, auth: true},
        {route: 'workspace/pointcloud/:id', moduleId: 'workspace/workspace-potree', nav: false, auth: true},
        {route: 'workspace/sk/:id', moduleId: 'workspace/workspace-sketch', nav: false, auth: true},
        {route: 'workspace/mv/:id', moduleId: 'workspace/map-view', nav: false, auth: true},
        { route: 'workspace/gallery/:id', moduleId: 'workspace/gallery', nav: false, auth: true, activationStrategy: 'replace' },
        { route: 'workspace/inspection/:id', moduleId: 'workspace/inspection', nav: false, auth: true, activationStrategy: 'replace' },
        { route: 'workspace/inspection-report/:id', moduleId: 'workspace/inspection-report', nav: false, auth: true, activationStrategy: 'replace' },

        { route: 'workspace/2dshare/:id', moduleId: 'workspace/workspace-2d-share', nav: false, auth: false },
        { route: 'workspace/threeshare/:id', moduleId: 'workspace/workspace-three-share', nav: false, auth: false },
        { route: 'workspace/pointcloudshare/:id', moduleId: 'workspace/workspace-potree-share', nav: false, auth: false },
        { route: 'workspace/skshare/:id', moduleId: 'workspace/workspace-sketch-share', nav: false, auth: false },
        { route: 'workspace/mvshare/:id', moduleId: 'workspace/map-view-share', nav: false, auth: false },
        { route: 'workspace/galleryshare/:id', moduleId: 'workspace/gallery-share', nav: false, auth: false },
        { route: 'workspace/inspectionshare/:id', moduleId: 'workspace/inspection-share', nav: false, auth: false },

        {route: 'waypoints/mission/:id', moduleId: 'waypoints/waypoints', nav: false, auth: true},
        {route: 'network', moduleId: 'dashboard/network', nav: false, auth: true},
        {route: 'missions', moduleId: 'missions/missions', nav: false, auth: true},
        {route: 'Locations', moduleId: 'locations/locations', nav: false, auth: true},
        {route: 'documents', moduleId: 'documents/documents', nav: false, auth: true},
        {route: 'checklist/:id', moduleId: 'checklist/checklist', nav: false, auth: true},
        {
          route: ['documents/document/:docid', 'documents/mission/:missionid'],
          moduleId: 'documents/document',
          nav: false,
          title: 'Document Details',
          auth: true,
          settings: {icon: 'anra-ico_plane_dark'}
        },
        {route: 'drone-view/:id', moduleId: 'drone-view/flight', nav: false, auth: true},
        {route: 'efforts/:id', moduleId: 'efforts/efforts', nav: false, auth: true},
        {
          route: ['logbook'],
          moduleId: 'logbook/logbook',
          nav: true,
          title: 'FLIGHT DATA',
          auth: true,
          href: '#/flight-data-listing',
          settings: {
            icon: 'anra-ico_plane_dark',
            subMenu: [
              {href: '#/flight-data-listing', title: 'Flight Log', settings: {icon: 'anra-ico_plane_dark'}},
              {href: '#/incidents', title: 'Incidents', settings: {icon: 'anra-ico_location_dark'}},
              {href: '#/importdata', title: 'Import Data', settings: {icon: 'anra-ico_system_dark'}},
              {href: '#/media', title: 'Media Management', settings: {icon: 'anra-ico_system_dark'}}
            ]
          }
        },
        {
          route: ['flight-data-listing/dashboard/:flightdataid', 'flight-data-listing/flightdata/:flightid', 'flight-data-listing/profile/:Profileflightdataid'],
          moduleId: 'flightdata/flight-data-view',
          nav: false
        },
        {route: 'flight-data-listing/:id', moduleId: 'flightdata/flight-data-listing', nav: false, auth: true},
        {route: 'flight-data-listing', moduleId: 'flightdata/flight-data-listing', nav: false, auth: true},
        {route: 'incidents', moduleId: 'incidents/incidents', nav: false, auth: true},
        {route: 'importdata', moduleId: 'import/importdata', nav: false, auth: true},
        {route: 'media', moduleId: 'media/media-listing', nav: false, auth: true},
        {
          route: ['maintenance'],
          moduleId: 'maintenance/maintenance',
          nav: true,
          title: 'EQUIPMENT',
          auth: true,
          href: '#/drones',
          settings: {
            icon: 'anra-ico_maintenance_dark',
            subMenu: [
              {href: '#/drones', title: 'Drones', settings: {icon: 'anra-ico_drone'}},
              {href: '#/batteries', title: 'Batteries', settings: {icon: 'anra-ico_batterycharge_dark'}},
              {href: '#/equipments', title: 'Equipments', settings: {icon: 'anra-ico_camera_dark'}},
              {href: '#/maintenance', title: 'Maintenance', settings: {icon: 'anra-ico_maintenance_dark'}}
            ]
          }
        },
        {route: 'drones', moduleId: 'drones/drones', nav: false, auth: true},
        {route: 'batteries', moduleId: 'batteries/batteries', nav: false, auth: true},
        {route: 'equipments', moduleId: 'equipments/listing', nav: false, auth: true},
        {route: 'maintenance', moduleId: 'maintenance/listing', nav: false, auth: true},
        { route: 'cycles/:id', moduleId: 'cycles/cycles', nav: false, auth: true },
        { route: 'sensors', moduleId: 'sensors/listing', nav: false, auth: true },
        {
          route: ['reports'],
          moduleId: 'reports/reports',
          nav: true,
          title: 'REPORTING',
          auth: true,
          settings: {icon: 'anra-ico_reporting_dark'}
        },
        {
          route:'Checklist',
          moduleId:'checklist/organizationchecklist',
          nav: true,
          title: 'CHECKLIST',
          auth: true,
          settings: { icon: 'anra-ico_checklist' }
        },
         {
          route:'certifications',
          moduleId:'certification/certifications',
          nav:true,
          auth:true,
          title:'TRAINING',
          settings:{icon:'anra-ico_certificate'}

        },        
        {route: 'compliance', moduleId: 'compliance/compliance', nav: false, auth: true},
        {route: 'customreport', moduleId: 'custom-report/customreport', nav: false, auth: true},
        {route: 'dataexport', moduleId: 'dataexport/dataexport', nav: false, auth: true},
        {
          route: ['Administration'],
          moduleId: 'administration/administration',
          nav: true,
          title: 'ADMINISTRATION',
          auth: true,
          href: '#/clients',
          settings: {
            icon: 'anra-ico_adminstration_dark'
          }
        },
        {
          route: 'user-drone-association/:id',
          moduleId: 'user-drone-association/user-drone-association',
          nav: false,
          auth: true
        },
        {route: 'Users', moduleId: 'users/users', nav: false, auth: true},
        {route: 'users/reset-password/:id', moduleId: 'users/reset-password', nav: false, auth: true},
        {route: 'Settings', moduleId: 'settings/settings', nav: false, auth: true},
        {route: 'Roles', moduleId: 'roles/roles', nav: false, auth: true},
        {route: 'Queue', moduleId: 'queue/queue', nav: false, auth: true},
        {route: 'Organizations', moduleId: 'organizations/organizations', nav: false, auth: true},
        {route: 'Clients', moduleId: 'clients/clients', nav: false, auth: true},
        {route: 'Projects', moduleId: 'projects/projects', nav: false, auth: true},
        { route: 'Preferences', moduleId: 'organizations/notification-preferences', nav: false, auth: true},
        {route: ['user-form'], moduleId: 'users/user', nav: false, title: 'Add/Edit User', auth: false, auth: true},
        {
          route: 'login', moduleId: 'security/login', nav: false, auth: false, title: 'Login'
        },
        {
          route: 'profile', moduleId: 'users/profile', nav: false, auth: true, title: 'My Profile'
        },
        {
          route: 'logout', moduleId: 'security/logout', nav: false, title: 'Logout'
        },
        {
          route: 'signup', moduleId: 'security/signup', nav: false, title: 'SignUp', auth: false
        },
        {
          route: 'alerts', moduleId: 'alerts/alerts', nav: false, auth: true, title: 'Alert'
        },
        {
          route: ['alerts/maintenance/:maintid', 'maintenance/:id'],
          moduleId: 'maintenance/maintenance-editor',
          nav: false,
          auth: true
        },
      ]);
    };

    this.router.configure(appRouterConfig);
  }

}
