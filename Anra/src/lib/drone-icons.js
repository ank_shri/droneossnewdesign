export class DroneIcons {
  droneIcons = {
    'dashboard': {
      description: 'Dashboard (small)',
      constructor: L.Icon.extend({
        options: {
          'iconUrl': '../image/quad-copter-small.png',
          'shadowUrl': null,
          'iconAnchor': new L.Point(45, 0)
        }
      })
    },

    'quad-copter': {
      description: 'Fixed Gray Marker (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/quad-copter-small.png',
          iconSize: [50, 47], // size of the icon
          iconAnchor: [25, 25], // point of the icon which will correspond to marker's location
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'quad-marker-orange': {
      description: 'Quad Orange Marker (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/quad-marker-orange.png',
          iconSize: [68, 100], // size of the icon
          iconAnchor: [34, 99], // point of the icon which will correspond to marker's location
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'fixed-green-marker': {
      description: 'Fixed Green Marker (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/drone-marker-green.png',
          iconSize: [64, 100], // size of the icon
          iconAnchor: [32, 99], // point of the icon which will correspond to marker's location
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'fixed-red-marker': {
      description: 'Fixed Red Marker (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/drone-marker-red.png',
          iconSize: [64, 100], // size of the icon
          iconAnchor: [32, 99], // point of the icon which will correspond to marker's location
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'fixed-gray-marker': {
      description: 'Fixed Gray Marker (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/drone-marker-gray.png',
          iconSize: [64, 100], // size of the icon
          iconAnchor: [32, 99], // point of the icon which will correspond to marker's location
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'greenicon': {
      description: 'GreenLeaf (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/leaf-green.png',
          shadowUrl: '../image/leaf-shadow.png',

          iconSize: [38, 95], // size of the icon
          shadowSize: [50, 64], // size of the shadow
          iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
          shadowAnchor: [4, 62],  // the same for the shadow
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'redicon': {
      description: 'RedLeaf (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/leaf-red.png',
          shadowUrl: '../image/leaf-shadow.png',

          iconSize: [38, 95], // size of the icon
          shadowSize: [50, 64], // size of the shadow
          iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
          shadowAnchor: [4, 62],  // the same for the shadow
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'orangeicon': {
      description: 'OrangeLeaf (small)',
      constructor: L.Icon.extend({
        options: {
          iconUrl: '../image/leaf-orange.png',
          shadowUrl: '../image/leaf-shadow.png',

          iconSize: [38, 95], // size of the icon
          shadowSize: [50, 64], // size of the shadow
          iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
          shadowAnchor: [4, 62],  // the same for the shadow
          popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        }
      })
    },

    'predatorsmall': {
      description: 'Predator (small)',
      constructor: L.Icon.extend({
        options: {
          'iconUrl': '../image/drone-tiny.png',
          'shadowUrl': null,
          'iconAnchor': new L.Point(37, 25),
          'iconSize': new L.Point(75, 50)
        }
      })
    },
    'predator': {
      description: 'Predator',
      constructor: L.Icon.extend({
        options: {
          'iconUrl': '../image/drone-sm.png',
          'shadowUrl': null,
          'iconAnchor': new L.Point(75, 50),
          'iconSize': new L.Point(150, 100)
        }
      })
    },
    'arduplane': {
      description: 'Generic Airplane',
      constructor: L.Icon.extend({
        options: {
          'iconUrl': '../image/plane.png',
          'shadowUrl': null,
          'iconAnchor': new L.Point(36, 38),
          'iconSize': new L.Point(73, 76)
        }
      })
    },
    'quad': {
      description: 'Quadcopter',
      constructor: L.Icon.extend({
        options: {
          'iconUrl': '../image/quad.png',
          'shadowUrl': null,
          'iconAnchor': new L.Point(37, 37),
          'iconSize': new L.Point(75, 75)
        }
      })
    }
  };
}
