import {HttpClient} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';
import {AureliaConfiguration} from 'aurelia-configuration';

@inject(AuthService, AureliaConfiguration)
export class CustomHttpClient extends HttpClient {
  constructor(auth, configure) {
    super();
    this.configure(config => {
      config
        .withBaseUrl(configure.get('gatewayBaseUrl'))
        .withDefaults({
          credentials: 'same-origin',
          headers: {
            'Accept': 'application/json',
            'X-Requested-With': 'Fetch'
          }
        })
        .withInterceptor(auth.tokenInterceptor)
        .withInterceptor({
          request(request) {
            console.log(`Requesting ${request.method} ${request.url}`);
            return request;
          },
          response(response) {
            console.log(`Received ${response.status} ${response.url}`);
            return response;
          }
        });
    });
  }
}
