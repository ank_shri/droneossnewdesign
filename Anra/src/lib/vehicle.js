import {inject, transient} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {MavelinkService} from 'lib/mavelink-service';
import {DroneService} from 'drones/droneService';

@inject(EventAggregator, MavelinkService, DroneService)
@transient()
export class Vehicle {
  constructor(events, mavelinkService, droneService) {
    this.icon = null;
    this.isWaiting = false;
    this.events = events;
    this.mavelinkService = mavelinkService;
    this.droneService = droneService;
  }

  initDroneEx(droneId) {
    this.isWaiting = true;
    this.droneId = droneId;
    this.droneService.getDrone(droneId)
      .then(result => {
        this.drone = result;
        this.init();
        this.events.publish('vehicle:ready', {
          isReady: true,
          lng: this.drone.BaseLng,
          lat: this.drone.BaseLat,
          droneId: this.drone.droneId
        });
        this.isWaiting = false;
      });
  }

  initDrone(mission) {
    console.log('vehicle initDrone ', mission.DroneId);
    this.droneId = mission.DroneId; 
    this.mission = mission;
    this.assignIcon();
    this.init();
  }

  init() {
    this.subscribeEvents();
    this.mavelinkService.connect(this.droneId,this.mission.MissionId);
  }


  subscribeEvents() {
    /* this.events.subscribe('new-position-msg', p => {
     this.updatePosition(p.lat, p.lng);
     })*/

    this.events.subscribe('new-packet', p => {
      if (this.droneId == p.droneId) {
        if (this.path === undefined || this.path == null) {
          this.trackPath(p.message.lat, p.message.lng);
        }
        this.updatePosition(p.message.lat, p.message.lng);
      }
    })
    /*
     this.events.subscribe('leaflet:ready', () => {
     this.assignIcon();
     this.mavelinkService.connect(this.drone.DroneId, this.drone.IpAddress, this.drone.Port);
     });
     */
    /*this.events.subscribe('vehicle:ready', () => {
     this.init();
     });*/

  }

  assignIcon() {
    let droneIcon = L.icon({
      iconUrl: './image/quad-copter-small.png',
      iconSize: [50, 47],
      iconAnchor: [25, 25],
      popupAnchor: [-3, -76]
    });

    this.guideMarker = new L.Marker(new L.LatLng(this.mission.BaseLat, this.mission.BaseLng), {'icon': droneIcon});

    this.events.publish('leaflet:add-vehicle', {'vehicle': this.guideMarker});

    this.updatePosition(this.mission.BaseLat, this.mission.BaseLng)
  }

  trackPath(lat, lng) {
    this.path = new L.Polyline(new L.LatLng(lat, lng), {'color': 'red'});
    this.events.publish('leaflet:add-layer', {'newlayer': this.path});

    this.events.publish('leaflet:set-view', {lat: lat, lng: lng, zoom: 18});
  }


  updatePosition(lat, lng) {
    var latlng = new L.LatLng(lat, lng);
    this.guideMarker.setLatLng(new L.LatLng(lat, lng));

   /* if (this.path) {
      this.path.addLatLng(latlng);
    }
    this.events.publish('leaflet:set-view', {lat: lat, lng: lng});*/
  }

  startTracking(missionId) {
    this.mavelinkService.startTracking(missionId);
  }

  stopTracking(missionId) {
    this.mavelinkService.stopTracking(missionId);
  }

  doCommand(cmd) {
    this.mavelinkService.doCommand(cmd);
  }


  readWaypoints() {
    this.mavelinkService.readWaypoints(this.droneId);
  }

  writeWaypoints(waypoints) {
    this.mavelinkService.writeWaypoints(waypoints, this.droneId);
  }

  saveWaypoints() {
    this.mavelinkService.saveWaypoints(this.droneId);
  }
}
