import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class ChecklistService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getChecklist(gufi) {
    return this.http.fetch(this.baseApi + 'checklist/MissionChecklist/' + gufi)
      .then(response => response.json());
  } 

  getOrgChecklist() {
    return this.http.fetch(this.baseApi + 'checklist/listing/')
      .then(response => response.json());
  }

  getOrgChecklistById(id) {
    return this.http.fetch(this.baseApi + 'checklist/' + id)
      .then(response => response.json());
  }

  getNewOrgChecklist() {
    return this.http.fetch(this.baseApi + 'checklist/NewOrganizationChecklist')
      .then(response => response.json());
  }

  saveOrgChecklist(item) {
    return this.http.fetch(this.baseApi + 'checklist/', {
      method: 'post',
      body: json(item)
    }).then(response => response.json());
  }

  saveMissionChecklist(MissionCheckListItems,missionId) {      
    return this.http.fetch(this.baseApi + 'checklist/SaveMissionChecklist/' + missionId, {
      method: 'post',
      body: json(MissionCheckListItems)
    }).then(response => response.json());
  }

  deleteOrgChecklistById(id) {
    return this.http.fetch(this.baseApi + 'checklist/' + id, {
      method: 'delete'
    })
      .then(response => {
        response.json();
      }
      );
  }

  getNewChecklistItem(id) {
    return this.http.fetch(this.baseApi + 'checklist/NewChecklistItem/' + id)
      .then(response => response.json());
  }

  saveCheckListItem(item) {
    return this.http.fetch(this.baseApi + 'checklist/SaveChecklistItem', {
      method: 'post',
      body: json(item)
    }).then(response => response.json());
  }

  getChecklistItems(id) {
    return this.http.fetch(this.baseApi + 'checklist/ChecklistItems/' + id)
      .then(response => response.json());
  }

  deleteCheckListItem(id) {
    return this.http.fetch(this.baseApi + 'checklistItem/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

}
