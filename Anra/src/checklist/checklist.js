﻿import { inject } from 'aurelia-framework';
import { ChecklistService } from 'checklist/checklistService';
import { Router } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';
import { PermissionService } from 'security/permissionService';
import {AnraNotification} from '../components/anra-notification';
import { MissionService } from 'missions/missionService';


@inject(ChecklistService, Router, EventAggregator, PermissionService, AnraNotification, MissionService)
export class CheckList {

  constructor(checklistService, router, events, permissionService, notification, missionService) {
    this.checklistService = checklistService;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;
    this.missionService = missionService;
    this.notification = notification;
    this.heading = 'Manage Mission Checklist';
    this.checklist = [];
    this.missionChecklistItems = null;
    this.permission = {};
    this.gufi = null;
    this.isWaiting = false;
    this.isReady = false;
  }

  activate(params) {
    this.gufi = params.id;
    this.initialize();
  }

  initialize() {
    this.isWaiting = true;
    let actions = [];
    actions.push(this.missionService.getMissionRefactor(this.gufi));
    actions.push(this.checklistService.getChecklist(this.gufi));


    Promise.all(actions)
      .then(results => {
        this.mission = results[0].Mission;
        this.checklist = results[1].Checklist;
        this.missionChecklistItems = results[1].MissionChecklistItems;

        if (this.checklist != null) {
          this.defaultData = JSON.stringify(this.checklist);
          this.initTreeview();          
          this.isReady = true;
        }
        else {
          this.notification.ShowInfo('No Checklist Items Found.');
          this.cancel();
        }
        this.isWaiting = false;        
      })
      .catch(e => this.notification.ShowInfo(e.message))
      .finally(() => {
        this.isWaiting = false;
      });
  }

  initTreeview() {
    let treeChecklist = $('#treeChecklist').treeview({
      data: this.defaultData,
      showIcon: false,
      showCheckbox: false,
      onNodeChecked: function(ev,node) {
        if (node.nodes !== undefined) {
          for (let i in node.nodes) {
            $('#treeChecklist').treeview('checkNode', node.nodes[i].nodeId);
          }
        }
      },
      onNodeUnchecked: function(ev,node) {
        if (node.nodes !== undefined) {
          for (let i in node.nodes) {
            $('#treeChecklist').treeview('uncheckNode', node.nodes[i].nodeId);
          }
        }
      }
    });

    treeChecklist.treeview('collapseAll', { silent: true });
  }

  saveItem() {
    this.isWaiting = true;
    let checkedItems = $('#treeChecklist').treeview('getChecked');
    let missionCheckedItems = checkedItems.filter(function (o) { return o.ParentId == 0; });

    this.missionChecklistItems = [];
    if (missionCheckedItems && missionCheckedItems.length > 0) {
      for (let i = 0; i < missionCheckedItems.length; i++) {
        let item = missionCheckedItems[i];
        let missionChecklistItem = {
          'ChecklistId': item.Id,
          'IsChecked': true,
          'MissionId': this.mission.MissionId
        };

        this.missionChecklistItems.push(missionChecklistItem);
      }      
    }

    // validation succeeded
    this.checklistService.saveMissionChecklist(this.missionChecklistItems, this.mission.MissionId)
      .then(result => {
        this.handleSaveResult(result);
        this.isWaiting = false;
        this.router.navigate('#/missions');

      }).catch((e) => {
        this.notification.ShowInfo('ShowInfo.');
        this.isWaiting = false;
      });
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.notification.ShowInfo('Mission Checklist Saved.');
    }
    else {
      this.notification.ShowInfo('Failed.');
    }
  }

  cancel() {
    this.router.navigate('#/missions');
  }
}
