import { inject } from 'aurelia-framework';
import { ChecklistService } from 'checklist/checklistService';
import { Router } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';
import { PermissionService } from 'security/permissionService';

import { MissionService } from 'missions/missionService';
// import { Prompt } from 'components/my-modal';
import {AnraNotification} from '../components/anra-notification';


@inject(ChecklistService, Router, EventAggregator, PermissionService, MissionService,AnraNotification)
export class organizationchecklist {

  //Set Filters for Searchbox
  filters = [
    { value: '', keys: ['Name', 'OrganizationName'] },
  ];

  constructor(checklistService, router, events, permissionService, missionService,notification) {
    this.checklistService = checklistService;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;
    this.missionService = missionService;
    this.notification = notification;
    this.heading = 'Manage Organization Checklist';
    this.checklists = [];
    this.checklist = undefined;
    this.checklistitems = [];
    //Set Default Paging Size
    this.pageSize = checklistService.defaultPagingSize;   
    this.isWaiting = false;
    this.showParent = false;
    this.showEdit = false;
    this.showView = false;
    this.showAddChecklistItem = false;
  }

  activate(params) 
  {
    console.log('hello',params);
    if(params.id)
    {
      console.log(params);
      this.viewItem(params.id);
    }
    
    this.getList();
  }

  attached(params) {
    
    this.subscribeEvents();
  }
  //detached() {
  //  this.subscriber.dispose();
  //}

  //getPermission(moduleId) {
  //    this.permissionService.getPermission(moduleId)
  //      .then(result => {
  //          this.permission = result;
  //      });
  //}

  subscribeEvents() {
    this.subscriber = this.events.subscribe('checklist-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber = this.events.subscribe('checklist-item-operation-edit', payload => {

      if (payload) {
        if (payload.id > 0) {
          this.editItem(payload.id);
        }
        this.reset();
      }
    });

  }

  getList() {

    this.isWaiting = true;
    this.checklistService.getOrgChecklist()
      .then(result => {
        this.checklists = result;
        this.showParent = true;
        this.showEdit = false;
        this.showView = false;
        this.showAddChecklistItem = false;
        this.isWaiting = false;
      })
      .catch(e => this.notification.error(e.message))
      .finally(() => {
        this.isWaiting = false;
      });
  }

  reset() {
    this.checklist = undefined;
    this.showParent = true;
    this.showEdit = false;
    this.showView = false;
    this.showAddChecklistItem = false;
  }

  add() {
    this.isWaiting = true;
    this.checklistService.getNewOrgChecklist()
      .then(result => {
        this.isWaiting = false;
        this.checklist = result;
        this.showParent = false;
        this.showEdit = true;
        this.showView = false;
        this.showAddChecklistItem = false;
      })
      .catch(e => this.notification.ShowInfo(e.message))
      .finally(() => {
        this.isWaiting = false;
      });
  }

  editItem(id) {
    this.isWaiting = true;
    this.checklistService.getOrgChecklistById(id)
      .then(result => {

        this.checklist = result;
        this.showParent = false;
        this.showEdit = true;
        this.showView = false;
        this.showAddChecklistItem = false;
        this.isWaiting = false;
      })
      .catch(e => this.notification.ShowInfo(e.message))
      .finally(() => {
        this.isWaiting = false;
      });
  }

  viewItem(id) {
    this.isWaiting = true;
    this.checklistService.getChecklistItems(id)
        .then(result => {              
          this.checklistitems = result;
          console.log(result)
          this.showParent = false;
          this.showEdit = false;
          this.showView = true;
          this.showAddChecklistItem = false;
          this.isWaiting = false;
      })
      .catch(e => this.notification.ShowInfo(e.message))
      .finally(() => {
        this.isWaiting = false;
      });
  }

  deleteItem(id) {
    this.isWaiting=true;
    this.notification.Confirm(
      "Are you sure you want to delete?"
    ).then(result => {
      if (result) {
        this.checklistService.deleteOrgChecklistById(id)
          .then(result => {
            this.notification.ShowInfo('Deleted Successfully.');
            this.getList();
          })
          .catch(e => this.notification.ShowInfo(e.message))
          .finally(() => {
            this.isWaiting = false;
          });
      }
      else{
        this.isWaiting=false;
      }
    });

    
  }

  saveItem() {
    return this.checklistService.saveOrgChecklist(this.checklist);
  }

  addChecklistItem(id) {
    this.checklistService.getNewChecklistItem(id)
      .then(result => {
        this.checklistitems = result;
        this.showParent = false;
        this.showEdit = false;
        this.showView = false;
        this.showAddChecklistItem = true;
      })
      .catch(e => this.notification.ShowInfo(e.message))
      .finally(() => {
        this.isWaiting = false;
      });
  }
}
