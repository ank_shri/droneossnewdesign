import { bindable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import { inject } from 'aurelia-dependency-injection';

@inject(EventAggregator, AnraNotification)
export class ViewCheckListItem {
  @bindable checklistitems;

  constructor(events, notification, controller) {
    this.events = events;
    this.notification = notification;
    this.isWaiting = false;
    this.treeviewItems = [];
  }

  attached() {
    this.Initialize();
  }


  Initialize() {
    if (this.treeviewItems.length > 0) {
      this.defaultData = JSON.stringify(this.treeviewItems);

      this.initTreeview();
    }
  }

  bind() {
    this.treeviewItems = this.checklistitems;
    this.heading = 'View Checklist Items';
  }

  cancel() {
    this.events.publish('checklist-item-operation', { refresh: false });
  }

  initTreeview() {

    let treeChecklist = $('#treeChecklist').treeview({
      data: this.defaultData,
      showIcon: false,
      showCheckbox: false
    });

    treeChecklist.treeview('collapseAll', { silent: true });
  }
}
