import { bindable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import { inject, NewInstance } from 'aurelia-dependency-injection';
import { ValidationController, ValidationRules, validateTrigger } from 'aurelia-validation';
import { BootstrapFormValidationRenderer } from "lib/bootstrap-form-validation-renderer.js";

@inject(EventAggregator, AnraNotification, NewInstance.of(ValidationController))
export class AddCheckList {
  @bindable checklist;
  @bindable save;
  controller = null;
  rules = null;

  constructor(events, notification, controller) {
    this.events = events;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
    this.showChecklist=true;
  }

  attached() {
    this.setupValidationRules();
  }

  detached() {
    //Finally Reset Validation
    this.controller.reset();
  }

  bind() {
    this.heading = this.checklist.Name != null ? 'Edit Organization Checklist' : 'Add Organization Checklist';
  }

  setupValidationRules() {

    this.rules = ValidationRules
      .ensure('Name').required()
      .ensure('Comments').required()
      .rules;
  }

  saveItem() {

    this.isWaiting = true;
    this.controller.validate({ object: this.checklist, rules: this.rules })
      .then(valresult => {
        if (valresult.valid) {
          // validation succeeded
          this.save()
            .then(result => {
              this.handleSaveResult(result);
              if (this.checklist.OrganizationChecklistId != '') {
                this.notification.ShowInfo('Updated Successfully.');
              }
              else {
                this.notification.ShowInfo('Added Successfully.');
              }
              this.isWaiting = false;
            })
        }
        else {
          // validation failed
          this.notification.ShowError('Please enter a valid input.');
          this.isWaiting = false;
        }
      }).catch((e) => {
        this.notification.ShowError('Error.');
        this.isWaiting = false;
      });

    this.isWaiting = false;
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.events.publish('checklist-item-operation', { refresh: true });
    }
    else {
      this.notification.ShowError(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('checklist-item-operation', { refresh: false });
  }
}
