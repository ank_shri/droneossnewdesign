import { bindable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import { inject, NewInstance } from 'aurelia-dependency-injection';
import { ValidationController, ValidationRules, validateTrigger } from 'aurelia-validation';
import { BootstrapFormValidationRenderer } from "lib/bootstrap-form-validation-renderer.js";
import { ChecklistService } from 'checklist/checklistService';

@inject(EventAggregator, AnraNotification, NewInstance.of(ValidationController), ChecklistService)
export class AddCheckListItem {
  @bindable checklistitems;
  controller = null;
  rules = null;

  constructor(events, notification, controller, checklistService) {
    this.events = events;
    this.notification = notification;
    this.checklistService = checklistService;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
    this.checklistitem = undefined;
    this.checklistdetails = [];
    this.treeviewItems = [];
    this.defaultData = undefined;
    this.showChecklist=true;
  }

  attached() {
    this.setupValidationRules();
    this.Initialize();
  }

  detached() {
    //Finally Reset Validation
    this.controller.reset();
  }

  Initialize() {
    if (this.checklistdetails.length > 0) {
      this.defaultData = JSON.stringify(this.treeviewItems);
      this.initTreeview();
    }
  }

  bind() {
    this.checklistitem = this.checklistitems.ChecklistItem;
    this.checklistdetails = this.checklistitems.ChecklistDetails;
    this.treeviewItems = this.checklistitems.TreeViewItems;
    this.heading = this.checklistitem.Title != null ? 'Edit Checklist Item' : 'Add Checklist Item';
  }

  setupValidationRules() {

    this.rules = ValidationRules
      .ensure('Title').required()
      .rules;
  }

  saveItem() {

    this.isWaiting = true;
    this.controller.validate({ object: this.checklistitem, rules: this.rules })
      .then(valresult => {
        if (valresult.valid) {
          this.checklistService.saveCheckListItem(this.checklistitem)
            .then(result => {
              this.handleSaveResult(result);
              if (this.checklistitem.Id > 0) {
                this.notification.ShowInfo('Updated Successfully.');
              }
              else {
                this.notification.ShowInfo('Added Successfully.');
              }
              this.isWaiting = false;
            });
        }
        else {
          // validation failed
          this.notification.ShowError('Please enter a valid input.');
          this.isWaiting = false;
        }
      }).catch((e) => {
        this.notification.ShowError('Error.');
        this.isWaiting = false;
      });
  }

  handleSaveResult(result) {
    if (result.IsSuccess) {
      this.getCheckListItems(this.checklistitem.OrganizationChecklistId);
    }
    else {
      this.notification.ShowError(result.ErrorMessage);
    }
  }

  cancel() {
    $('#treeChecklist').treeview('remove');
    this.events.publish('checklist-item-operation', { refresh: false });
  }

  getCheckListItems(id) {

    this.isWaiting = true;    
    this.checklistService.getNewChecklistItem(id)
      .then(result => {
        
        this.resetForm();
        this.checklistitems = result;
        this.checklistitem = this.checklistitems.ChecklistItem;                
        this.checklistdetails = this.checklistitems.ChecklistDetails;
        this.treeviewItems = this.checklistitems.TreeViewItems;        
        this.defaultData = JSON.stringify(this.treeviewItems);
        this.initTreeview();
        this.isWaiting = false;
      })
      .catch(e => this.notification.ShowError(e.message))
      .finally(() => {
        this.isWaiting = false;
      });
  }

  resetForm() {
    this.checklistitems = [];
    this.checklistitem = undefined;
    this.checklistdetails = [];
    this.treeviewItems = [];    
    this.defaultData = undefined;
  }

  initTreeview() {
    var treeChecklist = $('#treeChecklist').treeview({
      data: this.defaultData,
      showIcon: true,
      icon: "fa fa-bath",
      showCheckbox:  false,  
    });

    

    treeChecklist.treeview('collapseAll', { silent: true });
  }
}
