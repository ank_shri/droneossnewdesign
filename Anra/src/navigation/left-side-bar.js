import {Aurelia, bindable, inject, containerless} from "aurelia-framework";
import { AuthService } from "aurelia-authentication";
import { Router } from "aurelia-router";
import { EventAggregator } from 'aurelia-event-aggregator';

@containerless()
@inject(Aurelia, AuthService, Router, EventAggregator)

export class LeftSideBar{
    @bindable router = null;
  isDisplayed = false;

  constructor(aurelia, authService, router, events) {
    this.aurelia = aurelia;
    this.authService = authService;
    this.router = router;
    this.events = events;
    this.count = 0;
  }

  attached() {
    

  }
    
}