import {Aurelia, bindable, inject, containerless} from "aurelia-framework";
import { AuthService } from "aurelia-authentication";
import { Router } from "aurelia-router";
import { EventAggregator } from 'aurelia-event-aggregator';
import {AuthInterceptor, UIApplication} from "aurelia-ui-framework";
import {AlertService} from 'alerts/alertService';

@containerless()
@inject(Aurelia, AuthService, Router, EventAggregator,UIApplication,AlertService)

export class Navbar{
    @bindable router = null;
  isDisplayed = false;

  constructor(aurelia, authService, router, events,app, alertService) {
    this.aurelia = aurelia;
    this.authService = authService;
    this.router = router;
    this.events = events;
    this.alertService = alertService;
    this.count = 0;
    this.app = app;

  }

  attached() {
    this.getAlerts();
    console.log(">>>",this.router.navigation);
  }

  onLogout() {
    this.authService.logout();
    this.app.logout();
  }

  redirect(route){
    console.log("route",route);
    if(route == "#/network"){
        this.events.publish("network");
    }

    this.router.navigate(route);
  }

  getAlerts() {
    this.alertService.getAlerts()
      .then(result => {
        this.alertAggregate = result;
      });
  }
    
}