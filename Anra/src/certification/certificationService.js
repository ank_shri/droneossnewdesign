import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';
@inject(CustomHttpClient, AureliaConfiguration)
export class CertificationService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }
  saveCertificate(certificate) {
    return this.http.fetch(this.baseApi + 'training/', {
      method: 'post',
      body: json(certificate)
    }).then(response => response.json());
  }
  getUser(id) {
    return this.http.fetch(this.baseApi + 'users/' + id)
      .then(response => response.json());
  }
  getCertificates() {
    return this.http.fetch(this.baseApi + 'training/listing/')
    .then(response => response.json());
}
getNewCertificate()
{
  return this.http.fetch(this.baseApi + 'training/new')
  .then(response => response.json());

}
getCertificate(id)
{
  this.http
  return this.http.fetch(this.baseApi + 'training/'+id)
    .then(response => response.json());
}
deleteCertificate(id) {
  return this.http.fetch(this.baseApi + 'training/' + id, {
      method: 'delete'
    })
    .then(response => {
        response.json();
      }
    );
}
getOrganizations() {
  return this.http.fetch(this.baseApi + 'Organization/listing')
  .then(response => response.json());
}

  getUsers() {
    return this.http.fetch(this.baseApi + 'users/listing')
      .then(response => response.json());
  }

}
