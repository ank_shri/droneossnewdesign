import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Router} from 'aurelia-router';
import {CertificationService} from 'certification/certificationService'; 
import moment from 'moment';
@inject(EventAggregator, AnraNotification, NewInstance.of(ValidationController),Router,CertificationService)
export class Certification {
  @bindable training;
  @bindable save;
  rules=null;
  controller=null;

  constructor(events, notification, controller,router,certificationService) {
    this.events = events;
    this.router=router;
    this.certificationService=certificationService;
    this.notification = notification;
    this.controller = controller;
     this.allowedFiles = [".doc", ".docx", ".pdf", ".jpg", ".jpeg", ".png"];
    this.isWaiting = false;
    this.IsEditing=false;
    this.IsViewing=false;
    this.isAdmin=false;
    this.isAdministrator=false;
    this.isSuperAdmin=false;
    this.ShowCertificate=true;
    this.users=[];
    this.orgUsers=[];
    this.orgs=[];
    this.filteredUsers=[];
    this.mimetypeToext = {
      'image/jpeg': 'jpg',
      'image/png': 'png',
      'image/gif': 'gif',
      'application/pdf': 'pdf'
  };
    }

  attached(){
    
      this.setupValidationRules();
      if(this.training.TrainingId>0)
      {
         this.training.CourseDateCompleted = moment(this.training.CourseDateCompleted).format('YYYY-MM-DD');
         this.training.CourseDateExpiry = moment(this.training.CourseDateExpiry).format('YYYY-MM-DD');
      }

        this.Roletype=localStorage['Roles'];
        this.UserId=localStorage['UserId'];
        if(this.Roletype=='Super Admin' || this.Roletype=='Administrator' )
        {
          this.isAdmin=true;
           this.certificationService.getUser(this.UserId)
             .then(result => {
               this.OrgId = result.OrganizationId;
               localStorage['OrgId']=result.OrganizationId;
               this.getUsers();

             });
        }
        else{
          this.isAdmin=false;
        }

        if(this.Roletype=='Super Admin')
        {
          this.isSuperAdmin=true;
          this.getOrgs();
        }
        if(this.Roletype=='Administrator')
        {
          this.isAdministrator=true;
        }

  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }
  getUsers()
  {
    this.isWaiting=true;
     this.certificationService.getUsers()
       .then(result2 => {
         this.orgUsers = result2.Users;
         this.isWaiting=false;
       });
  }
  getOrgs()
  {
    this.certificationService.getOrganizations()
    .then(result => {
      this.orgs=result;
    });
  }

  setupValidationRules(){
    this.rules = ValidationRules
    .ensure('CourseName').required()
    .ensure('CourseProvider').required()
    .ensure('CourseDateCompleted').required()    
    .rules;
  }
  
  validateFileExtension() {

    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + this.allowedFiles.join('|') + ")$");
    var certFile = document.getElementById('certFile');

    var flagCertFile = true;

    if (certFile.files.length > 0 && certFile.files[0].name != null && !regex.test(certFile.files[0].name.toLowerCase())) {
      flagCertFile = false;
    }
    return flagCertFile;
  }

  loadUsers(id) {        
   
    this.filteredUsers = this.orgUsers.filter(function (entry) {
      return entry.OrganizationId == id;
  });
  
}
  saveItem()
  {

    this.controller.validate({ object: this.training, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                   if (this.validateFileExtension()) {
                     this.save()
                       .then(result => {
                         this.handleSaveResult(result); 
                         if (this.training.TrainingId!='') {
                           this.notification.error('Updated Successfully.');
                         } else {
                           this.notification.error('Added Successfully.');
                         }
                         this.isWaiting = false;
                          
                       })
                   }
                   else{
                        this.notification.error("Please upload files having extensions: " + this.allowedFiles.join(', ') + " only.");
                          
                   }
                 
              } 
              else {
                  // validation failed
                  this.notification.error('Please enter a valid input.');
              }
          });

          // $('#certFile input').replaceWith(input.val(''));
          $('#certFile').val(null);
  }

  fileSelected(event) {
    let reader = new FileReader();
    let file = event.target.files[0];
    
    if(event.target.id=='certFile')
    {
        reader.readAsDataURL(file);
        let fileName = file.name;
        reader.onload = () => {
            this.training.CourseAttachment = reader.result.split(',')[1];
        };
    }

}

handleSaveResult(result) {
  if (result.IsSuccess) {
    this.events.publish('certification-item-operation', {refresh: true});
  }
  else {
    this.notification.error(result.ErrorMessage);
  }
}

cancel() {
  this.events.publish('certification-item-operation', {refresh: false});
  this.orgUsers=[];
}

getContentType(data) {
  if (data.charAt(0) == '/') {
    return "image/jpeg";
  }
  else if (data.charAt(0) == 'R') {
    return "image/gif";
  }
  else if (data.charAt(0) == 'i') {
    return "image/png";
  }
  else if (data.charAt(0) == 'J') {
    return "application/pdf";
  }
}

base64toBlob(b64Data, contentType, sliceSize) {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

makeFileName(filename,mimetype) {
    var newFileName = filename;

    if (this.mimetypeToext.hasOwnProperty(mimetype)) {
        newFileName = filename + '.' + this.mimetypeToext[mimetype];
    }

    return newFileName;
}

  downloadDocument(data, filename) {
    if (/Edge/.test(navigator.userAgent)) {
      var contentType = this.getContentType(data);
      var blob = this.base64toBlob(data, contentType);
      var fileName = this.makeFileName(filename, contentType);
      navigator.msSaveBlob(blob, fileName);
    }
    else {
      var contentType = this.getContentType(data);
      var blob = this.base64toBlob(data, contentType);

      //Code to download File Locally
      var fileName = this.makeFileName(filename, contentType);
      var url = window.URL.createObjectURL(blob);
      var link = document.createElement('a');
      link.setAttribute('href', url);
      link.setAttribute('download', fileName);
      link.click();
      window.URL.revokeObjectURL(url);
    }
  //navigator.msSaveBlob(blob, fileName);
}


  
}
