import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Router} from 'aurelia-router';
import {CertificationService} from 'certification/certificationService';
import moment from 'moment';
import {AnraNotification} from '../components/anra-notification';
// import { Prompt} from 'components/my-modal';
@inject(EventAggregator, NewInstance.of(ValidationController),Router,CertificationService,AnraNotification)
export class Certifications {
  filters = [
    {value: '', keys: ['Title','LegalIdentification','OrganizationName']},
];
  
  constructor(events, controller,router,certificationService,notification) {
    this.events = events;
    this.router=router;
    this.certificationService=certificationService;
    this.controller = controller;
    // this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting=false;
    this.isEditing=false;
    this.isViewing-false;
    this.certificateDetails=[];
    this.certificateUser=[];
    this.certification=null;
    this.training=null;
    this.trainings=[];
    this.UserId=null;
    this.pageSize = certificationService.defaultPagingSize;
    this.notification=notification;
    // this.certificateRoute = router.routes[38].navModel;
    this.RoleType=null;
    this.OrgId=null;
    this.heading="Manage Certificates";
}
attached() {
  this.subscribeEvents();
  this.getList();
   this.UserId = localStorage['UserId'];
   this.RoleType=localStorage['Roles'];
  
 
}
getList()
{
   this.isWaiting=true; 
   this.certificationService.getCertificates()
     .then(result2 => {
       if(this.RoleType=='Super Admin' || this.RoleType=='Administrator')
       {
         this.certificateDetails = result2;
       }
       else
       {
       this.certificateUser = result2;
        for (var i = 0; i < this.certificateUser.length; i++) {
          var obj = this.certificateUser[i];
          if (obj.UserId == this.UserId) {
            this.certificateDetails.push(obj);
          }
        }
        
       }
      
       this.isWaiting=false;
     }).catch((e) => {
      this.notification.error(e.message);
      this.isWaiting = false;
  });
  
}

detached() {
  this.subscriber.dispose();
}
subscribeEvents() {
  this.subscriber = this.events.subscribe('certification-item-operation', payload => {

    if (payload) {
      if (payload.refresh) {
      this.getList();
      }
      this.reset();
    }
  });

  this.subscriber = this.events.subscribe('certification-item-operation-edit', payload => {

      if (payload) {
          if (payload.id > 0) {
              this.editItem(payload.id);
          }
          this.reset();
      }
  });

}
add() {
    this.isWaiting=true;
    this.certificationService.getNewCertificate()
    .then(result =>{
    this.training=result;
    this.training.CourseDateCompleted=moment().format();
    this.training.CourseDateExpiry=moment().format();
    this.isWaiting=false;
    this.isEditing=true;
    });
  }

  saveItem(item) {
    if(this.training.UserId==null || this.training.UserId=='')
    {
      
      this.training.UserId=this.UserId;
      this.training.OrganizationId=localStorage['OrgId'];
      this.certificateDetails=[];
       return this.certificationService.saveCertificate(this.training);
    }
    else{
        this.certificateDetails=[];
       return this.certificationService.saveCertificate(this.training);

    }
}
 reset() {
    this.training = null;
    this.isEditing = false;
    this.isViewing = false;
  }
 editItem(id) {
      this.isWaiting=true;
      this.certificationService.getCertificate(id)
     .then(result => {
     
       this.training = result;
       this.isWaiting=false; 
       this.isEditing = true;
       
     });
 }
  viewItem(id) {
        this.isWaiting=true;
        this.certificationService.getCertificate(id)
      .then(result => {
        this.training = result;
        this.isWaiting=false;
        this.isViewing = true;
        
      });
  }
   deleteItem(id) {

    this.isWaiting=true;
      this.notification.Confirm(
        "Are you sure you want to delete?"
      ).then(result => {
        if (result) {
          this.batteryService.deleteBattery(id)
          .then(res => {
            this.notification.error('Deleted Successfully.');
            this.getList();
            this.certificateDetails=[];
            this.isWaiting=false;
            });
        }
        else{
          this.isWaiting=false;
        }
      });
   }
  
}
