import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Router} from 'aurelia-router';
@inject(EventAggregator, AnraNotification, NewInstance.of(ValidationController),Router)
export class CertificationView {
  @bindable training;
  @bindable save;
  constructor(events, notification, controller,router) {
    this.events = events;
    this.router=router;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;
    this.isWaiting = false;
    this.isViewing=false;
    this.mimetypeToext = {
      'image/jpeg': 'jpg',
      'image/png': 'png',
      'image/gif': 'gif',
      'application/pdf': 'pdf'
  };    
}
getContentType(data) {
  if (data.charAt(0) == '/') {
    return "image/jpeg";
  }
  else if (data.charAt(0) == 'R') {
    return "image/gif";
  }
  else if (data.charAt(0) == 'i') {
    return "image/png";
  }
  else if (data.charAt(0) == 'J') {
    return "application/pdf";
  }
}

base64toBlob(b64Data, contentType, sliceSize) {
  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

makeFileName(filename,mimetype) {
    var newFileName = filename;

    if (this.mimetypeToext.hasOwnProperty(mimetype)) {
        newFileName = filename + '.' + this.mimetypeToext[mimetype];
    }

    return newFileName;
}

downloadDocument(data,filename) {
      if (/Edge/.test(navigator.userAgent)) {
        var contentType = this.getContentType(data);
        var blob = this.base64toBlob(data, contentType);
        var fileName = this.makeFileName(filename, contentType);
        navigator.msSaveBlob(blob, fileName);
      }
      else {
        var contentType = this.getContentType(data);
        var blob = this.base64toBlob(data, contentType);

        //Code to download File Locally
        var fileName = this.makeFileName(filename, contentType);
        var url = window.URL.createObjectURL(blob);
        var link = document.createElement('a');
        link.setAttribute('href', url);
        link.setAttribute('download', fileName);
        link.click();
        window.URL.revokeObjectURL(url);
      }
}


cancel() {
  this.events.publish('certification-item-operation', {
    refresh: false
  });
}

  
}
