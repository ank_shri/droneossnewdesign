export class LookupHelper {
  static GetChecklistType() {
    let types = {
      Category: 1,
      SubCategory: 2,
      Question: 3
    };

    return Object.keys(types).map(key => ({
      Id: types[key],
      Name: key
    }));
  }

  static FlightTypeLookup() {
    let types = {
      'Commercial-Agriculture': 1,
      'Commercial-Inspection': 2,
      'Commercial-Mapping/Survey': 3,
      'Commerical-Other': 4,
      'Commercial-Photo/Video': 5,
      'Emergency': 6,
      'Hobby-Entertainment': 7,
      'Maintenance': 8,
      'Science': 9,
      'Simulator': 10,
      'Test-Flight': 11,
      'Training-Flight': 12,
      'Other': 100
    };

    return Object.keys(types).map(key => ({
      Id: types[key],
      Name: key
    }));
  }
}
