import { AuthInterceptor, UIApplication } from 'aurelia-ui-framework';
import { AuthenticateStep, AuthService } from 'aurelia-authentication';
import { autoinject, inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { AuthorizeStep } from "common/authorizeStep";


@inject(Router)
export default class {
  constructor(router) {
    this.router = router;
    
  }

  configure() {
   
    let appRouterConfig = config => {
      config.addPipelineStep('authorize', AuthenticateStep);
      config.addPipelineStep("authorize", AuthorizeStep);
      this.setRoutes();
      config.map(this.routesMapping);
    };

    this.router.configure(appRouterConfig);
    
}




  setRoutes() {
    this.routesMapping = [

      {
        name: "home",
        route: ["", "dashboard"],
        moduleId: "dashboard/welcome",
        title: "HOME",
        nav: true,
        auth: true,
        settings: {
          icon: "anra-ico_cockpit_dark",
          roles: ["Super Admin", "Administrator", "Law Enforcement"]
        }
      },
      {
        name: "login",
        route: "login",
        moduleId: "security/login",
        name: 'login',
        nav: false,
        auth:false,
        title: 'Login',
        settings: {
          icon: 'anra-ico_cockpit_dark'
        }
      },
      {
        name:"logout",route:"logout",moduleId:"security/logout",nav:false,auth:false
      },
      {
        route:['network'],
        moduleId:'dashboard/network', 
        nav:true,
        title: 'Network',
        auth:true,
        settings: {
            icon: 'anra-ico_info_dark'
        }
      },
      {route: 'drone-view/:id', moduleId: 'drone-view/flight', nav: false, auth: true},
      {
        route: 'alerts', moduleId: 'alerts/alerts', nav: false, auth: true, title: 'Alert'
      },
      {
        name: "missions",
        route: 'missions',
        moduleId: 'missions/layout',
        nav: true,
        title: 'FLIGHT PLAN',
        auth: true,
        href: '#/missions',
        settings: {
          icon: 'anra-ico_flightplan_dark',
          // subMenu: [
          //   {href: '#/missions', title: 'Missions', settings: {icon: 'anra-ico_plane_dark'}},
          //   {href: '#/locations', title: 'Locations', settings: {icon: 'anra-ico_location_dark'}},
          //   {href: '#/documents', title: 'Documents', settings: {icon: 'anra-ico_certificate'}}
          // ]
        }
      },
      {
        name: "documents",
        route: 'documents',
        moduleId: 'documents/documents',
        nav: false,
        title: 'DOCUMENTS',
        auth: true,
        href: '#/documents',
        settings: {
          icon: 'anra-ico_certificate',
          // subMenu: [
          //   {href: '#/missions', title: 'Missions', settings: {icon: 'anra-ico_plane_dark'}},
          //   {href: '#/locations', title: 'Locations', settings: {icon: 'anra-ico_location_dark'}},
          //   {href: '#/documents', title: 'Documents', settings: {icon: 'anra-ico_certificate'}}
          // ]
        }
      },
      {
        route: ['mission-details', 'mission-details/:id'],
        moduleId: 'missions/mission',
        nav: false,
        title: 'Mission Details',
        auth: true,
        settings: {icon: 'anra-ico_plane_dark'}
      },
      {route: 'efforts/:id', moduleId: 'efforts/efforts', nav: false, auth: true},
      {
          route: 'flight-data-listing',
          moduleId: 'flightdata/flight_layout',
          nav: true,
          title: 'FLIGHT DATA',
          auth: true,
          href: '#/flight-data-listing',
          settings: {
            icon: 'anra-ico_plane_dark'
            // subMenu: [
            //   {href: '#/flight-data-listing', title: 'Flight Log', settings: {icon: 'anra-ico_plane_dark'}},
            //   {href: '#/incidents', title: 'Incidents', settings: {icon: 'anra-ico_location_dark'}},
            //   {href: '#/importdata', title: 'Import Data', settings: {icon: 'anra-ico_system_dark'}},
            //   {href: '#/media', title: 'Media Management', settings: {icon: 'anra-ico_system_dark'}}
            // ]
          }
        },

        {
          route: ['flight-data-listing/dashboard/:flightdataid', 'flight-data-listing/flightdata/:flightid', 'flight-data-listing/profile/:Profileflightdataid'],
          moduleId: 'flightdata/flight-data-view',
          nav: false
        },
        {route: 'drone-view/:id', moduleId: 'drone-view/flight', nav: false, auth: true},

        {
          route: ['maintenance'],
          moduleId: 'maintenance/maintenance',
          nav: true,
          title: 'EQUIPMENT',
          auth: true,
          href: '#/drones',
          settings: {
            icon: 'anra-ico_maintenance_dark',
            // subMenu: [
            //   {href: '#/drones', title: 'Drones', settings: {icon: 'anra-ico_drone'}},
            //   {href: '#/batteries', title: 'Batteries', settings: {icon: 'anra-ico_batterycharge_dark'}},
            //   {href: '#/equipments', title: 'Equipments', settings: {icon: 'anra-ico_camera_dark'}},
            //   {href: '#/maintenance', title: 'Maintenance', settings: {icon: 'anra-ico_maintenance_dark'}}
            // ]
          }
        },
        {route: 'drones', moduleId: 'drones/drone-layout', nav: false, auth: true},
        {route: 'batteries', moduleId: 'batteries/batteries', nav: false, auth: true},
        {route: 'equipments', moduleId: 'equipments/listing', nav: false, auth: true},
        {route: 'maintenance', moduleId: 'maintenance/listing', nav: false, auth: true},
        { route: 'cycles/:id', moduleId: 'cycles/cycles', nav: false, auth: true },
        { route: 'sensors', moduleId: 'sensors/listing', nav: false, auth: true },


        {
          route: ['reports'],
          moduleId: 'reports/report_layout',
          nav: true,
          title: 'REPORTING',
          auth: true,
          settings: {icon:'anra-ico_reporting_dark'}
        },

        {
          route:'certifications',
          moduleId:'certification/certifications',
          nav:true,
          auth:true,
          title:'TRAINING',
          settings:{icon:'anra-ico_certificate rm_id'}
        },
    
        {
          route: ['Checklist'],
          moduleId: 'checklist/organizationchecklist',
          nav: true,
          title: 'CHECKLIST',
          auth: true,
          settings: { icon: 'anra-ico_checklist' }
        },
        {route: 'checklist/:id', moduleId: 'checklist/checklist', nav: false, auth: true},
        {route: 'waypoints/mission/:id', moduleId: 'waypoints/waypoints', nav: false, auth: true},
        {
          route: ['clients'],
          moduleId: 'clients/client_layout',
          nav: true,
          title: 'ADMINISTRATION',
          auth: true,
          href: '#/clients',
          settings: {
            icon: 'anra-ico_adminstration_dark rm_id'
          }
        },
        {
          route: ['documents/document/:docid', 'documents/mission/:missionid'],
          moduleId: 'documents/document',
          nav: false,
          title: 'Document Details',
          auth: true,
          settings: {icon: 'anra-ico_plane_dark'}
        },
    ];
  }
}
