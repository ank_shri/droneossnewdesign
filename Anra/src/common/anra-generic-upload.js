import 'jquery';
import 'bootstrap-fileinput';
import {BindingEngine, bindable, customElement, inject, transient} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';
import qq from 'fine-uploader/lib/s3';
import ZipLoader from 'zip-loader';

@transient()
@customElement('anra-generic-upload')
@inject(EventAggregator, AureliaConfiguration, Notification)
export class AnraGenericUpload {

  @bindable referenceId;
  @bindable entityId;
  @bindable entityName;
  @bindable fileType;
  @bindable allowedExtensions;
  @bindable uploadPath;
  @bindable uploadComment;  
  @bindable uploadLimit;
  @bindable userId;
  constructor(events, config, notification) {
    this.events = events;
    this.config = config;
    this.notification = notification;
    this.baseApi = this.config.get('gatewayBaseUrl');
  }

  attached() {
        var scope = this;
        this.uploader = new qq.s3.FineUploader({
            debug: false,
            element: document.getElementById('file'),
            request: {
                endpoint: this.config.get('s3BucketUrl'),
                accessKey: this.config.get('s3AccessKey'),
                clockDrift: 500
            },
            signature: {
                endpoint: this.baseApi + '/s3/signature',
                version: 4
            },
            objectProperties: {
                region: this.config.get('s3Region'),
                acl: 'public-read',
                bucket: this.config.get('s3Bucket'),
                key: (id) => {
                    let obj = {
                        referenceId: this.referenceId,
                        entityId: this.entityId,
                        entityName: this.entityName,
                        comment: this.uploadComment,
                        fileType: this.fileType,
                        userId: this.userId,
                        location: this.config.get('s3BucketUrl'),
                        key: `${this.uploadPath}/${this.uploader.getName(id)}`,
                        files: []
                       
                    };
                    if (this.allowedExtensions.indexOf('zip') != -1) {
                        let file = this.uploader.getFile(id);
                        ZipLoader.unzip(file)
                            .then((item) => {
                                obj.files = Object.keys(item.files);
                            }).then(() => {
                                this.uploader.setUploadSuccessParams(obj, id);
                            });
                    }

                    this.uploader.setUploadSuccessParams(obj, id);

                    return `${this.uploadPath}/${this.uploader.getName(id)}`;
                }
            },
            validation: {
                itemLimit: this.uploadLimit,
                acceptFiles: this.allowedExtensions,
                allowedExtensions: this.allowedExtensions.replace(/\./g, '').split(',')
            },
            uploadSuccess: {
                method: 'PUT',
                endpoint: `${this.baseApi}/s3/generic/success`
            },
            callbacks: {
                onTotalProgress: (totalUploadedBytes, totalBytes) => {
                    let progressPercent = (totalUploadedBytes / totalBytes).toFixed(2);
                    if (isNaN(progressPercent)) {
                        $('#progress-text').text('');
                    } else {
                        $('#progress-text').text((progressPercent * 100).toFixed() + '%');
                    }
                },
                onError: function(id, name, errorReason, xhrOrXdr) {
                    console.error('Upload failed with reason: ', errorReason);
                },
                onComplete: function(id,name,responseJSON,xhr){
                  if(responseJSON.success){
                      scope.notification.info('File(s) Uploaded Successfully.');
                      scope.uploader.reset();
                  }
                  else{
                    scope.notification.error('Upload Failed.'); 
                  }                 
                },                
                onAllComplete: function(succeeded, failed){
                  console.log('All Complete:',succeeded);
                },
                onUpload: function(id, name){
                    console.log('on Upload');
                    var currentFileName = scope.uploader.getName(id);
                    var changedFileName = scope.entityName.concat(currentFileName.substr(currentFileName.indexOf("."),currentFileName.length));
                    scope.uploader.setParams({title: changedFileName}, id);
                    scope.uploader.setName(id, changedFileName);                                      
                },
                onValidate: function(file) {
                    if(scope.entityName && scope.entityName.trim().length > 0){
                      return true;                                   
                    }
                    else{
                      scope.notification.error('Please enter title.'); 
                      return false;
                    }
                }
            }
        });
    }

    setUploadParameters(file) {
            let obj = {
                parentId: this.parentId,
                entityId: this.entityId,
                fileType: this.fileType,
                location: this.config.get('s3BucketUrl'),
                files: []
            };

            if (file) {
                this.setCompressedFilesList(file, obj);
            }
            return obj;
    }

    setCompressedFilesList(file, obj) {

        ZipLoader.unzip(file)
            .then((item) => {
                obj.files = Object.keys(item.files);
            });
    }

  detached() {
    $('#fine-uploader-gallery').fileinput('destroy');
  }

}
