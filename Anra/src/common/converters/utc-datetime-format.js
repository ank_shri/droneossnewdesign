﻿import moment from 'moment';

export class UtcDateTimeFormatValueConverter {
    toView(value) {
        return moment.utc(value).utcOffset(moment().utcOffset()).format("L LT");
    }
}
