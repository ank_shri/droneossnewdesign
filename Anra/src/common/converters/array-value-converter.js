export class ArrayValueConverter {
  toView(value) {
    if (value && Array.isArray(value) && value.length > 0) {
      return value.join(',');
    }
    else {
      return null;
    }
  }

  fromView(value, obj) {
    if (obj) {
      obj = [];
      let items = value.split(',');
      obj = items;      
    }
    return obj;
  }
}
