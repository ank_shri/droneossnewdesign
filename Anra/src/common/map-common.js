import 'SpatialServer/Leaflet.MapboxVectorTile';
import * as L from "leaflet";
import 'leaflet-rotatedmarker';
import moment from 'moment';
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import { EventAggregator } from "aurelia-event-aggregator";
import { BindingEngine, inject, transient } from "aurelia-framework";
import { AureliaConfiguration } from "aurelia-configuration";
import { NotamService } from 'notams/notamService';
import { AnraMqttClient } from "components/mqttClient";

@inject(EventAggregator, AureliaConfiguration, AnraMqttClient, NotamService)
@transient()
export class MapCommon {

  MapLayerStreet = 'https://api.mapbox.com/styles/v1/aganjoo/cjm2gm64k98id2smqdpd30pan/tiles/256/{z}/{x}/{y}?access_token=';
  MapLayerStreetLight = 'https://api.mapbox.com/styles/v1/aganjoo/cjm251pve2gtb2smwezyqo4qn/tiles/256/{z}/{x}/{y}?access_token=';
  MapLayerStreetDark = 'https://api.mapbox.com/styles/v1/aganjoo/cjm2gw5p406qa2snwya18clid/tiles/256/{z}/{x}/{y}?access_token=';
  MapLayerSatellite = 'https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=';
  MapLayerSectional = 'https://wms.chartbundle.com/tms/v1.0/sec/{z}/{x}/{y}.png?type=google';

  showNotams = false;
  layerControl = null;

  popupOptions = {
    'minWidth': '300',
    'className': 'leaflet-popup-style'
  };

  constructor(events, config, anraMqttClient, notamService) {
    this.events = events;
    this.config = config;
    this.mqttClient = anraMqttClient;
    this.notamService = notamService;
    this.provider = new OpenStreetMapProvider();
  }

  getMap(containerId, layer = 'Street', zoomLevel = 12, lat, lng) {

    let accessToken = this.config.get("mapboxAccessToken");
    L.Icon.Default.imagePath = '/images';

    let baseMaps = {
      "Satellite": L.tileLayer(this.MapLayerSatellite + accessToken, { attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>' }),
      "Street": L.tileLayer(this.MapLayerStreet + accessToken, { attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>' }),
      "Street Light": L.tileLayer(this.MapLayerStreetLight + accessToken, { attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>' }),
      "Street Dark": L.tileLayer(this.MapLayerStreetDark + accessToken, { attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>' }),
      "VFR Sectional": L.tileLayer(this.MapLayerSectional, { attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>' })
    };

    let defaultLayer = baseMaps[layer];

    let map = L.map(containerId, {
      zoomControl: false,
      drawControl: false,
      editable: false,
      maxZoom: 30,
      attributionControl: false,
    }).setView([lat, lng], zoomLevel);

    L.control.zoom({ position: "topleft" }).addTo(map);
    this.layerControl = L.control.layers(baseMaps).addTo(map);
    defaultLayer.addTo(map);

    this.searchControl = new GeoSearchControl({
      provider: this.provider,
      autoClose: true
    });
    map.addControl(this.searchControl);
   


    map.doubleClickZoom.disable();

    this.map = map;

    return this.map;
  }

  addDrawControls(polgyonEnabled = true, markerEnabled = false) {
  
   
    this.drawControl = new L.Control.Draw({
      position: 'topleft',
      draw: {
        polygon: polgyonEnabled,
        polyline: false,
        rectangle: false,
        circle: false,
        marker: markerEnabled,
        circlemarker: false
      }
    });

    this.map.addControl(this.drawControl);
  }

  removeControls()
  {
    const parent = this;
    if (parent.drawControl) {
      parent.map.removeControl(parent.drawControl);
      this.drawControl="";
    }

  }

  

  displayNotams() {
    this.showNotams = true;

    this.notamService.getNotams()
      .then(result => {
        this.notams = [];
        result.map(obj => {
          if (obj.Status != "EXPIRED") {
            this.notams.push(obj);
          }
        });

        this.drawNotams();
        this.subscribeNotams();
      })
      .catch(e => {
        console.error(e);
      });
  }

  subscribeNotams() {
    this.mqttClient.subscribeTopic('notams/refresh');
    let watch1 = this.events.subscribe('notams/refresh', p => {
      if (p.packet) {
        this.notams = [];
        p.packet.map(obj => {
          if (obj.Status != "EXPIRED") {
            this.notams.push(obj);
          }
        });
        this.drawNotams();
      }
    });
  }

  drawNotams() {

    if (this.notams.length <= 0) {
      return;
    }
    let features = [];
    this.notams.map(x => {
      let notamGeo = x.AffectedArea;
      if (notamGeo.Region) {
        let feature = {
          type: 'Feature',
          geometry: {
            type: notamGeo.Region.type,
            coordinates: notamGeo.Region.coordinates
          },
          properties: {
            "Type": x.TypeName,
            "Number": x.NotamNumber,
            "Altitude": `Min: ${x.AffectedArea.MinAltitude} - Max:${x.AffectedArea.MaxAltitude} ft`,
            "Issue Date": moment.utc(x.IssueDate).utcOffset(moment().utcOffset()).format("L LT"),
            "Begin Date": moment.utc(x.BeginDate).utcOffset(moment().utcOffset()).format("L LT"),
            "End Date": moment.utc(x.EndDate).utcOffset(moment().utcOffset()).format("L LT"),
            "Authority": x.Authority,
            "Contact": x.PointOfContact,
            "Reason": x.NotamReason
          }
        };
        features.push(feature);

      }
      else if (notamGeo.Center) {

        let latlng = new L.latLng(notamGeo.Center.coordinates[1], notamGeo.Center.coordinates[0]);
        let radius = notamGeo.Radius;
        let circle = L.circle(latlng, radius, { color: "rgb(255,0,0)" });

        features.push(circle);
      }
    });

    if (this.notamLayer) {
      this.map.removeLayer(this.notamLayer);
    }

    if (features.length > 0) {
      let geoJsonData = {
        type: "FeatureCollection",
        crs: {
          type: "name",
          properties: { name: "urn:ogc:def:crs:OGC:1.3:CRS84" }
        },
        features: features
      };

      //Finally Draw All Coverage Areas on Map
      this.notamLayer = L.geoJson(geoJsonData, {
        style: (feature) => {
          return {
            weight: 2,
            opacity: 1,
            color: 'rgb(255,0,0)',
            dashArray: '4',
            fillOpacity: 0.2
          };
        },
        onEachFeature: (feature, layer) => {
          let content = `<div class="popup-data-title">NOTAM</div>`
          let properties = Object.entries(feature.properties);
          properties.forEach((item) => {
            content += `<div class="popup-data-item"><label>${item[0].replace('_', ' ')}</label>: ${item[1]}</div>`;
          })
          layer.bindPopup(content, this.popupOptions);
        }
      }).addTo(this.map);

      this.layerControl.addOverlay(this.notamLayer, 'Notam')
    }

  }
}
