export class Utility {

    static createGuid() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0,
          v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      });
    }
  
    static isValidGuid(guidvalue) {
      
      var regexGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/gi;
      return regexGuid.test(guidvalue);
    }
  
    static isEmptyGuid(guidvalue) {
        return guidvalue == '00000000-0000-0000-0000-000000000000';
    }
  //<<<<<<< HEAD
  
    static getContentType(filedata) {
  
        if (filedata.charAt(0) == '/') {
            return "image/jpeg";
        }
        else if (filedata.charAt(0) == 'R') {
            return "image/gif";
        }
        else if (filedata.charAt(0) == 'i') {
            return "image/png";
        }
        else if (filedata.charAt(0) == 'J') {
            return "application/pdf";
        }
    }
  
    static base64toBlob(filedata) {
  
        var contentType = this.getContentType(filedata) || '';
  
        var sliceSize = 512;
  
        var byteCharacters = atob(filedata);
        var byteArrays = [];
  
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
  
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
  
            var byteArray = new Uint8Array(byteNumbers);
  
            byteArrays.push(byteArray);
        }
  
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
  
    static downloadFile(fileURL) {
      //fileURL = fileURL.replace(/^https:\/\//i, 'http://');
      var req = new XMLHttpRequest();
      req.open("GET", fileURL, true);
      req.setRequestHeader('Access-Control-Allow-Origin', '*');
      req.responseType = "blob";
      req.onload = function (event) {
        var blob = req.response;
        var fileName = fileURL.substring(fileURL.lastIndexOf('/') + 1);
        var contentType = req.getResponseHeader("content-type");
  
        if (window.navigator.msSaveOrOpenBlob) {
          // Internet Explorer
          window.navigator.msSaveBlob(new Blob([blob], { type: contentType }), fileName);
        } else {
          var link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          link.download = fileName;
          link.click();
        }
      };
      req.send();
    }
  
    static disableButton(button) {
        button.attr("disabled", "disabled");
    }
  
    static enableButton(button) {
        button.removeAttr("disabled");
    }
  //=======
    static getFileName(url) {
      return url.substring(url.lastIndexOf('/') + 1);
    }
    //Method to remove duplicates enteries from an array.
    static removeDuplicates(arr,param2) {
      let unique_array = []
      //  remove duplicates enteries from array containing all the string values
      if (typeof((arr[0])) == 'string') {
        for (let i = 0; i < arr.length; i++) {
          if (unique_array.indexOf(arr[i]) == -1) {
            unique_array.push(arr[i])
          }
        }
      }
      // to give dynamic propety name in obj[name] name is dynamic here
      else {
        var name
        if (param2) {
          name = param2
        }
        else {
          name='name'
        }
         //  remove duplicates enteries from array containing all the object values
        var obj = {}
        for (let i = 0; i < arr.length; i++) {
          if (!obj[arr[i][name]]) {
            unique_array.push(arr[i]);
            obj[arr[i][name]] = arr[i];
          }
        }
      }
    return unique_array
    }
    //static downloadFile(fileURL) {
    //  var req = new XMLHttpRequest();
    //  req.open("GET", fileURL, true);
    //  req.responseType = "blob";
  
    //  req.onload = function (event) {
    //    var blob = req.response;
    //    var fileName = fileURL.substring(fileURL.lastIndexOf('/') + 1);
    //    var contentType = req.getResponseHeader("content-type");
    //    if (window.navigator.msSaveOrOpenBlob) {
    //      // Internet Explorer
    //      window.navigator.msSaveBlob(new Blob([blob], { type: contentType }), fileName);
    //    } else {
    //      var link = document.createElement('a');
    //      link.href = window.URL.createObjectURL(blob);
    //      link.download = fileName;
    //      link.click();
    //    }
    //  };
      //req.send();
  //>>>>>>> ImageGallery
    //}
  }
  