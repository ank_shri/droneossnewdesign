import 'jquery';
import 'bootstrap-fileinput';

import { BindingEngine, bindable, customElement, inject, transient } from 'aurelia-framework';

import { AureliaConfiguration } from 'aurelia-configuration';
import { EventAggregator } from 'aurelia-event-aggregator';
import { S3Services } from 'workspace/s3-services';
import ZipLoader from 'zip-loader';
import qq from 'fine-uploader/lib/s3';
import { Notification } from 'aurelia-notification';

@transient()
@customElement('anra-file-upload')
@inject(EventAggregator, AureliaConfiguration, S3Services, Notification)
export class AnraFileUpload {

    @bindable entityId;
    @bindable entityName;
    @bindable fileType;
    @bindable allowedExtensions;
    @bindable uploadPath;

    constructor(bindingEngine, config, s3services,notification) {
        this.bindingEngine = bindingEngine;
        this.config = config;
        this.s3services = s3services;
        this.baseApi = this.config.get('gatewayBaseUrl');
        this.notification = notification;
    }

    attached() {
       var scope=this
        this.uploader = new qq.s3.FineUploader({
            debug: false,
            element: document.getElementById('file'),
            request: {
                endpoint: this.config.get('s3BucketUrl'),
                accessKey: this.config.get('s3AccessKey'),
                clockDrift: 500
            },
            signature: {
                endpoint: this.baseApi + '/s3/signature',
                version: 4
            },
            objectProperties: {
                region: this.config.get('s3Region'),
                acl: 'public-read',
                bucket: this.config.get('s3Bucket'),
                key: (id) => {
                    let obj = {
                        entityId: this.entityId,
                        fileType: this.fileType,
                        location: this.config.get('s3BucketUrl'),
                        key: `${this.uploadPath}/${this.uploader.getName(id)}`,
                        files: []
                    };
                    if (this.entityName == '3d' || this.entityName == '2d') {
                        let file = this.uploader.getFile(id);
                        ZipLoader.unzip(file)
                            .then((item) => {
                                obj.files = Object.keys(item.files);
                            }).then(() => {
                                this.uploader.setUploadSuccessParams(obj, id);
                            });
                    }

                    this.uploader.setUploadSuccessParams(obj, id);

                    return `${this.uploadPath}/${this.uploader.getName(id)}`;
                }
            },
            validation: {
                acceptFiles: this.allowedExtensions,
                allowedExtensions: this.allowedExtensions.replace(/\./g, '').split(',')
            },
            uploadSuccess: {
                method: 'PUT',
                endpoint: `${this.baseApi}/s3/success/${this.entityName}`
            },
            callbacks: {
                onTotalProgress: (totalUploadedBytes, totalBytes) => {
                    let progressPercent = (totalUploadedBytes / totalBytes).toFixed(2);
                    if (isNaN(progressPercent)) {
                        $('#progress-text').text('');
                    } else {
                        $('#progress-text').text((progressPercent * 100).toFixed() + '%');
                    }
                },
                onError: function(id, name, errorReason, xhrOrXdr) {
                    console.error('Upload failed with reason: ', errorReason);
              },
              onAllComplete: function (succeeded, failed) {
                if (succeeded.length>0) {
                  console.log('All Complete:', failed);
                  scope.notification.info('Media Uploaded Successfully!!');
                }
                else {
                  scope.notification.error('Upload Failed!!'); 
                }
              },
            }
        });
    }

    setUploadParameters(file) {
        let obj = {
            entityId: this.entityId,
            fileType: this.fileType,
            location: this.config.get('s3BucketUrl'),
            files: []
        };

        if (file) {
            this.setCompressedFilesList(file, obj);
        }
        return obj;
    }

    setCompressedFilesList(file, obj) {

        ZipLoader.unzip(file)
            .then((item) => {
                obj.files = Object.keys(item.files);
            });
    }

    detached() {
        $('#fine-uploader-gallery').fileinput('destroy');
    }

}
