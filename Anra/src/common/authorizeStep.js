import {NavigationInstruction, Next, Redirect, RouterConfiguration} from "aurelia-router";
import {AuthService} from "aurelia-authentication";
import {inject} from "aurelia-framework";

@inject(AuthService)
export class AuthorizeStep {
  constructor(authService) {
    this.authService = authService;
  }

  run(navigationInstruction, next) {
    let payload = this.authService.getTokenPayload();

    let requiredRoles = navigationInstruction
      .getAllInstructions()
      .map(i => i.config.settings.roles)[0];

    let isUserPermited = true;
    if (payload && payload["Roles"] && requiredRoles) {
      isUserPermited = requiredRoles
        ? requiredRoles.some(r => payload["Roles"].includes(r))
        : true;
    }

    if (isUserPermited) return next();
    return next.cancel();
  }
}
