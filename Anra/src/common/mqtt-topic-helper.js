export class MqttTopicHelper {

  static IsDetectionTopic(topic) {
    return new RegExp("detects").test(topic);
  }

  static IsTelemetryTopic(topic) {
    return new RegExp("telemetry").test(topic);
  }

  static IsOperationTopic(topic) {
    return new RegExp("operation").test(topic);
  }

  static IsNegotiationTopic(topic) {
    return new RegExp("negotiation").test(topic);
  }

  static IsCollisionWarningTopic(topic) {
    return new RegExp("collision").test(topic);
  }
}
