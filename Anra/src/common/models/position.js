import * as UUID from "uuidjs";

export class Position {
  air_speed_source = undefined;
  air_speed_track_kn = 0;
  altitude_gps_wgs84_ft = 0;
  altitude_num_gps_satellites = 0;
  enroute_positions_id = UUID.generate();
  gufi = undefined;
  hdop_gps = 0;
  location = null;
  time_measured = null;
  time_received = null;
  time_sent = null;
  track_ground_speed_kn = 0;
  track_magnetic_north_deg = 0;
  track_true_north_deg = 0;
  user_id = undefined;
  vdop_gps = 0;
  altitude_gps = null;

  Init(packet) {
    this.air_speed_source = packet.air_speed_source;
    this.air_speed_track_kn = packet.air_speed_track_kn;
    this.altitude_gps_wgs84_ft = packet.altitude_gps_wgs84_ft;
    this.altitude_gps = packet.altitude_gps;
    this.altitude_num_gps_satellites = packet.altitude_num_gps_satellites;
    this.enroute_positions_id = packet.enroute_positions_id;
    this.gufi = packet.gufi;
    this.hdop_gps = packet.hdop_gps;
    this.location = packet.location;
    this.time_measured = packet.time_measured;
    this.time_received = packet.time_received;
    this.time_sent = packet.time_sent;
    this.track_ground_speed_kn = packet.track_ground_speed_kn;
    this.track_magnetic_north_deg = packet.track_magnetic_north_deg;
    this.track_true_north_deg = packet.track_true_north_deg;
    this.vdop_gps = packet.vdop_gps;
  }
}
