import { AuthInterceptor, UIApplication } from 'aurelia-ui-framework';
import { AuthenticateStep, AuthService } from 'aurelia-authentication';
import { autoinject, inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { AuthorizeStep } from "common/authorizeStep";


@inject(Router)
export default class {
  constructor(router) {
    this.router = router;
  }

  configure() {
    let appRouterConfignew = config => {
      config.addPipelineStep('authorize', AuthenticateStep);
      config.addPipelineStep("authorize", AuthorizeStep);
      this.setRoutesNew();
      config.map(this.routesMappingNew);
    };

    this.router.configure(appRouterConfignew);
  }


  setRoutesNew() {
    this.routesMappingNew = [
      {
        name: "missions",
        route: 'missions',
        moduleId: 'missions/layout',
        nav: true,
        title: 'FLIGHT PLAN',
        auth: true,
        href: '#/missions',
        settings: {
          icon: 'anra-ico_flightplan_dark',
        }
      },
      {
          route: 'flight-data-listing',
          moduleId: 'flightdata/flight_layout',
          nav: true,
          title: 'FLIGHT DATA',
          auth: true,
          href: '#/flight-data-listing',
          settings: {
            icon: 'anra-ico_plane_dark'
          }
        },

        {
          route: ['maintenance'],
          moduleId: 'maintenance/maintenance',
          nav: true,
          title: 'EQUIPMENT',
          auth: true,
          href: '#/drones',
          settings: {
            icon: 'anra-ico_maintenance_dark',
          }
        },
        {route: 'drones', moduleId: 'drones/drone-layout', nav: false, auth: true},
        {route: 'batteries', moduleId: 'batteries/batteries', nav: false, auth: true},
        {route: 'equipments', moduleId: 'equipments/listing', nav: false, auth: true},
        {route: 'maintenance', moduleId: 'maintenance/listing', nav: false, auth: true},
        { route: 'cycles/:id', moduleId: 'cycles/cycles', nav: false, auth: true },
        { route: 'sensors', moduleId: 'sensors/listing', nav: false, auth: true },

        {
          route: ['reports'],
          moduleId: 'reports/report_layout',
          nav: true,
          title: 'REPORTING',
          href: '#/reports',
          auth: true,
          settings: {icon:'anra-ico_reporting_dark'}
        },

        {
          route:'certifications',
          moduleId:'certification/certifications',
          nav:true,
          auth:true,
          title:'TRAINING',
          href: '#/certifications',
          settings:{icon:'anra-ico_certificate rm_id'}

        },
    
        {
          route: ['Checklist'],
          moduleId: 'checklist/organizationchecklist',
          nav: true,
          title: 'CHECKLIST',
          auth: true,
          href:'#/Checklist',
          settings: { icon: 'anra-ico_checklist' }
        },

        {
          route: ['clients'],
          moduleId: 'clients/client_layout',
          nav: true,
          title: 'ADMINISTRATION',
          auth: true,
          href: '#/clients',
          settings: {
            icon: 'anra-ico_adminstration_dark rm_id'
          }
        },
    ];
  }

}
