import {AuthService} from "aurelia-authentication";
import {inject} from "aurelia-framework";

@inject(AuthService)
export class AuthorizationFilterValueConverter {

  constructor(authService) {
    this.authService = authService;
  }

  toView(routes) {
    let isAuthenticated = this.authService.isAuthenticated();
    let payload = this.authService.getTokenPayload();
   
    let res = [];
    for (let route of routes) {
      if (
        route.config.auth === isAuthenticated &&
        route.config.settings.roles.some(r => payload["Role"].includes(r))
      ) {
        res.push(route);
      }
    }
    return res;
  }
}
