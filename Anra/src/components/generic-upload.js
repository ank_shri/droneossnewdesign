import {NewInstance, inject} from "aurelia-dependency-injection";
import {AureliaConfiguration} from 'aurelia-configuration';
import {EventAggregator} from "aurelia-event-aggregator";
import {AnraNotification} from "../components/anra-notification";
import {Router} from 'aurelia-router';

@inject(
  EventAggregator,
  AnraNotification,
  AureliaConfiguration,
  Router
)
export class GenericUpload {

  title = "Upload";
  parentId=null;
  entityId= null;
  entityName=null;  
  fileType = null;
  userId = null;
  uploadPath = null;
  uploadComment = null;
  isUploading = false;
  allowedExtensions = null;
  uploadLimit = 1;


  constructor(events, notification, config, router) {
    this.events = events;    
    this.notification = notification;
    this.config = config;
    this.router = router;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.allowedExtensions = this.config.get('genericUploadExtensions');
    this.uploadLimit = this.config.get('genericUploadLimit');
  }

  activate() {

    this.events.subscribe('show-generic-upload', p => {

        this.uploadAsset(
          p.title, 
          p.referenceId, 
          p.entityId,           
          p.fileType, 
          p.uploadPath,
          p.userId
        );
    });
}

  uploadAsset(title, referenceId, entityId, fileType, uploadPath, userId) {
    this.isUploading = true;
    this.title = title;
    this.referenceId = referenceId;
    this.entityId = entityId;    
    this.fileType = fileType;
    this.uploadPath = uploadPath;
    this.userId = userId;
  }

  reset() {
      this.title = "Upload";
      this.parentId = null;
      this.entityId = null;
      this.entityName = null;
      this.fileType = null;
      this.uploadPath = null;
      this.uploadComment = null;      
  }

  close() {
      this.reset();
      this.isUploading = false;      
  }
}
