import {inject, bindable, transient} from "aurelia-framework";
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(Element, EventAggregator)
export class SideNavBar {
  @bindable expanded;
  @bindable callback;

  constructor(element, events) {
    this.element = element;
    this.isNavVisible = false;
    this.navWidth = '0px'
    this.margin = '0px';
    this.overflow = 'hidden';
    this.expanded = false;
    this.events = events;
  }
  attached() {
    this.events.subscribe('open', p => {
      this.toggleNav();
    });
    this.events.subscribe('close', p => {
      this.toggleNav();
    });
  }
  bind() {
    if (this.expanded) {
      this.isNavVisible = true;
      this.navWidth = '370px';
      this.margin = '370px';
      this.overflow = 'visible';
    }
  }

  toggleNav() {
    if (!this.expanded) {
      if (document.getElementById("mySidenav").style.overflow == 'hidden') {
        $("#mySidenav").attr("style", "overflow:visible;width:370px");
        this.margin = '370px';
      }
      else {
        $("#mySidenav").attr("style", "overflow:hidden;width:0px");
        this.margin = '0px';
      }
    }
  }
}
