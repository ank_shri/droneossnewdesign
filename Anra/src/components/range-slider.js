import {customAttribute, customElement, inject, bindable} from 'aurelia-framework';
import 'jquery';
import 'bootstrap-slider';

@customAttribute('rangeslider')
@inject(Element)
export class RangesliderCustomAttribute {
  @bindable min;
  @bindable max;

  constructor(element) {
    this.element = element;
  }

  attached() {
    $(this.element).slider({tooltip: 'show', selection: 'after'})
      .on('change', e => {
        this.fireEvent(e.target, 'input');
      });
  }

  detached() {
    $(this.element).slider('destroy').off('change');
  }

  createEvent(name) {
    var event = document.createEvent('Event');
    event.initEvent(name, true, true);
    return event;
  }

  fireEvent(element, name) {
    var event = this.createEvent(name);
    element.dispatchEvent(event);
  }
}
