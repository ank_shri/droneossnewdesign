import { UIUtils, UIApplication } from "aurelia-ui-framework";
import { inject } from "aurelia-framework";
import * as toastr from "toastr";
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(UIApplication, EventAggregator)
export class AnraNotification {

  constructor(app, events) {
    this.app = app;
    this.events = events;
    this.audio = new Audio('audio/alert.mp3');
    this.audioCollision = new Audio('audio/danger_warning.mp3');
    this.audioInfo = new Audio('audio/alert.mp3');
    this.audioError = new Audio('audio/error_text_message.mp3');
    this.audioWarning = new Audio('audio/warning_half-life.mp3');
    this.status = true;
  }

  ShowInfo(msg, timeout = 5000) {
    if (this.status) {
      this.audioInfo.muted = false;
      this.audioInfo.play();
    }
    this.app.toast({ theme: "info", message: msg, timeout: timeout });
  }

  ShowWarning(msg, timeout = 5000) {
    if (this.status) {
      this.audioWarning.muted = false;
      this.audioWarning.play();
    }
    this.app.toast({ theme: "warning", message: msg, timeout: timeout });
  }

  ShowError(msg, timeout = 5000) {
    if (this.status) {
      this.audioError.muted = false;
      this.audioError.play();
    }
    this.app.toast({ theme: "danger", message: msg, timeout: timeout });
  }

  Confirm(msg) {
    return this.app.confirm(msg);
  }

  Alert(msg) {
    if (this.status) {
      this.audio.muted = false;
      this.audio.play();
    }
    this.app.alert(msg);
  }

  ShowTooltip(msg) {
    return Object.assign('bottom', {
      value: msg,
      theme: 'dark'
    });
  }

  ShowInfoEx(msg) {
    let options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-top-center",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    toastr.info(msg, '', options);
  }


  ShowWarningEx(msg, loc = 'top', duration = 300) {
    let options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": `toast-${loc}-center`,
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": duration,
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    toastr.warning(msg, '', options);
  }


  ShowCollisionWarning(msg) {
    if (this.status) {
      this.audioCollision.muted = false;
      this.audioCollision.play();
    }

    let options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-top-center",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    toastr.warning(msg, 'Warning', options);
  }


  ShowCriticalMessage(msg) {
    if (this.status) {
      this.audioCollision.muted = false;
      this.audioCollision.play();
    }
    this.audio.play();

    let options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-top-center",
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };

    toastr.error(msg, 'Critical', options);
  }
  soundStatus(sound) {
    this.status = sound;
    if (!sound) {
      this.audioWarning.muted = true;
      this.audioError.muted = true;
      this.audioInfo.muted = true;
      this.audio.muted = true;
      this.audioCollision.muted = true;
    }
  }
}
