import { customElement, transient, bindable, inject, BindingEngine } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { AureliaConfiguration } from 'aurelia-configuration';
import 'jquery';
import 'bootstrap-fileinput';

@transient()
@customElement('anra-single-upload')
@inject(EventAggregator, AureliaConfiguration)
export class AnraFileUpload {

  @bindable uploadUrl;
  @bindable fileExtensions;

  constructor(bindingEngine, config) {
    this.bindingEngine = bindingEngine;
    this.config = config;
    this.baseApi = this.config.get('gatewayBaseUrl');
  }

  attached() {
    
    $('#file').fileinput({
      uploadUrl: this.uploadUrl,
      allowedFileExtensions: this.fileExtensions,
      uploadAsync: true,
      minFileCount: 1,
      maxFileCount: 2,
      overwriteInitial: true,
      showUploadedThumbs: false
    });
  }

  detached() {
    $('#file').fileinput('destroy');
  }

}
