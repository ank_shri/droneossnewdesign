import {inject, bindable, transient} from "aurelia-framework";
import {EventAggregator} from 'aurelia-event-aggregator';
import { AureliaConfiguration } from "aurelia-configuration";
//import * as LGeo from 'leaflet-geodesy';
//import L from 'mapbox.js';

import * as L from 'leaflet';
import * as D from 'leaflet-draw';

@transient()
@inject(EventAggregator, AureliaConfiguration)
export class Leaflet {
  @bindable lng;
  @bindable lat;
  @bindable zoomLevel;
  @bindable canDraw;
  @bindable doubleClick;

  constructor(events, aureliaConfiguration) {
    this.lng = '-77.49129295349121';
    this.lat = '38.92035442107111';
    this.zoomLevel = 16;
    this.canDraw = false;
    this.events = events;
    this.waypointLayers = [];
    this.guideMarker = null;
    this.aureliaConfiguration = aureliaConfiguration;
  }

  bind(bindingContext) {
    this.parent = bindingContext;
  }

  attached() {
    let accessToken = this.configuration.get('mapboxAccessToken');


    var mapboxTiles = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
      attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    let map = L.map('mapid', {zoomControl: false, drawControl: this.canDraw})
      .addLayer(mapboxTiles)
      .setView([this.lat, this.lng], this.zoomLevel);

    L.control.zoom({
      position: 'bottomleft'
    }).addTo(map);

    map.doubleClickZoom.disable();

    this.map = map;
    L.Icon.Default.imagePath = 'styles/images';

    //let urlTemplate = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
    //map.addLayer(L.tileLayer(urlTemplate, {minZoom: 4}));
    if (this.canDraw) {
      this.attachDrawControls(map);
    }
    this.subscribeEvents();

    this.events.publish('leaflet:ready', {isReady: true});

    var self = this;
    this.map.on('dblclick', function (e) {
      if(self.guideMarker == null){
        self.guideMarker= new L.Marker(e.latlng).addTo(self.map);
      }else {
        self.guideMarker.setLatLng(e.latlng);
      }
      self.events.publish('leaflet-doubleclick', { lat: e.latlng.lat, lng: e.latlng.lng });
    });

  }


  attachedEx() {
    /* L.mapbox.accessToken = 'pk.eyJ1IjoiZ3VydmluZGVyMTE4IiwiYSI6ImNpdDdxbHdibjA3dngyenAyajFlZ2h1ajEifQ.sRvDL7drq3L90aM1xvOv9A';
     let map = L.mapbox.map('mapid', 'mapbox.satellite', {zoomControl: false})
     .setView([this.lat, this.lng], this.zoomLevel);

     L.control.zoom({ position: 'bottomleft' }).addTo(map);

     this.map = map;
     L.Icon.Default.imagePath = 'styles/images';

     /!* let urlTemplate = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
     map.addLayer(L.tileLayer(urlTemplate, {minZoom: 4}));*!/

     this.attachDrawControls(map);

     this.subscribeEvents();

     this.events.publish('leaflet:ready', {isReady: true});*/

  }

  attachDrawControls(map) {

    if (this.canDraw) {
      // Initialise the FeatureGroup to store editable layers
      var drawnItems = new L.FeatureGroup();

      map.addLayer(drawnItems);

      // Initialise the draw control and pass it the FeatureGroup of editable layers
      /*var drawControl = new L.Control.Draw({
       edit: {
       featureGroup: drawnItems
       }
       });*/
      var drawControl = new L.Control.Draw({
        position: 'bottomleft',
        draw: {
          polygon: true,
          polyline: false,
          rectangle: false,
          circle: false,
          marker: true,
        }
      });

      map.on('draw:created', function (event) {
        var layer = event.layer;
        drawnItems.addLayer(layer);
        var json = drawnItems.toGeoJSON();
        //vm.onDrawMap(json.features[0].geometry);
      });

      map.addControl(drawControl);
    }
  }

  subscribeEvents() {
    this.events.subscribe('leaflet:add-layer', p => {
      this.map.addLayer(p.newlayer);
    });

    this.events.subscribe('leaflet:add-vehicle', p => {
      this.map.addLayer(p.vehicle);
    });


    this.events.subscribe('network:adjust-view', p => {
      /* if (p.markers) {
       let group = new L.featureGroup(p.markers);
       this.map.fitBounds(group.getBounds(), {'padding': [30, 30]});
       }*/
    });

    this.events.subscribe('leaflet:set-view', p => {
      this.map.setView([p.lat, p.lng]);
      this.updateMapPan(p.lat, p.lng);
      if (p.zoom) {
        this.map.setZoom(p.zoom);
      }
    });

    this.events.subscribe('drone-view:waypoints', p => {
      //this.drawWaypoints(p.waypoints);
    });
  }

  drawWaypoints(waypoints) {
    if (this.waypointLayers.length > 0) {
      this.waypointLayers.forEach(function (item) {
        this.map.removeLayer(item);
      }, this);
    }

    let polyline = L.polyline(waypoints).addTo(this.map);
    this.waypointLayers.push(polyline);

    for (let i = 0; i < waypoints.length; i++) {
      let marker = L.circleMarker(waypoints[i], {
        radius: 4,
        fillColor: "#ff7800",
        color: "#000",
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8,
        'z-index': 10
      }).addTo(this.map);

      this.waypointLayers.push(marker);
    }

    this.map.setView([waypoints[0].lat, waypoints[0].lng]);
    this.map.setZoom(16);

  }

  updateMapPan(lat, lng) {
    var offset = this.map._getNewTopLeftPoint([lat, lng]).subtract(this.map._getTopLeftPoint());

    this.map.fire('movestart');
    this.map._rawPanBy(offset);
    this.map.fire('move');
    this.map.fire('moveend');
  }


}
