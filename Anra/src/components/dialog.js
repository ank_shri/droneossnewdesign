import { customElement, bindable, inject } from 'aurelia-framework';
import 'jquery';
import 'bootstrap';

@customElement('anra-dialog')
export class Dialog {

  @bindable display;
  @bindable title;
  @bindable displayFooter;
  @bindable onClose;
  @bindable onLoad;

  attached() {
    $('#myModal').modal({ show: true })
    if (this.onLoad) { this.onLoad(); }
  }

  constructor() {
    this.display = false;
  }

  closeDialog() {
    $('#myModal').modal('hide');
    if (this.onClose) this.onClose();
    this.display = false;
  }
}
