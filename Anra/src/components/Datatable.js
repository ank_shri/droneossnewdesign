import {bindable, LogManager} from 'aurelia-framework';
import $ from 'jquery';
import {DataTable} from 'datatables.net'; // eslint-disable-line
import * as dtSelect from 'datatables-select'; // eslint-disable-line

const DT_SELECT_EVENT = 'select.dt';
const DT_DESELECT_EVENT = 'deselect.dt';
const DT_DRAW_EVENT = 'draw.dt';

export class Datatable {

  logger = LogManager.getLogger('DataTable');

  @bindable id;
  @bindable data;
  @bindable cols;
  @bindable select = 'single';
  @bindable selectCallback;
  @bindable deselectCallback;

  dataTable;

  bind(vm) {
    this.vm = vm;
  }

  attached() {
    this.dataTable = $(this.dt).DataTable({ // eslint-disable-line
        select: this.select,
        data: this.data,
        columns: this.cols,
        stateSave: true
      })
      .on(DT_SELECT_EVENT, (e, d, t, idx) => this.doSelect(idx))
      .on(DT_DESELECT_EVENT, () => this.doDeselect())
      .on(DT_DRAW_EVENT, () => this.handleRedraw());
  }

  handleRedraw() {
    this.dataTable.rows().deselect();
  }

  doDeselect() {
    this.deselectCallback.call(this.vm);
  }

  doSelect(idx) {
    this.selectCallback.call(this.vm, this.data[idx]);
  }

  dataChanged() {
    this.logger.debug('data change detected - updating table');
    this.dataTable && this.dataTable.clear().rows.add(this.data).draw();
  }

  detached() {
    this.dataTable.off(DT_SELECT_EVENT); //not sure if destroy does this anyway
    this.dataTable.off(DT_DESELECT_EVENT);
    this.dataTable.off(DT_DRAW_EVENT);
    this.dataTable.destroy();
  }
}
