import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class MaintenanceService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getMaintenances() {
      return this.http.fetch(this.baseApi + 'maintenance/listing')
      .then(response => response.json());
  }

  getOverdueMaintenances() {
      return this.http.fetch(this.baseApi + 'maintenance/overduelist')
        .then(response => response.json());
  }

  getNewMaintenance() {
      return this.http.fetch(this.baseApi + 'maintenance/new')
      .then(response => response.json());
  }

  getMaintenance(id) {
      return this.http.fetch(this.baseApi + 'maintenance/' + id)
      .then(response => response.json());
  }

  deleteMaintenance(id) {
    return this.http.fetch(this.baseApi + 'maintenance/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveMaintenance(Maintenance) {
    return this.http.fetch(this.baseApi + 'maintenance/', {
      method: 'post',
      body: json(Maintenance)
    }).then(response => response.json());
  }
}
