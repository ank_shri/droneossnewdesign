import {inject} from 'aurelia-framework';
import {MaintenanceService} from 'maintenance/maintenanceService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {PermissionService} from 'security/permissionService';
import {Notification} from 'aurelia-notification';
import {DialogService} from 'aurelia-dialog';
import {Prompt} from 'components/my-modal';
import { SrcAlphaFactor } from '../../styles/assets/js/threejs/three';

@inject(MaintenanceService, Router, EventAggregator, PermissionService, Notification, DialogService)
export class Listing {

    //Set Filters for Searchbox
    overduefilter = [
              {value: '', keys: ['Name','Status']},
    ];

    maintenancefilter = [
          {value: '', keys: ['Name','Category','Status','OrganizationName']},
    ];

    constructor(maintenanceService, router, events, permissionService, notification, dialogService) {
        this.maintenanceService = maintenanceService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;
        this.notification = notification;
        this.heading = 'Manage Maintenance';
        this.maintenances = [];
        this.maintenancetocsv=[];
        this.overduemaintenances =[];
        this.maintenanceAggregate = null;

        this.isEditing = false;
        this.isViewing = false;

        this.permission={};
        this.dialogService = dialogService;
        //Set Default Paging Size
        this.pageSize = maintenanceService.defaultPagingSize;
        this.isWaiting = false;

    }

    activate() {
        this.getList();
        this.getOverdueList();
        this.Drone_partially_completed = '/image/Maintanance_img/drone_partially_completed.png';
        this.Drone_new_task = '/image/Maintanance_img/drone_newtask.png';
        this.Drone_postpone = '/image/Maintanance_img/drone_postpone.png';

        this.Equipments_partially_completed = '/image/Maintanance_img/equipments_partially_completed.png';
        this.Equipments_new_task = '/image/Maintanance_img/equipments_newtask.png';
        this.Equipments_postpone = '/image/Maintanance_img/equipments_postpone.png';

        this.Sensor_partially_completed = '/image/Maintanance_img/sensor_partially_completed.png';
        this.Sensor_new_task = '/image/Maintanance_img/sensor_newtask.png';
        this.Sensor_postpone = '/image/Maintanance_img/sensor_postpone.png';
    } 
    
    

    attached() {
        this.subscribeEvents();
    }
  detached() {
    this.subscriber.dispose();
  }
    getPermission(moduleId) {
        this.permissionService.getPermission(moduleId)
          .then(result => {
              this.permission = result;
          });
    }

    subscribeEvents() {
      this.subscriber = this.events.subscribe('maintenance-item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                    this.getOverdueList();
                }
                this.reset();
            }
        });

      this.subscriber = this.events.subscribe('maintenance-item-operation-edit', payload => {

            if (payload) {
                if (payload.id > 0) {
                    this.editItem(payload.id);
                }
                this.reset();
            }
        });
    }


    getList() {
        this.isWaiting = true;
        this.maintenanceService.getMaintenances()
        .then(result => {
            this.maintenances = result;
            this.isEditing = false;
            this.getPermission(11);
            this.isWaiting = false;
            var newJson=[];
            this.maintenances.forEach(function(item) {
            newJson.push({
            "Name":item.Name,
            "Category":item.Category,
            "Maintenance Date":moment(item.MaintenanceDate).format('YYYY-MM-DD'),
            "Status":item.Status,
            "Organization":item.OrganizationName,
            "Created Date":moment(item.DateCreated).format('YYYY-MM-DD')
                        });
            console.log(result);
            });
            this.maintenancetocsv=newJson;
        }).catch((e) => {
            this.notification.error(e.message);
            this.isWaiting = false;
        });
        
    }

    getOverdueList() {
        this.isWaiting = true;
        this.maintenanceService.getOverdueMaintenances()
          .then(result => {
              this.overduemaintenances = result;
              for(var i=0; i< this.overduemaintenances.length; i++){
                  let obj = this.overduemaintenances[i];
                  if(obj.NextActionAfterDate== null){
                      let MaintainDate = obj.MaintenanceDate;
                      let nextactionDate = moment(MaintainDate).add(obj.NextActionAfterHours, 'hours').format('MM/DD/YYYY');
                      obj.NextActionAfterHours = nextactionDate;
                  }
              }
              this.isEditing = false;
              this.isWaiting = false;
              console.log(result.length);
          });
          
    }

    // getNewOverdueList() {
    //     this.isWaiting = true;
    //     this.maintenanceService.getOverdueMaintenances()
    //       .then(result => {
    //           this.overduemaintenances = result;
    //           for(var i=0; i<=3; i++){
    //               this.listOverdues.push(result[i])
    //           }
    //       });
          
    // }

   

    reset() {
        this.maintenanceAggregate = null;
        this.isEditing = false;
        this.isViewing = false;
    }

    add(id) {
        this.isWaiting = true;
        this.router.navigate('maintenance/' + id);
    }

    editItem(id) {

        this.router.navigate('maintenance/' + id);
    }

    viewItem(id) {
        this.isWaiting = true;
        this.maintenanceService.getMaintenance(id)
          .then(result => {
              this.maintenanceAggregate = result;
              this.maintenanceAggregate.Maintenance.MaintenanceDate = moment(result.Maintenance.MaintenanceDate).format('YYYY-MM-DD');
              this.maintenanceAggregate.Maintenance.NextActionAfterDate = moment(result.Maintenance.NextActionAfterDate).format('YYYY-MM-DD');
              this.isViewing = true;
              this.isWaiting= false;
          });
    }

    deleteItem(id) {
    this.dialogService.open({viewModel: Prompt, model: 'Are you sure you want to delete?'}).then(response => {
         if (!response.wasCancelled) {
        this.maintenanceService.deleteMaintenance (id)
          .then(result => {
				  this.notification.error('Deleted Successfully.');
              this.getList();
			  });
         }
          });
    }

    saveItem(item) {
        return this.maintenanceService.saveMaintenance(this.maintenanceAggregate.Maintenance);
    }

    convertArrayOfObjectsToCSV(args) {  
        var result, ctr, keys, columnDelimiter, lineDelimiter, data;
    
        data = args.data || null;
        if (data == null || !data.length) {
            this.notification.error("No data available to export.");          
            return null;
        }
    
        columnDelimiter = args.columnDelimiter || ',';
        lineDelimiter = args.lineDelimiter || '\n';
    
        keys = Object.keys(data[0]);
    
        var columnHeaders = ['Name','Category','Maintainence Date','Status','Organization','Date Created'];
        
        result = '';
        result += columnHeaders.join(columnDelimiter);
        result += lineDelimiter;
    
        data.forEach(function(item) {
            ctr = 0;
            keys.forEach(function(key) {
                if (ctr > 0) result += columnDelimiter;
    
                result += item[key];
                ctr++;
            });
            result += lineDelimiter;
        });
    
        return result;
      }
    
        downloadCSV(args) {  
        var data, filename, link;
    
        //Setting Entity Attributes
    
        var csv = this.convertArrayOfObjectsToCSV({data:this.maintenancetocsv});
        if (csv == null) return;
    
        filename = args.filename || 'MaintainenceReport.csv';
    
        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,' + csv;
        }
        data = encodeURI(csv);
    
        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
        link.click();
    
    
    
        }
}
