import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';
import { SensorService } from '../sensors/sensorService';

@inject(EventAggregator, Notification)
export class MaintenanceView {
  @bindable maintenance;
  @bindable drones;
  @bindable sensors;
  @bindable equipments;
  @bindable statusList;
  @bindable save;
  @bindable organizations;
    constructor(events, notification) {
        this.events = events;
        this.notification = notification;
        this.IsSelectedCategoryIsDrone=false;
        this.IsSelectedCategoryIsEquipment=false;
        this.IsSelectedCategoryIsSensors=false;
        this.nextActionStatus=false;
        this.afterXHoursStatus=false;
    }    

    bind() {         
        
        this.heading = this.maintenance.Name != null ? 'View Maintenance' : 'Add Maintenance';
        this.IsSelectedCategoryIsEquipment = this.maintenance.Category == 'Equipment' ? true : false;
        this.IsSelectedCategoryIsDrone = this.maintenance.Category == 'Drone' ? true : false;
        this.IsSelectedCategoryIsSensors = this.maintenance.Category =='Sensor' ? true : false;
        this.nextActionStatus = this.maintenance.DueStatus == 'Next Action' ? true : false;
        this.afterXHoursStatus = this.maintenance.DueStatus == 'After X Hours' ? true : false;
    }
    attached()
    {
    }
    
    cancel() {
      this.events.publish('maintenance-item-operation', {refresh: false});
    }

    edit(maintenanceId) {
      this.events.publish('maintenance-item-operation-edit', {id: maintenanceId});
    }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
