import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
import {Notification} from 'aurelia-notification';
import {MaintenanceService} from 'maintenance/maintenanceService';
import {Router} from 'aurelia-router';
import moment from 'moment';


@inject(EventAggregator, Notification, MaintenanceService, Router, NewInstance.of(ValidationController))
export class MaintenanceEditor {
    controller = null;
    rules = null;

    constructor(events, notification, maintenanceService, router, controller) {
    this.events = events;
    this.router = router;
    this.maintenanceService = maintenanceService;
    this.notification = notification;
    this.controller = controller;
    this.controller.addRenderer(new BootstrapFormValidationRenderer());
    this.controller.validateTrigger = validateTrigger.change;

    this.IsSelectedCategoryIsDrone=false;
    this.IsSelectedCategoryIsEquipment=false;
    this.IsSelectedCategoryIsSensors=false;
    this.nextActionStatus=false;
    this.afterXHoursStatus=false;
    this.maintenanceId=null;
    this.maintenanceAggregate = null;
    this.maintenance = null;
    this.drones = null;
    this.sensors=null;
    this.equipments = null;
    this.statusList = null;
    this.organizations = null;
    this.categoryList = null;
    this.actionList = null;
      this.isWaiting = false;
      this.isReady = false;

  }

    attached(){        
        this.setupValidationRules();
    }

    detached() {
        //Finally Reset Validation
        this.controller.reset();
    }

  activate(params) {
      this.isWaiting = true;
      if(params.maintid) //execute when call from Alert
      {
          this.maintenanceId = parseInt(params.maintid);

          this.maintenanceService.getMaintenance(this.maintenanceId)
            .then(result => {
                this.maintenanceAggregate = result;
                this.maintenanceAggregate.Maintenance.MaintenanceDate = moment(result.Maintenance.MaintenanceDate).format('YYYY-MM-DD');
                this.maintenanceAggregate.Maintenance.NextActionAfterDate = moment(result.Maintenance.NextActionAfterDate).format('YYYY-MM-DD');
                this.initializeEntity();
                this.setupValidationRules();
              this.isWaiting = false;
              this.isReady = true;
            }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
            });
      }


      if(params.id) //execute when call from Maintenance
      {     
          if(parseInt(params.id) > 0){ //Edit Case
              this.maintenanceService.getMaintenance(parseInt(params.id))
                .then(result => {
                    this.maintenanceAggregate = result;
                    this.maintenanceAggregate.Maintenance.MaintenanceDate = moment(result.Maintenance.MaintenanceDate).format('YYYY-MM-DD');
                    this.maintenanceAggregate.Maintenance.NextActionAfterDate = moment(result.Maintenance.NextActionAfterDate).format('YYYY-MM-DD');
                    this.initializeEntity();
                    this.setupValidationRules();
                  this.isWaiting = false;
                  this.isReady = true;
                }).catch((e) => {
                  this.notification.error('Error.');
                  this.isWaiting = false;
                });
          }
          else // Add Case
          {
              this.maintenanceService.getNewMaintenance()
                .then(result => {
                    this.maintenanceAggregate = result;
                    this.maintenanceAggregate.Maintenance.MaintenanceDate = moment(result.Maintenance.MaintenanceDate).format('YYYY-MM-DD');
                    this.maintenanceAggregate.Maintenance.NextActionAfterDate = moment(result.Maintenance.NextActionAfterDate).format('YYYY-MM-DD');
                    this.initializeEntity();
                    this.setupValidationRules();
                  this.isWaiting = false;
                  this.isReady = true;
                }).catch((e) => {
                  this.notification.error('Error.');
                  this.isWaiting = false;
                });
          }
      }
  }

  initializeEntity(){
      this.maintenance = this.maintenanceAggregate.Maintenance;
      this.drones = this.maintenanceAggregate.Drones;
      this.equipments = this.maintenanceAggregate.Equipments;
      this.sensors = this.maintenanceAggregate.Sensors;
      this.statusList = this.maintenanceAggregate.StatusList;
      this.organizations = this.maintenanceAggregate.OrganizationLookup;
      this.categoryList = this.maintenanceAggregate.CategoryList;
      this.actionList = this.maintenanceAggregate.DueStatusList;
      this.heading = this.maintenance.Name != null ? 'Edit Maintenance' : 'Add Maintenance';
      this.IsSelectedCategoryIsEquipment = this.maintenance.Category == 'Equipment' ? true : false;
      this.IsSelectedCategoryIsDrone = this.maintenance.Category == 'Drone' ? true : false;
      this.IsSelectedCategoryIsSensors = this.maintenance.Category == 'Sensor' ? true : false;
      this.nextActionStatus = this.maintenance.DueStatus == 'Next Action' ? true : false;
      this.afterXHoursStatus = this.maintenance.DueStatus == 'After X Hours' ? true : false;
  }

  setupValidationRules() {   

      //Custom Rule For Date Validation  
      ValidationRules.customRule(
        'date',
        (value, obj) => value === null || value === undefined || value instanceof Date || moment(value,"YYYY-MM-DD",true).isValid(),
        '\${$displayName} must be a Date.'
      );

      this.rules = ValidationRules
           .ensure('Name').required()    
           .ensure('MaintenanceDate').required().satisfiesRule('date')    
           .ensure('CategoryId').matches(/^[1-9][0-9]*$/).required()
           .ensure('DroneId').matches(/^[1-9][0-9]*$/).required()
           .ensure('EquipmentId').matches(/^[1-9][0-9]*$/).required()
           .ensure('StatusId').matches(/^[1-9][0-9]*$/).required()
           .ensure('ExpenseCost').required()
           .ensure('OrganizationId').matches(/^[1-9][0-9]*$/).required()
           .ensure('DueStatusId').matches(/^[1-9][0-9]*$/).required()
           .ensure('NextActionAfterDate').required().satisfiesRule('date')
           .ensure('NextActionAfterHours').required()
           .rules;
  }

  removeRule(propName){
      let list = this.rules[0];
      for( var i = list.length; i--;){
          if ( list[i].property.name === propName) list.splice(i, 1);
      }
  }

  saveItem() {

      this.isWaiting = true;

      if(this.IsSelectedCategoryIsDrone){
          this.removeRule('EquipmentId');
          this.maintenance.EquipmentId = null;
          this.maintenance.SensorId=null;
      }

      if(this.IsSelectedCategoryIsEquipment){
          this.removeRule('DroneId');
          this.maintenance.DroneId = null;
          this.maintenance.SensorId=null;
      }

      if(this.IsSelectedCategoryIsSensors){
          this.removeRule('DroneId');
          this.removeRule('EquipmentId');
          this.maintenance.EquipmentId=null;
          this.maintenance.DroneId=null;
      }

      if(this.nextActionStatus){
          this.removeRule('NextActionAfterHours');
          this.maintenance.NextActionAfterHours = null;
      }

      if(this.afterXHoursStatus){
          this.removeRule('NextActionAfterDate');
          this.maintenance.NextActionAfterDate = null;
      }

      if(!this.maintenance.IsAdmin){
          this.removeRule('OrganizationId');
      }

      this.controller.validate({ object: this.maintenance, rules: this.rules })      
          .then(valresult => {
              if (valresult.valid) {
                  // validation succeeded
                      this.maintenanceService.saveMaintenance(this.maintenanceAggregate.Maintenance).then(result => {
                          this.handleSaveResult(result);
						  if(this.maintenance.MaintenanceId!='')
							{
								this.notification.error('Updated Successfully.');
							}
							else
							{
								this.notification.error('Added Successfully.');  
							}
						  this.isWaiting = false;
                      })
              } 
              else {
                  // validation failed
                  this.notification.error('Please enter a valid input.');
                  this.isWaiting = false;
              }
          }).catch((e) => {
              this.notification.error('Error.');
              this.isWaiting = false;
          });

      this.isWaiting = false;
  }

  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('maintenance-item-operation', {refresh: true});
        this.maintenanceId != null ? this.router.navigate('#/alerts') : this.router.navigate('#/maintenance');        
    }
    else {
      this.notification.error(result.ErrorMessage);
    }
  }

  cancel() {

      this.maintenanceId != null ? this.router.navigate('#/alerts') : this.router.navigate('#/maintenance');        
  }

  setCategory(event){
      if(event.target.selectedOptions.length > 0){
          this.IsSelectedCategoryIsDrone = event.target.selectedOptions[0].text == 'Drone' ? true : false;
          this.IsSelectedCategoryIsEquipment = event.target.selectedOptions[0].text == 'Equipment' ? true : false;
          this.IsSelectedCategoryIsSensors = event.target.selectedOptions[0].text == 'Sensor' ? true : false;
          console.log(this.IsSelectedCategoryIsDrone);
          console.log(this.IsSelectedCategoryIsEquipment);
          console.log(this.IsSelectedCategoryIsSensors);  
        }              
  }

  setDueStatus(event){
      if(event.target.selectedOptions.length > 0){
          this.nextActionStatus = event.target.selectedOptions[0].text == 'Next Action' ? true : false;
          this.afterXHoursStatus = event.target.selectedOptions[0].text == 'After X Hours' ? true : false;

          if(event.target.selectedOptions[0].text == 'Next Action'){
              this.maintenance.NextActionAfterDate = moment(this.maintenance.MaintenanceDate).add(1,'months').format('YYYY-MM-DD');
          }
      }              
  }

}
