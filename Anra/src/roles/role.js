import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';

@inject(EventAggregator, Notification)
export class Role {
  @bindable role;
  @bindable save;

    constructor(events, notification) {
        this.events = events;
        this.notification = notification;
    }

    bind() {
        this.heading = this.role.roleName != null ? 'Edit Role' : 'Add Role';
    }

    saveItem() {
		$.loader.open();
		
             return this.save()
                .then(result => {
                    this.handleSaveResult(result)
					if(this.role.RoleId!='')
						{
							this.notification.error('Updated Successfully.');
						}
						else
						{
							this.notification.error('Added Successfully.');  
						}
					  $.loader.close(true);
                }).catch((e) => {
                    this.notification.error(e);
					$.loader.close(true);
                });
    }

    handleSaveResult(result){
        if (result.IsSuccess) {
            this.events.publish('roles-item-operation', {refresh: true});
        }
        else {
            this.notification.error(result.ErrorMessage);
        }
    }

    cancel() {
        this.events.publish('roles-item-operation', {refresh: false});
    }
}
