import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class RoleService {

    constructor(http, config) {
        this.config = config;
        this.http = http;
        this.baseApi = this.config.get('gatewayBaseUrl');
        this.defaultPagingSize = this.config.get('defaultPagingSize');
    }

    getRoles() {
        return this.http.fetch(this.baseApi + 'roleprivileges/listing')
        .then(response => response.json());
    }

    getNewRole() {
        return this.http.fetch(this.baseApi + 'roleprivileges/new')
        .then(response => response.json());
    }

    getRole(id) {
        return this.http.fetch(this.baseApi + 'roleprivileges/' + id)
        .then(response => response.json());
    }

    getUserRole() {
        return this.http.fetch(this.baseApi + 'users/getrole')
        .then(response => response.text());
    }

    deleteRole(id) {
        return this.http.fetch(this.baseApi + 'roleprivileges/' + id, {
            method: 'delete'
        })
          .then(response => {
              response.json();
          }
          );
    }

    saveRole(role) {
        return this.http.fetch(this.baseApi + 'roleprivileges/', {
            method: 'post',
            body: json(role)
        }).then(response => response.json());
    }
}
