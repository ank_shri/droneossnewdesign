import {inject} from 'aurelia-framework';
import {RoleService} from 'roles/roleService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {PermissionService} from 'security/permissionService';


@inject(RoleService, Router, EventAggregator, PermissionService)
export class Roles {

    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['RoleName']},
    ];

    constructor(roleService, router, events, permissionService, notification) {
        this.roleService = roleService;
        this.router = router;
        this.events = events;
        this.permissionService = permissionService;

        this.heading = 'Manage Roles';
        this.roles = [];
        this.role = null;
        this.roleName = "";
        this.isEditing = false;
        this.isViewing = false;
        //Set Default Paging Size
        this.pageSize = roleService.defaultPagingSize;
    }

    activate() {
        this.getList();
    }

    attached() {
        this.subscribeEvents();
    }
    detached() {
      this.subscriber.dispose();
    }

    getRole() {
        if(localStorage["Roles"].indexOf("Super Admin") > -1)
        {
            this.roleName = "super admin";
            return;
        }

        if(localStorage["Roles"].indexOf("Admin") > -1)
        {
            this.roleName = "admin";
            return;
        }

        this.roleName = "others";   
    }

    subscribeEvents() {
      this.subscriber= this.events.subscribe('roles-item-operation', payload => {

            if (payload) {
                if (payload.refresh) {
                    this.getList();
                }
                this.reset();
            }
        });

      this.subscriber= this.events.subscribe('roles-item-operation-edit', payload => {

            if (payload) {
                if (payload.id > 0) {
                    this.editItem(payload.id);
                }
                this.reset();
            }
        });
    }


    getList() {
        this.isWaiting=true;
        this.roleService.getRoles()
        .then(result => {
            this.roles = result;
            this.isEditing = false;
            this.getRole();
            this.isWaiting=false;
        });
    }

    reset() {
        this.role = null;
        this.isEditing = false;
        this.isViewing=false;
    }

    add() {
        this.roleService.getNewRole()
          .then(result => {
              this.role = result;
              this.isEditing = true;
          });
    }

  editItem(id) {
        this.isWaiting = true;
        this.roleService.getRole(id)
          .then(result => {
            this.isWaiting = false;
              this.role = result;
              this.isEditing = true;
          });
    }

  viewItem(id) {
        this.isWaiting = true;
        this.roleService.getRole(id)
          .then(result => {              
              this.role = result;
            this.isViewing = true;
            this.isWaiting = false;
          });
    }

    deleteItem(id) {
        this.roleService.deleteRole(id)
          .then(result => {
              this.getList();
          });
    }

    saveItem(item) {
        return this.roleService.saveRole(this.role);
        
    }
}
