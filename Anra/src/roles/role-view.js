import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Notification} from 'aurelia-notification';

@inject(EventAggregator, Notification)
export class RoleView {
  @bindable role;

    constructor(events, notification) {
        this.events = events;
        this.notification = notification;
    }

    bind() {
        this.heading = this.role.RoleName != null ? 'View Role' : 'Add Role';
    }

    cancel() {
        this.events.publish('roles-item-operation', {refresh: false});
    }

    edit(RoleId) {
        this.events.publish('roles-item-operation-edit', {id: RoleId});
    }
}
