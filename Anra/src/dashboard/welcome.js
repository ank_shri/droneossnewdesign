import { AureliaConfiguration } from "aurelia-configuration";
import {inject} from 'aurelia-framework';
import {UIApplication} from "aurelia-ui-framework";
import {DashboardService} from './dashboardService';
import {Router} from 'aurelia-router';
import {AuthService} from "aurelia-authentication";
import AppRouterConfig from "../common/routes.config";


@inject(DashboardService, Router,AureliaConfiguration,UIApplication,AuthService, AppRouterConfig)
export class Welcome {
  heading = 'Welcome to the Aurelia Navigation App!';
  firstName = 'John';
  lastName = 'Doe';
  previousValue = this.fullName;  

  constructor(dashboardService, router,config,application,authService, appRouterConfig) {
    this.config = config;
    this.isWaiting = false;
    this.application = application;
    this.dashboardService = dashboardService;
    this.dashboardItem = null;
    this.router = router;
    this.authService = authService;
    this.appRouterConfig = appRouterConfig;
    this.maxValues=[];
    this.maxRangeVal=0;
    //Set Default Paging Size
    this.pageSize = dashboardService.defaultPagingSize;
  }

  viewItem(id){      
      this.router.navigate('flight-data-listing/dashboard/' + id);      
  }

  activate() {
    // this.routernavigationDashboard();

    // this.appRouterConfig.configure();
      this.isWaiting = true;
      //$.loader.open();
    this.dashboardService.getDashboard()
      .then(result => {
          this.dashboardItem = result;
          console.log("this.dashboardItem",this.dashboardItem);
          this.drawFlightSummaryChart();
          this.drawOperationalSummaryChart();
          this.isWaiting = false;
       
        //$.loader.close(true);
      })
      .catch(err => {
          this.isWaiting = false;
          //$.loader.close(true);
      })    
  }

  routernavigationDashboard(){
    let a = this.router.navigation;
    this.router.navigation = [];

    for(let i=0; i<=1; i++){
        let route1 = a[i];
        this.router.navigation.push(route1);
    }
  }

  roundUp(valueToRoundUp){
      this.maxRangeVal = Math.ceil((valueToRoundUp+1) / 10) * 10; //Math.round(valueToRoundUp/10) * 10;
  }

  drawFlightSummaryChart() {

      // Define the chart to be drawn.
      var items = this.dashboardItem.FlightSummaryList;
      var data = new google.visualization.DataTable(3);
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Total Flights');
      data.addColumn('number', 'Total Hours');
      
      this.maxValues = [];

      for (let i = 0; i < items.length; i++) 
      {
          var maxVal = Math.max(items[i].TotalFlights,items[i].TotalHours);
          this.maxValues.push(maxVal); 

          data.addRow([items[i].Month,items[i].TotalFlights,items[i].TotalHours]);
      }

      //Find Max Val For Vertical Axis
      this.roundUp(Math.max.apply(null, this.maxValues));

      var options = {
          backgroundColor: '#151315',
          chartArea: {
              width:'70%',
              height:'80%',
              'backgroundColor': {
                'fill': '#151315',
                'opacity': 100
             }
          },
          plotOptions: {
              column: {
                  minPointLength: 3
              }
          },
          legend: { position: 'top', maxLines: 1,'textStyle': { 'color': '#fff' }  },
          bar: {groupWidth: '100%'},          
          colors: ['#36e4c6', '#a1a1a1'],
          hAxis: {
              title: 'Months',              
              titleTextStyle:{color: '#fff', bold: true, fontSize: 18},
              textStyle: { color: '#fff'},
              viewWindow: {
                  min: ['Jan'],
                  max: ['Dec']
              }
          },
          vAxis: {
              title: 'Total Flights/Hours',
              titleTextStyle:{color: '#fff', bold: true, fontSize: 18},
              textStyle: { color: '#fff'},
              gridlines: {
                color: '#ccc'
              },
              viewWindow: {
                  min: [0],
                  max: [this.maxRangeVal]
              }
          },
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('flightSummary'));
      chart.draw(data, options);
  
  }

  drawOperationalSummaryChart() {
      console.log("start draw ops");
      // Define the chart to be drawn.
      var items = this.dashboardItem.OperationalSummaryList;
      var data = new google.visualization.DataTable(6);
      data.addColumn('string', 'Month');
      data.addColumn('number', 'Active Drones');
      data.addColumn('number', 'Onsite Time');
      data.addColumn('number', 'Travel Time');
      data.addColumn('number', 'Open Tasks');
      data.addColumn('number', 'No. Of Incidents');

      this.maxValues = [];

      for (let i = 0; i < items.length; i++) 
      {
          var maxVal = Math.max(items[i].TotalActiveDrones,items[i].TotalOnsiteTime,items[i].TotalTravelTime,items[i].TotalOpenTasks,items[i].TotalIncidents);
          this.maxValues.push(maxVal); 
          data.addRow([items[i].Month,items[i].TotalActiveDrones,items[i].TotalOnsiteTime,items[i].TotalTravelTime,items[i].TotalOpenTasks,items[i].TotalIncidents]);
      }

      //Find Max Val For Vertical Axis
      this.roundUp(Math.max.apply(null, this.maxValues));      

      var options = {
        backgroundColor: '#151315',
          chartArea: {
              width:'70%',
              height:'80%',
              'backgroundColor': {
                'fill': '#151315',
                'opacity': 100
             }
          },
          plotOptions: {
              column: {
                  minPointLength: 3
              }
          },
          legend: { position: 'top', maxLines: 2, 'textStyle': { 'color': '#fff' }   },
          bar: {groupWidth: '100%'},
		  colors: ['#36e4c6', '#a1a1a1','#3dc7c9', '#354052','#1a74b3'],          
          hAxis: {
              title: 'Months',              
              titleTextStyle:{color: '#fff', bold: true, fontSize: 18},
              textStyle: { color: '#fff'},
              viewWindow: {
                  min: ['Jan'],
                  max: ['Dec']
              }
          },
          vAxis: {
              title: 'Operation Details',
              titleTextStyle:{color: '#fff', bold: true, fontSize: 18},
              textStyle: { color: '#fff'},
              gridlines: {
                color: '#ccc'
              },
              viewWindow: {
                  min: [0],
                  max: [this.maxRangeVal]
              }
          },
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('operationalSummary'));
      chart.draw(data, options);
  }
}
