import {inject, BindingEngine} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';

@inject(EventAggregator, BindingEngine)
export class NetworkSideBar {
  constructor(events, bindingEngine) {
    this.events = events;
    this.bindingEngine = bindingEngine;
    this.subscriptions = [];

    this.selectedAirspaces = ['class_b'];
    this.selectedAirportOption = 'airports_recreational';
    this.selectedCautionaryOptions = [];
    this.selectedAdvisoryOptions = [];
    this.toggleAirport = true;
    this.toggleCaution = true;
    this.toggleAdvisory = true;
    this.togglePoup = false;
    this.helpTitle=null;
    this.helpText=null;

    this.airportOptions = [
      {value: 'airports_recreational', name: 'Recreational - 5 Mile Radius', helptitle:'Rules for Recreational Ops', helptext:'Making this selection will display areas around airports where recreational operations of drones may be restricted or require notice or authorization, depending upon the jurisdiction. For example, in the United States, Community-based guidelines require recreational operators to give notice for flights within 5 statute miles of an airport. Notice must be given to the airport operator and air traffic control tower, if the airport has a tower. Tap or click on an airspace area to see the airport operator phone number. In some places, you may provide digital notice of your flight by clicking the "Add flight here" button.'},
      {value: 'airports_commercial', name: 'Commerical - Blanket BOA', helptitle:'Rules for Commercial Ops', helptext:'Making this selection will display areas around airports where commercial operations of drones may be restricted or require notice or authorization, depending upon the jurisdictions. For example, in the United States, commercial operators with a Section 333 exemption from the FAA may operate under specified circumstances without a specific Certificate of Waiver or Authorization. This is referred to as the "Blanket COA". For more information, click here (https://www.faa.gov/news/updates/?newsId=82245).'}
    ];

    this.controlledAirspaceOptions = [
      {value: 'class_b', name: 'Class B', color: '0x1a74b3'},
      {value: 'class_c', name: 'Class C', color: '#9b6c9d'},
      {value: 'class_d', name: 'Class D', color: '#1a74b3'},
      {value: 'class_e0', name: 'Class E', color: '#9b6c9d'}
    ];

    this.cautionaryOptions = [
      {value: 'tfrs', name: 'Temporary Flight Restrictions', color: '0x1a74b3', helptitle:'Temporary Flight Restrictions (TFRs)', helptext:'Temporary Flight Restrictions (TFRs) in the United States are displayed. TFRs are used by the FAA to temporarily restrict flights in certain areas. Some TFRs have become more permanent, like those around Disneyland and Disneyworld, but most are event based. For example, TFRs are published when the President comes to town or to protect airspace for an airshow. The FAA publishes TFRs as necessary, but there are also "unpublished" TFRs for sporting events, which appear in AirMap.<br>Red areas represent active TFRs and Grey areas represent upcoming TFRs. You can also click these areas for more information including when the TFR is effective.'},
      {value: 'wildfires', name: 'Wildfires', color: '#9b6c9d', helptitle:'Wild Fires', helptext:'This layer depicts fires sourced directly from the Department of Interiors incident command system and other Computer Aided Dispatch systems implemented across different cities around the United States. The FAA does not issue Temporary Flight Restrictions for the vast majority of fires in the United States, even though many are fought with firefighting aircraft. In many states, interfering with firefighting activity is considered a crime.'},
      {value: 'sua_prohibited', name: 'Prohibited Special Use Airspace', color: '0x1a74b3', helptitle:'Prohibited Special Use Airspace', helptext:'Prohibited airspace protects the most sensitive regions, such as the homes of heads of state and important government facilities. Permission from the using agency (such as the Secret Service) is required to enter a prohibited airspace and is almost never available.'},
      {value: 'sua_restricted', name: 'Restricted Special Use Airspace', color: '#9b6c9d', helptitle:'Restricted Special Use Airspace', helptext:'Restricted areas are typically located around military installations or other areas where flight could be hazardous. Permission from the controlling agency (air traffic control) is required to enter these areas and is often not available. To simplify the map for small UAS operators, we have grouped other types of special use airspace in this category, including military operations areas, danger zones, warning areas, and other similar areas.'},
      {value: 'national_parks', name: 'National Parks', color: '#1a74b3', helptitle:'National Parks', helptext:'This layer depicts areas within the boundaries of units of the United States National Park System. Launching, landing, or operating unmanned aircraft is prohibited on lands and waters administered by the National Park Service within these areas. More information about the location of units of the National Park System and the National Park Service drone ban is available on the website of each park area which can be found on www.nps.gov.'},
      {value: 'noaa', name: 'NOAA Marine Protection Areas', color: '#9b6c9d', helptitle:'NOAA Marine Protection Areas', helptext:'Regulations of the National Oceanic and Atmospheric Administration in the United States prohibit certain flights of powered aircraft (including drones) in these areas. More information available at http://sanctuaries.noaa.gov/flight/welcome.html'}
    ];


    this.advisoryOptions = [
      {value: 'hospitals', name: 'Hospitals', color: '0x1a74b3'},
      {value: 'heliports', name: 'Heliports', color: '#9b6c9d'},
      {value: 'power_plants', name: 'Power Plants', color: '#1a74b3'},
      {value: 'schools', name: 'Schools', color: '#9b6c9d'}
    ];

  }

  attached() {
    let watch1 = this.bindingEngine.collectionObserver(this.selectedAirspaces).subscribe((splices) => {
      this.updateAirpspace();
    });

    let watch2 = this.bindingEngine.propertyObserver(this, 'selectedAirportOption').subscribe((newval, oldVal) => {
      this.updateAirpspace();
    });

    let watch3 = this.bindingEngine.collectionObserver(this.selectedCautionaryOptions).subscribe((splices) => {
      this.updateAirpspace();
    });

    let watch4 = this.bindingEngine.collectionObserver(this.selectedAdvisoryOptions).subscribe((splices) => {
      this.updateAirpspace();
    });

    this.subscriptions.push(watch1);
    this.subscriptions.push(watch2);
    this.subscriptions.push(watch3);
    this.subscriptions.push(watch4);
  }

  detached() {
    this.subscriptions.map(x => x.dispose());
  }

  updateAirpspace() {
    let layers = [];
    layers.push(this.selectedAirportOption);
    layers = layers.concat(this.selectedAirspaces);
    layers = layers.concat(this.selectedAdvisoryOptions);
    layers = layers.concat(this.selectedCautionaryOptions);

    if (layers.length > 0) {
      let selectedSpaces = layers.toString().split(',');
      this.events.publish('air-space-update', {'controlledSpace': selectedSpaces})
    }
    else {
      this.events.publish('air-space-update', {'controlledSpace': ''})
    }
  }

  toggle(par) {
    if (par == 1) {
      this.toggleAirport = !this.toggleAirport;
      //this.toggleCaution = false;
      //this.toggleAdvisory = false;
    }
    if (par == 2) {
      this.toggleCaution = !this.toggleCaution;
      //this.toggleAirport = false;
      //this.toggleAdvisory = false;
    }
    if (par == 3) {
      this.toggleAdvisory = !this.toggleAdvisory;
      //this.toggleAirport = false;
      //this.toggleCaution = false;
    }
  }

  popup(selectedOption) {

      if(selectedOption == 'controlledAirspace'){
          this.helpTitle = 'Controlled Airspace';
          this.helpText = 'Areas of controlled airspace below 500 feet are displayed. To simplify the map for small UAS operators, upper level airspace areas are not displayed. Some countries require authorization prior to flight in controlled airspace.';
      }
      else{
          this.helpTitle = selectedOption.helptitle;
          this.helpText = selectedOption.helptext;
      }

      this.togglePoup = !this.togglePoup;
  }

  close(par2) {
    if (par2 == 1) {
      this.togglePoup = !this.togglePoup;
    }
  }
}

