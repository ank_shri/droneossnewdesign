import {inject, BindingEngine} from 'aurelia-framework';
import {DroneService} from 'drones/droneService.js';
import {MavelinkService} from "lib/mavelink-service.js";
import {Vehicle} from 'lib/vehicle.js';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AureliaConfiguration} from "aurelia-configuration";
// import {Notification} from 'aurelia-notification';
import {CommandService} from 'drone-view/commandService';
import moment from 'moment';
import * as PahoMQTT from 'paho-mqtt';

import * as L from 'leaflet';
import * as D from 'leaflet-draw';
import * as T from 'SpatialServer/Leaflet.MapboxVectorTile';
import { Router } from 'aurelia-router';
import {AuthService} from "aurelia-authentication";
import AppRouterConfig from "../common/routeNetwork.config";

@inject(BindingEngine, EventAggregator, DroneService, MavelinkService, AureliaConfiguration, CommandService,Router,AuthService, AppRouterConfig)
export class Network {

  constructor(bindingEngine, events, droneService, mavelinkService, configure, commandService,router,authService, appRouterConfig) {
    this.events = events;
    this.router = router;
    this.authService = authService;
    this.appRouterConfig = appRouterConfig;
    // appRouterConfig.configure();
    this.droneService = droneService;
    this.mavelinkService = mavelinkService;
    this.configuration = configure;
    this.bindingEngine = bindingEngine;
    this.commandService = commandService;

    this.isWaiting = false;
    this.drones = null;
    this.vehicles = [];
    this.collisionLog = [];
    this.alerts = [];

    this.displayControls = false;
    this.zoomLevel = 18;
    this.lng = '-77.549752';
    this.lat = '38.926047';

    this.droneMarkerMap = new Map();
    this.controlledSpaceLayer = ['airports_recreational', 'class_b'];
    this.isMqttConnected = false;
    this.collisionWarning = false;
    this.currentMarkerLayer = null;

  }


  getDrones() {
    this.isWaiting = true;
    return this.droneService.getDrones()
      .then(result => {
        this.drones = result.filter(activedrones => activedrones.IsActive);
      })
      .then(() => {
        this.drones.map(drone => this.setupDronesNetwork(drone));
        L.layerGroup(this.getMarkers()).addTo(this.map);        
        this.adjustView();
        this.updateMarkers();
        this.drones.map(d => {
          this.mqtt.subscribe('drone/oss/' + d.DroneId + '/telemetry');
        });

        setInterval(() => this.checkCollisions(), 5000);
        this.isWaiting = false;
      })
      .catch(err => {
        console.error(err);
        this.isWaiting = false;
      })
  }

  attached() {
    // this.routerforNetworktab();
    this.mqttHost = this.configuration.get('mqttBrokerHost');
    this.mqttUser = this.configuration.get('mqttBrokerUserName');
    this.mqttPort = this.configuration.get('mqttBrokerPort');
    this.mqttPwd = this.configuration.get('mqttBrokerPwd');
    this.mqttUseSSL = this.configuration.get('mqttUseSSL')? true: false;

    this.initMqttClient();
    this.subscribeEvents();
    this.initMap();
  }

  detached() {
    this.mqtt.disconnect();
  }

  routerforNetworktab(){
    if(this.router.navigation.length === 2){
      this.appRouterConfig.configure();
    }
  }

  initMqttClient() {
    try {
      this.mqtt = new Paho.MQTT.Client(this.mqttHost, Number(this.mqttPort), "web_" + parseInt(Math.random() * 100, 10));
      this.mqtt.connect({useSSL: this.mqttUseSSL, userName: this.mqttUser, password: this.mqttPwd, onSuccess: () => this.onMqttConnect()});

      this.mqtt.onMessageArrived = (m) =>  this.handleNewPacket(m);
      this.mqtt.onConnectionLost = (p) => this.onMqttConnectionLost(p);
    }
    catch (err) {
      console.error(err);
    }
  }

  onMqttConnectionLost(p) {
    this.isMqttConnected = false;
  }

  onMqttConnect() {
    this.isMqttConnected = true;
    this.getDrones();
  }

  initMap() {
      let accessToken = this.configuration.get('mapboxAccessToken');

      var streetMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=' + accessToken, {
          attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
      });

      var satelliteMap = L.tileLayer('https://api.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=' + accessToken, {
          attribution: '© <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
      });

      let streetDarkMap = L.tileLayer(
        'https://api.mapbox.com/styles/v1/aganjoo/cjm2gw5p406qa2snwya18clid/tiles/256/{z}/{x}/{y}?access_token=' +
        accessToken, {
          attribution: '<a href="https://www.mapbox.com/map-feedback/">Mapbox</a>'
        }
      );

      this.airSpaceLayerGroup = new L.LayerGroup();

      this.baseMaps = {
          "Satellites": satelliteMap,
          "Streets": streetMap
      };

      this.overlayMaps = {
          "airspace": this.airSpaceLayerGroup
      }

      let map = L.map('map', {
          zoomControl: false,
          drawControl: false,
          editable: false,
          maxNativeZoom: 22,
          layers: [streetDarkMap]
      });

      L.control.zoom({
          maxZoom: 22,
          position: 'topleft'
      }).addTo(map);

      map.doubleClickZoom.disable();

      L.Icon.Default.imagePath = 'styles/images';
      this.map = map;

      map.setView(new L.LatLng(this.lat, this.lng), 18);   
      this.updateAirSpace();

    //this.attachDrawControls(map);
  }

  updateAirSpace() {
      var airmap_key = this.configuration.get('airmapKey');
      var layerString = this.controlledSpaceLayer.toString().split(',');

      if (this.mvtSource) {
          this.airSpaceLayerGroup.clearLayers();
          this.map.removeLayer(this.mvtSource);
      }

      var self = this;
      this.mvtSource = new L.TileLayer.MVTSource({
          url: 'https://api.airmap.io/maps/v4/tiles' + '/' + layerString + "/{z}/{x}/{y}.pbf?apikey=" + airmap_key + '&theme=satellite-streets-v9',
          clickableLayers: this.controlledSpaceLayer,
          getIDForLayerFeature: function (feature) {
              return Math.random().toString(36).substr(2, 9);
          },
          style: (feature) => {
              var layer = feature.properties.type;
              return this.getStyle(layer);
          },
          onClick: function (evt) {
              if (evt.feature) {

                  let props = Object.entries(evt.feature.properties);
                  var names = [];
                  props.forEach(x => {
                      if (x[0] == 'type') {
                          let val = x[1].toUpperCase().replace('_', ' ');
                          let dispProp = {
                              displayOrder: 1,
                              prop: '<div><strong>' + x[0].toUpperCase() + '</strong>: ' + val + '</div>'
                          };
                          names.push(dispProp);
                      }
                      else if (x[0] == 'url') {
                        let val = x[1];
                        let dispProp = {
                          displayOrder: 1,
                          prop: '<div><strong>' + x[0].toUpperCase() + '</strong>: ' + '<a href=' +val+ '>' + 'view more details' + '</a>' + '</div>'
                        };
                        names.push(dispProp);
                      }
                      else {
                          let dispProp = {
                              displayOrder: names.length + 2,
                              prop: '<div><strong>' + x[0].toUpperCase() + '</strong>: ' + x[1] + '</div>'
                          };
                          names.push(dispProp);
                      }
                  })

                  let content = names.sort(x => x.displayOrder).map(x => x.prop).join('');

                  var popup = L.popup()
                      .setLatLng(evt.latlng)
                      .setContent(content)
                      .openOn(self.map);
              }
          }
      });

      this.map.on("layerremove", function (removed) {
          if (removed.layer.removeChildLayers) {
              removed.layer.removeChildLayers(self.map);
          }
      });

      this.airSpaceLayerGroup.addLayer(this.mvtSource).addTo(this.map);

  }

  getStyle(layer) {
    var style = {};
    switch (layer) {
      case 'airports_recreational':
      case 'airports_recreational_private':
      case 'airports_commercial':
      case 'airports_commercial_private':
      case 'hospitals':
      case 'heliports':
      case 'power_plants':
      case 'schools':
        style.color = 'rgba(246, 165, 23, 0.2)';
        style.outline = {
          color: 'rgb(246, 165, 23)',
          size: 0
        }
        break;
      case 'national_parks':
      case 'noaa':
        style.color = 'rgba(224, 18, 18, 0.2)';
        style.outline = {
          color: 'rgb(224, 18, 18)',
          size: 0
        }
        break;
      case 'sua_restricted':
      case 'sua_prohibited':
        style.color = 'rgba(27, 90, 207, 0.2)';
        style.outline = {
          color: 'rgb(27, 90, 207)',
          size: 0
        }
        break;
      case 'class_b':
        style.color = 'rgba(26, 116, 179, 0.2)';
        style.outline = {
          color: 'rgb(26, 116, 179)',
          size: 0
        }
        break;
      case 'class_c':
        style.color = 'rgba(155, 108, 157, 0.3)';
        style.outline = {
          color: 'rgb(155, 108, 157)',
          size: 0
        }
        break;
      case 'class_d':
        style.color = 'rgba(26, 116, 179, 0.2)';
        style.outline = {
          color: 'rgb(26, 116, 179)',
          size: 0
        }
        break;
      case 'class_e0':
        style.color = 'rgba(155, 108, 157, 0.2)';
        style.outline = {
          color: 'rgb(155, 108, 157)',
          size: 0
        }
        break;
      case 'tfrs':
        style.color = 'rgba(224, 18, 18, 0.2)';
        style.outline = {
          color: 'rgb(224, 18, 18)',
          size: 0
        }
        break;
    }

    return style;
  }

  attachDrawControls(map) {
    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    var drawControl = new L.Control.Draw({
      position: 'topleft',
      draw: {
        polygon: true,
        polyline: true,
        rectangle: true,
        circle: true,
        marker: true,
        editable: true
      },
      edit: {
        featureGroup: drawnItems,
        edit: true
      }
    });

    map.on('draw:created', function (event) {
      var layer = event.layer;
      drawnItems.addLayer(layer);
      var json = drawnItems.toGeoJSON();
      //vm.onDrawMap(json.features[0].geometry);
    });

    map.addControl(drawControl);
  }

  subscribeEvents() {
    this.events.subscribe('air-space-update', p => {
      this.controlledSpaceLayer = p.controlledSpace;
      this.updateAirSpace();
    });
  }

  handleNewPacket(p) {
      //console.log('Telemetry Packet:',p);
    let packet = JSON.parse(p.payloadString)
    let item = this.droneMarkerMap.get(packet.DroneId);
    if (item.drone) {
      item.drone.packetTimeStamp = moment();
      item.drone.BaseLat = packet.Lat;
      item.drone.BaseLng = packet.Lng;
      item.drone.BaseAlt = packet.Alt;
      item.drone.isCurrentStatusActive = packet.Armed;

      this.updateMarkers();
    }
  }


  getDroneIcon(isAlive) {
    let droneIconOffline = L.icon({
      iconUrl: './image/drone-icon-grey.svg',
      iconSize: [35, 35],
      iconAnchor: [25, 25],
      popupAnchor: [-3, -4]
    });

    let droneIconOnline = L.icon({
      iconUrl: './image/drone-icon-new.svg',
      iconSize: [50, 47],
      iconAnchor: [25, 25],
      popupAnchor: [-3, -4]
    });

    return isAlive ? droneIconOnline : droneIconOffline;
  }

  setupDronesNetwork(drone) {
    if (drone.BaseLat && drone.BaseLng) {
      let guideMarker = new L.Marker(new L.LatLng(drone.BaseLat, drone.BaseLng), {'icon': this.getDroneIcon(false)}).bindPopup(drone.Title + '- Initializing').openPopup();
      guideMarker.setLatLng(new L.LatLng(drone.BaseLat, drone.BaseLng));
      drone.packetTimeStamp = moment();
      drone.collisionWarning = false;
      this.droneMarkerMap.set(drone.DroneId, {marker: guideMarker, drone: drone});

      //Create Coverage Area For Individual Drone
      var covergaeArea = this.getCoverageArea(drone);
      drone.coverageArea = covergaeArea;
      this.map.addLayer(covergaeArea);
    }
  }

  adjustView() {
    let markers = this.getMarkers();
    if (markers && markers.length > 0) {
      let group = new L.featureGroup(this.getMarkers());
      this.map.fitBounds(group.getBounds(), {'padding': [30, 30]});
    }
  }

  getMarkers() {
    let markers = [];
    this.droneMarkerMap.forEach((v, k, m) => {
      markers.push(v.marker);
    });
    return markers;
  }

  updateMarkers() {
    this.droneMarkerMap.forEach((v, k, m) => {
      if (this.isMqttConnected) {
        if (!v.drone.isCurrentStatusActive) {
          v.marker.setPopupContent(v.drone.Title + '- Offline');
          v.marker.setLatLng(new L.LatLng(v.drone.BaseLat, v.drone.BaseLng));
          v.marker.setIcon(this.getDroneIcon(false));
        }
        else {
          v.marker.setPopupContent(v.drone.Title + '- Online');
          v.marker.setLatLng(new L.LatLng(v.drone.BaseLat, v.drone.BaseLng));
          v.marker.setIcon(this.getDroneIcon(true));
        }

      }
      else {
        v.marker.setPopupContent(v.drone.Title + '- Offline');
        v.marker.setLatLng(new L.LatLng(v.drone.BaseLat, v.drone.BaseLng));
        v.marker.setIcon(this.getDroneIcon(false));
      }
      this.setCollisionLayer(v.drone);
    });
  }


  getCoverageArea(drone) {
    var clr = drone.collisionWarning ? '#ff0000' : drone.isCurrentStatusActive ? '#008080' : '#000000';
    var fillClr = drone.collisionWarning ? '#ff0000' : drone.isCurrentStatusActive ? '#00FFFF' : '#e8e8e8e8';

    var r = 0.3048 * drone.CollisionThresholdH;
    return L.circle([drone.BaseLat, drone.BaseLng], r, {
      color: clr,
      fillColor: fillClr,
      fillOpacity: 0.2
    });
  }

  checkCollisions() {

    if (typeof (Number.prototype.toRad) === "undefined") {
      Number.prototype.toRad = function () {
        return this * Math.PI / 180;
      }
    }

    var allDrones = this.drones;

    for (var i = 0; i < allDrones.length; i++) {
      var currentDrone = allDrones[i];

      if (!currentDrone.isCurrentStatusActive)
        continue;

      var thresholdH1 = currentDrone.CollisionThresholdH;
      var thresholdV1 = currentDrone.CollisionThresholdV;

      for (var j = 0; j < allDrones.length; j++) {
        var drone = allDrones[j];
        var isActive = drone.isCurrentStatusActive;

        isActive = true;

        if (drone.DroneId !== currentDrone.DroneId & isActive) {
          var lat1 = Number(currentDrone.BaseLat);
          var lon1 = Number(currentDrone.BaseLng);

          var lat2 = Number(drone.BaseLat);
          var lon2 = Number(drone.BaseLng);

          var R = 6371; // km
          var dLat = (lat2 - lat1).toRad();
          var dLon = (lon2 - lon1).toRad();
          var lat1 = lat1.toRad();
          var lat2 = lat2.toRad();

          var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
          var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
          var separation = R * c * 1000 * 3.28084; // In feet

          var alt1 = Number(currentDrone.BaseAlt);
          var alt2 = Number(drone.BaseAlt);


          var thresholdH2 = drone.CollisionThresholdH;
          var thresholdDistanceH = thresholdH1 + thresholdH2;


          var hBreach = (separation < thresholdDistanceH);

          var thresholdV2 = drone.CollisionThresholdV;
          var thresholdDistanceV = thresholdV1 + thresholdV2;
          var verticalSeparation = Math.abs(alt1 - alt2);

                  var vBreach = verticalSeparation < thresholdDistanceV;

                  if (hBreach && vBreach) {                      
                      var breachType = hBreach ? 'Horizontal' : (vBreach ? 'Vertical' : '');
                      var collissionMsg = this.createCollisionMessage(currentDrone, drone, breachType);
                      this.setCollisionWarning(true,currentDrone, drone, breachType); 
                      this.showCollisionWarning(currentDrone, drone, collissionMsg);
                      break;
                  }
                  else{
                      currentDrone.collisionWarning = false;
                      this.setCollisionLayer(currentDrone);
                  }
              }
          }
        }
      }    


  createCollisionMessage(currentDrone, drone, breachType) {
    var message = '';
    message += breachType + ' Collision Warning! ';
    message += '<br/> Time: ' + new Date();

    message += "<table border='1' cellpadding='5' style='width:90%;text-align:left;border-color:white'>";
    message += "<tr>";
    message += "<td>Name:</td>";
    message += "<td>" + currentDrone.Title + "</td>";
    message += "<td>" + drone.Title + "</td>";
    message += "</tr>";


    message += "<tr>";
    message += "<td>Lat:</td>";
    message += "<td>" + currentDrone.BaseLat + "</td>";
    message += "<td>" + drone.BaseLat + "</td>";

    message += "</tr>";
    message += "<tr>";
    message += "<td>Lon:</td>";
    message += "<td>" + currentDrone.BaseLng + "</td>";
    message += "<td>" + drone.BaseLng + "</td>";
    message += "</tr>";


    message += "<tr>";
    message += "<td>Altitude:</td>";
    message += "<td>" + currentDrone.BaseAlt + "</td>";
    message += "<td>" + drone.BaseAlt + "</td>";
    message += "</tr>";

    message += "</table>";

    return message;
  }

  setCollisionLayer(drone) {
    if (drone.coverageArea) {
      var currentCoverageArea = drone.coverageArea;
      this.map.removeLayer(currentCoverageArea);
    }

    var covergaeArea = this.getCoverageArea(drone);
    drone.coverageArea = covergaeArea;
    this.map.addLayer(covergaeArea);
  }

  setCollisionWarning(isColliding,currentDrone, drone, breachType) {
      if (!drone.collisionWarning) {          
          drone.collisionWarning = isColliding;
          this.setCollisionLayer(drone);

      if (isColliding) {
        var warning = {
          "Drone1": currentDrone.DroneId,
          "Lat1": currentDrone.BaseLat,
          "Lon1": currentDrone.BaseLng,
          "Alt1": currentDrone.BaseAlt,
          "Drone2": drone.DroneId,
          "Lat2": drone.BaseLat,
          "Lon2": drone.BaseLng,
          "Alt2": drone.BaseAlt,
          "BreachType": breachType,
          "TimeStamp": new Date()
        };

        var topic = 'drone/oss/collision';

        //Publish topic for Return to Home Command
        this.mqttClient.publish(topic, JSON.stringify(warning));

        this.saveWarning(JSON.stringify(warning));
      }
    }
  }

  showCollisionWarning(drone1, drone2, collissionMsg) {

    var droneId1 = drone1.DroneId;
    var droneId2 = drone2.DroneId;

    if (this.collisionLog.indexOf(droneId1) == -1 && this.collisionLog.indexOf(droneId2) == -1) {

      this.notification.error(collissionMsg);

      this.collisionLog.push(droneId1);
      this.collisionLog.push(droneId2);

      this.alerts.push(collissionMsg);
    }
  }

  saveWarning(warning) {
    this.commandService.saveCollisionWarning(warning).then(result => {
      if (result.IsSuccess) {
        console.log('Collision Warning Saved.');
      }
      else {
        console.log('Collision Warning Saving Failed - ', result.ErrorMessage);
      }
    });
  }
}
