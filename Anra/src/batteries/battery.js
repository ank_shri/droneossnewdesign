import {bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {inject,NewInstance} from 'aurelia-dependency-injection';
import {ValidationController,ValidationRules,validateTrigger} from 'aurelia-validation';
// import {BootstrapFormValidationRenderer} from "lib/bootstrap-form-validation-renderer.js";
// import {Notification} from 'aurelia-notification';
import {AnraNotification} from '../components/anra-notification';
import {Router} from 'aurelia-router';
import moment from 'moment';
import {UIApplication} from "aurelia-ui-framework";

@inject(EventAggregator, AnraNotification, Router, NewInstance.of(ValidationController),UIApplication)
export class Battery {
  @bindable battery;
  @bindable drones;
  @bindable organizations;
  @bindable save;
  controller = null;
  rules = null;

  constructor(events, notification, router, controller,application) {
    this.events = events;
    this.notification = notification;    
    this.router = router;
    this.controller = controller;
    this.application=application;
    this.isWaiting = false;
    this.isEdit = false;
    this.ShowBatteryInfo=true;
  }

  attached() {
      this.setupValidationRules();
  }

  detached() {
      //Finally Reset Validation
      this.controller.reset();
  }

  bind() {
      this.heading = this.battery.BatteryName != null ? 'Edit Battery' : 'Add Battery';
      this.isEdit = this.battery.BatteryName != null;
  }

  setupValidationRules() {   
    ValidationRules.customRule(
      'cellcountRange',
      (value, obj, min, max) => {
        var num = Number.parseInt(value);
        return num === null || num === undefined || (Number.isInteger(num) && num >= min && num <= max);
      },
      "${$displayName} must be an integer between 1 and 20.",
      (min, max) => ({ min, max })
    );
      this.rules = ValidationRules
            .ensure('BatteryName').required().maxLength(20)    
            .ensure('SerialNumber').required().maxLength(20)
            .ensure('DroneId').matches(/^[1-9][0-9]*$/).required()
            .ensure('CellCount').required().satisfiesRule('cellcountRange', 1, 20)
            .ensure('Capacity').required().maxLength(6)
            .ensure('InitialCycleCount').matches(/^\d+$/).required()
            .ensure('FirmwareVersion').required().maxLength(20)
            .ensure('HardwareVersion').required().maxLength(20)
            .ensure('PurchaseDate').required()
            .rules;

                       
  }

  saveItem() {
    this.isWaiting = true;
    this.controller
    .validate({object: this.battery, rules: this.rules})
      .then(valresult => {
        if (valresult.valid) {
          // validation succeeded
          this.save()
                  .then(result => {
                        this.handleSaveResult(result);
						if(this.battery.BatteryId!='')
						{
							this.notification.ShowInfo('Updated Successfully.');
						}
						else
						{
							this.notification.ShowInfo('Added Successfully.');  
						}
						this.isWaiting = false;
                    });
        
        }
        else {
          let messages = this.controller.errors.map(x => x.message);

          this.application.toast(
            Object.assign({ container: this.errorHolder }, {
              title: 'Errors!',
              message: messages.join(', '),
              theme: 'danger',
              timeout: 2000,
              glyph: 'glyph-alert-info'
            })
          );

          this.isWaiting = false;
        }
      })
      .catch(e => {
        console.error(e.ErrorMessage);
      });
  }
    


  handleSaveResult(result){
    if (result.IsSuccess) {
      this.events.publish('battery-item-operation', {refresh: true});
    }
    else {
      this.notification.ShowError(result.ErrorMessage);
    }
  }

  cancel() {
    this.events.publish('battery-item-operation', {refresh: false});
  }

  addCycles() {
      this.router.navigate('cycles/' + this.battery.BatteryId);
  }

}
