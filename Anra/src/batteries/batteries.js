import {inject} from 'aurelia-framework';
import {BatteryService} from 'batteries/batteryService';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';
import moment from 'moment';
import {AnraNotification} from '../components/anra-notification';
import {PermissionService} from 'security/permissionService';
// import {DialogService} from 'aurelia-dialog';
// import {Prompt} from 'components/my-modal';

@inject(BatteryService, Router, EventAggregator,AnraNotification, PermissionService)
export class Batteries {
    //Set Filters for Searchbox
    filters = [
              {value: '', keys: ['BatteryName','SerialNumber','Capacity']},
    ];

  constructor(batteryService, router, events, notification, permissionService) {
    this.batteryService = batteryService;
    this.router = router;
    this.events = events;
    this.permissionService = permissionService;
    this.heading = 'Manage Batteries';
    this.batteries = [];
    this.batteriestocsv=[];
    this.batteryAggregate = null;
    this.notification = notification;

    this.isEditing = false;
    this.isViewing = false;

    this.permission = {};
    //Set Default Paging Size
    this.pageSize = batteryService.defaultPagingSize;
    this.isWaiting = false;
  }

  activate() {
    this.getList();
    
  }

  attached() {
   
    this.subscribeEvents();
   
  }
  detached() {
    this.subscriber.dispose();
  }

  getPermission(moduleId) {
    this.permissionService.getPermission(moduleId)
      .then(result => {
        this.permission = result;
      });
  }

  subscribeEvents() {
    this.subscriber = this.events.subscribe('battery-item-operation', payload => {

      if (payload) {
        if (payload.refresh) {
          this.getList();
        }
        this.reset();
      }
    });

    this.subscriber = this.events.subscribe('battery-item-operation-edit', payload => {

        if (payload) {
            if (payload.id > 0) {
                this.editItem(payload.id);
            }
            this.reset();
        }
    });
  }

  getList() {      
      this.isWaiting = true;
    this.batteryService.getBatteries()
      .then(result => {
        this.batteries = result;
        this.isEditing = false;
        this.getPermission(6);       
        this.isWaiting = false;
        var newJson=[];
        this.batteries.forEach(function(item) {
          newJson.push({
          "Battery Name":item.BatteryName,
          "Purchase Date":moment(item.PurchaseDate).format('YYYY-MM-DD'),
          "Serial No":item.SerialNumber,
          "Cell Count":item.CellCount,
          "Capacity":item.Capacity,
          "Cycles/Flights":item.TotalCycles+'/'+item.TotalFlights,
          "Creation Date":moment(item.DateCreated).format('YYYY-MM-DD')
                      });
          });
          this.batteriestocsv=newJson;
          console.log(this.batteriestocsv);
      }).catch((e) => {
      this.notification.ShowError(e.ErrorMessage);      
      this.isWaiting = false;
    });
  }

  //Convert For CSV Convert
 

  reset() {
    this.batteryAggregate = null;
    this.isEditing = false;
    this.isViewing = false;
  }

  add() {
    this.isWaiting = true;
    this.batteryService.getNewBattery()
      .then(result => {
        this.isWaiting = false;
        this.batteryAggregate = result;
        this.isEditing = true;
      });
  }

  editItem(id) {
    this.isWaiting = true;
    this.batteryService.getBattery(id)
      .then(result => {        
        this.batteryAggregate = result;
        this.batteryAggregate.Battery.PurchaseDate = moment(result.Battery.PurchaseDate).format('YYYY-MM-DD');
        this.isEditing = true;
        this.isWaiting = false;
      });
  }

  viewItem(id) {
    this.isWaiting=true;
    this.batteryService.getBattery(id)
      .then(result => {        
        this.batteryAggregate = result;
        this.batteryAggregate.Battery.PurchaseDate = moment(result.Battery.PurchaseDate).format('YYYY-MM-DD');
        this.isViewing = true;
        this.isWaiting=false;
      });
  }

  deleteItem(id) {
    this.isWaiting=true;
      this.notification.Confirm(
        "Are you sure you want to delete?"
      ).then(result => {
        if (result) {
          this.batteryService.deleteBattery(id)
          .then(res => {
            this.getList();
            this.notification.ShowInfo('Deleted Successfully.');
            this.isWaiting=false;
            });
        }
        else{
          this.isWaiting=false;
        }
      });
  }

  saveItem(item) {
    return this.batteryService.saveBattery(this.batteryAggregate.Battery);
  }

  showCycles(id) {    
    this.router.navigate('cycles/' + id);
  }

  

   //Download CSV

   convertArrayOfObjectsToCSV(args) {  
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        this.notification.ShowError("No data available to export.");          
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    var columnHeaders = ['Name','Purchase Date','Serial No','Cell Count','Capacity','Cycles/Flights','Date Created'];
    
    result = '';
    result += columnHeaders.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
  }

    downloadCSV(args) {  
    var data, filename, link;

    //Setting Entity Attributes

    var csv = this.convertArrayOfObjectsToCSV({data:this.batteriestocsv});
    if (csv == null) return;

    filename = args.filename || 'BatteriesDetails.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();

    }

}
