import {inject} from 'aurelia-framework';
import {AureliaConfiguration} from 'aurelia-configuration';
import {json} from 'aurelia-fetch-client';
import {CustomHttpClient} from 'lib/customHttpClient';

@inject(CustomHttpClient, AureliaConfiguration)
export class BatteryService {

  constructor(http, config) {
    this.config = config;
    this.http = http;
    this.baseApi = this.config.get('gatewayBaseUrl');
    this.defaultPagingSize = this.config.get('defaultPagingSize');
  }

  getBatteries() {
      return this.http.fetch(this.baseApi + 'battery/listing')
      .then(response => response.json());
  }

  getNewBattery() {
      return this.http.fetch(this.baseApi + 'battery/new')
      .then(response => response.json());
  }

  getBattery(id) {
      return this.http.fetch(this.baseApi + 'battery/' + id)
      .then(response => response.json());
  }

  deleteBattery(id) {
    return this.http.fetch(this.baseApi + 'battery/' + id, {
        method: 'delete'
      })
      .then(response => {
          response.json();
        }
      );
  }

  saveBattery(Battery) {
    return this.http.fetch(this.baseApi + 'battery/', {
      method: 'post',
      body: json(Battery)
    }).then(response => response.json());
  }
}
