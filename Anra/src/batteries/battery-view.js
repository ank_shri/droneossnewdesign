import {inject, bindable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {AnraNotification} from '../components/anra-notification';

@inject(EventAggregator, AnraNotification)
export class BatteryView {
  @bindable battery;
  @bindable drones;
  @bindable save;
  @bindable organizations;

  constructor(events, notification) {
    this.events = events;
    this.notification = notification;    
  }  

  bind() {        
    this.heading = this.battery.BatteryName != null ? 'View Battery' : 'Add Battery';
  }

  cancel() {
    this.events.publish('battery-item-operation', {refresh: false});
  }

  edit(batteryId) {
      this.events.publish('battery-item-operation-edit', {id: batteryId});
  }
}

export class FilterOnPropertyValueConverter {
    toView(array: [], property: string, exp: number, name: string) {

        if (array === undefined || array === null || property === undefined || exp === undefined) {
            return array;
        }
        
        var arr = array.filter((item) => item[property] == (exp));
        
        return arr.length > 0 ? arr[0][name] : '' ;
    }
}
