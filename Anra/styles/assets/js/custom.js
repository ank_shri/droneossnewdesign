// custom js
function toggle(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'block')
        e.style.display = 'none';
    else
        e.style.display = 'block';
}

function sidebarCol() {
    var x = document.getElementById("body");
    if (x.className === "body-sidebar-toggle") {
        x.className = "body-sidebar-toggle-active";
    } else {
        x.className = "body-sidebar-toggle";
    }
}
function sideCol() {
    var x = document.getElementById("insp-main");
    if (x.className === "insp-sidebar-toggle") {
        x.className = "insp-sidebar-toggle-active";
    } else {
        x.className = "insp-sidebar-toggle";
    }
}
function rtNav() {
  var xy = document.getElementById("work-3d");
  if (xy !== null) {
    if (xy.className === "work-3d") {
      xy.className = "work-3d-act";
    } else {
      xy.className = "work-3d";
    }
  }
}
function toggleFullScreen() {
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    }  
  }  
}

